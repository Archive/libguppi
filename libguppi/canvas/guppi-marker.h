/* $Id$ */

/*
 * guppi-marker.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_MARKER_H
#define _INC_GUPPI_MARKER_H

#include <glib.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef enum {
  GUPPI_MARKER_NONE = 0,
  GUPPI_MARKER_CIRCLE,
  GUPPI_MARKER_DIAMOND,
  GUPPI_MARKER_CROSS,
  GUPPI_MARKER_X,
  GUPPI_MARKER_SQUARE,
  GUPPI_MARKER_AST,
  GUPPI_MARKER_TRIANGLE,
  GUPPI_MARKER_BAR,
  GUPPI_MARKER_HALF_BAR,
  GUPPI_MARKER_LAST,
  GUPPI_MARKER_UNKNOWN
} GuppiMarker;

const char *guppi_marker_to_string (GuppiMarker marker);
const char *guppi_marker_to_code   (GuppiMarker marker);
GuppiMarker guppi_marker_from_code (const char *code);

gboolean guppi_marker_get_size (GuppiMarker marker,
				double      size,
				double     *radius,
				double     *edge_width);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_MARKER_H */

/* $Id$ */
