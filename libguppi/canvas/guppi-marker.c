/* $Id$ */

/*
 * guppi-marker.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/canvas/guppi-marker.h>

#include <libguppi/useful/guppi-i18n.h>

#include <math.h>
#include <libguppi/useful/guppi-memory.h>

/* Magic constants */

#define CIRCLE_AREA_FACTOR   1.0
#define SQUARE_AREA_FACTOR   1.2533141373155002512  /* sqrt(pi / 2) */
#define TRIANGLE_AREA_FACTOR 1.55512030155621416074 /* sqrt(4 pi / (3 sqrt 3)) */

#define CIRCLE_DEFAULT_RADIUS   (4.0 * CIRCLE_AREA_FACTOR)
#define SQUARE_DEFAULT_RADIUS   (CIRCLE_DEFAULT_RADIUS * SQUARE_AREA_FACTOR)
#define TRIANGLE_DEFAULT_RADIUS (CIRCLE_DEFAULT_RADIUS * TRIANGLE_AREA_FACTOR)
#define STARBURST_DEFAULT_RADIUS 7.0

#define CIRCLE_DEFAULT_WIDTH   (0.20 * CIRCLE_DEFAULT_RADIUS)
#define SQUARE_DEFAULT_WIDTH   CIRCLE_DEFAULT_WIDTH
#define TRIANGLE_DEFAULT_WIDTH CIRCLE_DEFAULT_WIDTH
#define STARBURST_DEFAULT_WIDTH 2.5

#define STARBURST_EDGE_FACTOR 0.45

#define MIN_SIZE_FACTOR 0.75
#define MAX_SIZE_FACTOR 4.0


typedef struct _GuppiMarkerInfo GuppiMarkerInfo;
struct _GuppiMarkerInfo {
  GuppiMarker marker;
  const char *code;
  const char *string;

  double default_radius, default_edge_width;
};

static const GuppiMarkerInfo guppi_marker_info[GUPPI_MARKER_LAST] = {
  { GUPPI_MARKER_NONE, "none", N_("None"),
    0, 0 },

  { GUPPI_MARKER_CIRCLE, "circle", N_("Circle"),
    CIRCLE_DEFAULT_RADIUS, CIRCLE_DEFAULT_WIDTH },

  { GUPPI_MARKER_DIAMOND, "diamond", N_("Diamond"),
    SQUARE_DEFAULT_RADIUS, SQUARE_DEFAULT_WIDTH },

  { GUPPI_MARKER_CROSS, "cross", N_("Cross"),
    STARBURST_DEFAULT_RADIUS, STARBURST_DEFAULT_WIDTH },

  { GUPPI_MARKER_X, "x", N_("X"),
    STARBURST_DEFAULT_RADIUS, STARBURST_DEFAULT_WIDTH },

  { GUPPI_MARKER_AST, "asterisk", N_("Asterisk"),
    STARBURST_DEFAULT_RADIUS, STARBURST_DEFAULT_WIDTH },

  { GUPPI_MARKER_TRIANGLE, "triangle", N_("Triangle"),
    TRIANGLE_DEFAULT_RADIUS, TRIANGLE_DEFAULT_WIDTH },

  { GUPPI_MARKER_BAR, "bar", N_("Bar"),
    4, 1 },

  { GUPPI_MARKER_HALF_BAR, "halfbar", N_("Half Bar"),
    4, 1 }

};

static const GuppiMarkerInfo *
get_info (GuppiMarker marker)
{
  const GuppiMarkerInfo *info = NULL;

  g_return_val_if_fail ((int)marker < (int)GUPPI_MARKER_LAST, NULL);
  info = &guppi_marker_info[marker];
  g_return_val_if_fail (info->marker == marker, NULL);

  return info;
}

const char *
guppi_marker_to_string (GuppiMarker marker)
{
  const GuppiMarkerInfo *info = get_info (marker);
  g_return_val_if_fail (info != NULL, NULL);

  return _(info->string);
  
}

const char *
guppi_marker_to_code (GuppiMarker marker)
{
  const GuppiMarkerInfo *info = get_info (marker);
  g_return_val_if_fail (info != NULL, NULL);

  return info->code;
}

GuppiMarker
guppi_marker_from_code (const char *code)
{
  int i;

  g_return_val_if_fail (code && *code, GUPPI_MARKER_UNKNOWN);

  for (i = 0; i < GUPPI_MARKER_LAST; ++i) {
    if (! g_ascii_strcasecmp (code, guppi_marker_info[i].code))
      return guppi_marker_info[i].marker;
  }

  return GUPPI_MARKER_UNKNOWN;
}

gboolean
guppi_marker_get_size (GuppiMarker marker,
		       double      size,
		       double     *radius,
		       double     *edge_width)
{
  const GuppiMarkerInfo *info = get_info (marker);

  g_return_val_if_fail (info != NULL, FALSE);
  g_return_val_if_fail (size >= 0, FALSE);

  if (radius)
    *radius = info->default_radius * size;

  if (edge_width)
    *edge_width = info->default_edge_width * size;

  return TRUE;
}

/* $Id$ */
