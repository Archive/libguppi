/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pixbuf.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_PIXBUF_H__
#define __GUPPI_PIXBUF_H__

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

typedef struct _GuppiPixbuf GuppiPixbuf;

struct _GuppiPixbuf {
  gint refs;

  GdkPixbuf *pixbuf;
  gint x_base_point, y_base_point;

  guint color_mappable : 1;
};

GuppiPixbuf *guppi_pixbuf_new   (GdkPixbuf *pixbuf);

void         guppi_pixbuf_ref   (GuppiPixbuf *pixbuf);
void         guppi_pixbuf_unref (GuppiPixbuf *pixbuf);

void         guppi_pixbuf_paste              (GuppiPixbuf *pixbuf,
					      gint x, gint y,
					      guint alpha,
					      GdkPixbuf *destination);

void         guppi_pixbuf_color_mapped_paste (GuppiPixbuf *pixbuf,
					      gint x, gint y,
					      guint32 rgba_primary,
					      guint32 rgba_secondary,
					      guint32 rgba_boundary,
					      guint overall_alpha,
					      GdkPixbuf *destination);

void         guppi_pixbuf_auto_crop (GuppiPixbuf *pixbuf);

#endif /* __GUPPI_PIXBUF_H__ */

