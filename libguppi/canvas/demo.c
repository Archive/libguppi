/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <math.h>

#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/canvas/guppi-canvas.h>
#include <libguppi/canvas/guppi-canvas-gtk.h>

#define N 480
static void
render (GuppiCanvas *canvas)
{
  int i;
  ArtDRect bbox, clip;
  ArtPoint pt1, pt2;
  ArtPoint points[N+10];
  static double phase = 0;
  double x0, y0, w, h;

  guppi_canvas_get_bbox (canvas, &bbox);
  x0 = bbox.x0;
  y0 = bbox.y0;
  w = bbox.x1 - bbox.x0;
  h = bbox.y1 - bbox.y0;

  guppi_canvas_push_attributes (canvas);
  guppi_canvas_begin_drawing (canvas);

  /*****/

  clip.x0 = x0 + w/4;
  clip.y0 = y0 + h/4;
  clip.x1 = clip.x0 + w/2;
  clip.y1 = clip.y0 + h/2;

  guppi_canvas_set_line_width (canvas, 20.0);
  guppi_canvas_set_color_rgba (canvas, 0x00ff00ff);

  pt1.x = x0 + 0.5 * w;
  pt1.y = y0 + 0.5 * h;
  pt2.x = x0 + (0.56 + 0.3*fabs(sin(phase))) * w;
  pt2.y = y0 -.1 * h;
  guppi_canvas_draw_line (canvas, &pt1, &pt2);

  /*****/

  guppi_canvas_set_color_rgba (canvas, 0xd07040ff);
  guppi_canvas_set_edge_color_rgba (canvas, 0xffff00ff);
  guppi_canvas_set_line_width (canvas, 2.0);

  for (i = 0; i <= N; ++i) {
    double t = i/((double) N);
    double r = 0.30 + 0.15*sin(2*M_PI*9*t);
    points[i].x = x0 + (0.5 + r*cos(2*M_PI*t - phase)) * w;
    points[i].y = x0 + (0.5 + r*sin(2*M_PI*t - phase)) * h;
  }

  guppi_canvas_draw_polygon (canvas, N+1, points, TRUE, TRUE);

  /*****/

  /* guppi_canvas_set_clipping_rectangle (canvas, &clip); */

  guppi_canvas_set_color_rgba (canvas, 0xff0000ff);
  guppi_canvas_set_edge_color_rgba (canvas, 0xffff00ff);
  guppi_canvas_set_line_width (canvas, 2.0);

  for (i = 0; i <= N; ++i) {
    double t = i/((double) N);
    double r = 0.30 + 0.15*sin(2*M_PI*9*t);
    points[i].x = x0 + (0.5 + r*cos(2*M_PI*t + phase)) * w;
    points[i].y = y0 + (0.5 + r*sin(2*M_PI*t + phase)) * h;
  }

  guppi_canvas_draw_polygon (canvas, N+1, points, TRUE, TRUE);

  guppi_canvas_set_clipping_rectangle (canvas, NULL);


  /*****/

  guppi_canvas_set_color_rgba (canvas, 0xffff00ff);

  pt1.x = x0 + 0.5 * w;
  pt1.y = y0 + 0.5 * h;

  guppi_canvas_draw_circle (canvas, &pt1, 0.1*w, TRUE, FALSE);
  
  /*****/

  for (i = 0; i < 20; ++i) {
    double t = i/((double) 20);
    double r = 0.3 + 0.2 * sin (phase);

    points[i].x = x0 + (0.5 + r * cos (2*M_PI*t - 0.8*phase)) * w;
    points[i].y = y0 + (0.5 + r * sin (2*M_PI*t - 0.8*phase)) * h;
  }

  guppi_canvas_set_color_rgba (canvas, 0xffffff00 | (int)(0xff * fabs(sin(2.0*phase))));
  guppi_canvas_set_edge_color_rgba (canvas, 0xff);

  guppi_canvas_draw_markers (canvas,
			     GUPPI_MARKER_CIRCLE,
			     20,
			     points,
			     1 + 3*fabs(sin(0.6*phase)),
			     NULL, NULL, NULL);

  /*****/

  guppi_canvas_set_color_rgba (canvas, 0x8090a0a0);
  guppi_canvas_set_edge_color_rgba (canvas, 0x000000ff);

  pt1.x = x0 + 0.5 * w;
  pt1.y = y0 + 0.5 * h;

  guppi_canvas_draw_wedge (canvas, &pt1, MIN(w/3, h/3), 
			   1.2 * phase, 1.2 * phase + sin (phase) * M_PI/4, TRUE, TRUE);

  /*****/


  guppi_canvas_set_font_from_string (canvas, "Sans 12");
  guppi_canvas_set_color_rgba (canvas, RGBA_BLACK);

  pt1.x = x0 + (0.5 + 1 * cos(0.3*phase)) * w;
  pt1.y = y0 + (0.5 + 0.1 * sin(0.3*phase)) * h;

  guppi_canvas_draw_string (canvas,
			    &pt1,
			    GUPPI_CANVAS_ANCHOR_CENTER,
			    GUPPI_CANVAS_ROT_270,
			    "Dobrý den!");

  pt1.x = x0 + (0.5 + 0.1 * cos(0.7*phase)) * w;
  pt1.y = y0 + (0.5 + 0.7 * sin(0.7*phase)) * h;

  guppi_canvas_draw_string (canvas,
			    &pt1,
			    GUPPI_CANVAS_ANCHOR_CENTER,
			    GUPPI_CANVAS_ROT_180,
			    "Guppi!");

  /*****/

  guppi_canvas_end_drawing (canvas);
  guppi_canvas_pop_attributes (canvas);

  phase += 0.1;
}

static gboolean
anim (gpointer user_data)
{
  GuppiCanvas *canvas = user_data;
  render (canvas);
  
  return TRUE;
}

int
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *canvas;

  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  canvas = g_object_new (GUPPI_TYPE_CANVAS_GTK, NULL);

  guppi_canvas_gtk_set_fixed_size (GUPPI_CANVAS_GTK (canvas),
				   72*5, 72*5);

  gtk_container_add (GTK_CONTAINER (window), canvas);

  /* render (GUPPI_CANVAS (canvas)); */
  gtk_timeout_add (100, anim, canvas);

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}

