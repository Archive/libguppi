/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas-gtk.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_CANVAS_GTK_H__
#define __GUPPI_CANVAS_GTK_H__

#include <gtk/gtk.h>
#include <libart_lgpl/libart.h>

typedef struct _GuppiCanvasGtk GuppiCanvasGtk;
typedef struct _GuppiCanvasGtkClass GuppiCanvasGtkClass;
typedef struct _GuppiCanvasGtkAttributes GuppiCanvasGtkAttributes;
typedef struct _GuppiCanvasGtkItem GuppiCanvasGtkItem;

struct _GuppiCanvasGtkAttributes {
  guint32 color, edge_color;
  double line_width;
  PangoFontDescription *font;
  ArtDRect clipping_rect;

  guint clip_to_rect : 1;
};

struct _GuppiCanvasGtkItem {

  gboolean (*intersect) (GuppiCanvasGtkItem *item,
			 GuppiCanvasGtk     *canvas,
			 ArtIRect           *rectangle);

  void     (*update)    (GuppiCanvasGtkItem *item,
			 GuppiCanvasGtk     *canvas,
			 double              scale);

  void     (*draw)      (GuppiCanvasGtkItem *item,
			 int                 x0,
			 int                 y0,
			 GdkPixbuf          *pixbuf,
			 double              scale);

  void     (*destroy)   (GuppiCanvasGtkItem *item);

  GuppiCanvasGtkAttributes *attr;
};

void guppi_canvas_gtk_item_free (GuppiCanvasGtkItem *item);


struct _GuppiCanvasGtk {
  GtkDrawingArea parent;

  ArtDRect  bbox;
  GSList   *attributes;
  GSList   *items;

  GdkPixbuf *buffer;

  double x_dpi;
  double y_dpi;
  
  double width_in_pts;
  double height_in_pts;

  double floating_scale; /* set by _set_scale, use only in floating size mode */

  double scale;
  double x_pt2pix;
  double y_pt2pix;

  guint     fixed_size   : 1;
  guint     drawing      : 1;
  guint     needs_redraw : 1;
};

struct _GuppiCanvasGtkClass {
  GtkDrawingAreaClass parent_class;
};

#define GUPPI_TYPE_CANVAS_GTK            (guppi_canvas_gtk_get_type ())
#define GUPPI_CANVAS_GTK(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_CANVAS_GTK, GuppiCanvasGtk))
#define GUPPI_CANVAS_GTK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_CANVAS_GTK, GuppiCanvasGtkClass))
#define GUPPI_IS_CANVAS_GTK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_CANVAS_GTK))
#define GUPPI_IS_CANVAS_GTK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CANVAS_GTK))

GType guppi_canvas_gtk_get_type (void);

void  guppi_canvas_gtk_set_dpi (GuppiCanvasGtk *canvas,
				double          x_dpi,
				double          y_dpi);

void  guppi_canvas_gtk_set_fixed_size (GuppiCanvasGtk *canvas,
				       double          width_in_pts,
				       double          height_in_pts);

void  guppi_canvas_gtk_set_floating_size (GuppiCanvasGtk *canvas,
					  double          scale);

void  guppi_canvas_gtk_set_bbox (GuppiCanvasGtk *canvas,
				 ArtDRect       *bbox);

void  guppi_canvas_gtk_transform_point (GuppiCanvasGtk *canvas,
					ArtPoint       *point,
					ArtPoint       *pixel_point);

void  guppi_canvas_gtk_transform_points (GuppiCanvasGtk *canvas,
					 guint           num_points,
					 ArtPoint       *points,
					 ArtPoint       *pixel_points);

void  guppi_canvas_gtk_transform_points_to_path (GuppiCanvasGtk *canvas,
						 guint           num_points,
						 ArtPoint       *points,
						 ArtVpath       *path);

void  guppi_canvas_gtk_transform_path (GuppiCanvasGtk *canvas,
				       ArtVpath       *path);

void  guppi_canvas_gtk_transform_rectangle (GuppiCanvasGtk *canvas,
					    ArtDRect       *rectangle,
					    ArtDRect       *pixel_rectangle);

#endif /* __GUPPI_CANVAS_GTK_H__ */

