/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pixbuf.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/canvas/guppi-pixbuf.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-rgb.h>

GuppiPixbuf *
guppi_pixbuf_new (GdkPixbuf *pixbuf)
{
  GuppiPixbuf *gp;

  gp = guppi_new0 (GuppiPixbuf, 1);

  gp->refs   = 1;
  gp->pixbuf = pixbuf ? gdk_pixbuf_ref (pixbuf) : NULL;

  gp->x_base_point = 0;
  gp->y_base_point = 0;
  gp->color_mappable = FALSE;

  return gp;
}

void
guppi_pixbuf_ref (GuppiPixbuf *gp)
{
  if (gp != NULL) {
    g_return_if_fail (gp->refs >= 1);
    ++gp->refs;
  }
}

void
guppi_pixbuf_unref (GuppiPixbuf *gp)
{
  if (gp != NULL) {
    g_return_if_fail (gp->refs >= 1);
    --gp->refs;

    if (gp->refs == 0) {
      if (gp->pixbuf)
	gdk_pixbuf_unref (gp->pixbuf);
      guppi_free (gp);
    }
  }
}

void
guppi_pixbuf_paste (GuppiPixbuf *gp,
		    gint x, gint y,
		    guint alpha,
		    GdkPixbuf *destination)
{
  gboolean pixbuf_has_alpha;
  gint pixbuf_rowstride, pixbuf_step, pixbuf_w, pixbuf_h;
  gint dest_w, dest_h, dest_rowstride;

  g_return_if_fail (gp != NULL);
  g_return_if_fail (GDK_IS_PIXBUF (destination));

  if (gp->pixbuf == NULL)
    return;

  pixbuf_has_alpha = gdk_pixbuf_get_has_alpha (gp->pixbuf);
  pixbuf_rowstride = gdk_pixbuf_get_rowstride (gp->pixbuf);
  pixbuf_step      = pixbuf_has_alpha ? 4 : 3;
  pixbuf_w         = gdk_pixbuf_get_width (gp->pixbuf);
  pixbuf_h         = gdk_pixbuf_get_height (gp->pixbuf);

  dest_w         = gdk_pixbuf_get_width (destination);
  dest_h         = gdk_pixbuf_get_height (destination);
  dest_rowstride = gdk_pixbuf_get_rowstride (destination);

  /* Adjust for base point */
  x -= gp->x_base_point;
  y -= gp->y_base_point;

  if (x + pixbuf_w >= 0 && x < dest_w && y + pixbuf_h >= 0 && y < dest_h) {
    
    gint x0, y0, x1, y1, i, j;
    guchar *pixbuf_data, *dest_data, *pixbuf_run, *dest_run;

    /* Clip */
    x0 = MAX (x, 0);
    y0 = MAX (y, 0);
    x1 = MIN (x + pixbuf_w, dest_w);
    y1 = MIN (y + pixbuf_h, dest_h);

    pixbuf_data = gdk_pixbuf_get_pixels (gp->pixbuf);
    pixbuf_data += (y0 - y) * pixbuf_rowstride + pixbuf_step * (x0 - x);
    
    dest_data = gdk_pixbuf_get_pixels (destination);
    dest_data += y0 * dest_rowstride + 3 * x0;

    for (j = y0; j < y1; ++j) {
      pixbuf_run = pixbuf_data;
      dest_run = dest_data;

      for (i = x0; i < x1; ++i) {
	if (pixbuf_has_alpha) {
	  if (pixbuf_run[3] > 0) {
	    PIXEL_RGBA (dest_run, pixbuf_run[0], pixbuf_run[1], pixbuf_run[2], 
			((1 + pixbuf_run[3]) * (1 + alpha)) >> 8);
	  }
	} else {
	  dest_run[0] = pixbuf_run[0];
	  dest_run[1] = pixbuf_run[1];
	  dest_run[2] = pixbuf_run[2];
	}
	pixbuf_run += pixbuf_step;
	dest_run += 3;
      }
      
      pixbuf_data += pixbuf_rowstride;
      dest_data += dest_rowstride;
    }
  }
}

/* Map 0-255 onto 0-256 */
#define ADJ(x) ((x)<0x80?(x):((x)+1))

#define CUTOFF(x) {if ((x)>0xff) { (x) = 0xff; }}

void
guppi_pixbuf_color_mapped_paste (GuppiPixbuf *gp,
				 gint x, gint y,
				 guint32 rgba_primary,
				 guint32 rgba_secondary,
				 guint32 rgba_boundary,
				 guint alpha,
				 GdkPixbuf *destination)
{
  gint pixbuf_rowstride, pixbuf_step, pixbuf_w, pixbuf_h;
  gint dest_w, dest_h, dest_rowstride;
  guint r1, g1, b1, a1, r2, g2, b2, a2, r3, g3, b3, a3;
  guint orig_r, orig_g, orig_b, orig_a;
  guint final_r=0, final_g=0, final_b=0, final_a;
  gboolean pixbuf_has_alpha;

  g_return_if_fail (gp != NULL);
  g_return_if_fail (GDK_IS_PIXBUF (destination));

  if (gp->pixbuf == NULL)
    return;

  if (! gp->color_mappable) {
    guppi_pixbuf_paste (gp, x, y, alpha, destination);
    return;
  }

  pixbuf_rowstride = gdk_pixbuf_get_rowstride (gp->pixbuf);
  pixbuf_w         = gdk_pixbuf_get_width (gp->pixbuf);
  pixbuf_h         = gdk_pixbuf_get_height (gp->pixbuf);

  pixbuf_has_alpha = gdk_pixbuf_get_has_alpha (gp->pixbuf);
  pixbuf_step      = pixbuf_has_alpha ? 4 : 3;

  dest_w         = gdk_pixbuf_get_width (destination);
  dest_h         = gdk_pixbuf_get_height (destination);
  dest_rowstride = gdk_pixbuf_get_rowstride (destination);

  /* Adjust for base point */
  x -= gp->x_base_point;
  y -= gp->y_base_point;

  if (x + pixbuf_w >= 0 && x < dest_w && y + pixbuf_h >= 0 && y < dest_h) {
    
    gint x0, y0, x1, y1, i, j;
    guchar *pixbuf_data, *dest_data, *pixbuf_run, *dest_run;

    /* Clip */
    x0 = MAX (x, 0);
    y0 = MAX (y, 0);
    x1 = MIN (x + pixbuf_w, dest_w);
    y1 = MIN (y + pixbuf_h, dest_h);

    pixbuf_data = gdk_pixbuf_get_pixels (gp->pixbuf);
    pixbuf_data += (y0 - y) * pixbuf_rowstride + (pixbuf_step) * (x0 - x);
    
    dest_data = gdk_pixbuf_get_pixels (destination);
    dest_data += y0 * dest_rowstride + 3 * x0;

    UINT_TO_RGBA (rgba_primary,   &r1, &g1, &b1, &a1);
    UINT_TO_RGBA (rgba_secondary, &r2, &g2, &b2, &a2);
    UINT_TO_RGBA (rgba_boundary,  &r3, &g3, &b3, &a3);

    alpha = ADJ (alpha);

    for (j = y0; j < y1; ++j) {
      pixbuf_run = pixbuf_data;
      dest_run = dest_data;

      for (i = x0; i < x1; ++i) {

	orig_a = pixbuf_has_alpha ? pixbuf_run[3] : 0xff;

	if (orig_a > 0) {

	  orig_r = pixbuf_run[0];
	  orig_g = pixbuf_run[1];
	  orig_b = pixbuf_run[2];

	  orig_r = ADJ (orig_r);
	  orig_g = ADJ (orig_g);
	  orig_b = ADJ (orig_b);
	  orig_a = ADJ (orig_a);

	  if (a2 == 0) { 
	    /* Optimize for the fairly common case of no secondary color. */

	    final_a = (((orig_r * a1 + orig_b * a3) >> 8) * orig_a * alpha) >> 16;
	  
	    if (final_a > 0) {
	      final_r = (orig_r * r1 + orig_b * r3) >> 8;
	      final_g = (orig_r * g1 + orig_b * g3) >> 8;
	      final_b = (orig_r * b1 + orig_b * b3) >> 8;
	    }

	  } else {

	    final_a = (((orig_r * a1 + orig_g * a2 + orig_b * a3) >> 8) * orig_a * alpha) >> 16;
	  
	    if (final_a > 0) {
	      final_r = (orig_r * r1 + orig_g * r2 + orig_b * r3) >> 8;
	      final_g = (orig_r * g1 + orig_g * g2 + orig_b * g3) >> 8;
	      final_b = (orig_r * b1 + orig_g * b2 + orig_b * b3) >> 8;
	    }

	  }

	  if (final_a > 0) {
	    CUTOFF (final_r);
	    CUTOFF (final_g);
	    CUTOFF (final_b);
	    CUTOFF (final_a);
	    PIXEL_RGBA (dest_run, final_r, final_g, final_b, final_a);
	  }
	}

	pixbuf_run += pixbuf_step;
	dest_run += 3;
      }
      
      pixbuf_data += pixbuf_rowstride;
      dest_data += dest_rowstride;
    }
  }
}
