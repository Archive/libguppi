/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas-gtk-items.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/canvas/guppi-canvas-gtk-items.h>

#include <string.h>
#include <math.h>

#include <pango/pangoft2.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/canvas/guppi-pixbuf.h>
#include <libguppi/canvas/guppi-pixbuf-stock.h>


typedef struct _GuppiCanvasGtkItem_Line GuppiCanvasGtkItem_Line;
struct _GuppiCanvasGtkItem_Line {
  GuppiCanvasGtkItem item;

  ArtPoint point_1;
  ArtPoint point_2;

  ArtSVP  *svp;
};

static void
line_update (GuppiCanvasGtkItem *item,
	     GuppiCanvasGtk     *canvas,
	     double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Line *line_item;
  ArtPoint pixel_1, pixel_2;
  ArtVpath path[3];

  line_item = (GuppiCanvasGtkItem_Line *) item;

  if (line_item->svp)
    art_svp_free (line_item->svp);

  guppi_canvas_gtk_transform_point (canvas,
				    &line_item->point_1,
				    &pixel_1);

  guppi_canvas_gtk_transform_point (canvas,
				    &line_item->point_2,
				    &pixel_2);

  path[0].code = ART_MOVETO;
  path[0].x = pixel_1.x;
  path[0].y = pixel_1.y;

  path[1].code = ART_LINETO;
  path[1].x = pixel_2.x;
  path[1].y = pixel_2.y;
  
  path[2].code = ART_END;

  line_item->svp = art_svp_vpath_stroke (path,
					 ART_PATH_STROKE_JOIN_ROUND,
					 ART_PATH_STROKE_CAP_ROUND,
					 attr->line_width * scale,
					 1, 1);
}

static void
line_draw (GuppiCanvasGtkItem *item,
	   int                 x0,
	   int                 y0,
	   GdkPixbuf          *pixbuf,
	   double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Line *line_item;
  line_item = (GuppiCanvasGtkItem_Line *) item;

  if (line_item->svp == NULL)
    return;

  art_rgb_svp_alpha (line_item->svp,
		     x0, y0, 
		     x0 + gdk_pixbuf_get_width (pixbuf),
		     y0 + gdk_pixbuf_get_height (pixbuf),
		     attr->color,
		     gdk_pixbuf_get_pixels (pixbuf),
		     gdk_pixbuf_get_rowstride (pixbuf),
		     NULL);
}

static void
line_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_Line *line_item = (GuppiCanvasGtkItem_Line *) item;

  if (line_item->svp)
    art_svp_free (line_item->svp);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_line (ArtPoint *point_1,
				ArtPoint *point_2)
{
  GuppiCanvasGtkItem_Line *item;

  g_return_val_if_fail (point_1 != NULL, NULL);
  g_return_val_if_fail (point_2 != NULL, NULL);

  item = g_new0 (GuppiCanvasGtkItem_Line, 1);

  item->item.update  = line_update;
  item->item.draw    = line_draw;
  item->item.destroy = line_destroy;
  
  item->point_1    = *point_1;
  item->point_2    = *point_2;

  return (GuppiCanvasGtkItem *) item;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _GuppiCanvasGtkItem_Path GuppiCanvasGtkItem_Path;
struct _GuppiCanvasGtkItem_Path {
  GuppiCanvasGtkItem item;

  guint     num_points;
  ArtPoint *points;
  
  ArtVpath *path;
  ArtSVP   *svp;
};

static void
path_update (GuppiCanvasGtkItem *item,
	     GuppiCanvasGtk     *canvas,
	     double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Path *path_item;

  path_item = (GuppiCanvasGtkItem_Path *) item;

  if (path_item->svp)
    art_svp_free (path_item->svp);

  guppi_canvas_gtk_transform_points_to_path (canvas,
					     path_item->num_points,
					     path_item->points,
					     path_item->path);

  path_item->svp = art_svp_vpath_stroke (path_item->path,
					 ART_PATH_STROKE_JOIN_ROUND,
					 ART_PATH_STROKE_CAP_ROUND,
					 attr->line_width * scale,
					 1, 1);
}

static void
path_draw (GuppiCanvasGtkItem *item,
	   int                 x0,
	   int                 y0,
	   GdkPixbuf          *pixbuf,
	   double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Path *path_item;
  path_item = (GuppiCanvasGtkItem_Path *) item;

  if (path_item->svp == NULL)
    return;

  art_rgb_svp_alpha (path_item->svp,
		     x0, y0, 
		     x0 + gdk_pixbuf_get_width (pixbuf),
		     y0 + gdk_pixbuf_get_height (pixbuf),
		     attr->color,
		     gdk_pixbuf_get_pixels (pixbuf),
		     gdk_pixbuf_get_rowstride (pixbuf),
		     NULL);
}

static void
path_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_Path *path_item = (GuppiCanvasGtkItem_Path *) item;

  guppi_free (path_item->points);

  if (path_item->path)
    guppi_free (path_item->path);

  if (path_item->svp)
    art_svp_free (path_item->svp);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_path (guint     num_points,
				ArtPoint *points)
{
  GuppiCanvasGtkItem_Path *item;

  g_return_val_if_fail (num_points >= 2, NULL);
  g_return_val_if_fail (points != NULL, NULL);

  item = g_new0 (GuppiCanvasGtkItem_Path, 1);

  item->item.update  = path_update;
  item->item.draw    = path_draw;
  item->item.destroy = path_destroy;
  
  item->num_points = num_points;
  item->points     = guppi_new0 (ArtPoint, num_points);
  memcpy (item->points, points, num_points * sizeof (ArtPoint));

  item->path = guppi_new0 (ArtVpath, num_points+1);

  return (GuppiCanvasGtkItem *) item;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _GuppiCanvasGtkItem_Polygon GuppiCanvasGtkItem_Polygon;
struct _GuppiCanvasGtkItem_Polygon {
  GuppiCanvasGtkItem item;

  guint     num_points;
  ArtPoint *points;
  gboolean  filled;
  gboolean  with_edge;
  
  ArtVpath *path;

  ArtSVP   *svp, *edge_svp;
};

static void
polygon_update (GuppiCanvasGtkItem *item,
		GuppiCanvasGtk     *canvas,
		double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Polygon *polygon_item;

  polygon_item = (GuppiCanvasGtkItem_Polygon *) item;

  if (polygon_item->svp) {
    art_svp_free (polygon_item->svp);
    polygon_item->svp = NULL;
  }

  if (polygon_item->edge_svp) {
    art_svp_free (polygon_item->edge_svp);
    polygon_item->edge_svp = NULL;
  }

  guppi_canvas_gtk_transform_points_to_path (canvas,
					     polygon_item->num_points,
					     polygon_item->points,
					     polygon_item->path);

  if (polygon_item->filled) {
    polygon_item->svp = art_svp_from_vpath (polygon_item->path);
  }

  if (polygon_item->with_edge) {
    polygon_item->edge_svp = art_svp_vpath_stroke (polygon_item->path,
						   ART_PATH_STROKE_JOIN_ROUND,
						   ART_PATH_STROKE_CAP_ROUND,
						   attr->line_width * scale,
						   1, 1);
  }

}

static void
polygon_draw (GuppiCanvasGtkItem *item,
	      int                 x0,
	      int                 y0,
	      GdkPixbuf          *pixbuf,
	      double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Polygon *polygon_item;
  polygon_item = (GuppiCanvasGtkItem_Polygon *) item;

  if (polygon_item->svp) {
    art_rgb_svp_alpha (polygon_item->svp,
		       x0, y0, 
		       x0 + gdk_pixbuf_get_width (pixbuf),
		       y0 + gdk_pixbuf_get_height (pixbuf),
		       attr->color,
		       gdk_pixbuf_get_pixels (pixbuf),
		       gdk_pixbuf_get_rowstride (pixbuf),
		       NULL);
  }

  if (polygon_item->edge_svp) {
    art_rgb_svp_alpha (polygon_item->edge_svp,
		       x0, y0, 
		       x0 + gdk_pixbuf_get_width (pixbuf),
		       y0 + gdk_pixbuf_get_height (pixbuf),
		       attr->edge_color,
		       gdk_pixbuf_get_pixels (pixbuf),
		       gdk_pixbuf_get_rowstride (pixbuf),
		       NULL);
  }
}

static void
polygon_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_Polygon *polygon_item = (GuppiCanvasGtkItem_Polygon *) item;

  guppi_free (polygon_item->points);

  if (polygon_item->path)
    guppi_free (polygon_item->path);

  if (polygon_item->svp)
    art_svp_free (polygon_item->svp);

  if (polygon_item->edge_svp)
    art_svp_free (polygon_item->edge_svp);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_polygon (guint     num_points,
				   ArtPoint *points,
				   gboolean  filled,
				   gboolean  with_edge)
{
  GuppiCanvasGtkItem_Polygon *item;

  g_return_val_if_fail (num_points >= 2, NULL);
  g_return_val_if_fail (points != NULL, NULL);

  item = g_new0 (GuppiCanvasGtkItem_Polygon, 1);

  item->item.update  = polygon_update;
  item->item.draw    = polygon_draw;
  item->item.destroy = polygon_destroy;

  item->num_points = num_points;

  item->filled = filled;
  item->with_edge = with_edge;

  if (fabs (points[0].x - points[num_points-1].x) > 1e-8
      || fabs (points[0].y - points[num_points-1].y) > 1e-8) {
    ++item->num_points;
  }

  item->points  = guppi_new0 (ArtPoint, item->num_points);
  memcpy (item->points, points, num_points * sizeof (ArtPoint));

  /* manually close up the curve, to avoid problems with
     numerical stability */
  item->points[item->num_points - 1].x = points[0].x;
  item->points[item->num_points - 1].y = points[0].y;

  item->path = guppi_new0 (ArtVpath, item->num_points+1);

  return (GuppiCanvasGtkItem *) item;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

#define WEDGE_MAX_SEGMENTS 64

typedef struct _GuppiCanvasGtkItem_Wedge GuppiCanvasGtkItem_Wedge;
struct _GuppiCanvasGtkItem_Wedge {
  GuppiCanvasGtkItem item;

  ArtPoint point;
  double   radius;
  double   angle_1;
  double   angle_2;
  gboolean filled;
  gboolean with_edge;

  ArtVpath *path;
  ArtSVP   *svp, *edge_svp;
};

static void
wedge_update (GuppiCanvasGtkItem *item,
	      GuppiCanvasGtk     *canvas,
	      double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Wedge *wedge_item;
  double angle_diff;
  int i, N = 0;
  
  wedge_item = (GuppiCanvasGtkItem_Wedge *) item;

  if (wedge_item->svp) {
    art_svp_free (wedge_item->svp);
    wedge_item->svp = NULL;
  }

  if (wedge_item->edge_svp) {
    art_svp_free (wedge_item->edge_svp);
    wedge_item->edge_svp = NULL;
  }

  /* Build path */

  angle_diff = fabs (wedge_item->angle_2 - wedge_item->angle_1);

  /* FIXME: This determination of N is pretty ad-hoc.  Ideally, it should
     be range and scale-dependent.*/

  if (angle_diff > 0) {
    N = WEDGE_MAX_SEGMENTS * angle_diff / (2 * M_PI);
  } 

  if (N < 12)
    N = 12;
  else if (N > WEDGE_MAX_SEGMENTS)
    N = WEDGE_MAX_SEGMENTS;

  if (angle_diff < 2 * M_PI) {

    /* A pie slice */

    wedge_item->path[0].code = ART_MOVETO;
    wedge_item->path[0].x = wedge_item->point.x;
    wedge_item->path[0].y = wedge_item->point.y;

    for (i = 0; i <= N; ++i) {
      double theta = wedge_item->angle_1 + angle_diff * i/(double)N;

      wedge_item->path[i+1].code = ART_LINETO;
      wedge_item->path[i+1].x = wedge_item->point.x + wedge_item->radius * cos (theta);
      wedge_item->path[i+1].y = wedge_item->point.y + wedge_item->radius * sin (theta);
    }
    
    wedge_item->path[N+2].code = ART_LINETO;
    wedge_item->path[N+2].x = wedge_item->point.x;
    wedge_item->path[N+2].y = wedge_item->point.y;

    wedge_item->path[N+3].code = ART_END;

  } else {

    /* A full circle */

    for (i = 0; i < N; ++i) {
      double theta = 2 * M_PI * i/(double)N;
      
      wedge_item->path[i].code = i ? ART_LINETO : ART_MOVETO;
      wedge_item->path[i].x = wedge_item->point.x + wedge_item->radius * cos (theta);
      wedge_item->path[i].y = wedge_item->point.y + wedge_item->radius * sin (theta);
    }

    wedge_item->path[N].code = ART_LINETO;
    wedge_item->path[N].x = wedge_item->path[0].x;
    wedge_item->path[N].y = wedge_item->path[0].y;

    wedge_item->path[N+1].code = ART_END;
  }

  guppi_canvas_gtk_transform_path (canvas, wedge_item->path);

  if (wedge_item->filled) {
    wedge_item->svp = art_svp_from_vpath (wedge_item->path);
  }

  if (wedge_item->with_edge) {
    wedge_item->edge_svp = art_svp_vpath_stroke (wedge_item->path,
						 ART_PATH_STROKE_JOIN_ROUND,
						 ART_PATH_STROKE_CAP_ROUND,
						 attr->line_width * scale,
						 1, 1);
  }

}

static void
wedge_draw (GuppiCanvasGtkItem *item,
	    int                 x0,
	    int                 y0,
	    GdkPixbuf          *pixbuf,
	    double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Wedge *wedge_item;
  wedge_item = (GuppiCanvasGtkItem_Wedge *) item;

  if (wedge_item->svp) {
    art_rgb_svp_alpha (wedge_item->svp,
		       x0, y0, 
		       x0 + gdk_pixbuf_get_width (pixbuf),
		       y0 + gdk_pixbuf_get_height (pixbuf),
		       attr->color,
		       gdk_pixbuf_get_pixels (pixbuf),
		       gdk_pixbuf_get_rowstride (pixbuf),
		       NULL);
  }

  if (wedge_item->edge_svp) {
    art_rgb_svp_alpha (wedge_item->edge_svp,
		       x0, y0, 
		       x0 + gdk_pixbuf_get_width (pixbuf),
		       y0 + gdk_pixbuf_get_height (pixbuf),
		       attr->edge_color,
		       gdk_pixbuf_get_pixels (pixbuf),
		       gdk_pixbuf_get_rowstride (pixbuf),
		       NULL);
  }
}

static void
wedge_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_Wedge *wedge_item = (GuppiCanvasGtkItem_Wedge *) item;

  if (wedge_item->path)
    guppi_free (wedge_item->path);

  if (wedge_item->svp)
    art_svp_free (wedge_item->svp);

  if (wedge_item->edge_svp)
    art_svp_free (wedge_item->edge_svp);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_wedge (ArtPoint *point,
				 double    radius,
				 double    angle_1,
				 double    angle_2,
				 gboolean  filled,
				 gboolean  with_edge)
{
  GuppiCanvasGtkItem_Wedge *item;

  g_return_val_if_fail (point != NULL, NULL);

  item = g_new0 (GuppiCanvasGtkItem_Wedge, 1);

  item->item.update  = wedge_update;
  item->item.draw    = wedge_draw;
  item->item.destroy = wedge_destroy;

  item->point     = *point;
  item->radius    = radius;
  item->angle_1   = angle_1;
  item->angle_2   = angle_2;
  item->filled    = filled;
  item->with_edge = with_edge;

  item->path = g_new0 (ArtVpath, WEDGE_MAX_SEGMENTS+4);

  return (GuppiCanvasGtkItem *) item;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _GuppiCanvasGtkItem_Markers GuppiCanvasGtkItem_Markers;
struct _GuppiCanvasGtkItem_Markers {
  GuppiCanvasGtkItem item;

  GuppiMarker marker;
  guint       num_markers;
  ArtPoint   *positions;
  double      fixed_size;
  double     *variable_size;
  guint32     fixed_rgba;
  guint32    *variable_rgba;
  guint32     fixed_edge_rgba;
  guint32    *variable_edge_rgba;

  ArtPoint   *pixel_positions;
};

static void
markers_update (GuppiCanvasGtkItem *item,
		GuppiCanvasGtk     *canvas,
		double              scale)
{
  GuppiCanvasGtkItem_Markers *markers_item;
  markers_item = (GuppiCanvasGtkItem_Markers *) item;

  guppi_canvas_gtk_transform_points (canvas,
				     markers_item->num_markers,
				     markers_item->positions,
				     markers_item->pixel_positions);
}

static void
markers_draw (GuppiCanvasGtkItem *item,
	      int                 x0,
	      int                 y0,
	      GdkPixbuf          *pixbuf,
	      double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_Markers *markers_item;
  guint i;
  double size, last_size = 0, radius, edge_width;
  guint32 rgba, edge_rgba;
  GuppiPixbuf *marker_pixbuf = NULL;
  
  markers_item = (GuppiCanvasGtkItem_Markers *) item;

  size = markers_item->fixed_size;
  rgba = attr->color;
  edge_rgba = attr->edge_color;

  for (i = 0; i < markers_item->num_markers; ++i) {

    if (markers_item->variable_size)
      size = markers_item->variable_size[i];

    if (markers_item->variable_rgba)
      rgba = markers_item->variable_rgba[i];

    if (markers_item->variable_edge_rgba)
      edge_rgba = markers_item->variable_edge_rgba[i];

    if (marker_pixbuf == NULL || last_size != size) {

      guppi_marker_get_size (markers_item->marker,
			     size * scale,
			     &radius, &edge_width);

      guppi_pixbuf_unref (marker_pixbuf);

      switch (markers_item->marker) {
      case GUPPI_MARKER_CIRCLE:
	marker_pixbuf = guppi_pixbuf_stock_new_circle (radius, edge_width);
	break;
	
      case GUPPI_MARKER_DIAMOND:
	marker_pixbuf = guppi_pixbuf_stock_new_square (radius, edge_width, M_PI / 4);
	break;
	
      case GUPPI_MARKER_CROSS:
	marker_pixbuf = guppi_pixbuf_stock_new_starburst (radius, edge_width,
							  0.45 * edge_width, 
							  4, 0);
	break;

      case GUPPI_MARKER_X:
	marker_pixbuf = guppi_pixbuf_stock_new_starburst (radius, edge_width,
							  0.45 * edge_width,
							  4, M_PI / 4);
	break;

      case GUPPI_MARKER_SQUARE:
	marker_pixbuf = guppi_pixbuf_stock_new_square (radius, edge_width, 0);
	break;
	
      case GUPPI_MARKER_AST:
	marker_pixbuf = guppi_pixbuf_stock_new_starburst (radius, edge_width,
							  0.45 * edge_width,
							  5, - M_PI / 2);
	break;
	
      case GUPPI_MARKER_TRIANGLE:
	marker_pixbuf = guppi_pixbuf_stock_new_triangle (radius, edge_width, 0);
	break;
	
      case GUPPI_MARKER_BAR:
      case GUPPI_MARKER_HALF_BAR:
	marker_pixbuf = NULL;
	break;

      default:
	marker_pixbuf = NULL;

      }
    }

    if (marker_pixbuf) {
      gint x = (int) rint (markers_item->pixel_positions[i].x) - x0;
      gint y = (int) rint (markers_item->pixel_positions[i].y) - y0;
      guppi_pixbuf_color_mapped_paste (marker_pixbuf,
				       x, y,
				       rgba,
				       0,
				       edge_rgba,
				       0xff,
				       pixbuf);
    }
  }

  guppi_pixbuf_unref (marker_pixbuf);
}

static void
markers_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_Markers *markers_item;
  markers_item = (GuppiCanvasGtkItem_Markers *) item;

  guppi_free (markers_item->positions);
  guppi_free (markers_item->pixel_positions);
  guppi_free (markers_item->variable_size);
  guppi_free (markers_item->variable_rgba);
  guppi_free (markers_item->variable_edge_rgba);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_markers  (GuppiMarker marker,
				    guint       num_markers,
				    ArtPoint   *positions,
				    double      fixed_size,
				    double     *variable_size,
				    guint32    *variable_rgba,
				    guint32    *variable_edge_rgba)
{
  GuppiCanvasGtkItem_Markers *item;

  g_return_val_if_fail (num_markers > 0, NULL);
  g_return_val_if_fail (positions != NULL, NULL);

  item = guppi_new0 (GuppiCanvasGtkItem_Markers, 1);

  item->item.update  = markers_update;
  item->item.draw    = markers_draw;
  item->item.destroy = markers_destroy;

  item->marker          = marker;
  item->num_markers     = num_markers;
  item->fixed_size      = fixed_size;

  item->positions = guppi_new0 (ArtPoint, num_markers);
  memcpy (item->positions, positions, sizeof (ArtPoint) * num_markers);

  item->pixel_positions = guppi_new0 (ArtPoint, num_markers);
  
  if (variable_size) {
    item->variable_size = guppi_new0 (double, num_markers);
    memcpy (item->variable_size, variable_size, sizeof (double) * num_markers);
  }

  if (variable_rgba) {
    item->variable_rgba = guppi_new0 (guint32, num_markers);
    memcpy (item->variable_rgba, variable_rgba, sizeof (guint32) * num_markers);
  }

  if (variable_edge_rgba) {
    item->variable_edge_rgba = guppi_new0 (guint32, num_markers);
    memcpy (item->variable_edge_rgba, variable_edge_rgba, sizeof (guint32) * num_markers);
  }
  
  return (GuppiCanvasGtkItem *) item;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

typedef struct _GuppiCanvasGtkItem_String GuppiCanvasGtkItem_String;
struct _GuppiCanvasGtkItem_String {
  GuppiCanvasGtkItem item;

  ArtPoint position;
  GuppiCanvasAnchor anchor;
  GuppiCanvasRotation rotation;
  char *string;

  int width, height;
  ArtPoint pixel_position;
  GdkPixbuf *pixbuf;
};

static void
string_update (GuppiCanvasGtkItem *item,
	       GuppiCanvasGtk     *canvas,
	       double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  static PangoLayout *layout = NULL;
  PangoAttribute *pango_attr_scale, *pango_attr_font;
  PangoAttrList *pango_attr_list;
  int width, height;
  guchar *buffer;
  FT_Bitmap ft_bitmap;

  int i, j, k, rowstride, dx, dy;
  int row_step, col_step;
  guchar r, g, b;
  guchar *pixels, *pos, *row;

  GuppiCanvasGtkItem_String *string_item;

  string_item = (GuppiCanvasGtkItem_String *) item;

  if (layout == NULL) {
    layout = pango_layout_new (pango_ft2_get_context (guppi_get_monitor_x_dpi (),
						      guppi_get_monitor_y_dpi ()));
    g_assert (layout != NULL);
  }

  if (string_item->pixbuf) {
    gdk_pixbuf_unref (string_item->pixbuf);
    string_item->pixbuf = NULL;
  }

  /* Assemble our layout. */
  pango_attr_scale = pango_attr_scale_new (scale);
  pango_attr_scale->start_index = 0;
  pango_attr_scale->end_index = -1;

#if 0
  pango_attr_font = pango_attr_font_desc_new (attr->font);
  pango_attr_font->start_index = 0;
  pango_attr_font->end_index = -1;
#endif

  pango_attr_list = pango_attr_list_new ();
  //  pango_attr_list_insert (pango_attr_list, pango_attr_font);
  pango_attr_list_insert (pango_attr_list, pango_attr_scale);

  
  pango_layout_set_font_description (layout, attr->font);
  pango_layout_set_attributes (layout, pango_attr_list);
  pango_attr_list_unref (pango_attr_list);

  pango_layout_set_text (layout, string_item->string, -1);


  /* Use freetype to render the layout into a buffer.*/

  pango_layout_get_pixel_size (layout, &width, &height);

  if (width == 0 || height == 0)
    return;
  buffer = (guchar *) g_malloc0 (width * height);

  ft_bitmap.rows         = height;
  ft_bitmap.width        = width;
  ft_bitmap.pitch        = width;
  ft_bitmap.buffer       = buffer;
  ft_bitmap.num_grays    = 256;
  ft_bitmap.pixel_mode   = ft_pixel_mode_grays;
  ft_bitmap.palette_mode = 0;
  ft_bitmap.palette      = NULL;

  pango_ft2_render_layout (&ft_bitmap, layout, 0, 0);


  /* Create a pixbuf to hold the rendered image.  The size
     depends on whether or not we want the text to be rotated. */
  
  switch (string_item->rotation) {
    
  case GUPPI_CANVAS_ROT_0:
  case GUPPI_CANVAS_ROT_180:
    string_item->width = width;
    string_item->height = height;
    break;

  case GUPPI_CANVAS_ROT_90:
  case GUPPI_CANVAS_ROT_270:
    string_item->width = height;
    string_item->height = width;
    break;

  default:
    g_assert_not_reached ();
  }

  string_item->pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
					TRUE, 8,
					string_item->width,
					string_item->height);

  /* Copy the text rendered by freetype into a pixbuf,
     rotating it if necessary. */

  pixels = gdk_pixbuf_get_pixels (string_item->pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (string_item->pixbuf);

  r = UINT_RGBA_R (attr->color);
  g = UINT_RGBA_G (attr->color);
  b = UINT_RGBA_B (attr->color);

  k = 0;

  switch (string_item->rotation) {
  case GUPPI_CANVAS_ROT_0:
    col_step = 4;
    row_step = rowstride;
    pos = pixels;
    break;
    
  case GUPPI_CANVAS_ROT_90:
    col_step = -rowstride;
    row_step = 4;
    pos = pixels + (string_item->height-1) * rowstride;
    break;

  case GUPPI_CANVAS_ROT_180:
    col_step = -4;
    row_step = -rowstride;
    pos = pixels + (string_item->height-1) * rowstride + 4 * (string_item->width - 1);
    break;

  case GUPPI_CANVAS_ROT_270:
    col_step = rowstride;
    row_step = -4;
    pos = pixels + 4 * (string_item->width - 1);
    break;

  default:
    /* This is to get the compiler to not complain about using
       possibly uninitialized variables. */
    col_step = 0;
    row_step = 0;
    pos = NULL;

    g_assert_not_reached ();
  }

  for (j = 0; j < height; ++j) {
    row = pos;
    for (i = 0; i < width; ++i) {
      row[0] = r;
      row[1] = g;
      row[2] = b;
      row[3] = buffer[k];
      ++k;
      row += col_step;
    }
    pos += row_step;
  }

  g_free (buffer);
  
  /* Compute the pixel position, then adjust it according to
     how we want the text to be anchored. */

  guppi_canvas_gtk_transform_point (canvas,
                                    &string_item->position,
                                    &string_item->pixel_position);

  dx = dy = 0;

  switch (string_item->anchor) {

  case GUPPI_CANVAS_ANCHOR_NORTH:
    dx = -string_item->width / 2;
    break;

  case GUPPI_CANVAS_ANCHOR_NORTHWEST:
    /* do nothing */
    break;

  case GUPPI_CANVAS_ANCHOR_WEST:
    dy = -string_item->height / 2;
    break;

  case GUPPI_CANVAS_ANCHOR_SOUTHWEST:
    dy = -string_item->height;
    break;

  case GUPPI_CANVAS_ANCHOR_SOUTH:
    dx = -string_item->width / 2;
    dy = -string_item->height;
    break;

  case GUPPI_CANVAS_ANCHOR_SOUTHEAST:
    dx = -string_item->width;
    dy = -string_item->height;
    break;

  case GUPPI_CANVAS_ANCHOR_EAST:
    dx = -string_item->width;
    dy = -string_item->height / 2;
    break;

  case GUPPI_CANVAS_ANCHOR_NORTHEAST:
    dx = -string_item->width;
    break;

  case GUPPI_CANVAS_ANCHOR_CENTER:
    dx = -string_item->width / 2;
    dy = -string_item->height / 2;
    break;

  default:
    g_assert_not_reached ();

  }

  string_item->pixel_position.x += dx;
  string_item->pixel_position.y += dy;

}

static void
string_draw (GuppiCanvasGtkItem *item,
	     int                 x0,
	     int                 y0,
	     GdkPixbuf          *pixbuf,
	     double              scale)
{
  GuppiCanvasGtkAttributes *attr = item->attr;
  GuppiCanvasGtkItem_String *string_item;
  string_item = (GuppiCanvasGtkItem_String *) item;

  if (string_item->pixbuf) {

    int x, y, w, h, tx, ty, pix_w, pix_h;
    x = tx = (int) rint (string_item->pixel_position.x) - x0;
    y = ty = (int) rint (string_item->pixel_position.y) - y0;

    pix_w = gdk_pixbuf_get_width (pixbuf);
    pix_h = gdk_pixbuf_get_height (pixbuf);

    w = string_item->width;

    if (x < 0) {
      w += x;
      x = 0;
    }

    if (x + w >= pix_w)
      w = pix_w - x - 1;

    h = string_item->height;

    if (y < 0) {
      h += y;
      y = 0;
    }

    if (y + h >= pix_h)
      h = pix_h - y - 1;

    /* FIXME: Still don't fully handle cases where string_item->pixbuf
       isn't totally contained inside of pixbuf. */
    
    if (w > 0 && h > 0)
      gdk_pixbuf_composite (string_item->pixbuf,
			    pixbuf,
			    x, y,
			    w, h,
			    tx, ty, 1, 1,
			    GDK_INTERP_BILINEAR,
			    UINT_RGBA_A (attr->color));
  }
}

static void
string_destroy (GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkItem_String *string_item;
  string_item = (GuppiCanvasGtkItem_String *) item;

  g_free (string_item->string);

  if (string_item->pixbuf)
    gdk_pixbuf_unref (string_item->pixbuf);
}

GuppiCanvasGtkItem *
guppi_canvas_gtk_item_new_string (ArtPoint             *position,
				  GuppiCanvasAnchor     anchor,
				  GuppiCanvasRotation   rotation,
				  const char           *string)
{
  GuppiCanvasGtkItem_String *item;

  g_return_val_if_fail (position != NULL, NULL);
  g_return_val_if_fail (string != NULL, NULL);

  item = g_new0 (GuppiCanvasGtkItem_String, 1);

  item->item.update  = string_update;
  item->item.draw    = string_draw;
  item->item.destroy = string_destroy;

  item->position = *position;
  item->anchor   = anchor;
  item->rotation = rotation;
  item->string   = g_strdup (string);

  return (GuppiCanvasGtkItem *) item;
}
