/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_CANVAS_H__
#define __GUPPI_CANVAS_H__

#include <glib-object.h>
#include <pango/pango.h>
#include <libart_lgpl/libart.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libguppi/canvas/guppi-marker.h>

typedef enum {
  GUPPI_CANVAS_ANCHOR_NORTH,
  GUPPI_CANVAS_ANCHOR_NORTHWEST,
  GUPPI_CANVAS_ANCHOR_WEST,
  GUPPI_CANVAS_ANCHOR_SOUTHWEST,
  GUPPI_CANVAS_ANCHOR_SOUTH,
  GUPPI_CANVAS_ANCHOR_SOUTHEAST,
  GUPPI_CANVAS_ANCHOR_EAST,
  GUPPI_CANVAS_ANCHOR_NORTHEAST,
  GUPPI_CANVAS_ANCHOR_CENTER
} GuppiCanvasAnchor;

typedef enum {
  GUPPI_CANVAS_ROT_0,
  GUPPI_CANVAS_ROT_90,
  GUPPI_CANVAS_ROT_180,
  GUPPI_CANVAS_ROT_270
} GuppiCanvasRotation;

typedef struct _GuppiCanvas GuppiCanvas;
typedef struct _GuppiCanvasIface GuppiCanvasIface;

struct _GuppiCanvasIface {
  GTypeInterface interface;

  /*** Drawing VTable ***/

  void (*begin_drawing)       (GuppiCanvas *canvas);

  void (*end_drawing)         (GuppiCanvas *canvas);

  void (*get_bbox)            (GuppiCanvas *canvas,
			       ArtDRect    *bbox);


  /* Drawing Attibutes */

  void (*set_color_rgba)      (GuppiCanvas *canvas,
			       guint32      rgba);

  void (*set_edge_color_rgba) (GuppiCanvas *canvas,
			       guint32      rgba);

  void (*set_line_width)      (GuppiCanvas *canvas,
			       double       width);

  void (*set_font)            (GuppiCanvas          *canvas,
			       PangoFontDescription *font);

  void (*set_clipping_rect)   (GuppiCanvas *canvas,
			       ArtDRect    *rectangle);

  void (*push_attributes)     (GuppiCanvas *canvas);

  void (*pop_attributes)      (GuppiCanvas *canvas);


  /* Actual Drawing Functions */

  void (*draw_line)           (GuppiCanvas *canvas,
			       ArtPoint    *point_1,
			       ArtPoint    *point_2);

  void (*draw_path)           (GuppiCanvas *canvas,
			       guint        num_points,
			       ArtPoint    *points);

  void (*draw_polygon)        (GuppiCanvas *canvas,
			       guint        num_points,
			       ArtPoint    *points,
			       gboolean     filled,
			       gboolean     with_edge);

  void (*draw_wedge)          (GuppiCanvas *canvas,
			       ArtPoint    *point,
			       double       radius,
			       double       angle_1,
			       double       angle_2,
			       gboolean     filled,
			       gboolean     with_edge);

  void (*draw_markers)        (GuppiCanvas *canvas,
			       GuppiMarker  marker_type,
			       guint        num_markers,
			       ArtPoint    *positions,
			       double       fixed_size,
			       double      *variable_size,
			       guint32     *variable_rgba,
			       guint32     *variable_edge_rgba);

  void (*draw_pixbuf)         (GuppiCanvas *canvas,
			       ArtDRect    *destination_rectangle,
			       GdkPixbuf   *pixbuf);

  void (*draw_string)         (GuppiCanvas          *canvas,
			       ArtPoint             *position,
			       GuppiCanvasAnchor     anchor,
			       GuppiCanvasRotation   rotation,
			       const char           *string);
};

#define GUPPI_TYPE_CANVAS           (guppi_canvas_get_type ())
#define GUPPI_CANVAS(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_CANVAS, GuppiCanvas))
#define GUPPI_IS_CANVAS(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_CANVAS))
#define GUPPI_CANVAS_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_CANVAS, GuppiCanvasIface))

GType guppi_canvas_get_type            (void); 

void  guppi_canvas_begin_drawing       (GuppiCanvas *canvas);

void  guppi_canvas_end_drawing         (GuppiCanvas *canvas);

void  guppi_canvas_get_bbox            (GuppiCanvas *canvas,
					ArtDRect    *bbox);

void  guppi_canvas_set_color_rgba      (GuppiCanvas *canvas,
					guint32      rgba);

void  guppi_canvas_set_edge_color_rgba (GuppiCanvas *canvas,
					guint32      rgba);

void  guppi_canvas_set_line_width      (GuppiCanvas *canvas,
					double       width);

void  guppi_canvas_set_font            (GuppiCanvas          *canvas,
					PangoFontDescription *font);

void guppi_canvas_set_font_from_string (GuppiCanvas *canvas,
					const char  *font_name);

void  guppi_canvas_set_clipping_rectangle (GuppiCanvas *canvas,
					   ArtDRect    *rectangle);

void  guppi_canvas_push_attributes     (GuppiCanvas *canvas);

void  guppi_canvas_pop_attributes      (GuppiCanvas *canvas);

void  guppi_canvas_draw_line           (GuppiCanvas *canvas,
					ArtPoint    *point_1,
					ArtPoint    *point_2);

void  guppi_canvas_draw_path           (GuppiCanvas *canvas,
					guint        num_points,
					ArtPoint    *points);

void  guppi_canvas_draw_polygon        (GuppiCanvas *canvas,
					guint        num_points,
					ArtPoint    *points,
					gboolean     filled,
					gboolean     with_edge);

void  guppi_canvas_draw_circle         (GuppiCanvas *canvas,
					ArtPoint    *center_point,
					double       radius,
					gboolean     filled,
					gboolean     with_edge);

void  guppi_canvas_draw_wedge          (GuppiCanvas *canvas,
					ArtPoint    *point,
					double       radius,
					double       angle_1,
					double       angle_2,
					gboolean     filled,
					gboolean     with_edge);

void  guppi_canvas_draw_rectangle      (GuppiCanvas *canvas,
					ArtDRect    *rectangle,
					gboolean     filled,
					gboolean     with_edge);

void  guppi_canvas_draw_markers        (GuppiCanvas *canvas,
					GuppiMarker  marker_type,
					guint        num_markers,
					ArtPoint    *position,
					double       fixed_size,
					double      *variable_size,
					guint32     *variable_rgba,
					guint32     *variable_edge_rgba);

void  guppi_canvas_draw_pixbuf         (GuppiCanvas *canvas,
					ArtDRect    *destination_rectangle,
					GdkPixbuf   *pixbuf);

void  guppi_canvas_draw_string         (GuppiCanvas          *canvas,
					ArtPoint             *position,
					GuppiCanvasAnchor     anchor,
					GuppiCanvasRotation   rotation,
					const char           *string);

#endif /* __GUPPI_CANVAS_H__ */

