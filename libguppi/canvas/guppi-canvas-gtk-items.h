/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas-gtk-items.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_CANVAS_GTK_ITEMS_H__
#define __GUPPI_CANVAS_GTK_ITEMS_H__

#include <libguppi/canvas/guppi-canvas.h>
#include <libguppi/canvas/guppi-canvas-gtk.h>

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_line (ArtPoint *point_1,
						    ArtPoint *point_2);

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_path (guint     num_points,
						    ArtPoint *points);

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_polygon (guint     num_points,
						       ArtPoint *points,
						       gboolean  filled,
						       gboolean  with_edge);

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_wedge (ArtPoint *point,
						     double    radius,
						     double    angle_1,
						     double    angle_2,
						     gboolean  filled,
						     gboolean  with_edge);

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_markers (GuppiMarker marker,
						       guint       num_markers,
						       ArtPoint   *positions,
						       double      fixed_size,
						       double     *variable_size,
						       guint32    *variable_rgba,
						       guint32    *variable_edge_rgba);

GuppiCanvasGtkItem *guppi_canvas_gtk_item_new_string (ArtPoint             *position,
						      GuppiCanvasAnchor     anchor,
						      GuppiCanvasRotation   rotation,
						      const char           *string);


#endif /* __GUPPI_CANVAS_GTK_ITEMS_H__ */

