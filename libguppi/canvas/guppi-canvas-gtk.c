/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas-gtk.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/canvas/guppi-canvas-gtk.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/canvas/guppi-canvas.h>
#include <libguppi/canvas/guppi-canvas-gtk-items.h>

#define DEFAULT_FONT "Sans 12"

static GObjectClass *parent_class;


static GuppiCanvasGtkAttributes *
guppi_canvas_gtk_attributes_new (void)
{
  GuppiCanvasGtkAttributes *attr = guppi_new0 (GuppiCanvasGtkAttributes, 1);

  attr->color = RGBA_RED;
  attr->edge_color = RGBA_BLACK;
  attr->line_width = 0;
  attr->font = pango_font_description_from_string (DEFAULT_FONT);

  return attr;
}

static GuppiCanvasGtkAttributes *
guppi_canvas_gtk_attributes_clone (GuppiCanvasGtkAttributes *attr)
{
  GuppiCanvasGtkAttributes *clone;

  clone = guppi_new0 (GuppiCanvasGtkAttributes, 1);
  *clone = *attr;

  clone->font = pango_font_description_copy (attr->font);

  return clone;
}

static void
guppi_canvas_gtk_attributes_free (GuppiCanvasGtkAttributes *attr)
{
  pango_font_description_free (attr->font);
  guppi_free (attr);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_canvas_gtk_item_free (GuppiCanvasGtkItem *item)
{
  if (item->destroy)
    item->destroy (item);
  guppi_canvas_gtk_attributes_free (item->attr);
  guppi_free (item);
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static GuppiCanvasGtkAttributes *
guppi_canvas_gtk_get_attributes (GuppiCanvasGtk *canvas_gtk)
{
  GuppiCanvasGtkAttributes *attr;

  if (canvas_gtk->attributes == NULL) {
    
    attr = guppi_canvas_gtk_attributes_new ();
    canvas_gtk->attributes = g_slist_prepend (canvas_gtk->attributes,
					      attr);

  } else {
    
    attr = canvas_gtk->attributes->data;

  }

  return attr;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_canvas_gtk_add_item (GuppiCanvasGtk *canvas_gtk,
			   GuppiCanvasGtkItem *item)
{
  GuppiCanvasGtkAttributes *attr;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas_gtk));
  g_return_if_fail (item != NULL);

  if (! canvas_gtk->drawing) {
    g_warning ("Attempt to add item to canvas before "
	       "begin_drawing or after end_drawing!");
  }

  g_assert (item->draw);

  /* Attach the current attributes to the item */
  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  item->attr = guppi_canvas_gtk_attributes_clone (attr);

  canvas_gtk->items = g_slist_prepend (canvas_gtk->items,
				       item);

  canvas_gtk->needs_redraw = TRUE;

  gtk_widget_queue_draw (GTK_WIDGET (canvas_gtk));
}

static void
guppi_canvas_gtk_free_items (GuppiCanvasGtk *canvas_gtk)
{
  GSList *iter;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas_gtk));

  for (iter = canvas_gtk->items; iter != NULL; iter = iter->next) {
    GuppiCanvasGtkItem *item = iter->data;

    if (item)
      guppi_canvas_gtk_item_free (item);
  }
  
  g_slist_free (canvas_gtk->items);
  canvas_gtk->items = NULL;
}

static void
guppi_canvas_gtk_update (GuppiCanvasGtk *canvas_gtk)
{
  GSList *iter;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas_gtk));

  for (iter = canvas_gtk->items; iter != NULL; iter = iter->next) {
    GuppiCanvasGtkItem *item = iter->data;
    g_assert (item != NULL);

    if (item->update)
      item->update (item, canvas_gtk, canvas_gtk->scale);
  }

  canvas_gtk->needs_redraw = TRUE;
}

/* If rectangle == NULL, redraw everything */

static void
guppi_canvas_gtk_redraw (GuppiCanvasGtk *canvas_gtk,
			 ArtIRect       *rectangle)
{
  GSList *iter;
  
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas_gtk));

  g_assert (! canvas_gtk->drawing);

  /* FIXME: Should do the right thing if rectangle != NULL */
  gdk_pixbuf_fill (canvas_gtk->buffer, 0xffffffff);

  for (iter = canvas_gtk->items; iter != NULL; iter = iter->next) {
    GuppiCanvasGtkItem *item = iter->data;
    g_assert (item != NULL);

    if (rectangle == NULL
	|| item->intersect == NULL
	|| item->intersect (item, canvas_gtk, rectangle)) {

      int x0 = 0, y0 = 0;
      GdkPixbuf *clipped = NULL;

      /* If clipping is enabled, compute a subpixbuf of our current pixbuf
	 that corresponds to the clipping rectangle.  We pass this subpixbuf
	 to the ->draw function. */
      if (item->attr->clip_to_rect) {
	ArtDRect pixel_rect;
	int w, h;
	int pix_x, pix_y, pix_w, pix_h;

	w = gdk_pixbuf_get_width (canvas_gtk->buffer);
	h = gdk_pixbuf_get_height (canvas_gtk->buffer);

	guppi_canvas_gtk_transform_rectangle (canvas_gtk,
					      &item->attr->clipping_rect,
					      &pixel_rect);

	pix_x = (int) rint (pixel_rect.x0);
	pix_y = (int) rint (pixel_rect.y0);
	pix_w = (int) rint (pixel_rect.x1 - pixel_rect.x0 + 1);
	pix_h = (int) rint (pixel_rect.y1 - pixel_rect.y0 + 1);

	if (pix_x < 0)
	  pix_x = 0;
	if (pix_y < 0)
	  pix_y = 0;
	if (pix_w > w - pix_x)
	  pix_w = w - pix_x;
	if (pix_h > h - pix_y)
	  pix_h = h - pix_y;

	if (pix_x == 0 && pix_y == 0 && pix_w == w && pix_h == h) {

	  x0 = y0 = 0;
	  clipped = gdk_pixbuf_ref (canvas_gtk->buffer);

	} else if (pix_w <= 0 || pix_h <= 0) {

	  clipped = NULL;
	  
	} else {
	  
	  clipped = gdk_pixbuf_new_subpixbuf (canvas_gtk->buffer,
					      pix_x, pix_y, pix_w, pix_h);
	  x0 = pix_x;
	  y0 = pix_y;

	}

      } else { /* no clipping */

	x0 = y0 = 0;
	clipped = gdk_pixbuf_ref (canvas_gtk->buffer);
	
      }

      if (clipped) {
	item->draw (item, x0, y0, clipped, canvas_gtk->scale);
	gdk_pixbuf_unref (clipped);
      }
    }
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Implementation of GuppiCanvas */

static void
guppi_canvas_gtk_begin_drawing (GuppiCanvas *canvas)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  
  guppi_canvas_gtk_free_items (canvas_gtk);

  canvas_gtk->drawing = TRUE;
}

static void
guppi_canvas_gtk_end_drawing (GuppiCanvas *canvas)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  
  canvas_gtk->items = g_slist_reverse (canvas_gtk->items);

  canvas_gtk->drawing = FALSE;

  guppi_canvas_gtk_update (canvas_gtk);
}

static void
guppi_canvas_gtk_get_bbox (GuppiCanvas *canvas,
			   ArtDRect    *bbox)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;

  *bbox = canvas_gtk->bbox;
}

static void
guppi_canvas_gtk_set_color_rgba (GuppiCanvas *canvas,
				 guint32      rgba)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  attr->color = rgba;
}

static void
guppi_canvas_gtk_set_edge_color_rgba (GuppiCanvas *canvas,
				      guint32      rgba)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  attr->edge_color = rgba;
}

static void
guppi_canvas_gtk_set_line_width (GuppiCanvas *canvas,
				 double       width)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  attr->line_width = width;
}

static void
guppi_canvas_gtk_set_font (GuppiCanvas          *canvas,
			   PangoFontDescription *font)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  attr->font = pango_font_description_copy (font);
}

static void
guppi_canvas_gtk_set_clipping_rectangle (GuppiCanvas *canvas,
					 ArtDRect    *rectangle)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  attr = guppi_canvas_gtk_get_attributes (canvas_gtk);
  
  if (rectangle == NULL) {
    attr->clip_to_rect = FALSE;
  } else if (! attr->clip_to_rect) {
    attr->clipping_rect = *rectangle;
    attr->clip_to_rect = TRUE;
  } else {
    art_drect_intersect (&attr->clipping_rect,
			 &attr->clipping_rect,
			 rectangle);
    attr->clip_to_rect = TRUE;
  }
}

static void
guppi_canvas_gtk_push_attributes (GuppiCanvas *canvas)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr, *clone;

  if (canvas_gtk->attributes) {
    attr = canvas_gtk->attributes->data;
    clone = guppi_canvas_gtk_attributes_clone (attr);
  
    canvas_gtk->attributes = g_slist_prepend (canvas_gtk->attributes,
					      clone);
  }
}

static void
guppi_canvas_gtk_pop_attributes (GuppiCanvas *canvas)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkAttributes *attr;

  if (canvas_gtk->attributes) {
    attr = canvas_gtk->attributes->data;
    guppi_canvas_gtk_attributes_free (attr);
    
    canvas_gtk->attributes = g_slist_delete_link (canvas_gtk->attributes,
						  canvas_gtk->attributes);
  }
}

static void
guppi_canvas_gtk_draw_line (GuppiCanvas *canvas,
			    ArtPoint    *point_1,
			    ArtPoint    *point_2)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;
  
  item = guppi_canvas_gtk_item_new_line (point_1, point_2);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_draw_path (GuppiCanvas *canvas,
			    guint        num_points,
			    ArtPoint    *points)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;
  
  item = guppi_canvas_gtk_item_new_path (num_points, points);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_draw_polygon (GuppiCanvas *canvas,
			       guint        num_points,
			       ArtPoint    *points,
			       gboolean     filled,
			       gboolean     with_edge)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;

  item = guppi_canvas_gtk_item_new_polygon (num_points,
					    points,
					    filled,
					    with_edge);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_draw_wedge (GuppiCanvas *canvas,
			     ArtPoint    *point,
			     double       radius,
			     double       angle_1,
			     double       angle_2,
			     gboolean     filled,
			     gboolean     with_edge)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;

  item = guppi_canvas_gtk_item_new_wedge (point,
					  radius,
					  angle_1,
					  angle_2,
					  filled,
					  with_edge);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_draw_markers (GuppiCanvas *canvas,
			       GuppiMarker  marker_type,
			       guint        num_markers,
			       ArtPoint    *positions,
			       double       fixed_size,
			       double      *variable_size,
			       guint32     *variable_rgba,
			       guint32     *variable_edge_rgba)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;
  
  item = guppi_canvas_gtk_item_new_markers (marker_type,
					    num_markers,
					    positions,
					    fixed_size,
					    variable_size,
					    variable_rgba,
					    variable_edge_rgba);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_draw_pixbuf (GuppiCanvas *canvas,
			      ArtDRect    *destination_rectangle,
			      GdkPixbuf   *pixbuf)
{
  /* FIXME! */
  g_assert_not_reached ();
}

static void
guppi_canvas_gtk_draw_string (GuppiCanvas          *canvas,
			      ArtPoint             *position,
			      GuppiCanvasAnchor     anchor,
			      GuppiCanvasRotation   rotation,
			      const char           *string)
{
  GuppiCanvasGtk *canvas_gtk = (GuppiCanvasGtk *) canvas;
  GuppiCanvasGtkItem *item;

  item = guppi_canvas_gtk_item_new_string (position,
					   anchor,
					   rotation,
					   string);

  guppi_canvas_gtk_add_item (canvas_gtk, item);
}

static void
guppi_canvas_gtk_guppi_canvas_init (GuppiCanvasIface *iface)
{
  iface->begin_drawing       = guppi_canvas_gtk_begin_drawing;
  iface->end_drawing         = guppi_canvas_gtk_end_drawing;
  iface->get_bbox            = guppi_canvas_gtk_get_bbox;
  iface->set_color_rgba      = guppi_canvas_gtk_set_color_rgba;
  iface->set_edge_color_rgba = guppi_canvas_gtk_set_edge_color_rgba;
  iface->set_line_width      = guppi_canvas_gtk_set_line_width;
  iface->set_font            = guppi_canvas_gtk_set_font;
  iface->set_clipping_rect   = guppi_canvas_gtk_set_clipping_rectangle;
  iface->push_attributes     = guppi_canvas_gtk_push_attributes;
  iface->pop_attributes      = guppi_canvas_gtk_pop_attributes;
  iface->draw_line           = guppi_canvas_gtk_draw_line;
  iface->draw_path           = guppi_canvas_gtk_draw_path;
  iface->draw_polygon        = guppi_canvas_gtk_draw_polygon;
  iface->draw_wedge          = guppi_canvas_gtk_draw_wedge;
  iface->draw_markers        = guppi_canvas_gtk_draw_markers;
  iface->draw_pixbuf         = guppi_canvas_gtk_draw_pixbuf;
  iface->draw_string         = guppi_canvas_gtk_draw_string;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_canvas_declare_size_in_pixels (GuppiCanvasGtk *canvas,
				     int             width,
				     int             height)
{
  double physical_width = guppi_in2pt (width / canvas->x_dpi);
  double physical_height = guppi_in2pt (height / canvas->y_dpi);

  if (canvas->fixed_size) {

    canvas->bbox.x0 = 0;
    canvas->bbox.y0 = 0;
    canvas->bbox.x1 = canvas->width_in_pts;
    canvas->bbox.y1 = canvas->height_in_pts;

    canvas->scale = MIN (physical_width / canvas->width_in_pts,
			 physical_height / canvas->height_in_pts);

  } else {

    canvas->bbox.x0 = 0;
    canvas->bbox.y0 = 0;
    canvas->bbox.x1 = physical_width / canvas->floating_scale;
    canvas->bbox.y1 = physical_height / canvas->floating_scale;

    canvas->scale = canvas->floating_scale;

  }

  canvas->x_pt2pix = guppi_pt2in(1) * canvas->x_dpi * canvas->scale;
  canvas->y_pt2pix = guppi_pt2in(1) * canvas->y_dpi * canvas->scale;

  g_print ("scale = %g\n", canvas->scale);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gboolean
guppi_canvas_gtk_expose_event (GtkWidget *w,
			       GdkEventExpose *ev)
{
  GuppiCanvasGtk *canvas = GUPPI_CANVAS_GTK (w);

  g_assert (! canvas->drawing);

  if (canvas->needs_redraw) {
    guppi_canvas_gtk_redraw (canvas, NULL);
    canvas->needs_redraw = FALSE;
  }

  gdk_pixbuf_render_to_drawable (canvas->buffer,
				 w->window,
				 w->style->white_gc,
				 ev->area.x, ev->area.y,
				 ev->area.x, ev->area.y,
				 ev->area.width, ev->area.height,
				 GDK_RGB_DITHER_NORMAL, 0, 0);

  if (GTK_WIDGET_CLASS (parent_class)->expose_event)
    GTK_WIDGET_CLASS (parent_class)->expose_event (w, ev);

  return TRUE;
}

static gboolean
guppi_canvas_gtk_configure_event (GtkWidget *w,
				  GdkEventConfigure *ev)
{
  GuppiCanvasGtk *canvas = GUPPI_CANVAS_GTK (w);

  if (canvas->buffer)
    gdk_pixbuf_unref (canvas->buffer);

  canvas->buffer = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
				   FALSE, 8,
				   ev->width, ev->height);

  guppi_canvas_declare_size_in_pixels (canvas, ev->width, ev->height);
  
  guppi_canvas_gtk_update (canvas);

  canvas->needs_redraw = TRUE;

  if (GTK_WIDGET_CLASS (parent_class)->configure_event)
    GTK_WIDGET_CLASS (parent_class)->configure_event (w, ev);

  return FALSE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_canvas_gtk_class_init (GuppiCanvasGtkClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  
  parent_class = g_type_class_peek_parent (klass);

  widget_class->expose_event = guppi_canvas_gtk_expose_event;
  widget_class->configure_event = guppi_canvas_gtk_configure_event;
}

static void
guppi_canvas_gtk_init (GuppiCanvasGtk *canvas)
{
  canvas->bbox.x0 = 0;
  canvas->bbox.y0 = 0;
  canvas->bbox.x1 = 1;
  canvas->bbox.y1 = 1;

  canvas->x_dpi = guppi_get_monitor_x_dpi ();
  canvas->y_dpi = guppi_get_monitor_y_dpi ();

  canvas->floating_scale = 1;
  
  canvas->fixed_size = FALSE;
  canvas->needs_redraw = TRUE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GType
guppi_canvas_gtk_get_type (void)
{
  static GType type = 0;

  if (! type) {
    
    static const GTypeInfo info = {
      sizeof (GuppiCanvasGtkClass),
      NULL, NULL,
      (GClassInitFunc) guppi_canvas_gtk_class_init,
      NULL, NULL,
      sizeof (GuppiCanvasGtk),
      0,
      (GInstanceInitFunc) guppi_canvas_gtk_init
    };

    static const GInterfaceInfo guppi_canvas_info = {
      (GInterfaceInitFunc) guppi_canvas_gtk_guppi_canvas_init,
      NULL, NULL
    };

    type = g_type_register_static (GTK_TYPE_DRAWING_AREA,
				   "GuppiCanvasGtk",
				   &info,
				   0);
    
    g_type_add_interface_static (type,
				 GUPPI_TYPE_CANVAS,
				 &guppi_canvas_info);
				 
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_canvas_gtk_set_dpi (GuppiCanvasGtk *canvas,
			  double          x_dpi,
			  double          y_dpi)
{
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (x_dpi > 0);
  g_return_if_fail (y_dpi > 0);

  canvas->x_dpi = x_dpi;
  canvas->y_dpi = y_dpi;
  
  /* FIXME: readjust stuff */
}

void
guppi_canvas_gtk_set_fixed_size (GuppiCanvasGtk *canvas,
				 double          width_in_pts,
				 double          height_in_pts)
{
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (width_in_pts > 0);
  g_return_if_fail (height_in_pts > 0);

  canvas->fixed_size = TRUE;
  canvas->width_in_pts = width_in_pts;
  canvas->height_in_pts = height_in_pts;

  /* FIXME: readjust stuff */
}

void
guppi_canvas_gtk_set_floating_size (GuppiCanvasGtk *canvas,
				    double          scale)
{
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (scale > 0);

  canvas->fixed_size = FALSE;
  canvas->floating_scale = scale;

  /* FIXME: readjust stuff */
}

void
guppi_canvas_gtk_set_bbox (GuppiCanvasGtk *canvas,
			   ArtDRect       *bbox)
{
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (bbox != NULL);
  
  canvas->bbox = *bbox;
}

void
guppi_canvas_gtk_transform_point (GuppiCanvasGtk *canvas,
				  ArtPoint       *point,
				  ArtPoint       *pixel_point)
{
  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (point != NULL);
  g_return_if_fail (pixel_point != NULL);

  pixel_point->x = canvas->x_pt2pix * (point->x - canvas->bbox.x0);
  pixel_point->y = canvas->y_pt2pix * (canvas->bbox.y1 - point->y);
}

void
guppi_canvas_gtk_transform_points (GuppiCanvasGtk *canvas,
				   guint           num_points,
				   ArtPoint       *points,
				   ArtPoint       *pixel_points)
{
  guint i;
  double x_p2p, y_p2p;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (num_points >= 1);
  g_return_if_fail (points != NULL);
  g_return_if_fail (pixel_points != NULL);

  x_p2p = canvas->x_pt2pix;
  y_p2p = canvas->y_pt2pix;

  for (i = 0; i < num_points; ++i) {
    pixel_points[i].x = x_p2p * (points[i].x - canvas->bbox.x0);
    pixel_points[i].y = y_p2p * (canvas->bbox.y1 - points[i].y);
  }
}


void
guppi_canvas_gtk_transform_points_to_path (GuppiCanvasGtk *canvas,
					   guint           num_points,
					   ArtPoint       *points,
					   ArtVpath       *path)
{
  guint i;
  double x_p2p, y_p2p;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (num_points >= 2);
  g_return_if_fail (points != NULL);
  g_return_if_fail (path != NULL);

  x_p2p = canvas->x_pt2pix;
  y_p2p = canvas->y_pt2pix;

  for (i = 0; i < num_points; ++i) {
    path[i].code = ART_LINETO;
    path[i].x = x_p2p * (points[i].x - canvas->bbox.x0);
    path[i].y = y_p2p * (canvas->bbox.y1 - points[i].y);
  }

  path[0].code = ART_MOVETO;
  path[num_points].code = ART_END;
}

void
guppi_canvas_gtk_transform_path (GuppiCanvasGtk *canvas,
				 ArtVpath       *path)
{
  int i;
  double x_p2p, y_p2p;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (path != NULL);

  x_p2p = canvas->x_pt2pix;
  y_p2p = canvas->y_pt2pix;

  for (i = 0; path[i].code != ART_END; ++i) {
    path[i].x = x_p2p * (path[i].x - canvas->bbox.x0);
    path[i].y = y_p2p * (canvas->bbox.y1 - path[i].y);
  }
}

void
guppi_canvas_gtk_transform_rectangle (GuppiCanvasGtk *canvas,
				      ArtDRect       *rectangle,
				      ArtDRect       *pixel_rectangle)
{
  double t;

  g_return_if_fail (GUPPI_IS_CANVAS_GTK (canvas));
  g_return_if_fail (rectangle != NULL);
  g_return_if_fail (pixel_rectangle != NULL);

  pixel_rectangle->x0 = canvas->x_pt2pix * (rectangle->x0 - canvas->bbox.x0);
  pixel_rectangle->x1 = canvas->x_pt2pix * (rectangle->x1 - canvas->bbox.x0);
  pixel_rectangle->y0 = canvas->y_pt2pix * (canvas->bbox.y1 - rectangle->y0);
  pixel_rectangle->y1 = canvas->y_pt2pix * (canvas->bbox.y1 - rectangle->y1);
  
  if (pixel_rectangle->x0 > pixel_rectangle->x1) {
    t = pixel_rectangle->x0;
    pixel_rectangle->x0 = pixel_rectangle->x1;
    pixel_rectangle->x1 = t;
  }

  if (pixel_rectangle->y0 > pixel_rectangle->y1) {
    t = pixel_rectangle->y0;
    pixel_rectangle->y0 = pixel_rectangle->y1;
    pixel_rectangle->y1 = t;
  }
}

