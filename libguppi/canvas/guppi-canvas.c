/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-canvas.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/canvas/guppi-canvas.h>

static void
guppi_canvas_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  if (! initialized) {
    /* Foo */
    initialized = TRUE;
  }
}

GType
guppi_canvas_get_type (void) 
{
  static GType type = 0;

  if (! type) {

    static const GTypeInfo info = {
      sizeof (GuppiCanvasIface),
      guppi_canvas_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL, NULL
    };

    type = g_type_register_static (G_TYPE_INTERFACE,
				   "GuppiCanvas",
				   &info,
				   0);
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void 
guppi_canvas_begin_drawing (GuppiCanvas *canvas)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  if (iface->begin_drawing)
    iface->begin_drawing (canvas);
}

void 
guppi_canvas_end_drawing (GuppiCanvas *canvas)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  if (iface->end_drawing)
    iface->end_drawing (canvas);
}

void
guppi_canvas_get_bbox (GuppiCanvas *canvas,
		       ArtDRect    *bbox)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (bbox != NULL);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->get_bbox);
  iface->get_bbox (canvas, bbox);
}

void
guppi_canvas_set_color_rgba (GuppiCanvas *canvas,
			     guint32      rgba)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->set_color_rgba);
  iface->set_color_rgba (canvas, rgba);
}

void 
guppi_canvas_set_edge_color_rgba (GuppiCanvas *canvas,
				  guint32      rgba)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->set_edge_color_rgba);
  iface->set_edge_color_rgba (canvas, rgba);
}

void
guppi_canvas_set_line_width (GuppiCanvas *canvas,
			     double       width)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (width >= 0);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->set_line_width);
  iface->set_line_width (canvas, width);
}

void
guppi_canvas_set_font (GuppiCanvas          *canvas,
		       PangoFontDescription *font)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (font != NULL);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->set_font);
  iface->set_font (canvas, font);
}

void
guppi_canvas_set_font_from_string (GuppiCanvas *canvas,
				   const char  *font_name)
{
  PangoFontDescription *font;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (font_name && *font_name);

  font = pango_font_description_from_string (font_name);
  if (font) {
    guppi_canvas_set_font (canvas, font);
    pango_font_description_free (font);
  }
}

void
guppi_canvas_set_clipping_rectangle (GuppiCanvas *canvas,
				     ArtDRect    *rectangle)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->set_clipping_rect);
  iface->set_clipping_rect (canvas, rectangle);
}

void
guppi_canvas_push_attributes (GuppiCanvas *canvas)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->push_attributes);
  iface->push_attributes (canvas);
}

void
guppi_canvas_pop_attributes (GuppiCanvas *canvas)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->pop_attributes);
  iface->pop_attributes (canvas);
}

void 
guppi_canvas_draw_line (GuppiCanvas *canvas,
			ArtPoint    *point_1,
			ArtPoint    *point_2)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (point_1 != NULL);
  g_return_if_fail (point_2 != NULL);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_line);
  iface->draw_line (canvas, point_1, point_2);
}

void 
guppi_canvas_draw_path (GuppiCanvas *canvas,
			guint        num_points,
			ArtPoint    *points)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));

  if (num_points < 2)
    return;
  
  g_return_if_fail (points != NULL);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_path);
  iface->draw_path (canvas, num_points, points);
}

void
guppi_canvas_draw_polygon (GuppiCanvas *canvas,
			   guint        num_points,
			   ArtPoint    *points,
			   gboolean     filled,
			   gboolean     with_edge)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (num_points >= 0);

  if (num_points < 0)
    return;

  g_return_if_fail (points != NULL);

  if (!filled && !with_edge)
    return;
  
  iface = GUPPI_CANVAS_GET_IFACE (canvas);
  
  g_assert (iface->draw_polygon);
  iface->draw_polygon (canvas, num_points, points, filled, with_edge);
}

void
guppi_canvas_draw_circle (GuppiCanvas *canvas,
			  ArtPoint    *center_point,
			  double       radius,
			  gboolean     filled,
			  gboolean     with_edge)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (center_point != NULL);

  if (radius < 0)
    return;

  if (!filled && !with_edge)
    return;

  guppi_canvas_draw_wedge (canvas,
			   center_point,
			   radius,
			   0,
			   2 * M_PI,
			   filled,
			   with_edge);
}

void
guppi_canvas_draw_wedge (GuppiCanvas *canvas,
			 ArtPoint    *point,
			 double       radius,
			 double       angle_1,
			 double       angle_2,
			 gboolean     filled,
			 gboolean     with_edge)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (point != NULL);

  if (radius < 0)
    return;

  if (!filled && !with_edge)
    return;
  
  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_wedge);
  iface->draw_wedge (canvas,
		     point,
		     radius,
		     angle_1,
		     angle_2,
		     filled,
		     with_edge);
}

void
guppi_canvas_draw_rectangle (GuppiCanvas *canvas,
			     ArtDRect    *rectangle,
			     gboolean     filled,
			     gboolean     with_edge)
{
  ArtPoint pts[5];

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (rectangle != NULL);

  pts[0].x = rectangle->x0;
  pts[0].y = rectangle->y0;

  pts[1].x = rectangle->x1;
  pts[1].y = rectangle->y0;

  pts[2].x = rectangle->x1;
  pts[2].y = rectangle->y1;

  pts[3].x = rectangle->x0;
  pts[3].y = rectangle->y1;

  pts[4].x = rectangle->x0;
  pts[4].y = rectangle->y0;

  guppi_canvas_draw_polygon (canvas, 5, pts, filled, with_edge);
}

void
guppi_canvas_draw_markers (GuppiCanvas *canvas,
			   GuppiMarker  marker_type,
			   guint        num_markers,
			   ArtPoint    *position,
			   double       fixed_size,
			   double      *variable_size,
			   guint32     *variable_rgba,
			   guint32     *variable_edge_rgba)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (num_markers >= 0);
  
  if (num_markers == 0)
    return;

  g_return_if_fail (position != NULL);

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_markers);
  iface->draw_markers (canvas,
		       marker_type,
		       num_markers,
		       position,
		       fixed_size,
		       variable_size,
		       variable_rgba,
		       variable_edge_rgba);
}

void
guppi_canvas_draw_pixbuf (GuppiCanvas *canvas,
			  ArtDRect    *destination_rectangle,
			  GdkPixbuf   *pixbuf)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (destination_rectangle != NULL);
  g_return_if_fail (GDK_IS_PIXBUF (pixbuf));

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_pixbuf);
  iface->draw_pixbuf (canvas,
		      destination_rectangle,
		      pixbuf);
}

void
guppi_canvas_draw_string (GuppiCanvas          *canvas,
			  ArtPoint             *position,
			  GuppiCanvasAnchor     anchor,
			  GuppiCanvasRotation   rotation,
			  const char           *string)
{
  GuppiCanvasIface *iface;

  g_return_if_fail (GUPPI_IS_CANVAS (canvas));
  g_return_if_fail (position != NULL);
  g_return_if_fail (string != NULL);

  if (! *string)
    return;

  iface = GUPPI_CANVAS_GET_IFACE (canvas);

  g_assert (iface->draw_string);
  iface->draw_string (canvas,
		      position,
		      anchor,
		      rotation,
		      string);
}
