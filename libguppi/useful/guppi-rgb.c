/* $Id$ */

/*
 * guppi-rgb.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <libguppi/useful/guppi-rgb.h>

#include <config.h>
#include <math.h>
#include <ctype.h>

#include <libguppi/useful/guppi-convenient.h>


static GHashTable *rgb_hash = NULL;

static gboolean
populate_rgb_hash (const gchar * filename)
{
  FILE *in;
  gchar buffer[128];
  gint r, g, b;

  in = fopen (filename, "r");
  if (in == NULL)
    return FALSE;


  while (fgets (buffer, 127, in)) {
    if (sscanf (buffer, "%d %d %d", &r, &g, &b) == 3) {
      gchar *s = buffer;
      guint32 c = RGBA_TO_UINT (r, g, b, 0xff);

      g_strchomp (buffer);

      while (*s && !isalpha ((guchar)*s))
	++s;

      g_hash_table_insert (rgb_hash, guppi_strdup (s), GUINT_TO_POINTER (c));
    }
  }

  fclose (in);
  return TRUE;
}

static void
init_rgb_hash (void)
{
  const gchar *rgb_paths[] = {
    "/usr/X11R6/lib/X11/rgb.txt",
    "/usr/X/lib/X11/rgb.txt",
    "/usr/X/lib/rgb.txt",
    NULL
  };
  gint i;

  if (rgb_hash != NULL)
    return;

  if (rgb_hash == NULL)
    rgb_hash = g_hash_table_new (g_str_hash, g_str_equal);

  /* A horrible hack */
  for (i = 0; rgb_paths[i]; ++i)
    if (populate_rgb_hash (rgb_paths[i]))
      return;

  /* One way to get around this might be for Guppi to ship with its
     own copy of rgb.txt.  Ugly, but it would let us do color name->rgba
     conversions w/o needing an X server around... */

  g_warning ("Couldn't find rgb.txt!");
}

guint32
guppi_str2color_rgba (const gchar *color_name)
{
  gint r, g, b, a, rv;

  rv = sscanf (color_name, "#%2x%2x%2x%2x", &r, &g, &b, &a);
  if (rv == 3) {
    return RGB_TO_UINT (r, g, b);
  } else if (rv == 4) {
    return RGBA_TO_UINT (r, g, b, a);
  }

  /* If we have an X server, we try to use gdk. */
  if (gdk_init_check (NULL, NULL)) {
    GdkColor c;
    if (gdk_color_parse (color_name, &c)) {
      return RGBA_TO_UINT (c.red >> 8, c.green >> 8, c.blue >> 8, 0xff);
    } else {
      return 0;
    }
  } else {
    gpointer c;
    
    if (rgb_hash == NULL)
      init_rgb_hash ();

    c = g_hash_table_lookup (rgb_hash, color_name);

    return GPOINTER_TO_UINT (c);
  }
}

/* $Id$ */
