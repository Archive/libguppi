/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pango-helper.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/useful/guppi-pango-helper.h>

#include <libguppi/useful/guppi-metrics.h>
#include <pango/pangoft2.h>

void
guppi_pango_get_string_size (const char *str,
			     PangoFontDescription *font,
			     double *width_in_pts,
			     double *height_in_pts)
{
  static PangoLayout *layout;
  int w, h;
  double x_dpi, y_dpi;

  x_dpi = guppi_get_monitor_x_dpi ();
  y_dpi = guppi_get_monitor_y_dpi ();

  if (width_in_pts)
    *width_in_pts = 0;
  if (height_in_pts)
    *height_in_pts = 0;

  g_return_if_fail (str != NULL);
  g_return_if_fail (font != NULL);

  if (layout == NULL) {
    /* FIXME: need correct DPI! */
    layout = pango_layout_new (pango_ft2_get_context (x_dpi, y_dpi));
    g_assert (layout != NULL);
  }

  pango_layout_set_font_description (layout, font);
  pango_layout_set_text (layout, str, -1);

  pango_layout_get_size (layout, &w, &h);

  if (width_in_pts)
    *width_in_pts = (72/x_dpi) * w / (double) PANGO_SCALE;

  if (height_in_pts)
    *height_in_pts = (72/y_dpi) * h / (double) PANGO_SCALE;
}

