/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-xml.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_XML_H__
#define __GUPPI_XML_H__

#include <glib.h>
#include <gtk/gtktypeutils.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>

#include <libguppi/useful/guppi-unique-id.h>

/* Try to be compatible with old versions of libxml (before 1.8.8) */
#ifndef xmlChildrenNode
#define xmlChildrenNode childs
#define xmlRootNode root
#endif

typedef enum {
  GUPPI_XML_V1
} GuppiXMLVersion;

typedef struct _GuppiXMLDocument GuppiXMLDocument;
struct _GuppiXMLDocument {
  xmlDocPtr  doc;  /* XML Document    */
  xmlNsPtr   ns;   /* Main name space */

  GuppiXMLVersion version;

  GHashTable *uniq_table;
};

typedef struct _GuppiXMLDocumentWithNode GuppiXMLDocumentWithNode;
struct _GuppiXMLDocumentWithNode {
  GuppiXMLDocument *doc;
  xmlNodePtr node;
};

GuppiXMLDocument *guppi_xml_document_new        (void);

GuppiXMLDocument *guppi_xml_document_read_file  (const gchar *filename);
void              guppi_xml_document_write_file (GuppiXMLDocument *doc, const gchar *filename);

xmlNodePtr        guppi_xml_document_get_root   (GuppiXMLDocument *doc);
void              guppi_xml_document_set_root   (GuppiXMLDocument *doc, xmlNodePtr);

void              guppi_xml_document_free       (GuppiXMLDocument *doc);
void              guppi_xml_document_spew       (GuppiXMLDocument *doc);

void              guppi_xml_document_cache      (GuppiXMLDocument *doc, guppi_uniq_t id, gpointer ptr);
void              guppi_xml_document_cache_full (GuppiXMLDocument *doc, guppi_uniq_t id, gpointer ptr, GtkDestroyNotify destroy_fn);
gboolean          guppi_xml_document_has_cached (GuppiXMLDocument *doc, guppi_uniq_t id);
gpointer          guppi_xml_document_lookup     (GuppiXMLDocument *doc, guppi_uniq_t id);


/* Wrappers around some XML node creation/modification routines. */

xmlNodePtr guppi_xml_new_node       (GuppiXMLDocument *doc, const gchar *node_name);

xmlNodePtr guppi_xml_new_text_node  (GuppiXMLDocument *doc, const gchar *node_name, const gchar *node_text);
xmlNodePtr guppi_xml_new_text_nodef (GuppiXMLDocument *doc, const gchar *node_name, const gchar *node_text_format, ...);

void       guppi_xml_set_property        (xmlNodePtr node, const gchar *name, const gchar *value);
void       guppi_xml_set_propertyf       (xmlNodePtr node, const gchar *name, const gchar *value_format, ...);
void       guppi_xml_set_property_bool   (xmlNodePtr node, const gchar *name, gboolean value);
void       guppi_xml_set_property_int    (xmlNodePtr node, const gchar *name, gint value);
void       guppi_xml_set_property_double (xmlNodePtr node, const gchar *name, double value);

gchar     *guppi_xml_get_property        (xmlNodePtr node, const gchar *name);
gboolean   guppi_xml_get_property_bool   (xmlNodePtr node, const gchar *name, gboolean default_value);
gint       guppi_xml_get_property_int    (xmlNodePtr node, const gchar *name, gint default_value);
double     guppi_xml_get_property_double (xmlNodePtr node, const gchar *name, double default_value);



#endif /* __GUPPI_XML_H__ */

