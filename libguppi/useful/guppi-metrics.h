/* $Id$ */

/*
 * guppi-metrics.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_METRICS_H
#define _INC_GUPPI_METRICS_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef enum { GUPPI_INVALID_METRIC,
  GUPPI_PT,
  GUPPI_IN,
  GUPPI_CM,
  GUPPI_MM,
  GUPPI_X_PX,
  GUPPI_Y_PX,
  GUPPI_PX
} guppi_metric_t;

void guppi_set_monitor_dpi (double x, double y);
void guppi_set_monitor_size (double x, double y, guppi_metric_t units);

double guppi_get_monitor_x_dpi (void);
double guppi_get_monitor_y_dpi (void);

/* point <=> inch conversions */
double guppi_pt2in (double pt);
double guppi_in2pt (double in);

/* point <=> centimeter conversions */
double guppi_pt2cm (double pt);
double guppi_cm2pt (double cm);

/* point <=> millimeter conversions */
double guppi_pt2mm (double pt);
double guppi_mm2pt (double mm);

/* point <=> pica conversions */
double guppi_pt2pc (double pt);
double guppi_pc2pt (double pc);

/* point <=> pixel conversions:
   Since pixels are often not square, we have both x- and y-direction
   conversion factors. */
double guppi_x_pt2px (double pt);
double guppi_x_px2pt (double px);
double guppi_y_pt2px (double pt);
double guppi_y_px2pt (double px);

/* Average of the x and y conversions... */
double guppi_pt2px (double pt);
double guppi_px2pt (double px);

/* Distance between two pixels, in points. */
double guppi_px_dist (double px0, double py0, double px1, double py1);

/* Convert x from (units) to pts */
double guppi_to_pt (double x, guppi_metric_t units);

/* Convert x from pts to (units) */
double guppi_from_pt (double x, guppi_metric_t units);

const gchar *guppi_metric_name (guppi_metric_t units);


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_METRICS_H */

/* $Id$ */
