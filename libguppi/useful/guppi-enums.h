/* $Id$ */

/*
 * guppi-enums.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ENUMS_H
#define _INC_GUPPI_ENUMS_H

/* #include <gtk/gtk.h> */
#include <glib.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef enum {
  GUPPI_COMPASS_INVALID = 0,
  GUPPI_NORTH = 1 << 0,
  GUPPI_SOUTH = 1 << 1,
  GUPPI_EAST = 1 << 2,
  GUPPI_WEST = 1 << 3
} guppi_compass_t;

typedef enum {
  GUPPI_ALIGNMENT_INVALID = 0,
  GUPPI_LEFT,
  GUPPI_RIGHT,
  GUPPI_TOP,
  GUPPI_BOTTOM,
  GUPPI_CENTER_X,
  GUPPI_CENTER_Y,
  GUPPI_WIDTH,
  GUPPI_HEIGHT
} guppi_alignment_t;

typedef enum {
  GUPPI_ORIENTATION_INVALID = 0,
  GUPPI_HORIZONTAL,
  GUPPI_VERTICAL
} guppi_orientation_t;

typedef enum {
  GUPPI_META_AXIS    = 0,
  GUPPI_X_AXIS       = 1,
  GUPPI_Y_AXIS       = 2,
  GUPPI_Z_AXIS       = 3,
  GUPPI_T_AXIS       = 4,
  GUPPI_LAST_AXIS    = 5,
  GUPPI_INVALID_AXIS = 6
} guppi_axis_t;


const gchar *guppi_compass2str (guppi_compass_t val);
guppi_compass_t guppi_str2compass (const gchar *str);

const gchar *guppi_alignment2str (guppi_alignment_t val);
guppi_alignment_t guppi_str2alignment (const gchar *str);

#define guppi_alignment_is_horizontal(x) \
((x) == GUPPI_LEFT || (x) == GUPPI_RIGHT || (x) == GUPPI_CENTER_X || (x) == GUPPI_WIDTH)

#define guppi_alignment_is_vertical(x) \
((x) == GUPPI_TOP || (x) == GUPPI_BOTTOM || (x) == GUPPI_CENTER_Y || (x) == GUPPI_HEIGHT)

#define guppi_alignment_matches_orientation(align, orien) \
(((orien) == GUPPI_HORIZONTAL && guppi_alignment_is_horizontal(align)) || \
 ((orien) == GUPPI_VERTICAL && guppi_alignment_is_vertical(align)))

const gchar *guppi_orientation2str (guppi_orientation_t val);
guppi_orientation_t guppi_str2orientation (const gchar *str);

const gchar *guppi_axis2str (guppi_axis_t val);
guppi_axis_t guppi_str2axis (const gchar *str);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ENUMS_H */

/* $Id$ */
