/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-unique-id.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/useful/guppi-unique-id.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <sys/types.h>

#include <libguppi/useful/guppi-convenient.h>

static void
init_unique_id (void)
{
  gint fd;
  unsigned seed = 0;

  /* Get some quasi-random number based on system info.  This is an
     awful way to get a seed. */
  seed = (unsigned)time (NULL) * (unsigned)getpid () + (unsigned)getppid ();

  /* If we have /dev/urandom, read some random bytes to use as a seed.
     Hopefully we will be able to overwrite at least part of seed. */
  fd = open ("/dev/urandom", O_RDONLY);
  if (fd >= 0) {
    read (fd, &seed, sizeof (seed));
    close (fd);
  }

  srandom (seed);
}

guppi_uniq_t
guppi_unique_id (void)
{
  static gboolean initialized = FALSE;
  guint32 time_part, rand_part;
  guppi_uniq_t uid = 0;
  static guint count_part = 0;

  if (!initialized) {
    init_unique_id ();
    initialized = TRUE;
  }
  
  time_part = (guint32) time (NULL);
  rand_part = (guint32) random ();
  /* XOR top and bottom 16 bits of the random part together. */
  rand_part = (rand_part ^ (rand_part >> 16)) & 0xffff;

  uid = (((guppi_uniq_t)time_part) << 32)
    | (((guppi_uniq_t)(rand_part & 0x000fffff)) << 12)
    | (guppi_uniq_t)(count_part & 0x00000fff);

  ++count_part;
  
  return uid;
}

gchar *
guppi_uniq2str (guppi_uniq_t x)
{
  gchar *str = guppi_new0 (gchar, 18);
  sprintf (str, "%x-%x", (guint32)(x >> 32), (guint32)(x & 0xffffffff));
  return str;
}

guppi_uniq_t
guppi_str2uniq (const gchar *str)
{
  guint32 a, b;
  if (sscanf (str, "%x-%x", &a, &b) != 2)
    return 0;

  return (((guppi_uniq_t)a) << 32) | ((guppi_uniq_t)b);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static guint
uniq_hash (gconstpointer key)
{
  guppi_uniq_t x = *(guppi_uniq_t *)key;
  guint hash = ((guint)(x & (guint64)0xffffffff)) ^ (guint)(x >> 32);
  return hash;
}

static gint
uniq_equal (gconstpointer a, gconstpointer b)
{
  guppi_uniq_t x = *(guppi_uniq_t *)a;
  guppi_uniq_t y = *(guppi_uniq_t *)b;
  return guppi_uniq_eq (x, y);
}

GHashTable *
guppi_uniq_table_new (void)
{
  return g_hash_table_new (uniq_hash, uniq_equal);
}

void
guppi_uniq_table_destroy (GHashTable *hash)
{
  g_return_if_fail (hash);
  g_hash_table_foreach (hash, guppi_free_hash_key, NULL);
  g_hash_table_destroy (hash);
}

void
guppi_uniq_table_add (GHashTable *hash, guppi_uniq_t x, gpointer ptr)
{
  g_return_if_fail (hash);
  if (!guppi_uniq_table_contains (hash, x)) {
    guppi_uniq_t *p = guppi_new0 (guppi_uniq_t, 1);
    *p = x;
    g_hash_table_insert (hash, p, ptr);
  }
}

gboolean
guppi_uniq_table_contains (GHashTable *hash, guppi_uniq_t x)
{
  g_return_val_if_fail (hash, FALSE);

  return g_hash_table_lookup_extended (hash, &x, NULL, NULL);
}

gpointer
guppi_uniq_table_lookup (GHashTable *hash, guppi_uniq_t x)
{
  return g_hash_table_lookup (hash, &x);
}

/* $Id$ */
