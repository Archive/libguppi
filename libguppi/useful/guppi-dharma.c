/* $Id$ */

/*
 * guppi-dharma.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/useful/guppi-dharma.h>

#include <string.h>

#include <glib.h>
#include <libguppi/useful/guppi-memory.h>

#define GUPPI_MANTRA "Om Mani Padme Hung "
#define GUPPI_WHEEL_MANTRA_COUNT 10
#define GUPPI_WHEEL_INTERVAL 20

static const gchar *guppi_mantra = GUPPI_MANTRA;
static gsize guppi_wheel_size = 0;
static gchar *guppi_wheel;

static gint guppi_wheel_rotation_count = 0;

void
guppi_dharma_turn_wheel (void)
{
  gint i, j;
  gchar c;
  gchar *p;

  if (guppi_wheel_size == 0)
    guppi_dharma_init ();

  for (j = 0; j < guppi_wheel_size; ++j) {

    /*
      A single shift operation, corresponding to turning the wheel
      one "click".
    */

    c = guppi_wheel[0];
    p = guppi_wheel+1;
    for (i = 1; i < guppi_wheel_size; ++i) {
      gchar t = *p;
      *p = c;
      c = t;
      ++p;
    }
    guppi_wheel[0] = c;
  }

  ++guppi_wheel_rotation_count;
}

static gint
wheel_cb (gpointer unused)
{
  guppi_dharma_turn_wheel ();
  return TRUE;
}

void
guppi_dharma_turn_wheel_automatically (void)
{
  static gboolean turning = FALSE;

  if (turning)
    return;

  guppi_dharma_init ();

  /* Set up timeout */
  g_timeout_add (GUPPI_WHEEL_INTERVAL * 1000,
		 wheel_cb, NULL);

  turning = TRUE;
}

void
guppi_dharma_init (void)
{
  gint i;
  gchar *buf;

  if (guppi_wheel_size > 0)
    return;

  /* Allocate wheel buffer */
  guppi_wheel_size = strlen (guppi_mantra) * GUPPI_WHEEL_MANTRA_COUNT;
  guppi_wheel = guppi_new (gchar, guppi_wheel_size);
  guppi_permanent_alloc (guppi_wheel);

  /* Initialize wheel buffer */
  buf = guppi_wheel;
  for (i = 0; i < GUPPI_WHEEL_MANTRA_COUNT; ++i) {
    const gchar *p = guppi_mantra;
    while (*p) {
      *buf = *p;
      ++buf;
      ++p;
    }
  }
}


/* $Id$ */
