/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-attribute-bag.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ATTRIBUTE_BAG_H
#define _INC_GUPPI_ATTRIBUTE_BAG_H

#include <glib-object.h>
#include <libguppi/useful/guppi-defs.h>

#include <libguppi/useful/guppi-attribute-flavor.h>

BEGIN_GUPPI_DECLS;

struct _GuppiAttributeBagPrivate;
typedef struct _GuppiAttributeBagPrivate GuppiAttributeBagPrivate;

typedef struct _GuppiAttributeBag GuppiAttributeBag;
typedef struct _GuppiAttributeBagClass GuppiAttributeBagClass;

typedef void (*GuppiAttributeBagFn) (GuppiAttributeBag *, const gchar *name, gpointer user_data);


struct _GuppiAttributeBag {
  GObject parent;

  GuppiAttributeBagPrivate *priv;
};

struct _GuppiAttributeBagClass {
  GObjectClass parent_class;

  void (*added) (GuppiAttributeBag *, const gchar *name);
  void (*changed) (GuppiAttributeBag *, const gchar *name);
};

#define GUPPI_TYPE_ATTRIBUTE_BAG            (guppi_attribute_bag_get_type ())
#define GUPPI_ATTRIBUTE_BAG(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_ATTRIBUTE_BAG, GuppiAttributeBag))
#define GUPPI_ATTRIBUTE_BAG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_ATTRIBUTE_BAG, GuppiAttributeBagClass))
#define GUPPI_IS_ATTRIBUTE_BAG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_ATTRIBUTE_BAG))
#define GUPPI_IS_ATTRIBUTE_BAG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ATTRIBUTE_BAG))

GType guppi_attribute_bag_get_type (void);

GuppiAttributeBag *guppi_attribute_bag_new (void);
GuppiAttributeBag *guppi_attribute_bag_copy (GuppiAttributeBag *);

void guppi_attribute_bag_add (GuppiAttributeBag *bag,
			      GuppiAttributeFlavor flavor,
			      const gchar *name,
			      const gchar *description);

void guppi_attribute_bag_add_with_default (GuppiAttributeBag *bag,
					   GuppiAttributeFlavor flavor,
					   const gchar *name,
					   const gchar *description,
					   ...);

gboolean guppi_attribute_bag_contains (GuppiAttributeBag *bag, const gchar *name);
GuppiAttributeFlavor guppi_attribute_bag_get_flavor (GuppiAttributeBag *bag, const gchar *name);

void guppi_attribute_bag_foreach (GuppiAttributeBag *bag, GuppiAttributeBagFn fn, gpointer user_data);

gboolean guppi_attribute_bag_get1  (GuppiAttributeBag *bag, const gchar *key, gpointer dest);
gboolean guppi_attribute_bag_vset1  (GuppiAttributeBag *bag, const gchar *key, va_list *varargs);

gboolean guppi_attribute_bag_vget (GuppiAttributeBag *bag, va_list varargs);
gboolean guppi_attribute_bag_vset (GuppiAttributeBag *bag, va_list *varargs);
gboolean guppi_attribute_bag_get  (GuppiAttributeBag *bag, ...);
gboolean guppi_attribute_bag_set  (GuppiAttributeBag *bag, ...);


void guppi_attribute_bag_restore_default (GuppiAttributeBag *bag, const gchar *name);
void guppi_attribute_bag_restore_all_defaults (GuppiAttributeBag *bag);

void guppi_attribute_bag_dump (GuppiAttributeBag *bag);

xmlNodePtr guppi_attribute_bag_export_xml (GuppiAttributeBag *bag, GuppiXMLDocument *doc);
gboolean   guppi_attribute_bag_import_xml (GuppiAttributeBag *bag, GuppiXMLDocument *doc, xmlNodePtr node);
void       guppi_attribute_bag_spew_xml   (GuppiAttributeBag *bag);

			      

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_ATTRIBUTE_BAG_H */

/* $Id$ */
