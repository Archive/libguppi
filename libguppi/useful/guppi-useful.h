/* $Id$ */

/*
 * guppi-useful.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_USEFUL_H
#define _INC_GUPPI_USEFUL_H

#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-defaults.h>
#include <libguppi/useful/guppi-enums.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-paths.h>
#include <libguppi/useful/guppi-plug-in-spec.h>
#include <libguppi/useful/guppi-plug-in.h>
#include <libguppi/useful/guppi-string.h>
#include <libguppi/useful/guppi-version.h>

#include <libguppi/useful/guppi-useful-init.h>

#endif /* _INC_GUPPI_USEFUL_H */

/* $Id$ */
