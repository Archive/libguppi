/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plug-in.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/useful/guppi-plug-in.h>

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <gmodule.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-plug-in-spec.h>

#define GUPPI_PLUG_IN_MAGIC_NUMBER 0xb0bee003

void
guppi_plug_in_construct (GuppiPlugIn *pi)
{
  pi->magic_number = GUPPI_PLUG_IN_MAGIC_NUMBER;
}

void
guppi_plug_in_free (GuppiPlugIn *pi)
{
  if (pi) {
    if (pi->finalize)
      pi->finalize (pi);
    guppi_free0 (pi->load_path);
    guppi_free (pi);
  }
}

/***********************************************************************/

GuppiPlugIn *
guppi_plug_in_load (const gchar *path)
{
  GModule *module;
  gboolean found_symbol;
  gpointer plug_in = NULL;
  GuppiPlugIn *pi;

  g_return_val_if_fail (path != NULL, NULL);

  module = g_module_open (path, G_MODULE_BIND_LAZY);
  if (module == NULL) {
    g_warning ("Attempt to open plug-in %s failed: %s",
	       path, g_module_error ());
    return NULL;
  }

  found_symbol = g_module_symbol (module, "guppi_plug_in", &plug_in);
  if (!found_symbol) {
    g_warning ("Can't find symbol guppi_plug_in in %s", path);
    g_module_close (module);
    return NULL;
  }

  if (plug_in == NULL) {
    g_warning ("In %s, the symbol guppi_plug_in is NULL", path);
    g_module_close (module);
    return NULL;
  }

  pi = ((GuppiPlugIn * (*)(void)) plug_in) ();
  if (pi == NULL) {
    g_warning ("In %s, guppi_plug_in() returned NULL", path);
    g_module_close (module);
    return NULL;
  }

  if (pi->magic_number != GUPPI_PLUG_IN_MAGIC_NUMBER) {
    g_warning
      ("In %s, guppi_plug_in() returned a structure with a bad magic number.",
       path);
    g_module_close (module);
    return NULL;
  }

  pi->load_path = guppi_strdup (path);
  pi->reserved = module;

  return pi;
}

const gchar *
guppi_plug_in_type (GuppiPlugIn *pi)
{
  g_return_val_if_fail (pi != NULL, NULL);

  if (pi->spec == NULL)
    return NULL;

  return ((GuppiPlugInSpec *) pi->spec)->type;
}

const gchar *
guppi_plug_in_code (GuppiPlugIn *pi)
{
  g_return_val_if_fail (pi != NULL, NULL);
  
  if (pi->spec == NULL)
    return NULL;

  return ((GuppiPlugInSpec *) pi->spec)->code;
}

/* $Id$ */
