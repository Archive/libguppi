/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-string.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_STRING_H
#define _INC_GUPPI_STRING_H

#include <glib.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

gboolean guppi_string_is_boolean (const gchar *);
gboolean guppi_string2boolean (const gchar *);
void guppi_boolean2string (gboolean, gchar *, gsize);

gboolean guppi_string_is_int (const gchar *);
gboolean guppi_string_is_number (const gchar *);

/* Number of strange characters in a block of memory: useful for
   trying to guess if it is actually part of some sort of binary file. */
gsize guppi_string_noise_count (const gchar *, gsize);


/* Copy the string, replacing characters that shouldn't be in filenames. */
gchar *guppi_string_canonize_filename (const gchar *str);


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_STRING_H */

/* $Id$ */
