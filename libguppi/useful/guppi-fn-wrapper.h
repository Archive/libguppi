/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-fn-wrapper.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_FN_WRAPPER_H
#define _INC_GUPPI_FN_WRAPPER_H

#include <glib-object.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

enum {
  GUPPI_FN_D__I,
  GUPPI_FN_D__I_D,
  GUPPI_FN_D__D,
  GUPPI_FN_D__D_D
};


typedef double (*GuppiFn_d__i) (gint, gpointer);
typedef double (*GuppiFn_d__i_d) (gint, double, gpointer);
typedef double (*GuppiFn_d__d) (double, gpointer);
typedef double (*GuppiFn_d__d_d) (double, double, gpointer);


typedef struct _GuppiFnWrapper GuppiFnWrapper;
typedef struct _GuppiFnWrapperClass GuppiFnWrapperClass;

struct _GuppiFnWrapper {
  GObject parent;

  gint type;
  gpointer function;
  gpointer user_data;
};

struct _GuppiFnWrapperClass {
  GObjectClass parent_class;
};

#define GUPPI_TYPE_FN_WRAPPER            (guppi_fn_wrapper_get_type())
#define GUPPI_FN_WRAPPER(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_FN_WRAPPER, GuppiFnWrapper))
#define GUPPI_FN_WRAPPER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_FN_WRAPPER, GuppiFnWrapperClass))
#define GUPPI_IS_FN_WRAPPER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_FN_WRAPPER))
#define GUPPI_IS_FN_WRAPPER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_FN_WRAPPER))

GType guppi_fn_wrapper_get_type (void);

GuppiFnWrapper *guppi_fn_wrapper_new_d__i (GuppiFn_d__i, gpointer);
GuppiFnWrapper *guppi_fn_wrapper_new_d__i_d (GuppiFn_d__i_d, gpointer);
GuppiFnWrapper *guppi_fn_wrapper_new_d__d (GuppiFn_d__d, gpointer);
GuppiFnWrapper *guppi_fn_wrapper_new_d__d_d (GuppiFn_d__d_d, gpointer);

void guppi_fn_wrapper_set_user_data (GuppiFnWrapper *, gpointer);

double guppi_fn_wrapper_eval_d__i (GuppiFnWrapper *, gint);
double guppi_fn_wrapper_eval_d__i_d (GuppiFnWrapper *, gint, double);
double guppi_fn_wrapper_eval_d__d (GuppiFnWrapper *, double);
double guppi_fn_wrapper_eval_d__d_d (GuppiFnWrapper *, double, double);

void guppi_fn_wrapper_eval_d__d_bulk (GuppiFnWrapper *, double *, gsize);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_FN_WRAPPER_H */

/* $Id$ */
