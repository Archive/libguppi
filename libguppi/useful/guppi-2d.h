/* $Id$ */

/*
 * guppi-2d.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_2D_H
#define _INC_GUPPI_2D_H

/* #include <gnome.h> */
#include <glib.h>
#include <libart_lgpl/art_vpath.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS

gboolean guppi_2d_line_segment_intersection (double pt1_x, double pt1_y,
					     double pt2_x, double pt2_y,
					     double pt3_x, double pt3_y,
					     double pt4_x, double pt4_y);

/* Does the line segment intersect the window? */
gboolean guppi_2d_line_segment_window_query (double seg_x0, double seg_y0,
					     double seg_x1, double seg_y1,
					     double x0, double y0,
					     double x1, double y1);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_2D_H */

/* $Id$ */
