/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plug-in-spec.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLUG_IN_SPEC_H
#define _INC_GUPPI_PLUG_IN_SPEC_H

#include <glib.h>
#include <libguppi/useful/guppi-plug-in.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPlugInSpec GuppiPlugInSpec;
struct _GuppiPlugInSpec {
  gchar *path;

  gchar *code;
  gchar *type;

  gchar *name;
  gchar *comment;

  gint major_version, minor_version, micro_version;

  gchar **copyright_vec;
  gchar **author_vec;
  gchar **keyword_vec;

  gchar **depends_vec;
  gchar **provides_vec;

  gchar **exported_symbol_vec;

  gchar *preloaded_scm_filename;
  gchar *preloaded_py_filename;
  gchar *so_filename;
  gchar *icon;

  GuppiPlugIn *plug_in;
};

typedef void (*GuppiPlugInSpecFn) (GuppiPlugInSpec *, gpointer);

GuppiPlugInSpec *guppi_plug_in_spec_new (const gchar *spec_file);
void             guppi_plug_in_spec_free (GuppiPlugInSpec *);

GuppiPlugIn *guppi_plug_in_spec_plug_in (GuppiPlugInSpec *);

GuppiPlugInSpec *guppi_plug_in_spec_lookup (const gchar *type,
					    const gchar *code);

gboolean guppi_plug_in_exists (const gchar *type, const gchar *code);
gboolean guppi_plug_in_is_loaded (const gchar *type, const gchar *code);
void guppi_plug_in_force_load (const gchar *type, const gchar *code);

GuppiPlugIn *guppi_plug_in_lookup (const gchar *type, const gchar *code);

void guppi_plug_in_path_set (const gchar *path);
void guppi_plug_in_path_prepend (const gchar *path);
void guppi_plug_in_path_append (const gchar *path);
void guppi_plug_in_path_reset_to_default (void);

void guppi_plug_in_path_dump (void);

void guppi_plug_in_spec_find (const gchar *path, gboolean recursive);
void guppi_plug_in_spec_find_all (void);

/* for backward compatibility, equiv to guppi_plug_in_spec_find_all() */
void guppi_plug_in_load_all (void);

void guppi_plug_in_spec_load_by_type (const gchar *type);


void guppi_plug_in_spec_foreach (GuppiPlugInSpecFn, gpointer);
void guppi_plug_in_spec_foreach_of_type (const gchar *type,
					 GuppiPlugInSpecFn, gpointer);

gint guppi_plug_in_count (void);
gint guppi_plug_in_count_by_type (const gchar *type);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_PLUG_IN_SPEC_H */

/* $Id$ */
