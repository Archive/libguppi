/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-metric-entry.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_METRIC_ENTRY_H
#define _INC_GUPPI_METRIC_ENTRY_H

/* #include <gtk/gtk.h> */
#include <gtk/gtkhbox.h>
#include <libguppi/useful/guppi-metrics.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiMetricEntry GuppiMetricEntry;
typedef struct _GuppiMetricEntryClass GuppiMetricEntryClass;

struct _GuppiMetricEntry {
  GtkHBox parent;

  guppi_metric_t displayed_units;
  double pt_val;

  GtkWidget *entry;
  GtkWidget *units_opt;

  gboolean no_negatives;
  gboolean no_zero;
};

struct _GuppiMetricEntryClass {
  GtkHBoxClass parent_class;

  void (*changed_value) (GuppiMetricEntry *);
  void (*changed_units) (GuppiMetricEntry *);
};

#define GUPPI_TYPE_METRIC_ENTRY (guppi_metric_entry_get_type())
#define GUPPI_METRIC_ENTRY(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_METRIC_ENTRY,GuppiMetricEntry))
#define GUPPI_METRIC_ENTRY0(obj) ((obj) ? (GUPPI_METRIC_ENTRY(obj)) : NULL)
#define GUPPI_METRIC_ENTRY_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_METRIC_ENTRY,GuppiMetricEntryClass))
#define GUPPI_IS_METRIC_ENTRY(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_METRIC_ENTRY))
#define GUPPI_IS_METRIC_ENTRY0(obj) (((obj) == NULL) || (GUPPI_IS_METRIC_ENTRY(obj)))
#define GUPPI_IS_METRIC_ENTRY_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_METRIC_ENTRY))

GType guppi_metric_entry_get_type (void);

void guppi_metric_entry_construct (GuppiMetricEntry *);
GtkWidget *guppi_metric_entry_new (void);

double guppi_metric_entry_pt_value (GuppiMetricEntry *);
void guppi_metric_entry_set_pt_value (GuppiMetricEntry *, double);

guppi_metric_t guppi_metric_entry_displayed_units (GuppiMetricEntry *);
void guppi_metric_entry_set_displayed_units (GuppiMetricEntry *,
					     guppi_metric_t);



END_GUPPI_DECLS;

#endif /* _INC_GUPPI_METRIC_ENTRY_H */

/* $Id$ */
