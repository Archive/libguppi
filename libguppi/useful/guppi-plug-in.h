/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plug-in.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLUG_IN_H
#define _INC_GUPPI_PLUG_IN_H

#include <glib.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPlugIn GuppiPlugIn;

struct _GuppiPlugIn {
  guint32 magic_number;
  gchar *load_path;
  gpointer reserved;
  gpointer spec;

  void (*finalize) (GuppiPlugIn *);
};

void guppi_plug_in_construct (GuppiPlugIn *);
void guppi_plug_in_free (GuppiPlugIn *);

GuppiPlugIn *guppi_plug_in_load (const gchar * path);

const gchar *guppi_plug_in_type (GuppiPlugIn *);
const gchar *guppi_plug_in_code (GuppiPlugIn *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_PLUG_IN_H */

/* $Id$ */
