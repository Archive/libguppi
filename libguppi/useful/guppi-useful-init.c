/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-useful-init.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/useful/guppi-useful-init.h>

#include <stdlib.h>

#include <libguppi/useful/gnan.h>
#include <libguppi/useful/guppi-dharma.h>
#include <libguppi/useful/guppi-paths.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-plug-in-spec.h>

void
guppi_useful_init (void)
{
  const gchar *verb;

  gnan_init ();

  guppi_dharma_init ();
  guppi_dharma_turn_wheel_automatically ();

  guppi_paths_init ();

  guppi_plug_in_path_reset_to_default ();

  if ((verb = getenv ("GUPPI_VERBOSITY"))) {
    gint verbosity = GUPPI_NORMAL_VERBOSITY;

    if (! g_ascii_strcasecmp ("silent", verb))
      verbosity = GUPPI_SILENT;
    else if (! g_ascii_strcasecmp ("verbose", verb))
      verbosity = GUPPI_VERBOSE;
    else if (! g_ascii_strcasecmp ("veryverbose", verb)) 
      verbosity = GUPPI_VERY_VERBOSE;

    guppi_set_verbosity (verbosity);
  }
}
