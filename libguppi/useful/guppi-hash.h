/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-hash.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_HASH_H__
#define __GUPPI_HASH_H__

#include <glib.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiHash GuppiHash;
struct _GuppiHash;

GuppiHash *guppi_hash_new     (void);
GuppiHash *guppi_hash_copy    (GuppiHash *);
void       guppi_hash_free    (GuppiHash *);
void       guppi_hash_reset   (GuppiHash *);

void       guppi_hash_raw     (GuppiHash *, gconstpointer ptr, gsize N);
void       guppi_hash_string  (GuppiHash *, const gchar *str);
void       guppi_hash_double  (GuppiHash *, double x);
void       guppi_hash_int     (GuppiHash *, gint x);
void       guppi_hash_uint    (GuppiHash *, guint x);
void       guppi_hash_boolean (GuppiHash *, gboolean x);

gint       guppi_hash_cmp     (GuppiHash *, GuppiHash *);
gboolean   guppi_hash_eq      (GuppiHash *, GuppiHash *);
gboolean   guppi_hash_neq     (GuppiHash *, GuppiHash *);

guint      guppi_hash_to_uint (GuppiHash *);


END_GUPPI_DECLS;

#endif /* __GUPPI_HASH_H__ */

