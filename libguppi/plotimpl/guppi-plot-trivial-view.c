/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-plot-trivial-view.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-plot-trivial-view.h>

#include <libguppi/plotimpl/guppi-plot-trivial-state.h>

static GObjectClass *parent_class;

static void
guppi_plot_trivial_view_render (GuppiElementView *view,
				ArtDRect         *bbox,
				GuppiCanvas      *canvas)
{
  GuppiPlotTrivialState *state = GUPPI_PLOT_TRIVIAL_STATE (guppi_element_view_state (view));
  
  guppi_canvas_set_color_rgba (canvas, state->bg_color);
  guppi_canvas_draw_rectangle (canvas, bbox, TRUE, FALSE);

  if (GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view))->render)
    GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view))->render (view, bbox, canvas);
}

static void
guppi_plot_trivial_view_class_init (GuppiPlotTrivialViewClass *klass)
{
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  view_class->render = guppi_plot_trivial_view_render;
}

static void
guppi_plot_trivial_view_init (GuppiPlotTrivialView *view)
{
  
}

GType
guppi_plot_trivial_view_get_type (void)
{
  static GType type = 0;

  if (! type) {

    static const GTypeInfo type_info = {
      sizeof (GuppiPlotTrivialViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_plot_trivial_view_class_init,
      NULL, NULL,
      sizeof (GuppiPlotTrivialView),
      0,
      (GInstanceInitFunc) guppi_plot_trivial_view_init
    };

    type = g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW,
				   "GuppiPlotTrivialView",
				   &type_info,
				   0);

  }

  return type;
}
