/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-plot-trivial-state.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_PLOT_TRIVIAL_STATE_H__
#define __GUPPI_PLOT_TRIVIAL_STATE_H__

#include <libguppi/plot/guppi-element-state.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPlotTrivialState GuppiPlotTrivialState;
typedef struct _GuppiPlotTrivialStateClass GuppiPlotTrivialStateClass;

struct _GuppiPlotTrivialState {
  GuppiElementState parent;
  
  guint bg_color;
};

struct _GuppiPlotTrivialStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_PLOT_TRIVIAL_STATE (guppi_plot_trivial_state_get_type())
#define GUPPI_PLOT_TRIVIAL_STATE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_PLOT_TRIVIAL_STATE,GuppiPlotTrivialState))
#define GUPPI_PLOT_TRIVIAL_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_TRIVIAL_STATE,GuppiPlotTrivialStateClass))
#define GUPPI_IS_PLOT_TRIVIAL_STATE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_PLOT_TRIVIAL_STATE))
#define GUPPI_IS_PLOT_TRIVIAL_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_TRIVIAL_STATE))

GType guppi_plot_trivial_state_get_type (void);

END_GUPPI_DECLS;

#endif /* __GUPPI_PLOT_TRIVIAL_STATE_H__ */

