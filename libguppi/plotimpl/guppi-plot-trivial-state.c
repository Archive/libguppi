/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-plot-trivial-state.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-plot-trivial-state.h>

#include <libguppi/plotimpl/guppi-plot-trivial-view.h>

static GObjectClass *parent_class;

static void
guppi_plot_trivial_state_class_init (GuppiPlotTrivialStateClass *klass)
{
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  state_class->view_type = GUPPI_TYPE_PLOT_TRIVIAL_VIEW;
}

static void
guppi_plot_trivial_state_init (GuppiPlotTrivialState *state)
{
  
}

GType
guppi_plot_trivial_state_get_type (void)
{
  static GType type = 0;

  if (! type) {

    static const GTypeInfo type_info = {
      sizeof (GuppiPlotTrivialStateClass),
      NULL, NULL,
      (GClassInitFunc) guppi_plot_trivial_state_class_init,
      NULL, NULL,
      sizeof (GuppiPlotTrivialState),
      0,
      (GInstanceInitFunc) guppi_plot_trivial_state_init
    };

    type = g_type_register_static (GUPPI_TYPE_ELEMENT_STATE,
				   "GuppiPlotTrivialState",
				   &type_info,
				   0);

  }

  return type;
}
