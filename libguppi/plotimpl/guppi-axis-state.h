/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_AXIS_STATE_H
#define _INC_GUPPI_AXIS_STATE_H

#include <pango/pango.h>

#include <libguppi/useful/guppi-defs.h>
#include <libguppi/useful/guppi-enums.h>
#include <libguppi/canvas/guppi-canvas.h>
#include <libguppi/plot/guppi-element-state.h>
#include <libguppi/plot/guppi-axis-markers.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiAxisState GuppiAxisState;
typedef struct _GuppiAxisStateClass GuppiAxisStateClass;

struct _GuppiAxisState {
  GuppiElementState parent;

  PangoLayout *scratch_layout;
};

struct _GuppiAxisStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_AXIS_STATE (guppi_axis_state_get_type())
#define GUPPI_AXIS_STATE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_AXIS_STATE,GuppiAxisState))
#define GUPPI_AXIS_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_AXIS_STATE,GuppiAxisStateClass))
#define GUPPI_IS_AXIS_STATE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_AXIS_STATE))
#define GUPPI_IS_AXIS_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_AXIS_STATE))

GType guppi_axis_state_get_type (void);

GuppiElementState *guppi_axis_state_new (void);

void guppi_axis_state_tick_properties (GuppiAxisState        *axis_state,
				       const GuppiTick       *tick,
				       gboolean              *show_tick,
				       guint32               *color,
				       double                *thickness,
				       double                *length,
				       gboolean              *show_label,
				       double                *label_offset,
				       guint32               *label_color,
				       PangoFontDescription **label_font);

gchar *guppi_axis_state_get_legend (GuppiAxisState *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_AXIS_STATE_H */

/* $Id$ */
