/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-scatter-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-scatter-view.h>

#include <math.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/plotimpl/guppi-scatter-state.h>

static GObjectClass *parent_class = NULL;

static void
guppi_scatter_view_finalize (GObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
changed (GuppiElementView *gev)
{
  GuppiScatterView *view;
  GuppiElementState *state;
  GuppiSeqScalar *x_data = NULL;
  GuppiSeqScalar *y_data = NULL;
  guppi_uniq_t xuid, yuid;

  view = GUPPI_SCATTER_VIEW (gev);
  state = guppi_element_view_state (gev);

  guppi_element_state_get (state, "x_data", &x_data, "y_data", &y_data, NULL);

  xuid = x_data ? guppi_data_unique_id (GUPPI_DATA (x_data)) : 0;
  yuid = y_data ? guppi_data_unique_id (GUPPI_DATA (y_data)) : 0;

  if (! guppi_uniq_eq (view->last_x, xuid)) {
    guppi_element_view_cartesian_set_preferred_view ((GuppiElementViewCartesian *)gev, GUPPI_X_AXIS);
    view->last_x = xuid;
  }

  if (! guppi_uniq_eq (view->last_y, yuid)) {
    guppi_element_view_cartesian_set_preferred_view ((GuppiElementViewCartesian *)gev, GUPPI_Y_AXIS);
    view->last_y = yuid;
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (gev);
}

static void
view_init (GuppiElementView *gev)
{
  GuppiElementViewCartesian *cart = GUPPI_ELEMENT_VIEW_CARTESIAN (gev);
  GuppiElementState *state = guppi_element_view_state (gev);
  GuppiScatterView *view = GUPPI_SCATTER_VIEW (gev);
  GuppiSeqScalar *x_data = NULL;
  GuppiSeqScalar *y_data = NULL;

  guppi_element_state_get (state, "x_data", &x_data, "y_data", &y_data, NULL);

  view->last_x = x_data ? guppi_data_unique_id (GUPPI_DATA (x_data)) : 0;
  view->last_y = y_data ? guppi_data_unique_id (GUPPI_DATA (y_data)) : 0;

  guppi_element_view_cartesian_add_view_interval (cart, GUPPI_X_AXIS);
  guppi_element_view_cartesian_add_view_interval (cart, GUPPI_Y_AXIS);
 
  guppi_element_view_cartesian_set_axis_marker_type (cart, GUPPI_X_AXIS, GUPPI_AXIS_SCALAR);
  guppi_element_view_cartesian_set_axis_marker_type (cart, GUPPI_Y_AXIS, GUPPI_AXIS_SCALAR);
}

static gboolean
valid_range (GuppiViewInterval *vi, GuppiSeqScalar *data, double *a, double *b)
{
  gint i, i0, i1;
  double min, max, w;
  gboolean first_min = TRUE, first_max = TRUE;

  min = guppi_seq_scalar_min (data);
  max = guppi_seq_scalar_max (data);
  
  /* Automatically do the "smart" range-finding when we have a reasonable number of points. */
  if (!(guppi_view_interval_valid (vi, min) && guppi_view_interval_valid (vi, max))) {

    /* FIXME: We should take our visibility mask into account here. */

    for (i = i0; i <= i1; ++i) {
      double x = guppi_seq_scalar_get (data, i);

      guppi_seq_bounds (GUPPI_SEQ (data), &i0, &i1);
      
      if (guppi_view_interval_valid (vi, x)) {

	if (first_min) {
	  min = x;
	  first_min = FALSE;
	} else {
	  if (x < min)
	    min = x;
	}

	if (first_max) {
	  max = x;
	  first_max = FALSE;
	} else {
	  if (x > max)
	    max = x;
	}

      }
    }

    if (first_min || first_max)
      return FALSE;
  }

  /* Add 5% in 'margins' */
  w = max - min;
  min -= w * 0.025;
  max += w * 0.025;

  if (a) 
    *a = min;
  if (b)
    *b = max;

  return TRUE;
}

static gboolean
preferred_range (GuppiElementViewCartesian *cart, guppi_axis_t ax, double *a, double *b)
{
  GuppiElementState *state = guppi_element_view_state ((GuppiElementView *) cart);
  GuppiSeqScalar *data = NULL;

  if (ax == GUPPI_X_AXIS)
    guppi_element_state_get (state, "x_data", &data, NULL);
  else if (ax == GUPPI_Y_AXIS)
    guppi_element_state_get (state, "y_data", &data, NULL);
  else
    return FALSE;

  if (data)
    return valid_range (guppi_element_view_cartesian_get_view_interval (cart, ax), data, a, b);

  return FALSE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
render (GuppiElementView *view,
	ArtDRect         *render_bbox,
	GuppiCanvas      *canvas)
{
  GuppiElementState *state = guppi_element_view_state (view);
  GuppiSeqScalar *x_data = NULL, *y_data = NULL;
  GuppiViewInterval *vi_x, *vi_y;
  int i, i0, i1, N;
  ArtPoint *pos;

  guppi_element_state_get (state,
			   "x_data", &x_data,
			   "y_data", &y_data,
			   NULL);

  if (x_data == NULL || y_data == NULL)
    return;

  vi_x = guppi_element_view_cartesian_get_view_interval (GUPPI_ELEMENT_VIEW_CARTESIAN (view),
							 GUPPI_X_AXIS);

  vi_y = guppi_element_view_cartesian_get_view_interval (GUPPI_ELEMENT_VIEW_CARTESIAN (view),
							 GUPPI_Y_AXIS);

  guppi_seq_common_bounds (GUPPI_SEQ (x_data), GUPPI_SEQ (y_data), &i0, &i1);
  N = i1 - i0 + 1;

  pos = guppi_new (ArtPoint, N);

  for (i = i0; i <= i1; ++i) {
    double x, y;

    x = guppi_seq_scalar_get (x_data, i);
    y = guppi_seq_scalar_get (y_data, i);

    pos[i-i0].x = guppi_view_interval_conv_fn (vi_x, x);
    pos[i-i0].y = guppi_view_interval_conv_fn (vi_y, y);
  }

  guppi_element_view_conv_bulk (view, pos, pos, N);

  guppi_canvas_draw_markers (canvas, GUPPI_MARKER_CIRCLE, N, pos,
			     1, NULL, NULL, NULL);

  guppi_free (pos);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_scatter_view_class_init (GuppiScatterViewClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);
  GuppiElementViewCartesianClass *cart_class = GUPPI_ELEMENT_VIEW_CARTESIAN_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_scatter_view_finalize;

  view_class->changed   = changed;
  view_class->view_init = view_init;
  view_class->render    = render;

  cart_class->preferred_range  = preferred_range;
}

static void
guppi_scatter_view_init (GuppiScatterView * obj)
{

}

GType
guppi_scatter_view_get_type (void)
{
  static GType guppi_scatter_view_type = 0;
  if (!guppi_scatter_view_type) {
    static const GTypeInfo guppi_scatter_view_info = {
      sizeof (GuppiScatterViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_scatter_view_class_init,
      NULL, NULL,
      sizeof (GuppiScatterView),
      0,
      (GInstanceInitFunc) guppi_scatter_view_init
    };

    guppi_scatter_view_type = g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN,
						      "GuppiScatterView",
						      &guppi_scatter_view_info, 0);
  }
  return guppi_scatter_view_type;
}

/***************************************************************************/

/* $Id$ */
