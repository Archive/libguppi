/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-plotimpl-init.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-plotimpl-init.h>

#include <glib.h>

#include <libguppi/plotimpl/guppi-axis-state.h>
#include <libguppi/plotimpl/guppi-scatter-state.h>
#include <libguppi/plotimpl/guppi-text-state.h>

void
guppi_plotimpl_init (void)
{
  static gboolean inited = FALSE;

  if (inited)
    return;
  inited = TRUE;

  guppi_element_state_register ("axis",    GUPPI_TYPE_AXIS_STATE);
  guppi_element_state_register ("scatter", GUPPI_TYPE_SCATTER_STATE);
  guppi_element_state_register ("text",    GUPPI_TYPE_TEXT_STATE);
}
