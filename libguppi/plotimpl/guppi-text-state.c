/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-text-state.h>

#include <string.h>

#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-defaults.h>
#include <libguppi/plotimpl/guppi-text-view.h>

static GObjectClass *parent_class = NULL;

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_text_state_class_init (GuppiTextStateClass * klass)
{
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  state_class->view_type = GUPPI_TYPE_TEXT_VIEW;
}

#if 0
static void
bag_changed_cb (GuppiAttributeBag *bag, const gchar *key, gpointer closure)
{
  GuppiTextState *state = GUPPI_TEXT_STATE (closure);

  if (!strcmp (key, "text")) {
    /* This forces our changed_size emission to happen now, rather than during
       the update -- which causes us to render twice: once when the text
       has changed but the item hasn't resized, and then once when the item
       is the right size.  This is jerky and ugly... */
    guppi_text_state_get_block (state);
  }
}
#endif

static void
guppi_text_state_init (GuppiTextState *obj)
{
  GuppiAttributeBag *bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_STRING,    "text",          NULL, "");
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "font::adopt",   NULL, guppi_default_font ());
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "color",         NULL, RGBA_BLACK);

#if 0  
  g_signal_connect (bag,
		    "changed",
		    (GCallback) bag_changed_cb,
		    obj);
#endif
}

GType 
guppi_text_state_get_type (void)
{
  static GType guppi_text_state_type = 0;
  
  if (!guppi_text_state_type) {
  
    static const GTypeInfo guppi_text_state_info = {
      sizeof (GuppiTextStateClass),
      NULL, NULL,
      (GClassInitFunc) guppi_text_state_class_init,
      NULL, NULL,
      sizeof (GuppiTextState),
      0,
      (GInstanceInitFunc) guppi_text_state_init
    };
    
    guppi_text_state_type = g_type_register_static (GUPPI_TYPE_ELEMENT_STATE,
						    "GupiTextState",
						    &guppi_text_state_info, 0);
  }
  
  return guppi_text_state_type;
}

GuppiElementState *
guppi_text_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_object_new (guppi_text_state_get_type ()));
}

/* $Id$ */

