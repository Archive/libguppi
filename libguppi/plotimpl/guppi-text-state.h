/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TEXT_STATE_H
#define _INC_GUPPI_TEXT_STATE_H

#include <libguppi/plot/guppi-element-state.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiTextState GuppiTextState;
typedef struct _GuppiTextStateClass GuppiTextStateClass;

struct _GuppiTextState {
  GuppiElementState parent;
};

struct _GuppiTextStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_TEXT_STATE (guppi_text_state_get_type())
#define GUPPI_TEXT_STATE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_TEXT_STATE,GuppiTextState))
#define GUPPI_TEXT_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_TEXT_STATE,GuppiTextStateClass))
#define GUPPI_IS_TEXT_STATE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_TEXT_STATE))
#define GUPPI_IS_TEXT_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_TEXT_STATE))

GType guppi_text_state_get_type (void);

GuppiElementState *guppi_text_state_new (void);

#endif /* _INC_GUPPI_TEXT_STATE_H */

END_GUPPI_DECLS;

/* $Id$ */
