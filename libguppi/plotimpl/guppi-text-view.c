/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-text-view.h>

#include <libguppi/useful/guppi-pango-helper.h>

static GObjectClass *parent_class = NULL;

static void
compute_text_size_request (GuppiTextView *text_view)
{
  GuppiElementState *state = guppi_element_view_state ((GuppiElementView *) text_view);
  char *text;
  PangoFontDescription *font;
  double width = 0, height = 0;
  
  guppi_element_state_get (state,
			   "text", &text,
			   "font", &font,
			   NULL);

  if (text && *text && font) {
    guppi_pango_get_string_size (text, font, &width, &height);
  }

  guppi_element_view_set_request ((GuppiElementView *) text_view, width, height);
}

static void
changed (GuppiElementView *view)
{
  compute_text_size_request ((GuppiTextView *) view);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (view);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
render (GuppiElementView *view,
	ArtDRect         *render_bbox,
	GuppiCanvas      *canvas)
{
  GuppiElementState *state;
  char *text;
  PangoFontDescription *font;
  guint32 color;
  ArtPoint pt;

  state = guppi_element_view_state (view);

  guppi_element_state_get (state,
			   "text", &text,
			   "font", &font,
			   "color", &color,
			   NULL);

  pt.x = pt.y = 0.5;

  guppi_element_view_conv (view, &pt, &pt);

  guppi_canvas_set_color_rgba (canvas, color);
  guppi_canvas_set_font (canvas, font);
  guppi_canvas_draw_string (canvas,
			    &pt,
			    GUPPI_CANVAS_ANCHOR_CENTER,
			    GUPPI_CANVAS_ROT_0,
			    text);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
view_init (GuppiElementView *view)
{
  compute_text_size_request ((GuppiTextView *) view);
}

static void
guppi_text_view_class_init (GuppiTextViewClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  view_class->changed   = changed;
  view_class->view_init = view_init;
  view_class->render    = render;
}

static void
guppi_text_view_init (GuppiTextView *obj)
{

}

GType
guppi_text_view_get_type (void)
{
  static GType guppi_text_view_type = 0;

  if (!guppi_text_view_type) {
    
    static const GTypeInfo guppi_text_view_info = {
      sizeof (GuppiTextViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_text_view_class_init,
      NULL, NULL,
      sizeof (GuppiTextView),
      0,
      (GInstanceInitFunc) guppi_text_view_init
    };
    
    guppi_text_view_type = g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW,
						   "GuppiTextView",
						   &guppi_text_view_info, 0);
  }
  
  return guppi_text_view_type;
}


/* $Id$ */
