/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-axis-state.h>

#include <math.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-defaults.h>
#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/useful/guppi-pango-helper.h>

#include <libguppi/data/guppi-data-flavor.h>
#include <libguppi/data/guppi-data-socket.h>
#include <libguppi/data/guppi-seq-scalar.h>

#include <libguppi/plotimpl/guppi-axis-view.h>

static GObjectClass *parent_class = NULL;

static void
guppi_axis_state_finalize (GObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_axis_state_class_init (GuppiAxisStateClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_axis_state_finalize;

  state_class->view_type = GUPPI_TYPE_AXIS_VIEW;
}

static void
guppi_axis_state_init (GuppiAxisState *obj)
{
  GuppiAttributeBag *bag;
  double inch = guppi_in2pt (1.0);

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "data::socket::adopt", NULL, guppi_data_socket_new_by_type (GUPPI_TYPE_SEQ_SCALAR));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT,       "position",       NULL, GUPPI_NORTH);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_edge",      NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "edge_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "edge_thickness", NULL, inch / 64);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_legend",    NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_STRING,    "legend",         NULL, "");
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "legend_color",   NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "legend_font",    NULL, guppi_default_font ());
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "legend_offset",  NULL, inch / 16);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "label_offset",         NULL, inch / 64);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "rotate_labels",        NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "shrink_labels_to_fit", NULL, TRUE);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_lone_labels",        NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "extra_lone_label_offset", NULL, inch / 16);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "lone_label_color",        NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "lone_label_font",         NULL, guppi_default_font ());

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_major_ticks",     NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "major_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "major_tick_thickness", NULL, inch / 64);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "major_tick_length",    NULL, inch / 12);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_major_labels",    NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "major_label_color",    NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "major_label_font",     NULL, guppi_default_font ());

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_minor_ticks",     NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "minor_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "minor_tick_thickness", NULL, inch / 96);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "minor_tick_length",    NULL, inch / 16);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_minor_labels",    NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "minor_label_color",    NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "minor_label_font",     NULL, guppi_default_font ());

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_micro_ticks",     NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "micro_tick_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "micro_tick_thickness", NULL, inch / 128);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "micro_tick_length",    NULL, inch / 24);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "show_micro_labels",    NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "micro_label_color",    NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_FONT,      "micro_label_font",     NULL, guppi_default_font ());

}

GType
guppi_axis_state_get_type (void)
{
  static GType guppi_axis_state_type = 0;
  if (!guppi_axis_state_type) {
    static const GTypeInfo guppi_axis_state_info = {
      sizeof (GuppiAxisStateClass),
      NULL, NULL,
      (GClassInitFunc) guppi_axis_state_class_init,
      NULL, NULL,
      sizeof (GuppiAxisState),
      0,
      (GInstanceInitFunc) guppi_axis_state_init
    };
    guppi_axis_state_type = g_type_register_static (GUPPI_TYPE_ELEMENT_STATE,
						    "GuppiAxisState",
						    &guppi_axis_state_info, 0);
  }
  return guppi_axis_state_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiElementState *
guppi_axis_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_object_new (guppi_axis_state_get_type ()));
}

void
guppi_axis_state_tick_properties (GuppiAxisState *state,
				  const GuppiTick *tick,
				  gboolean *show_tick,
				  guint32 *color,
				  double *thickness,
				  double *length,
				  gboolean *show_label,
				  double *label_offset,
				  guint32 *label_color,
				  PangoFontDescription **label_font)
{
  g_return_if_fail (GUPPI_IS_AXIS_STATE (state));

  if (show_tick)
    *show_tick = FALSE;

  if (show_label)
    *show_label = FALSE;

  g_return_if_fail (state != NULL);
  g_return_if_fail (GUPPI_IS_AXIS_STATE (state));

  g_return_if_fail (tick != NULL);

  if (label_offset)
    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "label_offset", label_offset,
			     NULL);

  switch (guppi_tick_type (tick)) {

  case GUPPI_TICK_NONE:

    if (show_tick)
      *show_tick = FALSE;
    if (color)
      *color = 0;
    if (thickness)
      *thickness = 0;
    if (length)
      *length = 0;
    
    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "show_lone_labels",        show_label,
			     "extra_lone_label_offset", label_offset,
			     "lone_label_color",        label_color,
			     "lone_label_font",         label_font,
			     NULL);
    break;

  case GUPPI_TICK_MAJOR:
  case GUPPI_TICK_MAJOR_RULE:

    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "show_major_ticks",     show_tick,
			     "major_tick_color",     color,
			     "major_tick_thickness", thickness,
			     "major_tick_length",    length,
			     "show_major_labels",    show_label,
			     "major_label_color",    label_color,
			     "major_label_font",     label_font,
			     NULL);
    break;

  case GUPPI_TICK_MINOR:
  case GUPPI_TICK_MINOR_RULE:

    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "show_minor_ticks",     show_tick,
			     "minor_tick_color",     color,
			     "minor_tick_thickness", thickness,
			     "minor_tick_length",    length,
			     "show_minor_labels",    show_label,
			     "minor_label_color",    label_color,
			     "minor_label_font",     label_font,
			     NULL);
    break;

  case GUPPI_TICK_MICRO:
  case GUPPI_TICK_MICRO_RULE:

    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "show_micro_ticks",     show_tick,
			     "micro_tick_color",     color,
			     "micro_tick_thickness", thickness,
			     "micro_tick_length",    length,
			     "show_micro_labels",    show_label,
			     "micro_label_color",    label_color,
			     "micro_label_font",     label_font,
			     NULL);
    break;

  default:

    g_assert_not_reached ();

  }
}

gchar *
guppi_axis_state_get_legend (GuppiAxisState *state)
{
  gboolean show_legend;
  gchar *legend = NULL;
  GuppiData *data = NULL;

  g_return_val_if_fail (GUPPI_IS_AXIS_STATE (state), NULL);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "show_legend", &show_legend,
			   "legend",      &legend,
			   "data",        &data,
			   NULL);

  if (! show_legend)
    return NULL;

  /* FIXME: is this strdup correct, or are we leaking something
     somewhere? */

  if (legend == NULL && data != NULL) {
    legend = guppi_strdup (guppi_data_get_label (data));
  }

  guppi_unref (data);

  return legend;
}

/* $Id$ */
