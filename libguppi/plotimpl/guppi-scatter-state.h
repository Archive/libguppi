/* $Id$ */

/*
 * guppi-scatter-state.h
 *
 * Copyright (C) 1999, 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001, 2002 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCATTER_STATE_H
#define _INC_GUPPI_SCATTER_STATE_H

#include <libguppi/useful/guppi-defs.h>
#include <libguppi/plot/guppi-element-state.h>
#include <libguppi/canvas/guppi-marker.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiScatterState GuppiScatterState;
typedef struct _GuppiScatterStateClass GuppiScatterStateClass;

struct _GuppiScatterState {
  GuppiElementState object;
};

struct _GuppiScatterStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_SCATTER_STATE (guppi_scatter_state_get_type())
#define GUPPI_SCATTER_STATE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), \
                                  GUPPI_TYPE_SCATTER_STATE,GuppiScatterState))
#define GUPPI_SCATTER_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), \
                                          GUPPI_TYPE_SCATTER_STATE, \
					  GuppiScatterStateClass))
#define GUPPI_IS_SCATTER_STATE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), \
                                     GUPPI_TYPE_SCATTER_STATE))
#define GUPPI_IS_SCATTER_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), \
                                    GUPPI_TYPE_SCATTER_STATE))

GType guppi_scatter_state_get_type (void);

GuppiElementState *guppi_scatter_state_new (void);

gboolean guppi_scatter_state_get_point_properties (GuppiScatterState *,
						   gint index,
						   gboolean *visible,
						   GuppiMarker *marker,
						   guint32 *color,
						   double *size1,
						   double *size2);

gboolean guppi_scatter_state_closest_point (GuppiScatterState *,
					    double x, double y, double max_r,
					    double x_scale, double y_scale,
					    gint *index);

void guppi_scatter_state_brush_rectangle (GuppiScatterState *,
					  double x0, double y0,
					  double x1, double y1,
					  gboolean hidden);

void guppi_scatter_state_brush_circle (GuppiScatterState *,
				       double x, double y, double r_pixels,
				       double x_scale, double y_scale,
				       gboolean hidden);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_SCATTER-STATE_H */

/* $Id$ */
