/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-axis-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plotimpl/guppi-axis-view.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-pango-helper.h>
#include <libguppi/plot/guppi-axis-markers.h>
#include <libguppi/plotimpl/guppi-axis-state.h>

static GObjectClass *parent_class = NULL;

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static double
compute_axis_shrink_factor (GuppiAxisView *axis_view)
{
  return 1.0;
}

static void
compute_axis_size_request (GuppiAxisView *axis_view)
{
  GuppiElementState *state;
  GuppiAxisMarkers *am;

  guppi_compass_t pos;
  gboolean show_edge, horizontal = TRUE;
  double edge_thickness, legend_offset;
  gchar *legend;
  PangoFontDescription *legend_font;
  double w = 0, h = 0;
  gint i;

  state = guppi_element_view_state ((GuppiElementView *) axis_view);

  guppi_element_state_get (state,
			   "position", &pos,
			   "show_edge", &show_edge,
			   "edge_thickness", &edge_thickness,
			   "legend_font", &legend_font,
			   "legend_offset", &legend_offset,
			   NULL);

  switch (pos) {
  case GUPPI_NORTH:
  case GUPPI_SOUTH:
    horizontal = TRUE;
    break;

  case GUPPI_EAST:
  case GUPPI_WEST:
    horizontal = FALSE;
    break;

  default:
    g_assert_not_reached ();
  }

  legend = guppi_axis_state_get_legend ((GuppiAxisState *) state);

  am = guppi_element_view_cartesian_get_axis_markers ((GuppiElementViewCartesian *) axis_view,
						      GUPPI_META_AXIS);

  /* Account for the size of the axis labels */

  for (i = am ? guppi_axis_markers_size (am) - 1 : -1; i >= 0; --i) {
    
    const GuppiTick *tick;
    gboolean show_tick, show_label;
    double length, label_offset;
    PangoFontDescription *font;
    double tick_w = 0, tick_h = 0;

    tick = guppi_axis_markers_get (am, i);
    
    guppi_axis_state_tick_properties ((GuppiAxisState *) state,
				      tick,
				      &show_tick,
				      NULL, /* don't care about color */
				      NULL, /* don't care about thickness */
				      &length,
				      &show_label,
				      &label_offset,
				      NULL, /* don't care about label color */
				      &font);

    if (show_label && guppi_tick_is_labelled (tick)) {
      guppi_pango_get_string_size (guppi_tick_label (tick),
				   font,
				   &tick_w, &tick_h);

      if (horizontal)
	tick_h += label_offset;
      else
	tick_w += label_offset;

    }

    if (show_tick) {
      if (horizontal)
	tick_h += length;
      else
	tick_w += length;
    }
				    
    if (tick_w > w)
      w = tick_w;

    if (tick_h > h)
      h = tick_h;
  }

#if 0
  /* Account for the edge thickness */

  if (show_edge) {
    if (horizontal)
      h += edge_thickness;
    else
      w += edge_thickness;
  }
#endif

  /* Account for the height of the legend */

  if (legend && *legend) {
    double legend_h;
    guppi_pango_get_string_size (legend, legend_font, NULL, &legend_h);

    if (horizontal) 
      h += legend_h + legend_offset;
    else
      w += legend_h + legend_offset;
  }

  /* Actually make the allocation request */

  if (horizontal)
    w = -1; /* For north or south axes, we don't have a specific width request. */
  else
    h = -1; /* ...and for east/west axes we don't care about height. */

  guppi_element_view_set_request ((GuppiElementView *) axis_view, w, h);

  guppi_free (legend);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
changed (GuppiElementView *view)
{ 
  compute_axis_size_request ((GuppiAxisView *) view);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (view);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
render (GuppiElementView *view,
	ArtDRect         *render_bbox,
	GuppiCanvas      *canvas)
{
  GuppiElementState *state;
  GuppiAxisMarkers *am;
  GuppiViewInterval *vi;
  guppi_compass_t pos;
  gboolean show_edge, horizontal = TRUE;
  double edge_thickness, legend_offset;
  guint32 edge_color;
  gchar *legend;
  PangoFontDescription *legend_font;
  ArtPoint pt1, pt2, pt3;
  gint i;

  state = guppi_element_view_state (view);

  guppi_element_state_get (state,
			   "position", &pos,
			   "show_edge", &show_edge,
			   "edge_thickness", &edge_thickness,
			   "edge_color", &edge_color,
			   "legend_font", &legend_font,
			   "legend_offset", &legend_offset,
			   NULL);
  
  switch (pos) {
  case GUPPI_NORTH:
  case GUPPI_SOUTH:
    horizontal = TRUE;
    break;

  case GUPPI_EAST:
  case GUPPI_WEST:
    horizontal = FALSE;
    break;

  default:
    g_assert_not_reached ();
  }

  vi = guppi_element_view_cartesian_get_view_interval ((GuppiElementViewCartesian *) view,
						       GUPPI_META_AXIS);

  /* Render the edge */

  if (show_edge) {

    switch (pos) {

    case GUPPI_NORTH:
      pt1.x = 0;
      pt1.y = 0;
      pt2.x = 1;
      pt2.y = 0;
      break;

    case GUPPI_SOUTH:
      pt1.x = 0;
      pt1.y = 1;
      pt2.x = 1;
      pt2.y = 1;
      break;

    case GUPPI_EAST:
      pt1.x = 0;
      pt1.y = 0;
      pt2.x = 0;
      pt2.y = 1;
      break;

    case GUPPI_WEST:
      pt1.x = 1;
      pt1.y = 0;
      pt2.x = 1;
      pt2.y = 1;
      break;

    default:
      g_assert_not_reached ();
    }

    guppi_element_view_conv (view, &pt1, &pt1);
    guppi_element_view_conv (view, &pt2, &pt2);

    guppi_canvas_set_line_width (canvas, edge_thickness);
    guppi_canvas_set_color_rgba (canvas, edge_color);
    guppi_canvas_draw_line (canvas, &pt1, &pt2);
  }

  /* Render our markers */

  am = guppi_element_view_cartesian_get_axis_markers ((GuppiElementViewCartesian *) view,
						      GUPPI_META_AXIS);

  for (i = am ? guppi_axis_markers_size (am) - 1 : -1; i >= 0; --i) {

    const GuppiTick *tick;
    gboolean show_tick, show_label;
    guint32 tick_color, label_color;
    double t, length, thickness, label_offset;
    PangoFontDescription *label_font;
    GuppiCanvasAnchor anchor;

    tick = guppi_axis_markers_get (am, i);

    guppi_axis_state_tick_properties ((GuppiAxisState *) state,
				      tick,
				      &show_tick,
				      &tick_color,
				      &thickness,
				      &length,
				      &show_label,
				      &label_offset,
				      &label_color,
				      &label_font);

    t = guppi_tick_position (tick);
    t = guppi_view_interval_conv_fn (vi, t);

    switch (pos) {
    case GUPPI_NORTH:
      pt1.x = t;
      pt1.y = 0;
      guppi_element_view_conv (view, &pt1, &pt1);

      pt2 = pt1;
      pt2.y += length;

      pt3 = pt2;
      pt3.y += label_offset;

      anchor = GUPPI_CANVAS_ANCHOR_SOUTH;
      break;

    case GUPPI_SOUTH:
      pt1.x = t;
      pt1.y = 1;
      guppi_element_view_conv (view, &pt1, &pt1);

      pt2 = pt1;
      pt2.y -= length;

      pt3 = pt2;
      pt3.y -= label_offset;

      anchor = GUPPI_CANVAS_ANCHOR_NORTH;
      break;

    case GUPPI_EAST:
      pt1.x = 0;
      pt1.y = t;
      guppi_element_view_conv (view, &pt1, &pt1);

      pt2 = pt1;
      pt2.x += length;
      
      pt3 = pt2;
      pt3.y += label_offset;

      anchor = GUPPI_CANVAS_ANCHOR_WEST;
      break;
      
    case GUPPI_WEST:
      pt1.x = 1;
      pt1.y = t;
      guppi_element_view_conv (view, &pt1, &pt1);

      pt2 = pt1;
      pt2.x -= length;

      pt3 = pt2;
      pt3.x -= label_offset;

      anchor = GUPPI_CANVAS_ANCHOR_EAST;
      break;

    default:
      g_assert_not_reached ();
      
    }

    if (show_tick) {
      guppi_canvas_set_line_width (canvas, thickness);
      guppi_canvas_set_color_rgba (canvas, tick_color);
      guppi_canvas_draw_line (canvas, &pt1, &pt2);
    }

    if (show_label && guppi_tick_is_labelled (tick)) {
      guppi_canvas_set_font (canvas, label_font);
      guppi_canvas_set_color_rgba (canvas, label_color);
      guppi_canvas_draw_string (canvas,
				&pt3,
				anchor,
				GUPPI_CANVAS_ROT_0,
				guppi_tick_label (tick));
    }
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
view_init (GuppiElementView *view)
{
  compute_axis_size_request ((GuppiAxisView *) view);

  guppi_element_view_cartesian_add_view_interval ((GuppiElementViewCartesian *) view,
						  GUPPI_META_AXIS);

  guppi_element_view_set_clipped (view, FALSE);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->view_init)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->view_init (view);
}

static void
guppi_axis_view_class_init (GuppiAxisViewClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  view_class->changed   = changed;
  view_class->view_init = view_init;
  view_class->render    = render;
}

static void
guppi_axis_view_init (GuppiAxisView *obj)
{

}

GType
guppi_axis_view_get_type (void)
{
  static GType guppi_axis_view_type = 0;
  if (!guppi_axis_view_type) {
    static const GTypeInfo guppi_axis_view_info = {
      sizeof (GuppiAxisViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_axis_view_class_init,
      NULL, NULL,
      sizeof (GuppiAxisView),
      0,
      (GInstanceInitFunc) guppi_axis_view_init

    };
    guppi_axis_view_type = g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN,
						   "GuppiAxisView",
						   &guppi_axis_view_info, 0);
  }
  return guppi_axis_view_type;
}

/* $Id$ */
