/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * libguppi.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __LIBGUPPI_H__
#define __LIBGUPPI_H__

#include <libguppi/guppi-init.h>

#include <libguppi/useful/libguppi-useful.h>
#include <libguppi/canvas/libguppi-canvas.h>
#include <libguppi/specfns/libguppi-specfns.h>
#include <libguppi/math/libguppi-math.h>
#include <libguppi/layout/libguppi-layout.h>
#include <libguppi/data/libguppi-data.h>
#include <libguppi/plot/libguppi-plot.h>


#endif /* __LIBGUPPI_H__ */

