/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 1999-2000 EMC Capital Management, Inc.
 * Copyright (C) 2001-2002 The Free Software Foundation
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <math.h>
#include <gtk/gtk.h>
#include <libguppi/libguppi.h>
#include <sys/types.h>

#define WIDTH 360
#define HEIGHT 360

static gboolean
perturb_data (gpointer ptr)
{
  GuppiSeqScalar **data = ptr;
  GuppiSeqScalar *x_data = data[0];
  GuppiSeqScalar *y_data = data[1];

  int i, i0, i1;
  double x, y, dx, dy, grp_dx, grp_dy;

  static double t = 0;

  grp_dx = cos(t)/100;
  grp_dy = sin(t)/100;
  t += 0.1;

  guppi_seq_common_bounds (GUPPI_SEQ (x_data), GUPPI_SEQ (y_data), &i0, &i1);

  for (i = i0; i <= i1; ++i) {
    dx = 0.05 * (random() / (double) RAND_MAX - 0.5) + grp_dx;
    dy = 0.05 * (random() / (double) RAND_MAX - 0.5) + grp_dy;

    x = guppi_seq_scalar_get (x_data, i);
    y = guppi_seq_scalar_get (y_data, i);

    guppi_seq_scalar_set (x_data, i, x + dx);
    guppi_seq_scalar_set (y_data, i, y + dy);
  }
  
  return TRUE;
}

static gboolean
move_view_1 (GuppiViewInterval *vi)
{
  static double t = 0;
  guppi_view_interval_translate (vi, sin(t)/10);
  t += 0.1;
  return TRUE;
}

static gboolean
move_view_2 (GuppiViewInterval *vi)
{
  static double t = 0;
  double t0, t1;

  guppi_view_interval_range (vi, &t0, &t1);
  
  if (t1 - t0 > 0.02) {
    guppi_view_interval_rescale_around_point (vi,
					      (t0 + t1)/2,
					      1 + cos(t)/10);
  } else {    
    guppi_view_interval_set (vi, -1, 1);
  }
  t += 0.13;
  return TRUE;
}

static GuppiElementView *
build_scatter_plot_1 (void)
{
  GuppiSeqScalar *x_data, *y_data;
  GuppiElementState *scatter_s, *axis_n_s, *axis_s_s, *axis_e_s, *axis_w_s;
  GuppiElementView  *scatter_v, *axis_n_v, *axis_s_v, *axis_e_v, *axis_w_v;
  GuppiGroupView    *group_v;

  int i;
  const int N = 500;

  x_data = GUPPI_SEQ_SCALAR (guppi_data_new_by_name ("GuppiDataDoubleArray"));
  y_data = GUPPI_SEQ_SCALAR (guppi_data_new_by_name ("GuppiDataDoubleArray"));

  {
    static GuppiSeqScalar *foo[2];

    foo[0] = x_data;
    foo[1] = y_data;
    g_timeout_add (100, perturb_data, foo);
  }

  for (i = 0; i <= N; ++i) {
    double t = i / (double)N;
    double x, y;

    t *= 2 * M_PI;

    x = cos(3 * t);
    y = sin(5 * t);

    guppi_seq_scalar_append (x_data, x);
    guppi_seq_scalar_append (y_data, y);
  }

  scatter_s = guppi_element_state_new ("scatter",
				       "x_data", x_data,
				       "y_data", y_data,
				       NULL);

  scatter_v = guppi_element_view_new (scatter_s, NULL);

  guppi_element_view_cartesian_set_preferred_view (GUPPI_ELEMENT_VIEW_CARTESIAN (scatter_v), GUPPI_X_AXIS);
  guppi_element_view_cartesian_set_preferred_view (GUPPI_ELEMENT_VIEW_CARTESIAN (scatter_v), GUPPI_Y_AXIS);

  axis_n_s = guppi_element_state_new ("axis",
				      "position", GUPPI_NORTH,
				      NULL);
  axis_n_v = guppi_element_view_new (axis_n_s, NULL);

  axis_s_s = guppi_element_state_new ("axis",
				      "position", GUPPI_SOUTH,
				      NULL);
  axis_s_v = guppi_element_view_new (axis_s_s, NULL);

  axis_e_s = guppi_element_state_new ("axis",
				      "position", GUPPI_EAST,
				      NULL);
  axis_e_v = guppi_element_view_new (axis_e_s, NULL);

  axis_w_s = guppi_element_state_new ("axis",
				      "position", GUPPI_WEST,
				      NULL);
  axis_w_v = guppi_element_view_new (axis_w_s, NULL);

  group_v = (GuppiGroupView *) guppi_group_view_new ();

  guppi_group_view_add (group_v, scatter_v);
  guppi_group_view_add (group_v, axis_n_v);
  guppi_group_view_add (group_v, axis_s_v);
  guppi_group_view_add (group_v, axis_e_v);
  guppi_group_view_add (group_v, axis_w_v);


  guppi_layout_engine_add_rules (guppi_group_view_layout (group_v),
				 guppi_layout_rule_new_flush_top (GUPPI_LAYOUT_ITEM (axis_n_v), 0),
				 guppi_layout_rule_new_flush_bottom (GUPPI_LAYOUT_ITEM (axis_s_v), 0),
				 guppi_layout_rule_new_flush_left (GUPPI_LAYOUT_ITEM (axis_w_v), 0),
				 guppi_layout_rule_new_flush_right (GUPPI_LAYOUT_ITEM (axis_e_v), 0),
				 guppi_layout_rule_new_vertically_aligned (GUPPI_LAYOUT_ITEM (axis_n_v),
									   GUPPI_LAYOUT_ITEM (scatter_v), 0),
				 guppi_layout_rule_new_vertically_aligned (GUPPI_LAYOUT_ITEM (scatter_v),
									   GUPPI_LAYOUT_ITEM (axis_s_v), 0),
				 guppi_layout_rule_new_horizontally_aligned (GUPPI_LAYOUT_ITEM (axis_w_v),
									     GUPPI_LAYOUT_ITEM (scatter_v), 0),
				 guppi_layout_rule_new_horizontally_aligned (GUPPI_LAYOUT_ITEM (scatter_v),
									     GUPPI_LAYOUT_ITEM (axis_e_v), 0),
				 NULL);

  guppi_element_view_cartesian_connect_axis_markers (scatter_v,
						     GUPPI_X_AXIS,
						     axis_n_v,
						     GUPPI_META_AXIS);

  guppi_element_view_cartesian_connect_axis_markers (scatter_v,
						     GUPPI_X_AXIS,
						     axis_s_v,
						     GUPPI_META_AXIS);

  guppi_element_view_cartesian_connect_axis_markers (scatter_v,
						     GUPPI_Y_AXIS,
						     axis_w_v,
						     GUPPI_META_AXIS);

  guppi_element_view_cartesian_connect_axis_markers (scatter_v,
						     GUPPI_Y_AXIS,
						     axis_e_v,
						     GUPPI_META_AXIS);

#if 1
  g_timeout_add (100,
		 move_view_1,
		 guppi_element_view_cartesian_get_view_interval (scatter_v, GUPPI_X_AXIS));

  g_timeout_add (50,
		 move_view_2,
		 guppi_element_view_cartesian_get_view_interval (scatter_v, GUPPI_Y_AXIS));
#endif
		 
  return group_v;
}

static GuppiElementView *
build_scatter_plot_2 (void)
{
  GuppiSeqScalar *x_data, *y_data;
  GuppiElementState *scatter_s;
  GuppiElementView  *scatter_v;

  int i;
  const int N = 500;

  x_data = GUPPI_SEQ_SCALAR (guppi_data_new_by_name ("GuppiDataDoubleArray"));
  y_data = GUPPI_SEQ_SCALAR (guppi_data_new_by_name ("GuppiDataDoubleArray"));

  for (i = 0; i <= N; ++i) {
    double t = i / (double)N;
    double x, y;

    x = t - 0.5;
    y = x*x;

    guppi_seq_scalar_append (x_data, x);
    guppi_seq_scalar_append (y_data, y);
  }

  scatter_s = guppi_element_state_new ("scatter",
				       "x_data", x_data,
				       "y_data", y_data,
				       NULL);
  
  scatter_v = guppi_element_view_new (scatter_s, NULL);

  guppi_element_view_cartesian_set_preferred_view (GUPPI_ELEMENT_VIEW_CARTESIAN (scatter_v), GUPPI_X_AXIS);
  guppi_element_view_cartesian_set_preferred_view (GUPPI_ELEMENT_VIEW_CARTESIAN (scatter_v), GUPPI_Y_AXIS);

  return scatter_v;
}

GuppiElementView *
build_root_view (void)
{
  GuppiRootGroupView *root_v;
  GuppiElementView *contents_1_v;
  GuppiElementView *contents_2_v;
  
  GuppiElementState *title_s;
  GuppiElementView *title_v;


  root_v = guppi_root_group_view_new ();

  contents_1_v = build_scatter_plot_1 ();
  contents_2_v = build_scatter_plot_2 ();

  title_s = guppi_element_state_new ("text",
				     "text", "Pango-rific! \305\200\305\201\305\202\305\203\305\204\305\205\305\206\305\207\305\210\305\211\305\212\305\213\305\214\305\215\305\216\305\217",
				     "font", pango_font_description_from_string ("Arial 18"),
				     NULL);
  title_v = guppi_element_view_new (title_s, NULL);

  guppi_group_view_add (GUPPI_GROUP_VIEW (root_v), contents_1_v);
  guppi_group_view_add (GUPPI_GROUP_VIEW (root_v), title_v);

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_fill_horizontally (GUPPI_LAYOUT_ITEM (contents_1_v), 0, 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_flush_bottom (GUPPI_LAYOUT_ITEM (contents_1_v), 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_flush_top (GUPPI_LAYOUT_ITEM (title_v), 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_center_horizontally (GUPPI_LAYOUT_ITEM (title_v)));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_vertically_adjacent (GUPPI_LAYOUT_ITEM (title_v), GUPPI_LAYOUT_ITEM (contents_1_v), 0));



#if 0
  guppi_group_view_add (GUPPI_GROUP_VIEW (root_v), contents_2_v);
  
  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_fill_vertically (GUPPI_LAYOUT_ITEM (contents_1_v), 0, 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_fill_vertically (GUPPI_LAYOUT_ITEM (contents_2_v), 0, 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_same_width (GUPPI_LAYOUT_ITEM (contents_1_v),
								  GUPPI_LAYOUT_ITEM (contents_2_v)));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_horizontally_aligned (GUPPI_LAYOUT_ITEM (contents_1_v),
									    GUPPI_LAYOUT_ITEM (contents_2_v), 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_flush_left (GUPPI_LAYOUT_ITEM (contents_1_v), 0));

  guppi_layout_engine_add_rule (guppi_group_view_layout (GUPPI_GROUP_VIEW (root_v)),
				guppi_layout_rule_new_flush_right (GUPPI_LAYOUT_ITEM (contents_2_v), 0));
#endif

  guppi_root_group_view_set_size (root_v, WIDTH, HEIGHT);

  return (GuppiElementView *) root_v;
}

struct ViewAndCanvasInfo {
  GuppiElementView *view;
  GuppiCanvas *canvas;
};

static gint pending_re_render = 0;

static gboolean
re_render (gpointer user_data)
{
  struct ViewAndCanvasInfo *info = user_data;
  static int count = 1;
  static time_t start = 0;
  time_t now;

  if (start == 0)
    time (&start);
  time (&now);

  if (count % 10 == 0)
    g_print ("render #%d (%g renders/sec)\n", count, now > start ? count / (double)(now-start) : 0);
  guppi_canvas_begin_drawing (info->canvas);
  guppi_element_view_render (info->view, NULL, info->canvas);
  guppi_canvas_end_drawing (info->canvas);
  ++count;

  guppi_unref (info->view);
  guppi_unref (info->canvas);
  guppi_free (info);

  pending_re_render = 0;
  return FALSE;
}

static void
schedule_re_render (GuppiElementView *view, gpointer user_data)
{
  struct ViewAndCanvasInfo *info;

  if (pending_re_render != 0)
    return;

  info = guppi_new (struct ViewAndCanvasInfo, 1);
  info->view = guppi_ref (view);
  info->canvas = guppi_ref (user_data);
  
  pending_re_render = g_idle_add (re_render, info);
}


int
main (int argc, char *argv[])
{
  GtkWidget *window, *canvas;
  GuppiElementView *scatter_v;

  gtk_init (&argc, &argv);
  guppi_init ();

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  canvas = g_object_new (GUPPI_TYPE_CANVAS_GTK, NULL);

  guppi_canvas_gtk_set_fixed_size (GUPPI_CANVAS_GTK (canvas),
				   WIDTH, HEIGHT);

  gtk_container_add (GTK_CONTAINER (window), canvas);

  scatter_v = build_root_view ();

  guppi_canvas_begin_drawing (GUPPI_CANVAS (canvas));
  guppi_element_view_render (scatter_v, NULL, GUPPI_CANVAS (canvas));
  guppi_canvas_end_drawing (GUPPI_CANVAS (canvas));

  g_signal_connect (scatter_v, "changed", (GCallback) schedule_re_render, canvas);


  gtk_widget_show_all (window);
  gtk_main ();
  
  return 0;
}

/* $Id$ */
