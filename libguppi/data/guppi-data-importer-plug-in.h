/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-importer-plug-in.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_IMPORTER_PLUG_IN_H
#define _INC_GUPPI_DATA_IMPORTER_PLUG_IN_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-plug-in.h>
#include <libguppi/useful/guppi-plug-in-spec.h>
#include <libguppi/data/guppi-data-importer.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiDataImporterPlugIn GuppiDataImporterPlugIn;
typedef struct _GuppiDataImporterPlugInClass GuppiDataImporterPlugInClass;

struct _GuppiDataImporterPlugIn {
  GuppiPlugIn parent;

  GtkType importer_type;
  GuppiDataImporter *(*construct) (void);

  /* Try to figure out when this plug-in is applicable */
  gboolean imports_files;
  GSList *accepted_extensions;
  GSList *rejected_extensions;
  gboolean reject_if_not_accepted;
  gboolean reject_binary_extensions;
  double (*assess_by_filename) (const gchar *);
  double (*assess_by_peek) (gpointer data, gsize size);
};

struct _GuppiDataImporterPlugInClass {
  GuppiPlugInClass parent_class;
};

#define GUPPI_TYPE_DATA_IMPORTER_PLUG_IN (guppi_data_importer_plug_in_get_type())
#define GUPPI_DATA_IMPORTER_PLUG_IN(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_IMPORTER_PLUG_IN,GuppiDataImporterPlugIn))
#define GUPPI_DATA_IMPORTER_PLUG_IN0(obj) ((obj) ? (GUPPI_DATA_IMPORTER_PLUG_IN(obj)) : NULL)
#define GUPPI_DATA_IMPORTER_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_IMPORTER_PLUG_IN,GuppiDataImporterPlugInClass))
#define GUPPI_IS_DATA_IMPORTER_PLUG_IN(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_IMPORTER_PLUG_IN))
#define GUPPI_IS_DATA_IMPORTER_PLUG_IN0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_IMPORTER_PLUG_IN(obj)))
#define GUPPI_IS_DATA_IMPORTER_PLUG_IN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_IMPORTER_PLUG_IN))

GtkType guppi_data_importer_plug_in_get_type (void);

GuppiPlugIn *guppi_data_importer_plug_in_new (void);

void guppi_data_importer_plug_in_add_accepted_extension (GuppiDataImporterPlugIn *, const gchar *);
void guppi_data_importer_plug_in_add_accepted_extensions (GuppiDataImporterPlugIn *, const gchar * many[]);
void guppi_data_importer_plug_in_add_rejected_extension (GuppiDataImporterPlugIn *, const gchar *);
void guppi_data_importer_plug_in_add_rejected_extensions (GuppiDataImporterPlugIn *, const gchar * many[]);



typedef struct _GuppiDataImporterAssessment GuppiDataImporterAssessment;
struct _GuppiDataImporterAssessment {
  double confidence;
  GuppiPlugInSpec *spec;
  GuppiDataImporterPlugIn *plug_in;
};

GList *guppi_data_importer_plug_in_assess (const gchar * filename,
					   gpointer data, gsize size);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_IMPORTER_PLUG_IN_H */

/* $Id$ */
