/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-change.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/data/guppi-data-change.h>

#include <glib.h>

static GHashTable         *type_name_hash = NULL;
static GuppiDataChangeType unique_type    = 1;

GuppiDataChangeType
guppi_data_change_type_get_unique (const char *type_name)
{
  int this_type;

  g_return_val_if_fail (type_name && *type_name, 0);

  this_type = unique_type;
  ++unique_type;

  if (type_name_hash == NULL)
    type_name_hash = g_hash_table_new (NULL, NULL);

  g_hash_table_insert (type_name_hash,
		       GINT_TO_POINTER (this_type),
		       g_strdup (type_name));
		       

  return this_type;
}

const char *
guppi_data_change_type_to_string (GuppiDataChangeType type)
{
  g_return_val_if_fail (type != 0, NULL);
  g_return_val_if_fail (type_name_hash != NULL, NULL);

  return g_hash_table_lookup (type_name_hash, GINT_TO_POINTER (type));
}

char *
guppi_data_change_get_type_string (GuppiDataChange *change)
{
  const char *type_str, *subtype_str;
  
  g_return_val_if_fail (change != NULL, NULL);
  
  type_str = guppi_data_change_type_to_string (change->type);
  subtype_str = guppi_data_change_type_to_string (change->subtype);

  return g_strdup_printf ("%s::%s",
			  type_str ? type_str : "Unknown",
			  subtype_str ? subtype_str : "Unknown");
}
