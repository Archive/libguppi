/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-seq-change.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_SEQ_CHANGE_H__
#define __GUPPI_SEQ_CHANGE_H__

#include <glib.h>
#include <libguppi/data/guppi-data-change.h>

typedef struct _GuppiSeqChange_Shift  GuppiSeqChange_Shift;
typedef struct _GuppiSeqChange_Set    GuppiSeqChange_Set;
typedef struct _GuppiSeqChange_Insert GuppiSeqChange_Insert;
typedef struct _GuppiSeqChange_Grow   GuppiSeqChange_Grow;
typedef struct _GuppiSeqChange_Delete GuppiSeqChange_Delete;

struct _GuppiSeqChange_Shift {
  GuppiDataChange parent;

  int shift;
};

struct _GuppiSeqChange_Set {
  GuppiDataChange parent;

  int i0, i1;
};

struct _GuppiSeqChange_Insert {
  GuppiDataChange parent;

  int i;
  gsize N;
};

struct _GuppiSeqChange_Grow {
  GuppiDataChange parent;

  int i0, i1;
};

struct _GuppiSeqChange_Delete {
  GuppiDataChange parent;

  int i;
  gsize N;
};

GuppiDataChangeType guppi_seq_change_get_type        (void);
gboolean            guppi_data_change_is_seq_change  (GuppiDataChange *);

GuppiDataChangeType guppi_seq_change_get_shift_type  (void);
GuppiDataChangeType guppi_seq_change_get_set_type    (void);
GuppiDataChangeType guppi_seq_change_get_insert_type (void);
GuppiDataChangeType guppi_seq_change_get_grow_type   (void);
GuppiDataChangeType guppi_seq_change_get_delete_type (void);

void                guppi_seq_change_shift_init      (GuppiSeqChange_Shift *);
void                guppi_seq_change_set_init        (GuppiSeqChange_Set *);
void                guppi_seq_change_insert_init     (GuppiSeqChange_Insert *);
void                guppi_seq_change_grow_init       (GuppiSeqChange_Grow *);
void                guppi_seq_change_delete_init     (GuppiSeqChange_Delete *);


#endif /* __GUPPI_SEQ_CHANGE_H__ */

