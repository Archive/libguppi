/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-attribute.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_ATTRIBUTES_H__
#define __GUPPI_DATA_ATTRIBUTES_H__

#include  "guppi-attributes.h"
#include <libguppi/data/guppi-data.h>

void       guppi_attributes_declare_data (GuppiAttributes *, const gchar *name);
GuppiData *guppi_attributes_get_data     (GuppiAttributes *, const gchar *name);
void       guppi_attributes_set_data     (GuppiAttributes *, const gchar *name, GuppiData *);

#endif /* __GUPPI_DATA_ATTRIBUTES_H__ */

