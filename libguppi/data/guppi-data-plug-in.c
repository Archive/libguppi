/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-plug-in.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-data-plug-in.h>

#include <libguppi/useful/guppi-memory.h>

GuppiPlugIn *
guppi_data_plug_in_new (void)
{
  GuppiPlugIn *pi = (GuppiPlugIn *) guppi_new0 (GuppiDataPlugIn, 1);
  guppi_plug_in_construct (pi);
  return pi;
}

GuppiData *
guppi_data_plug_in_create_data (GuppiDataPlugIn *plugin)
{
  g_return_val_if_fail (plugin != NULL, NULL);
  g_return_val_if_fail (plugin->constructor, NULL);

  return plugin->constructor ();
}


/* $Id$ */
