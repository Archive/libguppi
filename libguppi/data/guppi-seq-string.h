/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-string.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_STRING_H
#define _INC_GUPPI_SEQ_STRING_H

/* #include <gtk/gtk.h> */

#include <libguppi/data/guppi-seq.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiSeqString GuppiSeqString;
typedef struct _GuppiSeqStringIface GuppiSeqStringIface;

struct _GuppiSeqStringIface {
  GTypeInterface interface;

  const gchar *(*get)      (GuppiSeqString *, gint);
  void         (*set)      (GuppiSeqString *, gint, gchar *);
  void         (*insert)   (GuppiSeqString *, gint, gchar *);
};


#define GUPPI_TYPE_SEQ_STRING           (guppi_seq_string_get_type())
#define GUPPI_SEQ_STRING(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_SEQ_STRING,GuppiSeqString))
#define GUPPI_IS_SEQ_STRING(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_SEQ_STRING))
#define GUPPI_SEQ_STRING_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_SEQ_STRING, GuppiSeqStringIface))

GType guppi_seq_string_get_type (void);

gboolean guppi_seq_string_is_read_only (GuppiSeqString *);
gboolean guppi_seq_string_can_change   (GuppiSeqString *);

const gchar *guppi_seq_string_get (GuppiSeqString *, gint);

/*
 _nc == "no-copy" version of function, assumes ownership of pointers
*/

void guppi_seq_string_set_nc (GuppiSeqString *, gint, gchar *);
void guppi_seq_string_set (GuppiSeqString *, gint, const gchar *);

void guppi_seq_string_prepend_nc (GuppiSeqString *, gchar *);
void guppi_seq_string_prepend (GuppiSeqString *, const gchar *);

void guppi_seq_string_append_nc (GuppiSeqString *, gchar *);
void guppi_seq_string_append (GuppiSeqString *, const gchar *);

void guppi_seq_string_insert_nc (GuppiSeqString *, gint, gchar *);
void guppi_seq_string_insert (GuppiSeqString *, gint, const gchar *);

void guppi_seq_string_spew (GuppiSeqString *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_SEQ_STRING_H */

/* $Id$ */
