/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data-importer.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_IMPORTER_H
#define _INC_GUPPI_DATA_IMPORTER_H

/* #include <gtk/gtk.h> */
#include  "guppi-file.h"
#include  "guppi-stream.h"
#include <libguppi/data/guppi-data.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiDataImporter GuppiDataImporter;
typedef struct _GuppiDataImporterClass GuppiDataImporterClass;

typedef void (*import_iter_fn) (GuppiData *, gpointer);

struct _GuppiDataImporter {
  GtkObject parent;

  GuppiFile *file;
  GuppiStream *str;
};

struct _GuppiDataImporterClass {
  GtkObjectClass parent_class;

  const gchar *name;
  void (*set_source_hook) (GuppiDataImporter *);
  void (*import) (GuppiDataImporter *,
		  void (*iter_fn) (GuppiData *, gpointer),
		  gpointer);

  GtkWidget *(*state_edit_widget) (GuppiDataImporter *);
};

#define GUPPI_TYPE_DATA_IMPORTER (guppi_data_importer_get_type())
#define GUPPI_DATA_IMPORTER(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_IMPORTER,GuppiDataImporter))
#define GUPPI_DATA_IMPORTER0(obj) ((obj) ? (GUPPI_DATA_IMPORTER(obj)) : NULL)
#define GUPPI_DATA_IMPORTER_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_IMPORTER,GuppiDataImporterClass))
#define GUPPI_IS_DATA_IMPORTER(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_IMPORTER))
#define GUPPI_IS_DATA_IMPORTER0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_IMPORTER(obj)))
#define GUPPI_IS_DATA_IMPORTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_IMPORTER))

GtkType guppi_data_importer_get_type (void);

/* virtual constructors */
GuppiDataImporter *guppi_data_importer_new (const gchar *name, 
					    const gchar *first_arg_name, ...);
GuppiDataImporter *guppi_data_importer_newv (const gchar *name,
					     guint nargs, GtkArg *args);

const gchar *guppi_data_importer_get_name (GuppiDataImporter *);

void guppi_data_importer_set_source (GuppiDataImporter *, GuppiFile *);
GuppiFile *guppi_data_importer_file (GuppiDataImporter *);
GuppiStream *guppi_data_importer_stream (GuppiDataImporter *);
const gchar *guppi_data_importer_filename (GuppiDataImporter *);

gint guppi_data_importer_import (GuppiDataImporter *,
				 import_iter_fn fn, 
				 gpointer user_data);
void guppi_data_importer_import_interactive (GuppiDataImporter *,
					     import_iter_fn fn,
					     gpointer fn_user_data,
					     void (*post_hook) (gint, gpointer),
					     gpointer post_user_data);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_IMPORTER_H */

/* $Id$ */
