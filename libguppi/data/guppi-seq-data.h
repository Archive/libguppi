/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-data.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_DATA_H
#define _INC_GUPPI_SEQ_DATA_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/data/guppi-seq.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqData GuppiSeqData;
typedef struct _GuppiSeqDataClass GuppiSeqDataClass;

struct _GuppiSeqData {
  GuppiSeq parent;
};

struct _GuppiSeqDataClass {
  GuppiSeqClass parent_class;
};

#define GUPPI_TYPE_SEQ_DATA (guppi_seq_data_get_type())
#define GUPPI_SEQ_DATA(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_DATA,GuppiSeqData))
#define GUPPI_SEQ_DATA0(obj) ((obj) ? (GUPPI_SEQ_DATA(obj)) : NULL)
#define GUPPI_SEQ_DATA_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_DATA,GuppiSeqDataClass))
#define GUPPI_IS_SEQ_DATA(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_DATA))
#define GUPPI_IS_SEQ_DATA0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_DATA(obj)))
#define GUPPI_IS_SEQ_DATA_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_DATA))

GtkType guppi_seq_data_get_type (void);

GuppiData *guppi_seq_data_get (const GuppiSeqData *, gint);
void guppi_seq_data_set (GuppiSeqData *, gint, GuppiData *);

void guppi_seq_data_prepend (GuppiSeqData *, GuppiData *);
void guppi_seq_data_append (GuppiSeqData *, GuppiData *);
void guppi_seq_data_insert (GuppiSeqData *, gint, GuppiData *);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_DATA_H */

/* $Id$ */
