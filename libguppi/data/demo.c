/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-init.h>

#include <libguppi/useful/guppi-useful.h>
#include <libguppi/data/guppi-data-init.h>
#include <libguppi/data/guppi-date-series.h>

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
v_bounds (GDate *sd, GDate *ed, gpointer ud)
{
  GuppiDateSeries *ser = GUPPI_DATE_SERIES (ud);

  if (sd) *sd = *guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser));
  if (ed) *ed = *guppi_date_indexed_end (GUPPI_DATE_INDEXED (ser));
}

static gboolean
v_valid (GDate *dt, gpointer ud)
{
  GuppiDateSeries *ser = GUPPI_DATE_SERIES (ud);

  return guppi_date_indexed_valid (GUPPI_DATE_INDEXED (ser), dt);
}

static double
v_get (GDate *dt, gpointer ud)
{
  GuppiDateSeries *ser = GUPPI_DATE_SERIES (ud);

  return 1 + guppi_date_series_get (ser, dt);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */


static void
show (const GDate *dt)
{
  g_print ("%d/%d/%d", g_date_get_month ((GDate*)dt), g_date_get_day ((GDate*)dt), g_date_get_year ((GDate*)dt));
}

static void
dump (GuppiDateSeries *pd)
{
  GDate dt;
  double m, M;

  g_print ("Start Date: ");
  show (guppi_date_indexed_start (GUPPI_DATE_INDEXED (pd)));
  g_print ("\n");

  g_print ("  End Date: ");
  show (guppi_date_indexed_end (GUPPI_DATE_INDEXED (pd)));
  g_print ("\n");

  g_print ("      Size: %d\n", 
	   guppi_date_indexed_size (GUPPI_DATE_INDEXED (pd)));

  dt = *guppi_date_indexed_start (GUPPI_DATE_INDEXED (pd));

  while (guppi_date_indexed_in_bounds (GUPPI_DATE_INDEXED (pd), &dt)) {

    if (guppi_date_indexed_valid (GUPPI_DATE_INDEXED (pd), &dt)) {

      show (&dt);

      g_print (" %g\n", guppi_date_series_get (pd, &dt));
    }

    g_date_add_days (&dt, 1);
  }

  guppi_date_series_get_bounds (pd,
				guppi_date_indexed_start (GUPPI_DATE_INDEXED (pd)),	
				guppi_date_indexed_end (GUPPI_DATE_INDEXED (pd)),
				&m, &M);
  g_print ("Min: %g  Max: %g\n", m, M);
}

int
main (int argc, gchar * argv[])
{
  GuppiDateSeries *ser;
  GuppiDateSeries *ser2;
  GDate dt;
  gint i;
  double x;

  g_date_clear (&dt, 1);

  gnome_init ("import_demo", "0.0", argc, argv);
  force_development_path_hacks ();
  guppi_useful_init_without_guile ();
  guppi_data_init ();
  guppi_plug_in_load_all ();

  ser = GUPPI_DATE_SERIES (guppi_date_series_new ());

  g_date_set_dmy (&dt, 2, 1, 1990);

  srandom (time (NULL));

  for (i=0; i<10; ++i) {
    x = random () % 100;

    g_print ("set ");
    show (&dt);
    g_print (" to %g\n", x);
    
    guppi_date_series_set (ser, &dt, x);

    g_date_add_days (&dt, 1);
    if (i % 5 == 4)
      g_date_add_days (&dt, 2);
  }

  dump (ser);

  ser2 = GUPPI_DATE_SERIES (
	  guppi_data_new (GUPPI_TYPE_DATE_SERIES, "calc",
			  "bounds_fn", v_bounds,
			  "valid_fn", v_valid,
			  "get_fn", v_get,
			  "user_data", ser,
			  NULL));

  dump (ser2);

  return 0;
}




/* $Id$ */
