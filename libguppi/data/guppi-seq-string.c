/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-string.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-seq-string.h>

#include <stdlib.h>
#include <string.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-memory.h>

static void
guppi_seq_string_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  if (! initialized) {

    /* Foo */
    
    initialized = TRUE;
  }
}

GType
guppi_seq_string_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (GuppiSeqStringIface),
      guppi_seq_string_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL
    };
    type = g_type_register_static (G_TYPE_INTERFACE, "GuppiSeqString", &info, 0);
#ifdef WORKING_INTERFACE_PREREQS
    g_type_interface_add_prerequisite (type, GUPPI_TYPE_SEQ);
#endif
  }

  return type;
}

gboolean
guppi_seq_string_is_read_only (GuppiSeqString *seq)
{
  GuppiSeqStringIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_STRING (seq), TRUE);
  iface = GUPPI_SEQ_STRING_GET_IFACE (seq);

  return iface->set == NULL;
}

gboolean
guppi_seq_string_can_change (GuppiSeqString *seq)
{
  return ! guppi_seq_string_is_read_only (seq);
}

const gchar *
guppi_seq_string_get (GuppiSeqString *seq, gint i)
{
  GuppiSeqStringIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_STRING (seq), NULL);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), NULL);

  iface = GUPPI_SEQ_STRING_GET_IFACE (seq);
  g_assert (iface->get);

  return iface->get (seq, i);
}

void
guppi_seq_string_set_nc (GuppiSeqString *seq, gint i, gchar *str)
{
  GuppiSeqStringIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_string_can_change (seq));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  iface = GUPPI_SEQ_STRING_GET_IFACE (seq);
  g_assert (iface->set);

  iface->set (seq, i, str);
}

void
guppi_seq_string_set (GuppiSeqString *seq, gint i, const gchar *str)
{
  guppi_seq_string_set_nc (seq, i, guppi_strdup (str));
}

void
guppi_seq_string_prepend_nc (GuppiSeqString *seq, gchar *str)
{
  gint first;
  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert_nc (seq, first, str);
}

void
guppi_seq_string_prepend (GuppiSeqString *seq, const gchar *str)
{
  gint first;
  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert (seq, first, str);
}

void
guppi_seq_string_append_nc (GuppiSeqString *seq, gchar *str)
{
  gint last;
  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert_nc (seq, last + 1, str);
}

void
guppi_seq_string_append (GuppiSeqString *seq, const gchar *str)
{
  gint last;
  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_string_insert (seq, last + 1, str);
}

void
guppi_seq_string_insert_nc (GuppiSeqString *seq, gint i, gchar *str)
{
  GuppiSeqStringIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_STRING (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_string_can_change (seq));

  iface = GUPPI_SEQ_STRING_GET_IFACE (seq);
  g_assert (iface->insert);

  iface->insert (seq, i, str);
}

void
guppi_seq_string_insert (GuppiSeqString *seq, gint i, const gchar *str)
{
  guppi_seq_string_insert_nc (seq, i, guppi_strdup (str));
}

void
guppi_seq_string_spew (GuppiSeqString *seq)
{
  int i, i0, i1;

  g_return_if_fail (GUPPI_IS_SEQ_STRING (seq));

  guppi_seq_bounds (GUPPI_SEQ (seq), &i0, &i1);

  for (i = i0; i <= i1; ++i) {
    const char *str = guppi_seq_string_get (seq, i);
    
    if (str)
      g_print ("%4d: \"%s\"\n", i, str);
    else
      g_print ("%4d: (null)\n", i);
  }
}

/* $Id$ */
