/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-boolean.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_BOOLEAN_H
#define _INC_GUPPI_SEQ_BOOLEAN_H

/* #include <gtk/gtk.h> */

#include <libguppi/data/guppi-seq.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqBoolean GuppiSeqBoolean;
typedef struct _GuppiSeqBooleanClass GuppiSeqBooleanClass;

struct _GuppiSeqBoolean {
  GuppiSeq parent;
};

struct _GuppiSeqBooleanClass {
  GuppiSeqClass parent_class;

  /* virtual functions */
  gboolean (*get)         (GuppiSeqBoolean *, gint);
  void     (*set)         (GuppiSeqBoolean *, gint, gboolean);
  void     (*set_all)     (GuppiSeqBoolean *, gboolean);
  void     (*set_many)    (GuppiSeqBoolean *, const gint *, gsize, gboolean);
  void     (*insert)      (GuppiSeqBoolean *, gint, gboolean);
  void     (*insert_many) (GuppiSeqBoolean *, gint, gboolean, gsize);
  void     (*bitwise_and) (GuppiSeqBoolean *, GuppiSeqBoolean *);
  void     (*bitwise_or)  (GuppiSeqBoolean *, GuppiSeqBoolean *);
  void     (*bitwise_xor) (GuppiSeqBoolean *, GuppiSeqBoolean *);
  void     (*bitwise_not) (GuppiSeqBoolean *);
  gint     (*next_true)   (GuppiSeqBoolean *, gint);
  gsize    (*true_count)  (GuppiSeqBoolean *);

};

#define GUPPI_TYPE_SEQ_BOOLEAN (guppi_seq_boolean_get_type())
#define GUPPI_SEQ_BOOLEAN(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_BOOLEAN,GuppiSeqBoolean))
#define GUPPI_SEQ_BOOLEAN0(obj) ((obj) ? (GUPPI_SEQ_BOOLEAN(obj)) : NULL)
#define GUPPI_SEQ_BOOLEAN_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_BOOLEAN,GuppiSeqBooleanClass))
#define GUPPI_IS_SEQ_BOOLEAN(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_BOOLEAN))
#define GUPPI_IS_SEQ_BOOLEAN0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_BOOLEAN(obj)))
#define GUPPI_IS_SEQ_BOOLEAN_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_BOOLEAN))

GtkType guppi_seq_boolean_get_type (void);

gboolean guppi_seq_boolean_get (const GuppiSeqBoolean *seq, gint i);
void guppi_seq_boolean_set (GuppiSeqBoolean *seq, gint i, gboolean x);
void guppi_seq_boolean_set_all (GuppiSeqBoolean *seq, gboolean x);
void guppi_seq_boolean_set_many (GuppiSeqBoolean *seq, gint *indices, gsize N, gboolean x);

void guppi_seq_boolean_clear (GuppiSeqBoolean *seq);

void guppi_seq_boolean_prepend (GuppiSeqBoolean *seq, gboolean x);
void guppi_seq_boolean_prepend_many (GuppiSeqBoolean *seq, gboolean x, gsize N);

void guppi_seq_boolean_append (GuppiSeqBoolean *seq, gboolean x);
void guppi_seq_boolean_append_many (GuppiSeqBoolean *seq, gboolean x, gsize N);

void guppi_seq_boolean_insert (GuppiSeqBoolean *seq, gint i, gboolean x);
void guppi_seq_boolean_insert_many (GuppiSeqBoolean *seq, gint i, gboolean x, gsize N);

void guppi_seq_boolean_bitwise_and (GuppiSeqBoolean *seq,
				    const GuppiSeqBoolean *other);
void guppi_seq_boolean_bitwise_or (GuppiSeqBoolean *seq,
				   const GuppiSeqBoolean *other);
void guppi_seq_boolean_bitwise_xor (GuppiSeqBoolean *seq,
				    const GuppiSeqBoolean *other);
void guppi_seq_boolean_bitwise_not (GuppiSeqBoolean *seq);

gint guppi_seq_boolean_first_true (const GuppiSeqBoolean *seq);
gint guppi_seq_boolean_next_true (const GuppiSeqBoolean *seq, gint i);

gint guppi_seq_boolean_first_jointly_true (const GuppiSeqBoolean *a,
					   const GuppiSeqBoolean *b);
gint guppi_seq_boolean_next_jointly_true (const GuppiSeqBoolean *a,
					  const GuppiSeqBoolean *b, gint i);

gsize guppi_seq_boolean_true_count (const GuppiSeqBoolean *seq);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_BOOLEAN_H */

/* $Id$ */
