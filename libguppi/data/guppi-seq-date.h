/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-date.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_DATE_H
#define _INC_GUPPI_SEQ_DATE_H

/* #include <gtk/gtk.h> */

#include <libguppi/data/guppi-seq.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeqDate GuppiSeqDate;
typedef struct _GuppiSeqDateClass GuppiSeqDateClass;

struct _GuppiSeqDate {
  GuppiSeq parent;
};

struct _GuppiSeqDateClass {
  GuppiSeqClass parent_class;

  /* virtual functions */
  GDate *(*get)    (GuppiSeqDate *, gint);
  void   (*set)    (GuppiSeqDate *, gint, GDate *);
  void   (*insert) (GuppiSeqDate *, gint, GDate *);
  GDate *(*min)    (GuppiSeqDate *);
  GDate *(*max)    (GuppiSeqDate *);
  gint   (*lookup) (GuppiSeqDate *, GDate *);
};

#define GUPPI_TYPE_SEQ_DATE (guppi_seq_date_get_type())
#define GUPPI_SEQ_DATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_DATE,GuppiSeqDate))
#define GUPPI_SEQ_DATE0(obj) ((obj) ? (GUPPI_SEQ_DATE(obj)) : NULL)
#define GUPPI_SEQ_DATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_DATE,GuppiSeqDateClass))
#define GUPPI_IS_SEQ_DATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_DATE))
#define GUPPI_IS_SEQ_DATE0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_DATE(obj)))
#define GUPPI_IS_SEQ_DATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_DATE))

GtkType guppi_seq_date_get_type (void);

GDate *guppi_seq_date_get (GuppiSeqDate *, gint);
void guppi_seq_date_set (GuppiSeqDate *, gint, GDate *);
void guppi_seq_date_prepend (GuppiSeqDate *, GDate *);
void guppi_seq_date_append (GuppiSeqDate *, GDate *);
void guppi_seq_date_insert (GuppiSeqDate *, gint, GDate *);

GDate *guppi_seq_date_min (GuppiSeqDate *);
GDate *guppi_seq_date_max (GuppiSeqDate *);

gint guppi_seq_date_lookup (GuppiSeqDate *, GDate *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_DATE_H */

/* $Id$ */
