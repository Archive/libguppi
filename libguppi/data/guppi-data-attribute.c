/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-attribute.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/data/guppi-data-attribute.h>

#include <libguppi/useful/guppi-memory.h>

static gpointer
data_va2p (va_list *args)
{
  GuppiData *data = va_arg (*args, GuppiData *);
  guppi_ref (data);
  return data;
}

static void
data_p2va (gpointer ptr, va_list *args)
{
  GuppiData **dp = va_arg (*args, GuppiData **);
  *dp = (GuppiData *)ptr;
}

static gpointer
data_import (GuppiXMLDocument *doc, xmlNodePtr node)
{
  return guppi_data_import_xml (doc, node);
}

static xmlNodePtr
data_export (GuppiXMLDocument *doc, gpointer ptr)
{
  return ptr ? guppi_data_export_xml (GUPPI_DATA (ptr), doc) : NULL;
}

static void
data_dest (gpointer ptr)
{
  guppi_unref (ptr);
}

void
guppi_attributes_declare_data (GuppiAttributes *ax, const gchar *name)
{
  g_return_if_fail (ax && GUPPI_IS_ATTRIBUTES (ax));
  g_return_if_fail (name);

  guppi_attributes_declare_full_custom (ax, name, "data", data_va2p, data_p2va, data_export, data_import, data_dest);
}

GuppiData *
guppi_attributes_get_data (GuppiAttributes *ax, const gchar *name)
{
  g_return_val_if_fail (ax && GUPPI_IS_ATTRIBUTES (ax), NULL);
  g_return_val_if_fail (name, NULL);
  g_return_val_if_fail (guppi_attributes_check_type (ax, name, "data"), NULL);

  return GUPPI_DATA0 (guppi_attributes_get_raw (ax, name));
}

void
guppi_attributes_set_data (GuppiAttributes *ax, const gchar *name, GuppiData *data)
{
  g_return_if_fail (ax && GUPPI_IS_ATTRIBUTES (ax));
  g_return_if_fail (name);
  g_return_if_fail (guppi_attributes_check_type (ax, name, "data"));
  g_return_if_fail (GUPPI_IS_DATA0 (data));


  guppi_ref (data);
  guppi_attributes_set_raw (ax, name, data);
}









