/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-seq-change.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/data/guppi-seq-change.h>

#include <string.h>

GuppiDataChangeType
guppi_seq_change_get_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("GuppiSeqChange");
  }

  return type;
}

gboolean
guppi_data_change_is_seq_change (GuppiDataChange *change)
{
  g_return_val_if_fail (change != NULL, FALSE);

  return change->type == guppi_seq_change_get_type ();
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiDataChangeType
guppi_seq_change_get_shift_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("shift");
  }

  return type;
}

GuppiDataChangeType
guppi_seq_change_get_set_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("set");
  }

  return type;
}

GuppiDataChangeType
guppi_seq_change_get_insert_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("insert");
  }

  return type;
}

GuppiDataChangeType
guppi_seq_change_get_grow_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("grow");
  }

  return type;
}

GuppiDataChangeType
guppi_seq_change_get_delete_type (void)
{
  static GuppiDataChangeType type = 0;

  if (! type) {
    type = guppi_data_change_type_get_unique ("delete");
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_seq_change_init (GuppiDataChange *change, GuppiDataChangeType subtype, gsize size)
{
  /* Zero out the object in memory */
  memset (change, 0, size);

  change->type    = guppi_seq_change_get_type ();
  change->subtype = subtype;
}

void
guppi_seq_change_shift_init (GuppiSeqChange_Shift *change)
{
  g_return_if_fail (change != NULL);
  guppi_seq_change_init ((GuppiDataChange *) change,
			 guppi_seq_change_get_shift_type (),
			 sizeof (change));
}

void
guppi_seq_change_set_init (GuppiSeqChange_Set *change)
{
  g_return_if_fail (change != NULL);
  guppi_seq_change_init ((GuppiDataChange *) change,
			 guppi_seq_change_get_set_type (),
			 sizeof (change));
}

void
guppi_seq_change_insert_init (GuppiSeqChange_Insert *change)
{
  g_return_if_fail (change != NULL);
  guppi_seq_change_init ((GuppiDataChange *) change,
			 guppi_seq_change_get_insert_type (),
			 sizeof (change));
}

void
guppi_seq_change_grow_init (GuppiSeqChange_Grow *change)
{
  g_return_if_fail (change != NULL);
  guppi_seq_change_init ((GuppiDataChange *) change,
			 guppi_seq_change_get_grow_type (),
			 sizeof (change));
}

void
guppi_seq_change_delete_init (GuppiSeqChange_Delete *change)
{
  g_return_if_fail (change != NULL);
  guppi_seq_change_init ((GuppiDataChange *) change,
			 guppi_seq_change_get_delete_type (),
			 sizeof (change));
}



