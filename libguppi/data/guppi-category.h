/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-category.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_CATEGORY_H
#define _INC_GUPPI_CATEGORY_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/data/guppi-data.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiCategory GuppiCategory;
typedef struct _GuppiCategoryClass GuppiCategoryClass;

typedef gint code_t;
typedef void (*GuppiCategoryFn) (const gchar *str, code_t code, gpointer data);

struct _GuppiCategory {
  GuppiData parent;
};

struct _GuppiCategoryClass {
  GuppiDataClass parent_class;

  gsize        (*size)      (GuppiCategory *);
  void         (*codes)     (GuppiCategory *, code_t *min, code_t *max, code_t *unused);
  void         (*define)    (GuppiCategory *, gchar *, code_t);
  const gchar *(*code2name) (GuppiCategory *, code_t);
  code_t       (*name2code) (GuppiCategory *, const gchar *);
  void         (*foreach)   (GuppiCategory *, GuppiCategoryFn, gpointer);
};

#define GUPPI_TYPE_CATEGORY (guppi_category_get_type ())
#define GUPPI_CATEGORY(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_CATEGORY,GuppiCategory))
#define GUPPI_CATEGORY0(obj) ((obj) ? (GUPPI_CATEGORY(obj)) : NULL)
#define GUPPI_CATEGORY_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_CATEGORY,GuppiCategoryClass))
#define GUPPI_IS_CATEGORY(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_CATEGORY))
#define GUPPI_IS_CATEGORY0(obj) (((obj) == NULL) || (GUPPI_IS_CATEGORY(obj)))
#define GUPPI_IS_CATEGORY_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_CATEGORY))

GtkType guppi_category_get_type (void);

#define GUPPI_INVALID_CODE ((code_t)G_MAXINT)
#define GUPPI_CATEGORY_CODE_TO_POINTER(x) (GINT_TO_POINTER((gint)(x)))
#define GUPPI_CATEGORY_POINTER_TO_CODE(x) ((code_t)(GPOINTER_TO_INT(x)))

gsize guppi_category_size (GuppiCategory *);

code_t guppi_category_min_code (GuppiCategory *);
code_t guppi_category_max_code (GuppiCategory *);
code_t guppi_category_unused_code (GuppiCategory *);

gboolean guppi_category_define (GuppiCategory *, const gchar *str, code_t);
code_t guppi_category_add_by_name (GuppiCategory *, const gchar *str);

gboolean guppi_category_contains (GuppiCategory *, const gchar *);
gboolean guppi_category_contains_code (GuppiCategory *, code_t);

code_t guppi_category_find_by_name (GuppiCategory *, const gchar *);
const gchar *guppi_category_find_by_code (GuppiCategory *, code_t);

gboolean guppi_category_rename (GuppiCategory *, code_t, const gchar *);

void guppi_category_foreach (GuppiCategory *, GuppiCategoryFn, gpointer);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_CATEGORY_H */

/* $Id$ */
