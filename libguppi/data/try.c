/* $Id$ */

/*
 * try.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-guile.h"
#include <libguppi/data/guppi-data-init.h>

static void
real_main (void *closure, int argc, char *argv[])
{
  guppi_data_init ();
  scm_shell (argc, argv);
}

int
main (gint argc, char *argv[])
{
  gtk_init (&argc, &argv);

  scm_boot_guile (argc, argv, real_main, 0);
  g_assert_not_reached ();
  return 0;
}




/* $Id$ */
