/* $Id$ */

/*
 * guppi-data-transform.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-data-transform.h>

#include <math.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/data/guppi-seq-scalar.h>


gboolean
guppi_data_transform_supported (GuppiDataTransform * xform, GuppiData * data)
{
  g_return_val_if_fail (xform != NULL
			&& GUPPI_IS_DATA_TRANSFORM (xform), FALSE);
  g_return_val_if_fail (data != NULL && GUPPI_IS_DATA (data), FALSE);

  if (xform->supported_type &&
      !gtk_type_is_a (GTK_OBJECT_TYPE (data), xform->supported_type))
      return FALSE;

  if (xform->support_check_cb && !xform->support_check_cb (data))
    return FALSE;

  return TRUE;
}

GuppiData *
guppi_data_transform_process (GuppiDataTransform * xform, GuppiData * data)
{
  GuppiData *results;
  gchar *new_label;

  g_return_val_if_fail (xform != NULL
			&& GUPPI_IS_DATA_TRANSFORM (xform), NULL);
  g_return_val_if_fail (data != NULL && GUPPI_IS_DATA (data), NULL);

  results = xform->transform_cb (data);
  new_label = guppi_strdup_printf (xform->product_name_template,
			       guppi_data_label (data));
  guppi_data_set_label (results, new_label);
  guppi_free (new_label);

  return results;
}


static GList *transform_registry = NULL;

void
guppi_data_transforms_register (GuppiDataTransform * xform)
{
  g_return_if_fail (xform != NULL && GUPPI_IS_DATA_TRANSFORM (xform));

  transform_registry = g_list_append (transform_registry, xform);

  guppi_permanent_alloc (xform);
  guppi_permanent_alloc (xform->name);
  guppi_permanent_alloc (xform->product_name_template);
}

GList *
guppi_data_transforms_find_supported (GuppiData * data)
{
  GList *results = NULL;
  GList *iter = transform_registry;

  g_return_val_if_fail (data != NULL && GUPPI_IS_DATA (data), NULL);

  while (iter != NULL) {
    GuppiDataTransform *xform = GUPPI_DATA_TRANSFORM (iter->data);
    if (guppi_data_transform_supported (xform, data))
      results = g_list_append (results, xform);
    iter = g_list_next (iter);
  }

  return results;
}

/**************************************************************************/

/* Code for some basic transforms */

static double
sqrt_calc (double x, gpointer ptr)
{
  return sqrt (x);
}

static gboolean
sqrt_supported_cb (GuppiData * data)
{
  GuppiSeqScalar *ss = GUPPI_SEQ_SCALAR (data);

  return guppi_seq_scalar_min (ss) >= 0;
}

static GuppiData *
sqrt_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", sqrt_calc,
				       NULL);

  return xformed;
}

static GuppiDataTransform *
sqrt_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Square Root"));
  xform->product_name_template = guppi_strdup (_("sqrt(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = sqrt_supported_cb;
  xform->transform_cb = sqrt_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
sq_calc (double x, gpointer ptr)
{
  return x * x;
}

static GuppiData *
sq_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", sq_calc,
				       NULL);

  return xformed;
}

static GuppiDataTransform *
sq_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Square"));
  xform->product_name_template = guppi_strdup (_("%s^2"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = NULL;
  xform->transform_cb = sq_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
log_calc (double x, gpointer ptr)
{
  return log (x);
}

static gboolean
log_supported_cb (GuppiData * data)
{
  GuppiSeqScalar *ss = GUPPI_SEQ_SCALAR (data);

  return guppi_seq_scalar_min (ss) > 0;
}

static GuppiData *
log_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", log_calc,
				       NULL);

  return xformed;
}

static GuppiDataTransform *
log_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Log"));
  xform->product_name_template = guppi_strdup (_("log(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = log_supported_cb;
  xform->transform_cb = log_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
logit_calc (double x, gpointer ptr)
{
  return log (x / (1 - x));
}

static gboolean
logit_supported_cb (GuppiData * data)
{
  GuppiSeqScalar *ss = GUPPI_SEQ_SCALAR (data);

  return guppi_seq_scalar_min (ss) > 0 && guppi_seq_scalar_max (ss) < 1;
}

static GuppiData *
logit_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", logit_calc,
				       NULL);

  return xformed;
}

static GuppiDataTransform *
logit_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Logit"));
  xform->product_name_template = guppi_strdup (_("logit(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = logit_supported_cb;
  xform->transform_cb = logit_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
exp_calc (double x, gpointer ptr)
{
  return exp (x);
}

static GuppiData *
exp_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", exp_calc,
				       NULL);

  return xformed;
}

static GuppiDataTransform *
exp_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Exp"));
  xform->product_name_template = guppi_strdup (_("exp(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = NULL;
  xform->transform_cb = exp_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
abs_calc (double x, gpointer ptr)
{
  return fabs (x);
}

static GuppiData *
abs_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", abs_calc,
				       NULL);
  return xformed;
}

static GuppiDataTransform *
abs_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Absolute Value"));
  xform->product_name_template = guppi_strdup (_("abs(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = NULL;
  xform->transform_cb = abs_transform_cb;

  return xform;
}

/*  **  **  **  **  **  **  **  **  **  **  **  **  **  **  **  ** ** ** */

static double
std_calc (double x, gpointer ptr)
{
  double *y = (double *) ptr;
  return (x - y[0]) / y[1];
}

static GuppiData *
std_transform_cb (GuppiData * data)
{
  GuppiData *xformed = guppi_data_new (GUPPI_TYPE_SEQ_SCALAR,
				       "func",
				       "seq_scalar_arg1", data,
				       "unary_function_C", std_calc,
				       NULL);
  return xformed;
}

static GuppiDataTransform *
std_transform (void)
{
  GuppiDataTransform *xform = guppi_new0 (GuppiDataTransform, 1);

  xform->name = guppi_strdup (_("Standardize"));
  xform->product_name_template = guppi_strdup (_("std(%s)"));
  xform->supported_type = GUPPI_TYPE_SEQ_SCALAR;
  xform->support_check_cb = NULL;
  xform->transform_cb = std_transform_cb;

  return xform;
}


/**************************************************************************/

void
guppi_data_transforms_init (void)
{
  /* A chance to define some basic types. */

  guppi_data_transforms_register (sqrt_transform ());
  guppi_data_transforms_register (sq_transform ());
  guppi_data_transforms_register (log_transform ());
  guppi_data_transforms_register (logit_transform ());
  guppi_data_transforms_register (exp_transform ());
  guppi_data_transforms_register (abs_transform ());
  guppi_data_transforms_register (std_transform ());
}





/* $Id$ */
