/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-scalar.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_SCALAR_H
#define _INC_GUPPI_SEQ_SCALAR_H

/* #include <gtk/gtk.h> */
#include <libguppi/data/guppi-seq.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiSeqScalar GuppiSeqScalar;
typedef struct _GuppiSeqScalarIface GuppiSeqScalarIface;

typedef struct _GuppiIndexedPair GuppiIndexedPair;
struct _GuppiIndexedPair {
  gint i;
  double x, y;
};
typedef void (*GuppiIndexedPairFn) (GuppiIndexedPair *, gsize N, gpointer closure);

struct _GuppiSeqScalarIface {
  GTypeInterface interface;

  void     (*range)       (GuppiSeqScalar *, double *min, double *max);
  double   (*sum)         (GuppiSeqScalar *);
  double   (*sum_abs)     (GuppiSeqScalar *);
  double   (*var)         (GuppiSeqScalar *);
  gboolean (*quartiles)   (GuppiSeqScalar *, double *q1, double *med, double *q3);
  gboolean (*percentile)  (GuppiSeqScalar *, double p, double *x);

  double   (*get)         (GuppiSeqScalar *, gint);
  void     (*set_many)    (GuppiSeqScalar *, gint start, const double *, gint stride, gsize N);
  void     (*insert_many) (GuppiSeqScalar *, gint, const double *, gint stride, gsize N);

  double  *(*raw_access)  (GuppiSeqScalar *, gint *stride);

};

#define GUPPI_TYPE_SEQ_SCALAR           (guppi_seq_scalar_get_type())
#define GUPPI_SEQ_SCALAR(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_SEQ_SCALAR, GuppiSeqScalar))
#define GUPPI_IS_SEQ_SCALAR(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_SEQ_SCALAR))
#define GUPPI_SEQ_SCALAR_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_SEQ_SCALAR, GuppiSeqScalarIface))

GType guppi_seq_scalar_get_type (void);

gboolean guppi_seq_scalar_is_read_only (GuppiSeqScalar *seq);
gboolean guppi_seq_scalar_can_change   (GuppiSeqScalar *seq);

double guppi_seq_scalar_get      (GuppiSeqScalar *seq, gint i);
void   guppi_seq_scalar_set      (GuppiSeqScalar *seq, gint i, double val);
void   guppi_seq_scalar_set_many (GuppiSeqScalar *seq, gint start, const double *vals, gint stride, gsize N);

void guppi_seq_scalar_prepend           (GuppiSeqScalar *seq, double val);
void guppi_seq_scalar_prepend_many      (GuppiSeqScalar *seq, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_prepend_repeating (GuppiSeqScalar *seq, double val, gsize N);

void guppi_seq_scalar_append           (GuppiSeqScalar *seq, double val);
void guppi_seq_scalar_append_many      (GuppiSeqScalar *seq, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_append_repeating (GuppiSeqScalar *seq, double val, gsize N);

void guppi_seq_scalar_insert           (GuppiSeqScalar *seq, gint i, double val);
void guppi_seq_scalar_insert_many      (GuppiSeqScalar *seq, gint i, const double *vals, gint stride, gsize N);
void guppi_seq_scalar_insert_repeating (GuppiSeqScalar *seq, gint i, double val, gsize N);

double guppi_seq_scalar_min        (GuppiSeqScalar *seq);
double guppi_seq_scalar_max        (GuppiSeqScalar *seq);
double guppi_seq_scalar_sum        (GuppiSeqScalar *seq);
double guppi_seq_scalar_sum_abs    (GuppiSeqScalar *seq);
double guppi_seq_scalar_mean       (GuppiSeqScalar *seq);
double guppi_seq_scalar_var        (GuppiSeqScalar *seq);
double guppi_seq_scalar_vars       (GuppiSeqScalar *seq);
double guppi_seq_scalar_sdev       (GuppiSeqScalar *seq);
double guppi_seq_scalar_sdevs      (GuppiSeqScalar *seq);
double guppi_seq_scalar_q1         (GuppiSeqScalar *seq);
double guppi_seq_scalar_median     (GuppiSeqScalar *seq);
double guppi_seq_scalar_q3         (GuppiSeqScalar *seq);
double guppi_seq_scalar_percentile (GuppiSeqScalar *seq, double p);

const double *guppi_seq_scalar_raw (GuppiSeqScalar *seq, gint *stride);

#define guppi_seq_scalar_raw_get(ptr, str, i) \
(*(const double *)(((gchar *)ptr)+(str)*(i)))


END_GUPPI_DECLS;

#endif /* _INC_GUPPI_SEQ_SCALAR_H */

/* $Id$ */
