/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-seq-rgba.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_SEQ_RGBA_H__
#define __GUPPI_SEQ_RGBA_H__

#include <libguppi/data/guppi-seq.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiSeqRGBA GuppiSeqRGBA;
typedef struct _GuppiSeqRGBAIface GuppiSeqRGBAIface;

struct _GuppiSeqRGBAIface {
  GTypeInterface interface;

  guint32 (*get)    (GuppiSeqRGBA *, int);
  guint32 (*set)    (GuppiSeqRGBA *, int, guint32);
  void    (*insert) (GuppiSeqRGBA *, int, guint32);
};

#define GUPPI_TYPE_SEQ_RGBA           (guppi_seq_rgba_get_type())
#define GUPPI_SEQ_RGBA(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_SEQ_RGBA, GuppiSeqRGBA))
#define GUPPI_IS_SEQ_RGBA(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_SEQ_RGBA))
#define GUPPI_SEQ_RGBA_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_SEQ_RGBA, GuppiSeqRGBAIface))

GType guppi_seq_rgba_get_type (void);

guint32 guppi_seq_rgba_get    (GuppiSeqRGBA *, int i);
void    guppi_seq_rgba_set    (GuppiSeqRGBA *, int i, guint32 color);
void    guppi_seq_rgba_insert (GuppiSeqRGBA *, int i, guint32 color);


END_GUPPI_DECLS;

#endif /* __GUPPI_SEQ_RGBA_H__ */

