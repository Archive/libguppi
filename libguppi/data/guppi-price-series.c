/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-price-series.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-price-series.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>

typedef struct _GuppiPriceSeriesPrivate GuppiPriceSeriesPrivate;
struct _GuppiPriceSeriesPrivate {
  gboolean cached_day;
  GDate last_date;
  guint last_valid;
  guint last_get_flag;
  double last_get_value;
};

#define priv(x) ((GuppiPriceSeriesPrivate*)((x)->opaque_internals))

static GtkObjectClass * parent_class = NULL;

static void
guppi_price_series_finalize (GtkObject *obj)
{
  GuppiPriceSeries *ser = GUPPI_PRICE_SERIES (obj);

  guppi_free (ser->opaque_internals);
  ser->opaque_internals = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
changed (GuppiData *data)
{
  GuppiPriceSeriesPrivate *p = priv (GUPPI_PRICE_SERIES (data));

  if (GUPPI_DATA_CLASS (parent_class)->changed)
    GUPPI_DATA_CLASS (parent_class)->changed (data);

  p->cached_day = FALSE;
}

static xmlNodePtr
export_xml_element (GuppiDateIndexed *ind, const GDate *dt, GuppiXMLDocument *doc)
{
  GuppiPriceSeries *ser = GUPPI_PRICE_SERIES (ind);
  guint code;
  xmlNodePtr day_node;
  
  code = guppi_price_series_valid (ser, (GDate *)dt);

  if (!code)
    return NULL;

  day_node = guppi_xml_new_node (doc, "Day");

  /* date property is added automatically */

  if (code & PRICE_OPEN) {
    xmlAddChild (day_node, guppi_xml_new_text_nodef (doc, "open", "%g", guppi_price_series_open (ser, (GDate *)dt)));
  }

  if (code & PRICE_HIGH) {
    xmlAddChild (day_node, guppi_xml_new_text_nodef (doc, "high", "%g", guppi_price_series_high (ser, (GDate *)dt)));
  }

  if (code & PRICE_LOW) {
    xmlAddChild (day_node, guppi_xml_new_text_nodef (doc, "low", "%g", guppi_price_series_low (ser, (GDate *)dt)));
  }

  if (code & PRICE_CLOSE) {
    xmlAddChild (day_node, guppi_xml_new_text_nodef (doc, "close", "%g", guppi_price_series_close (ser, (GDate *)dt)));
  }

  return day_node;
}

static gboolean
import_xml_element (GuppiDateIndexed *ind, const GDate *dt, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiPriceSeries *ser = GUPPI_PRICE_SERIES (ind);
  gchar *s;
  guint32 code = 0;
  double op=0, hi=0, lo=0, cl=0;

  if (strcmp (node->name, "Day")) {
    g_warning ("Unknown element type \"%s\"", node->name);
    return FALSE;
  }

  /* date property is read automatically */

  node = node->xmlChildrenNode;
  
  while (node) {

    s = xmlNodeListGetString (doc->doc, node->xmlChildrenNode, 1);
    g_assert (s);
    
    if (!strcmp (node->name, "open")) {

      if (code & PRICE_OPEN)
	g_warning ("open multiply defined");
      code |= PRICE_OPEN;
      op = atof (s);

    } else if (!strcmp (node->name, "high")) {

      if (code & PRICE_HIGH)
	g_warning ("high multiply defined");
      code |= PRICE_HIGH;
      hi = atof (s);

    } else if (!strcmp (node->name, "low")) {

      if (code & PRICE_LOW)
	g_warning ("low multiply defined");
      code |= PRICE_LOW;
      lo = atof (s);

    } else if (!strcmp (node->name, "close")) {

      if (code & PRICE_CLOSE)
	g_warning ("close multiply defined");
      code |= PRICE_CLOSE;
      cl = atof (s);

    } else {
      g_warning ("Unknown tag \"%s\"", node->name);
    }

    free (s);
    node = node->next;
  }
  
  if (code & PRICE_OPEN)
    guppi_price_series_set_open (ser, (GDate *) dt, op);
  if (code & PRICE_HIGH)
    guppi_price_series_set_high (ser, (GDate *) dt, hi);
  if (code & PRICE_LOW)
    guppi_price_series_set_low (ser, (GDate *) dt, lo);
  if (code & PRICE_CLOSE)
    guppi_price_series_set_close (ser, (GDate *) dt, cl);

  return TRUE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_price_series_class_init (GuppiPriceSeriesClass *klass)
{
  GtkObjectClass* object_class = (GtkObjectClass *)klass;
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);
  GuppiDateIndexedClass *dind_class = GUPPI_DATE_INDEXED_CLASS (klass);

  parent_class = gtk_type_class(GUPPI_TYPE_DATE_INDEXED);

  object_class->finalize = guppi_price_series_finalize;

  data_class->changed = changed;

  dind_class->export_xml_element = export_xml_element;
  dind_class->import_xml_element = import_xml_element;
}

static void
guppi_price_series_init (GuppiPriceSeries *obj)
{
  GuppiPriceSeriesPrivate *p = guppi_new (GuppiPriceSeriesPrivate, 1);

  p->cached_day = FALSE;

  obj->opaque_internals = p;
}

GtkType
guppi_price_series_get_type (void)
{
  static GtkType guppi_price_series_type = 0;
  if (!guppi_price_series_type) {
    static const GtkTypeInfo guppi_price_series_info = {
      "GuppiPriceSeries",
      sizeof(GuppiPriceSeries),
      sizeof(GuppiPriceSeriesClass),
      (GtkClassInitFunc)guppi_price_series_class_init,
      (GtkObjectInitFunc)guppi_price_series_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_price_series_type = gtk_type_unique (GUPPI_TYPE_DATE_INDEXED, &guppi_price_series_info);
  }
  return guppi_price_series_type;
}

/***************************************************************************/

typedef struct _GuppiDataOp_PriceSeries GuppiDataOp_PriceSeries;
struct _GuppiDataOp_PriceSeries {
  GuppiDataOp op;

  GDate date;
  guint code;
  double value;
};

static void
op_set (GuppiData *d, GuppiDataOp *op)
{
  GuppiPriceSeries *ser = GUPPI_PRICE_SERIES (d);
  GuppiDataOp_PriceSeries *ps_op = (GuppiDataOp_PriceSeries *)op;

  GuppiPriceSeriesPrivate *p;
  GuppiPriceSeriesClass *klass;

  p = priv (ser);

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  g_assert (klass->set);

  klass->set (ser, ps_op->code, &ps_op->date, ps_op->value);

  if (p->cached_day && g_date_compare (&p->last_date, &ps_op->date)) {
    p->last_valid |= ps_op->code;
    if (p->last_get_flag == ps_op->code)
      p->last_get_value = ps_op->value;
  }
}

/***************************************************************************/

static gboolean
single_bit (guint code)
{
  if (code == 0)
    return FALSE;

  while (code) {
    guint bottom_bit = code & 1;
    code = code>>1;
    if (code && bottom_bit)
      return FALSE;
  }
  return TRUE;
}

static void
cache_valid (GuppiPriceSeriesPrivate *p, const GDate *date, guint code)
{
  if (p->cached_day && g_date_compare (&p->last_date, (GDate *) date) == 0)
    return;

  p->last_date = *date;
  p->last_get_flag = 0;
  p->last_valid = code;
  p->cached_day = TRUE;
}

static void
cache_value (GuppiPriceSeriesPrivate *p, const GDate *date, guint code, double x)
{
  if (p->cached_day && g_date_compare (&p->last_date, (GDate *) date)) {
    p->last_get_flag = code;
    p->last_get_value = x;
  }
}

guint
guppi_price_series_valid (GuppiPriceSeries *ser, const GDate *date)
{
  GuppiPriceSeriesPrivate *p;

  GuppiPriceSeriesClass *klass;
  guint code;

  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), 0);
  g_return_val_if_fail (date && g_date_valid ((GDate *) date), 0);

  p = priv (ser);

  if (p->cached_day && g_date_compare (&p->last_date, (GDate *) date) == 0)
    return p->last_valid;

  if (!guppi_date_indexed_in_bounds (GUPPI_DATE_INDEXED(ser), date))
    return 0;

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  g_assert (klass->valid);

  code = klass->valid (ser, date);

  cache_valid (p, date, code);

  return code;
}

double
guppi_price_series_get (GuppiPriceSeries *ser, guint get_code, const GDate *date)
{
  GuppiPriceSeriesPrivate *p;
  GuppiPriceSeriesClass *klass;
  gboolean date_match;
  guint valid_code;
  
  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), 0);
  g_return_val_if_fail (single_bit (get_code), 0);
  g_return_val_if_fail (date && g_date_valid ((GDate *) date), 0);

  p = priv (ser);

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  date_match = p->cached_day && (g_date_compare (&p->last_date, (GDate *) date) == 0);

  if (date_match)
    valid_code = p->last_valid;
  else {
    g_assert (klass->valid);
    valid_code = klass->valid (ser, date);
    cache_valid (p, date, valid_code);
  }
  
  g_return_val_if_fail (valid_code && get_code, 0);

  if (date_match && (p->last_get_flag & get_code)) {

    return p->last_get_value;

  } else {
    double x;

    g_assert (klass->get);

    x = klass->get (ser, get_code, date);

    cache_value (p, date, get_code, x);

    return x;
  }

  g_assert_not_reached();
  return 0;
}

void
guppi_price_series_set (GuppiPriceSeries *ser, guint code, const GDate *date,
			double val)
{
  GuppiDataOp_PriceSeries op;

  g_return_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser));
  g_return_if_fail (single_bit (code));
  g_return_if_fail (date && g_date_valid ((GDate *) date));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (ser)));

  op.op.op = op_set;
  op.date = *date;
  op.code = code;
  op.value = val;

  guppi_data_add_pending_op (GUPPI_DATA (ser), (GuppiDataOp *)&op);
  guppi_data_changed (GUPPI_DATA (ser));
}

double
guppi_price_series_open (GuppiPriceSeries *ser, const GDate *date)
{
  return guppi_price_series_get (ser, PRICE_OPEN, date);
}

double
guppi_price_series_high (GuppiPriceSeries *ser, const GDate *date)
{
  return guppi_price_series_get (ser, PRICE_HIGH, date);
}

double
guppi_price_series_low (GuppiPriceSeries *ser, const GDate *date)
{
  return guppi_price_series_get (ser, PRICE_LOW, date);
}

double
guppi_price_series_close (GuppiPriceSeries *ser, const GDate *date)
{
  return guppi_price_series_get (ser, PRICE_CLOSE, date);
}

void
guppi_price_series_set_open (GuppiPriceSeries *ser, const GDate *date, double x)
{
  guppi_price_series_set (ser, PRICE_OPEN, date, x);
}

void
guppi_price_series_set_high (GuppiPriceSeries *ser, const GDate *date, double x)
{
  guppi_price_series_set (ser, PRICE_HIGH, date, x);
}

void
guppi_price_series_set_low (GuppiPriceSeries *ser, const GDate *date, double x)
{
  guppi_price_series_set (ser, PRICE_LOW, date, x);
}

void
guppi_price_series_set_close (GuppiPriceSeries *ser, const GDate *date, double x)
{
  guppi_price_series_set (ser, PRICE_CLOSE, date, x);
}

gint
guppi_price_series_get_many (GuppiPriceSeries *ser,
			     const GDate *start_date, gint count,
			     double *timestamp_buffer,
			     double *open_buffer,
			     double *high_buffer,
			     double *low_buffer,
			     double *close_buffer)
{
  GuppiPriceSeriesClass *klass;

  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), 0);
  g_return_val_if_fail (start_date && g_date_valid ((GDate *) start_date), 0);

  if (count == 0)
    return 0;

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  g_assert (klass->get_many);
  return klass->get_many (ser, start_date, count,
			       timestamp_buffer,
			       open_buffer, high_buffer, low_buffer, close_buffer);
}

gint
guppi_price_series_get_range (GuppiPriceSeries *ser,
			      const GDate *start_date, const GDate *end_date,
			      double *timestamp_buffer,
			      double *open_buffer,
			      double *high_buffer,
			      double *low_buffer,
			      double *close_buffer)
{
  GuppiPriceSeriesClass *klass;
  GDate sd, ed;

  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), 0);
  g_return_val_if_fail (start_date && g_date_valid ((GDate *) start_date), 0);
  g_return_val_if_fail (end_date && g_date_valid ((GDate *) end_date), 0);

  sd = *start_date;
  ed = *end_date;

  guppi_date_indexed_clamp (GUPPI_DATE_INDEXED (ser), &sd);
  guppi_date_indexed_clamp (GUPPI_DATE_INDEXED (ser), &ed);

  if (g_date_gt (&sd, &ed)) {
    GDate dt = sd;
    sd = ed;
    ed = dt;
  }

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  g_assert (klass->get_range);

  return klass->get_range (ser, &sd, &ed,
			   timestamp_buffer,
			   open_buffer, high_buffer,
			   low_buffer, close_buffer);
}

gboolean
guppi_price_series_get_bounds (GuppiPriceSeries *ser, guint code,
			       const GDate *start_date, const GDate *end_date,
			       double *min, double *max)
{
  GuppiPriceSeriesPrivate *p;
  GuppiPriceSeriesClass *klass;
  GDate sd, ed;

  g_return_val_if_fail (ser && GUPPI_IS_PRICE_SERIES (ser), TRUE);
  g_return_val_if_fail (single_bit (code), TRUE);
  g_return_val_if_fail (start_date && g_date_valid ((GDate *) start_date), TRUE);
  g_return_val_if_fail (end_date && g_date_valid ((GDate *) end_date), TRUE);

  if (guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser)))
    return FALSE;

  sd = *start_date;
  ed = *end_date;

  guppi_date_indexed_clamp (GUPPI_DATE_INDEXED (ser), &sd);
  guppi_date_indexed_clamp (GUPPI_DATE_INDEXED (ser), &ed);

  if (g_date_gt (&sd, &ed)) {
    GDate dt = sd;
    sd = ed;
    ed = dt;
  }

  p = priv (ser);

  klass = GUPPI_PRICE_SERIES_CLASS (GTK_OBJECT (ser)->klass);

  if (klass->get_bounds) {

    return klass->get_bounds (ser, code, &sd, &ed, min, max);

  } else {

    GDate dt = sd;
    double x, m = 0, M = -1;
    gboolean non_empty = FALSE;
    
    do {

      if (guppi_price_series_valid (ser, &dt) & code) {

	x = guppi_price_series_get (ser, code, &dt);
	
	if (!non_empty) {
	  m = x;
	  M = x;
	} else {
	  if (x < m) m = x;
	  if (x > M) M = x;
	}

	non_empty = TRUE;
      }

      g_date_add_days (&dt, 1);

    } while (g_date_lteq (&dt, &ed));

    if (min) *min = m;
    if (max) *max = M;

    return non_empty;
  }

  g_assert_not_reached ();

  return FALSE;
}
