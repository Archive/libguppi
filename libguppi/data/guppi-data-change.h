/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-change.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_CHANGE_H__
#define __GUPPI_DATA_CHANGE_H__

struct _GuppiData;

typedef int GuppiDataChangeType;
typedef struct _GuppiDataChange GuppiDataChange;

struct _GuppiDataChange {
  GuppiDataChangeType type;
  GuppiDataChangeType subtype;
  struct _GuppiData *data;
};

GuppiDataChangeType guppi_data_change_type_get_unique (const char *type_name);
const char         *guppi_data_change_type_to_string  (GuppiDataChangeType type);

/* Returns a string containing the name of the change type
   as "TypeName::SubtypeName". */
char               *guppi_data_change_get_type_string (GuppiDataChange *change);


#endif /* __GUPPI_DATA_CHANGE_H__ */

