/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001-2002 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_H
#define _INC_GUPPI_SEQ_H

#include <glib-object.h>
#include <libguppi/data/guppi-data.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSeq GuppiSeq;
typedef struct _GuppiSeqIface GuppiSeqIface;

#define GUPPI_SEQ_EVIL_INDEX G_MAXINT

struct _GuppiSeqIface {
  GTypeInterface interface;

  /* virtual functions */
  void (*size_hint)       (GuppiSeq *, gsize);
  void (*get_bounds)      (GuppiSeq *, gint *min, gint *max);
  void (*shift_indices)   (GuppiSeq *, gint delta);
  void (*insert_generic)  (GuppiSeq *, gint, gsize);
  void (*delete_many)     (GuppiSeq *, gint, gsize);

  xmlNodePtr (*export_xml_element) (GuppiSeq *seq, gint i, GuppiXMLDocument *);
  gboolean   (*import_xml_element) (GuppiSeq *seq, GuppiXMLDocument *, xmlNodePtr);
};

#define GUPPI_TYPE_SEQ (guppi_seq_get_type())
#define GUPPI_SEQ(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), GUPPI_TYPE_SEQ, GuppiSeq))
#define GUPPI_IS_SEQ(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj),  GUPPI_TYPE_SEQ))
#define GUPPI_SEQ_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_SEQ, GuppiSeqIface))

GType    guppi_seq_get_type              (void);

void     guppi_seq_size_hint             (GuppiSeq *seq, gsize expected_size);

void     guppi_seq_indices               (GuppiSeq *seq, gint *min, gint *max);
void     guppi_seq_bounds                (GuppiSeq *seq, gint *min, gint *max);
gint     guppi_seq_min_index             (GuppiSeq *seq);
gint     guppi_seq_max_index             (GuppiSeq *seq);

gsize    guppi_seq_size                  (GuppiSeq *seq);
gboolean guppi_seq_empty                 (GuppiSeq *seq);
gboolean guppi_seq_nonempty              (GuppiSeq *seq);

gboolean guppi_seq_in_bounds             (GuppiSeq *seq, gint i);
gboolean guppi_seq_contains_bounds       (GuppiSeq *seq, GuppiSeq *seq2);
gboolean guppi_seq_equal_bounds          (GuppiSeq *seq, GuppiSeq *seq2);
void     guppi_seq_common_bounds         (GuppiSeq *seq, GuppiSeq *seq2, gint *min, gint *max);

void     guppi_seq_shift_indices         (GuppiSeq *seq, gint delta);
void     guppi_seq_set_min_index         (GuppiSeq *seq, gint min);
void     guppi_seq_set_max_index         (GuppiSeq *seq, gint max);

void     guppi_seq_delete                (GuppiSeq *seq, gint i);
void     guppi_seq_delete_many           (GuppiSeq *seq, gint i, gsize N);
void     guppi_seq_delete_range          (GuppiSeq *seq, gint i0, gint i1);

void     guppi_seq_grow_to_include       (GuppiSeq *seq, gint i);
void     guppi_seq_grow_to_include_range (GuppiSeq *seq, gint i0, gint i1);
void     guppi_seq_grow_to_overlap       (GuppiSeq *seq, GuppiSeq *seq_to_overlap);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_H */

/* $Id$ */
