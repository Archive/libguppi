/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-integer.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-seq-integer.h>

#include <stdlib.h>
#include <string.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-string.h>

static void
guppi_seq_integer_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  while (! initialized) {
    /* Foo */
    initialized = TRUE;
  }
}

GType
guppi_seq_integer_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (GuppiSeqIntegerIface),
      guppi_seq_integer_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL
    };
    type = g_type_register_static (G_TYPE_INTERFACE, "GuppiSeqInteger", &info, 0);
#ifdef WORKING_INTERFACE_PREREQS
    g_type_interface_add_prerequisite (type, GUPPI_TYPE_SEQ);
#endif
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_seq_integer_is_read_only (GuppiSeqInteger *seq)
{
  GuppiSeqIntegerIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_INTEGER (seq), TRUE);
  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
    
  return iface->set_many == NULL || iface->insert_many == NULL;
}

gboolean
guppi_seq_integer_can_change (GuppiSeqInteger *seq)
{
  return ! guppi_seq_integer_is_read_only (seq);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

int
guppi_seq_integer_get (GuppiSeqInteger *seq, int i)
{
  GuppiSeqIntegerIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_INTEGER (seq), i);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), 0);

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->get);

  return iface->get (seq, i);
}

void
guppi_seq_integer_set (GuppiSeqInteger *seq, int i, int val)
{
  GuppiSeqIntegerIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->set_many);

  iface->set_many (seq, i, &val, sizeof (int), 1);
}

void
guppi_seq_integer_set_many (GuppiSeqInteger *seq,
			    int i, 
			    const int *ptr,
			    int stride,
			    gsize N)
{
  GuppiSeqIntegerIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  if (N == 0)
    return;
  g_return_if_fail (ptr != NULL);
  
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i + N - 1));

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->set_many);

  iface->set_many (seq, i, ptr, stride, N);
}


void
guppi_seq_integer_prepend (GuppiSeqInteger *seq, int val)
{
  gint first;
  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_integer_insert (seq, first, val);
}

void
guppi_seq_integer_prepend_many (GuppiSeqInteger *seq,
				const gint *ptr, int stride,
				gsize N)
{
  gint first;
  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  if (N == 0)
    return;
  g_return_if_fail (ptr != NULL);

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_integer_insert_many (seq, first, ptr, stride, N);
}

void
guppi_seq_integer_append (GuppiSeqInteger *seq, gint val)
{
  gint last;
  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_integer_insert (seq, last + 1, val);
}

void
guppi_seq_integer_append_many (GuppiSeqInteger *seq, 
			       const gint *ptr, int stride,
			       gsize N)
{
  gint last;
  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  if (N == 0)
    return;
  g_return_if_fail (ptr != NULL);

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_integer_insert_many (seq, last + 1, ptr, stride, N);
}

void
guppi_seq_integer_insert (GuppiSeqInteger *seq, gint i, gint val)
{
  g_return_if_fail (seq != NULL);
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  guppi_seq_integer_insert_many (seq, i, &val, 0, 1);
}

void
guppi_seq_integer_insert_many (GuppiSeqInteger *seq, 
			       gint i,
			       const gint *ptr, int stride,
			       gsize N)
{
  GuppiSeqIntegerIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_INTEGER (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_integer_can_change (seq));

  if (N == 0)
    return;
  g_return_if_fail (ptr != NULL);

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->insert_many);

  iface->insert_many (seq, i, ptr, stride, N);
}

gint
guppi_seq_integer_min (GuppiSeqInteger *seq)
{
  GuppiSeqIntegerIface *iface;
  int min;

  g_return_val_if_fail (GUPPI_IS_SEQ_INTEGER (seq), 0);

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->range);

  iface->range (seq, &min, NULL);

  return min;
}

gint
guppi_seq_integer_max (GuppiSeqInteger *seq)
{
  GuppiSeqIntegerIface *iface;
  int max;

  g_return_val_if_fail (GUPPI_IS_SEQ_INTEGER (seq), 0);

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->range);

  iface->range (seq, NULL, &max);

  return max;
}

gint 
guppi_seq_integer_frequency (GuppiSeqInteger *seq, gint k)
{
  GuppiSeqIntegerIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_INTEGER (seq), 0);

  iface = GUPPI_SEQ_INTEGER_GET_IFACE (seq);
  g_assert (iface->frequency);

  return iface->frequency (seq, k);
}

/* $Id$ */
