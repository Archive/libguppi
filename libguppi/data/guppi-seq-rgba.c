/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-seq-rgba.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/data/guppi-seq-rgba.h>

static void
guppi_seq_rgba_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  while (! initialized) {
    /* Foo */
    initialized = TRUE;
  }
}

GType
guppi_seq_rgba_get_type (void)
{
  static GType type = 0;

  if (!type) {
    static const GTypeInfo info = {
      sizeof (GuppiSeqRGBAIface),
      guppi_seq_rgba_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL
    };
    type = g_type_register_static (G_TYPE_INTERFACE, "GuppiSeqRGBA", &info, 0);
#ifdef WORKING_INTERFACE_PREREQS
    g_type_interface_add_prerequisite (type, GUPPI_TYPE_SEQ);
#endif
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

guint32
guppi_seq_rgba_get (GuppiSeqRGBA *seq, int i)
{
  GuppiSeqRGBAIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_RGBA (seq), 0);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), 0);

  iface = GUPPI_SEQ_RGBA_GET_IFACE (seq);
  g_assert (iface->get);

  return iface->get (seq, i);
}

void
guppi_seq_rgba_set (GuppiSeqRGBA *seq, int i, guint32 c)
{
  GuppiSeqRGBAIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_RGBA (seq));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  iface = GUPPI_SEQ_RGBA_GET_IFACE (seq);
  g_assert (iface->set);

  iface->set (seq, i, c);
}

void
guppi_seq_rgba_insert (GuppiSeqRGBA *seq, int i, guint32 c)
{
  GuppiSeqRGBAIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_RGBA (seq));

  iface = GUPPI_SEQ_RGBA_GET_IFACE (seq);
  g_assert (iface->insert);

  iface->insert (seq, i, c);
}
