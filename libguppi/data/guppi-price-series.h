/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-price-series.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PRICE_SERIES_H
#define _INC_GUPPI_PRICE_SERIES_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/data/guppi-date-indexed.h>

BEGIN_GUPPI_DECLS

enum {
  PRICE_OPEN             = 1<<0,
  PRICE_HIGH             = 1<<1,
  PRICE_LOW              = 1<<2,
  PRICE_CLOSE            = 1<<3,
  PRICE_CHANGE           = 1<<4,
  PRICE_DAILY_RANGE      = 1<<5,
  PRICE_TRUE_RANGE       = 1<<6,
  PRICE_OVERNIGHT_CHANGE = 1<<7,
  PRICE_VOLUME           = 1<<8,
  PRICE_OPEN_INTEREST    = 1<<9
};


typedef struct _GuppiPriceSeries GuppiPriceSeries;
typedef struct _GuppiPriceSeriesClass GuppiPriceSeriesClass;

struct _GuppiPriceSeries {
  GuppiDateIndexed parent;
  gpointer opaque_internals;
};

struct _GuppiPriceSeriesClass {
  GuppiDateIndexedClass parent_class;

  guint (*valid) (GuppiPriceSeries *ser, const GDate *date);
  double (*get) (GuppiPriceSeries *ser, guint code, const GDate *date);
  void (*set) (GuppiPriceSeries *ser, guint code, const GDate *date, double x);

  gint (*get_many) (GuppiPriceSeries *ser,
		    const GDate *start_date, gint count,
		    double *timestamp_buffer,
		    double *open_buffer, double *high_buffer,
		    double *low_buffer, double *close_buffer);

  gint (*get_range) (GuppiPriceSeries *ser,
		     const GDate *start_date, const GDate *end_date,
		     double *timestamp_buffer,
		     double *open_buffer, double *high_buffer,
		     double *low_buffer, double *close_buffer);
		     
  gboolean (*get_bounds) (GuppiPriceSeries *ser, guint code,
			  const GDate *start_date, const GDate *end_date,
			  double *min, double *max);

};

#define GUPPI_TYPE_PRICE_SERIES (guppi_price_series_get_type ())
#define GUPPI_PRICE_SERIES(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PRICE_SERIES,GuppiPriceSeries))
#define GUPPI_PRICE_SERIES0(obj) ((obj) ? (GUPPI_PRICE_SERIES(obj)) : NULL)
#define GUPPI_PRICE_SERIES_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PRICE_SERIES,GuppiPriceSeriesClass))
#define GUPPI_IS_PRICE_SERIES(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PRICE_SERIES))
#define GUPPI_IS_PRICE_SERIES0(obj) (((obj) == NULL) || (GUPPI_IS_PRICE_SERIES(obj)))
#define GUPPI_IS_PRICE_SERIES_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PRICE_SERIES))

GtkType guppi_price_series_get_type (void);

guint guppi_price_series_valid (GuppiPriceSeries *ser, const GDate *date);
double guppi_price_series_get (GuppiPriceSeries *ser, guint code, const GDate *date);
void guppi_price_series_set (GuppiPriceSeries *ser, guint code, const GDate *date,
			     double value);

double guppi_price_series_open (GuppiPriceSeries *ser, const GDate *date);
double guppi_price_series_high (GuppiPriceSeries *ser, const GDate *date);
double guppi_price_series_low (GuppiPriceSeries *ser, const GDate *date);
double guppi_price_series_close (GuppiPriceSeries *ser, const GDate *date);

void guppi_price_series_set_open (GuppiPriceSeries *ser, const GDate *date, double);
void guppi_price_series_set_high (GuppiPriceSeries *ser, const GDate *date, double);
void guppi_price_series_set_low (GuppiPriceSeries *ser, const GDate *date, double);
void guppi_price_series_set_close (GuppiPriceSeries *ser, const GDate *date, double);

gint guppi_price_series_get_many (GuppiPriceSeries *ser,
				  const GDate *start_date, gint count,
				  double *timestamp_buffer,
				  double *open_buffer, double *high_buffer,
				  double *low_buffer, double *close_buffer);

gint guppi_price_series_get_range (GuppiPriceSeries *ser,
				   const GDate *start_date, const GDate *end_date,
				   double *timestamp_buffer,
				   double *open_buffer, double *high_buffer,
				   double *low_buffer, double *close_buffer);

gboolean guppi_price_series_get_bounds (GuppiPriceSeries *ser, guint code,
					const GDate *start_date, const GDate *end_date,
					double *min, double *max);
				       


END_GUPPI_DECLS

#endif /* _INC_GUPPI_PRICE_SERIES_H */

/* $Id$ */
