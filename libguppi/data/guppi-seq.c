/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-seq.h>

#include <string.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/data/guppi-data.h>
#include <libguppi/useful/guppi-marshal.h>

static void
guppi_seq_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  if (! initialized) {

    /* Foo */

  }
}

GType
guppi_seq_get_type (void)
{
  static GType guppi_seq_type = 0;

  if (!guppi_seq_type) {
    static const GTypeInfo guppi_seq_info = {
      sizeof (GuppiSeqIface),
      guppi_seq_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL
    };
    guppi_seq_type = g_type_register_static (G_TYPE_INTERFACE, "GuppiSeq", &guppi_seq_info, 0);
#ifdef WORKING_INTERFACE_PREREQS
    g_type_interface_add_prerequisite (guppi_seq_type, GUPPI_TYPE_DATA);
#endif
  }

  return guppi_seq_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/**
 * guppi_seq_size_hint
 * @seq: The sequence
 * @expected_size: The expected maximum size of the sequence
 *
 * Inform the implementation of the expected maximum size of the sequence.
 * In some cases, this information can be used to allocate memory in a
 * more efficient fashion.
 */
void
guppi_seq_size_hint (GuppiSeq *seq, gsize expected_size)
{
  GuppiSeqIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ (seq));

  iface = GUPPI_SEQ_GET_IFACE (seq);

  if (iface->size_hint) {

    /* Read-only data silently ignores size hints.  It likes the size it
       is just fine, thank you. */
    if (guppi_data_is_read_only (GUPPI_DATA (seq)))
      return;

    iface->size_hint (seq, expected_size);
  }
}

/**
 * guppi_seq_indices
 * @seq: The sequence
 * @min: A pointer to a variable to contain the lowest index value
 * @max: A pointer to a variable to contain the highest index bound
 * 
 * Get the minimum and maximum legal values of the sequence's
 * position index.  Either @min or @max may be %NULL.
 */
void
guppi_seq_indices (GuppiSeq *seq, gint *min, gint *max)
{
  GuppiSeqIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ (seq));

  if (min || max) {
    iface = GUPPI_SEQ_GET_IFACE (seq);
    g_assert (iface->get_bounds);
    iface->get_bounds (seq, min, max);
  }
}

void
guppi_seq_bounds (GuppiSeq *seq, gint *min, gint *max)
{
  guppi_seq_indices (seq, min, max);
}

/**
 * guppi_seq_min_index
 * @seq: The sequence
 *
 * Returns: The sequence's lowest legal index value.
 */
gint
guppi_seq_min_index (GuppiSeq *seq)
{
  gint min = GUPPI_SEQ_EVIL_INDEX;
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), GUPPI_SEQ_EVIL_INDEX);
  guppi_seq_indices (seq, &min, NULL);
  return min;
}

/**
 * guppi_seq_max_index
 * @seq: The sequence
 *
 * Returns: The sequence's highest legal index value.
 */
gint
guppi_seq_max_index (GuppiSeq *seq)
{
  gint max = GUPPI_SEQ_EVIL_INDEX;
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), GUPPI_SEQ_EVIL_INDEX);
  guppi_seq_indices (seq, NULL, &max);
  return max;
}

/**
 * guppi_seq_size
 * @seq: The sequence
 *
 * Computes the length of the sequence.
 *
 * Returns: The number of elements in the sequence.
 */
gsize
guppi_seq_size (GuppiSeq *seq)
{
  gint min, max;
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), 0);
  guppi_seq_indices (seq, &min, &max);
  g_assert (max + 1 >= min);
  return max + 1 - min;
}

/**
 * guppi_seq_empty
 * @seq: The sequence
 *
 * Checks if @seq is of zero length.
 *
 * Returns: %TRUE if @seq contains any elements, %FALSE otherwise.
 */
gboolean
guppi_seq_empty (GuppiSeq *seq)
{
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), TRUE);
  return guppi_seq_size (seq) == 0;
}

/**
 * guppi_seq_nonempty
 * @seq: The sequence
 *
 * Checks if @seq contains any elements.
 *
 * Returns: %TRUE if @seq contains no elements whatsoever, %FALSE otherwise.
 */
gboolean
guppi_seq_nonempty (GuppiSeq *seq)
{
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), FALSE);
  return guppi_seq_size (seq) > 0;
}

/**
 * guppi_seq_in_bounds
 * @seq: The sequence
 * @i: An index value
 *
 * Checks if @i lies within the sequence's bounds.
 *
 * Returns: %TRUE if @i is a legal index value for @seq.
 */
gboolean 
guppi_seq_in_bounds (GuppiSeq *seq, gint i)
{
  gint min = 0, max = -1;
  g_return_val_if_fail (GUPPI_IS_SEQ (seq), FALSE);
  guppi_seq_indices (seq, &min, &max);
  return min <= i && i <= max;
}

/**
 * guppi_seq_contains_bounds
 * @seq: A sequence
 * @seq2: Another sequence
 *
 * Check if a sequence's index bounds completely contain those
 * of another sequence.
 *
 * Returns: %TRUE if the bounds of @seq2 are contained entirely within
 * those of @seq.
 */
gboolean 
guppi_seq_contains_bounds (GuppiSeq *seq, GuppiSeq *seq2)
{
  gint amin = 0, amax = -1, bmin = 0, bmax = -1;

  g_return_val_if_fail (GUPPI_IS_SEQ (seq), FALSE);
  g_return_val_if_fail (GUPPI_IS_SEQ (seq2), FALSE);
  
  guppi_seq_indices (seq, &amin, &amax);
  guppi_seq_indices (seq2, &bmin, &bmax);

  return amin <= bmin && bmax <= amax;
}

/**
 * guppi_seq_equal_bounds
 * @seq: A sequence
 * @seq2: Another sequence
 *
 * Check if two sequences have the same upper and lower index bounds.
 *
 * Returns: %TRUE if the bounds of @seq and @seq2 are identical.
 */
gboolean
guppi_seq_equal_bounds (GuppiSeq *seq, GuppiSeq *seq2)
{
  gint amin = 0, amax = -1, bmin = 0, bmax = -1;

  g_return_val_if_fail (GUPPI_IS_SEQ (seq), FALSE);
  g_return_val_if_fail (GUPPI_IS_SEQ (seq2), FALSE);

  guppi_seq_indices (seq, &amin, &amax);
  guppi_seq_indices (seq2, &bmin, &bmax);

  return amin == bmin && bmax == amax;
}

/**
 * guppi_seq_common_bounds
 * @seq: A sequences
 * @seq2: Another sequences
 * @min: A pointer to a variable to contain the lower index value
 * @max: A pointer to a variable to contain the upper index value
 *
 * Compute the largest range of index values that are valid
 * for both @seq and @seq2.
 */
void
guppi_seq_common_bounds (GuppiSeq *seq, GuppiSeq *seq2,
			 gint *min, gint *max)
{
  gint amin = 0, amax = -1, bmin = 0, bmax = -1;

  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (GUPPI_IS_SEQ (seq2));

  if (min || max) {
    guppi_seq_indices (seq, &amin, &amax);
    guppi_seq_indices (seq, &bmin, &bmax);

    if (min)
      *min = MAX (amin, bmin);
    if (max)
      *max = MIN (amax, bmax);
  }
}

/**
 * guppi_seq_shift_indices
 * @seq: A sequences
 * @delta: The amount of shift the indices by.
 *
 * Shift the sequence's indices by @delta.
 */
void
guppi_seq_shift_indices (GuppiSeq *seq, gint delta)
{
  GuppiSeqIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));

  if (delta == 0)
    return;

  iface = GUPPI_SEQ_GET_IFACE (seq);

  if (iface->shift_indices) {
    iface->shift_indices (seq, delta);
  }
}

/**
 * guppi_seq_set_min_index
 * @seq: A sequence
 * @min: The new lower index value
 *
 * Adjust the sequence's index values such that
 * the lower bound becomes @min.
 */
void
guppi_seq_set_min_index (GuppiSeq *seq, gint min)
{
  gint old_min;
  g_return_if_fail (GUPPI_IS_SEQ (seq));
  old_min = guppi_seq_min_index (seq);
  guppi_seq_shift_indices (seq, min - old_min);
}

/**
 * guppi_seq_set_max_index
 * @seq: A sequence
 * @max: The new upper index value
 *
 * Adjust the sequence's index values such that
 * the lower bound becomes @max.
 */
void
guppi_seq_set_max_index (GuppiSeq *seq, gint max)
{
  gint old_max;
  g_return_if_fail (GUPPI_IS_SEQ (seq));
  old_max = guppi_seq_max_index (seq);
  guppi_seq_shift_indices (seq, max - old_max);
}


/**
 * guppi_seq_delete
 * @seq: A sequence
 * @i: A position index
 *
 * Remove the @i-th element of sequence @seq.  The position index
 * of all elements above the @i-th position are decremented.
 * @seq must be writeable and @i must be in-bounds.
 */
void
guppi_seq_delete (GuppiSeq *seq, gint i)
{
  guppi_seq_delete_many (seq, i, 1);
}

/**
 * guppi_seq_delete_many
 * @seq: A sequence
 * @i: A position index
 * @N: A count
 *
 * Remove @N elements from sequence @seq, starting at the @i-th.  The
 * position index of all elements following the removed elements are
 * decreased accordingly.
 * @seq must be writeable and @i must be in-bounds.
 */
void
guppi_seq_delete_many (GuppiSeq *seq, gint i, gsize N)
{
  GuppiSeqIface *iface;
  gint i1;

  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (seq, i));

  if (N == 0)
    return;

  iface = GUPPI_SEQ_GET_IFACE (seq);

  if (iface->delete_many) {

    i1 = guppi_seq_max_index (seq);
    if (i + N - 1 > i1)
      N = i1 - i + 1;

    iface->delete_many (seq, i, N);
  }
}

/**
 * guppi_seq_delete_range
 * @seq: A sequence
 * @i0: The lower index bound
 * @i1: The upper index bound
 *
 * Remove the elements at positions @i0 through @i1 from sequence
 * @seq.  The position index of all elements following the removed
 * elements are decreased accordingly.  @seq must be writeable and both
 * @i0 and @i1 must be in-bounds.
 */
void
guppi_seq_delete_range (GuppiSeq *seq, gint i0, gint i1)
{
  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_in_bounds (seq, i0));
  g_return_if_fail (guppi_seq_in_bounds (seq, i1));

  guppi_2sort_i (&i0, &i1);

  guppi_seq_delete_many (seq, i0, i1 - i0 + 1);
}

/**
 * guppi_seq_grow_to_include
 * @seq: A sequence
 * @i: A position index
 *
 * Expand the bounds of the sequence @seq sufficiently to
 * contain @i.  If @i is already in-bounds, this operation
 * does nothing.  @seq must be writeable.
 *
 * The default values assigned to any elements added to @seq
 * are type-dependent.
 */
void
guppi_seq_grow_to_include (GuppiSeq *seq, gint i)
{
  guppi_seq_grow_to_include_range (seq, i, i);
}

/**
 * guppi_seq_grow_to_include_range
 * @seq: A sequence
 * @i0: The lower index bound
 * @i1: The upper index bound
 *
 * Expand the bounds of the sequence @seq sufficiently to contain the
 * range @i0 to @i1.  If that range is already in-bounds, this
 * operation does nothing.  @seq must be writeable.
 *
 * The default values assigned to any elements added to @seq
 * are type-dependent.
 */
void
guppi_seq_grow_to_include_range (GuppiSeq *seq, gint j0, gint j1)
{
  GuppiSeqIface *iface = GUPPI_SEQ_GET_IFACE (seq);
  gint i0, i1;

  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));

  if (guppi_seq_in_bounds (seq, j0) && guppi_seq_in_bounds (seq, j1))
    return;

  guppi_2sort_i (&j0, &j1);

  guppi_seq_bounds (seq, &i0, &i1);

  if (guppi_seq_size (seq) == 0) {
    iface->insert_generic (seq, 0, j1 - j0 + 1);
    iface->shift_indices (seq, j0 - i0);
    return;
  }

  /* Grow to the front */
  if (j0 < i0) {
    iface->insert_generic (seq, i0, i0 - j0);
    iface->shift_indices (seq, j0 - i0);
  }

  /* Grow to the back */
  if (i1 < j1) {
    iface->insert_generic (seq, i1+1, j1 - i1);
  }
}

/**
 * guppi_seq_grow_to_overlap
 * @seq: A sequence
 * @seq_to_overlap: The sequence that @seq is to overlap
 *
 * Expand the bounds of the sequence @seq sufficiently to contain the
 * full range of indices of @seq_to_overlap.  If that range is already
 * in-bounds, this operation does nothing.  @seq must be writeable.
 *
 * The default values assigned to any elements added to @seq
 * are type-dependent.
 */

void
guppi_seq_grow_to_overlap (GuppiSeq *seq, GuppiSeq *seq_to_overlap)
{
  gint i0, i1;

  g_return_if_fail (GUPPI_IS_SEQ (seq));
  g_return_if_fail (GUPPI_IS_SEQ (seq_to_overlap));

  if (guppi_seq_size (seq_to_overlap) > 0) {
    guppi_seq_indices (seq_to_overlap, &i0, &i1);
    guppi_seq_grow_to_include_range (seq, i0, i1);
  }
}

/**************************************************************************/

#if 0
static void
export_xml (GuppiData *data, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiSeq *seq = GUPPI_SEQ (data);
  GuppiSeqClass *klass = GUPPI_SEQ_CLASS (GTK_OBJECT (data)->klass);
  gint i, i0, i1;
  xmlNodePtr seq_node;
  gboolean has_missing;

  g_return_if_fail (klass->export_xml_element != NULL);

  guppi_seq_bounds (seq, &i0, &i1);
  has_missing = guppi_seq_has_missing (seq);

  seq_node = guppi_xml_new_node (doc, "Sequence");
  if (i0 <= i1) {
    guppi_xml_set_property_int (seq_node, "min_index", i0);
    guppi_xml_set_property_int (seq_node, "max_index", i1);
  }
  guppi_xml_set_property_bool (seq_node, "has_missing", has_missing);

  xmlAddChild (node, seq_node);

  for (i = i0; i <= i1; ++i) {
    xmlNodePtr el_node;
    
    if (has_missing && guppi_seq_missing (seq, i)) {
      el_node = guppi_xml_new_node (doc, "missing_value");
    } else {
      el_node = klass->export_xml_element (seq, i, doc);
    }
    xmlAddChild (seq_node, el_node);
  }

  if (GUPPI_DATA_CLASS (parent_class)->export_xml)
    GUPPI_DATA_CLASS (parent_class)->export_xml (data, doc, node);
}

static gboolean
import_xml (GuppiData *data, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiSeq *seq = GUPPI_SEQ (data);
  GuppiSeqClass *klass = GUPPI_SEQ_CLASS (GTK_OBJECT (data)->klass);

  if (! strcmp (node->name, "Sequence")) {

    gint i, i0, i1;
    gboolean has_missing;
  
    i0 = guppi_xml_get_property_int (node, "min_index", 0);
    i1 = guppi_xml_get_property_int (node, "max_index", -1);
    has_missing = guppi_xml_get_property_bool (node, "has_missing", TRUE);

    if (i0 > i1)
      return TRUE;

    node = node->xmlChildrenNode;

    i = i0;
    while (node != NULL && i <= i1) {
      if (has_missing && ! strcmp (node->name, "missing_value")) {
	guppi_seq_append_missing (seq);
      } else if (! klass->import_xml_element (seq, doc, node))
	return FALSE;
      node = node->next;
      ++i;
    }
    
    return TRUE;
  }

  /* Pass along any non-Sequence nodes. */

  if (GUPPI_DATA_CLASS (parent_class)->import_xml)
    return GUPPI_DATA_CLASS (parent_class)->import_xml (data, doc, node);
  else
    return FALSE;
}
#endif

/* $Id$ */
