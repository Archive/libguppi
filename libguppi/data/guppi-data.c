/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-data.h>

#include <string.h>

#include <zlib.h>
#include <libxml/xmlmemory.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-marshal.h>
#include <libguppi/useful/guppi-plug-in-spec.h>
#include <libguppi/data/guppi-data-plug-in.h>

static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  CHANGED_LABEL,
  CHANGED_SUBDATA,
  LAST_SIGNAL
};
static guint data_signals[LAST_SIGNAL] = { 0 };


static void
process_pending_op (GuppiData *data, GuppiDataChange *change)
{
  GList *op_list;
  GuppiDataOp *op;

  g_return_if_fail (GUPPI_IS_DATA (data));

  op_list = (GList *) data->pending_ops;
  g_return_if_fail (op_list != NULL);

  op = (GuppiDataOp *) op_list->data;
  g_return_if_fail (op != NULL);

  /* Sanity check */
  g_assert (op->change == change);
  if (change) {
    g_assert (change->data == data);
  }

  if (op->op)
    op->op (data, op);

  data->pending_ops = g_list_remove_link (op_list, op_list);
  g_list_free_1 (op_list);
}

static void
changed (GuppiData *data, GuppiDataChange *change)
{
  process_pending_op (data, change);
}

static void
changed_label (GuppiData *data, const char *new_label)
{
  gchar *old = data->label;
  data->label = guppi_strdup (new_label);
  guppi_free (old);
}

static void
changed_subdata (GuppiData *data, GuppiDataChange *change)
{
  process_pending_op (data, change);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_data_construct (GuppiData *data)
{
  GuppiDataClass *klass;

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));
  
  if (!klass->class_validated && klass->validate_class != NULL) {
    klass->class_validated = klass->validate_class (klass);
    if (! klass->class_validated)
      g_printerr ("Class '%s' is not fully specified.\n\n", g_type_name (G_OBJECT_CLASS_TYPE (klass)));
    klass->class_validated = TRUE; /* only complain once */
  }
}

GuppiData *
guppi_data_new (GType type)
{
  GuppiData *data;

  g_return_val_if_fail (type, NULL);

  if (g_type_is_a (type, GUPPI_TYPE_DATA)) {
    data = (GuppiData *) guppi_object_new (type);
  } else {
    g_warning ("Type '%s' is-not-a GuppiData", g_type_name (type));
    return NULL;
  }

  guppi_data_construct (data);

  return data;
}

GuppiData *
guppi_data_new_by_name (const gchar *type)
{
  GuppiPlugIn *pi;
  GuppiData *data;
  GuppiDataClass *klass;
  GType t;

  g_return_val_if_fail (type && *type, NULL);

  t = g_type_from_name (type);
  if (t) 
    return guppi_data_new (t);

  pi = guppi_plug_in_lookup ("data_impl", type);
  if (pi == NULL) {
    g_warning("Plug-in data_impl::%s not found", type);
    return NULL;
  }
    
  data = guppi_data_plug_in_create_data ((GuppiDataPlugIn *) pi);
  if (data == NULL) {
    g_warning ("%s's constructor returned NULL", type);
    return NULL;
  }
 
  g_return_val_if_fail (GUPPI_IS_DATA (data), NULL);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  if (klass->plug_in_code == NULL) {
    klass->plug_in_code = guppi_strdup (type);
    guppi_permanent_alloc (klass->plug_in_code);
  }

  guppi_data_construct (data);

  return data;
}

/**
 * guppi_data_unique_id
 * @data: A #GuppiData object.
 *
 * Returns a unique identifier associated with @data.
 */
guppi_uniq_t
guppi_data_unique_id (GuppiData *data)
{
  g_return_val_if_fail (GUPPI_IS_DATA (data), 0);

  return data->id;
}

/**
 * guppi_data_get_label
 * @data: A #GuppiData object.
 *
 * Returns the label associated with @data.
 */
const gchar *
guppi_data_get_label (GuppiData *data)
{
  g_return_val_if_fail (GUPPI_IS_DATA (data), NULL);

  return data->label;
}

/**
 * guppi_data_set_label
 * @data: A #GuppiData object.
 * @label: A descriptive label.
 *
 * Associates the label @label with @data.
 */
void
guppi_data_set_label (GuppiData *data, const gchar *label)
{
  g_return_if_fail (GUPPI_IS_DATA (data));
  g_return_if_fail (label != NULL);

  if (data->label == NULL || strcmp (label, data->label)) {
    g_signal_emit (data, data_signals[CHANGED_LABEL], 0, label);
  }
}

/**
 * guppi_data_is_read_only
 * @data: A #GuppiData object.
 *
 * Returns %TRUE is @data is a read-only object.  If an object is read-only,
 * all potentially state-altering operations are illegal.
 */
gboolean
guppi_data_is_read_only (GuppiData *data)
{
  GuppiDataClass *klass;

  g_return_val_if_fail (GUPPI_IS_DATA (data), FALSE);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));
  return data->read_only || klass->read_only;
}

/**
 * guppi_data_can_change 
 * @data: A #GuppiData object.
 *
 * Returns %TRUE is @data is not a read-only object.  This is the opposite
 * of guppi_data_can_change().
 */
gboolean 
guppi_data_can_change (GuppiData *data)
{
  return !guppi_data_is_read_only (data);
}

static void
guppi_data_add_pending_op (GuppiData *data, GuppiDataOp *op)
{
  /* The data pointer gets stamped into the GuppiDataChange struct
     automatically. */
  if (op->change)
    op->change->data = data;

  data->pending_ops = g_list_prepend ((GList *) data->pending_ops, op);
}

void
guppi_data_execute_op (GuppiData *data, GuppiDataOp *op)
{
  g_return_if_fail (GUPPI_IS_DATA (data));
  g_assert (guppi_data_can_change (data));
  g_return_if_fail (op != NULL);

  guppi_data_add_pending_op (data, op);
  g_signal_emit (data, data_signals[CHANGED], 0, op->change);
}

void
guppi_data_execute_subdata_op (GuppiData *data, GuppiDataOp *op)
{
  g_return_if_fail (GUPPI_IS_DATA (data));
  g_return_if_fail (op != NULL);

  guppi_data_add_pending_op (data, op);
  g_signal_emit (data, data_signals[CHANGED_SUBDATA], 0, op->change);
}

/**
 * guppi_data_changed
 * @data: A #GuppiData object.
 *
 * Force @data to emit a "changed" signal.
 */
void
guppi_data_changed (GuppiData *data)
{
  GuppiDataOp op;

  g_return_if_fail (GUPPI_IS_DATA (data));

  op.op = NULL;
  op.change = NULL;

  guppi_data_execute_op (data, &op);
}

/**
 * guppi_data_changed_subdata
 * @data: A #GuppiData object.
 *
 * Force @data to emit a "changed_subdata" signal.
 */
void
guppi_data_changed_subdata (GuppiData *data)
{
  GuppiDataOp op;

  g_return_if_fail (GUPPI_IS_DATA (data));

  op.op = NULL;
  op.change = NULL;

  guppi_data_execute_subdata_op (data, &op);
}

/**
 * guppi_data_copy
 * @data: A #GuppiData object.
 *
 * Returns a copy of @data.
 */
GuppiData *
guppi_data_copy (GuppiData *data)
{
  GuppiDataClass *klass;
  GuppiData *copy;

  g_return_val_if_fail (GUPPI_IS_DATA (data), NULL);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));
  g_return_val_if_fail (klass->copy != NULL, NULL);

  copy = klass->copy (data);

  copy->read_only = data->read_only;
  copy->label     = guppi_strdup_printf (_("Copy of %s"), guppi_data_get_label (data));

  return copy;
}

/**
 * guppi_data_get_size_in_bytes
 * @data: A #GuppiData object.
 *
 * Returns the total quantity of memory being used by @data, in bytes.
 * For some data interfaces and implementations, this is only an estimate,
 * not an exact figure.
 */
gint
guppi_data_get_size_in_bytes (GuppiData *data)
{
  GuppiDataClass *klass;

  g_return_val_if_fail (GUPPI_IS_DATA (data), -1);
  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  return klass->get_size_in_bytes ? klass->get_size_in_bytes (data) : -1;
}

/**
 * guppi_data_get_size_info
 * @data: A #GuppiData object.
 *
 * Returns a short, human-readable description of @data.
 * The exact nature of the description depends on the
 * @data's type.
 */
gchar *
guppi_data_get_size_info (GuppiData *data)
{
  GuppiDataClass *klass;

  g_return_val_if_fail (GUPPI_IS_DATA (data), NULL);
  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  return klass->get_size_info ? klass->get_size_info (data) : NULL;
}

/**************************************************************************/

/**
 * guppi_data_export_xml
 * @data: A #GuppiData object.
 * @doc: The #GuppiXMLDocument that will contain the exported XML.
 *
 * Build an XML tree describing @data.
 * Returns: the top node of the constructed XML.
 */
xmlNodePtr
guppi_data_export_xml (GuppiData *data, GuppiXMLDocument *doc)
{
  GuppiDataClass *klass;
  xmlNodePtr data_node = NULL;

  g_return_val_if_fail (GUPPI_IS_DATA (data), NULL);
  g_return_val_if_fail (doc, NULL);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  data_node = xmlNewNode (doc->ns, "Data");
  xmlNewProp (data_node, "Type", klass->plug_in_code);

  if (data->label)
    xmlNewTextChild (data_node, doc->ns, "Label", data->label);

  if (klass->export_xml)
    klass->export_xml (data, doc, data_node);

  return data_node;
}
/**
 * guppi_data_import_xml
 * @doc: An XML document.
 * @node: The top node of a description of some data from @doc.
 *
 * Scan the XML tree contained in @node and use it to construct a #GuppiData object.
 * If the XML is malformed or any other errors occur in processing, %NULL is returned.
 *
 * Returns: the #GuppiData object constructed from the XML in @node.
 */
GuppiData *
guppi_data_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  gchar *type_name;
  GuppiDataClass *klass;
  GuppiData *data = NULL;
  gboolean loaded_label = FALSE;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "Data")) 
    return NULL;

  /* Construct base object from type and impl information */

  type_name = xmlGetProp (node, "Type");
  if (type_name == NULL) {
    g_message ("Missing data Type property");
    return NULL;
  }

  data = guppi_data_new_by_name (type_name);

  if (data == NULL) {
    g_message ("Unknown type \"%s\"", type_name);
    xmlFree (type_name);
    return NULL;
  }
  
  xmlFree (type_name);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  node = node->xmlChildrenNode;

  while (node != NULL) {

    if (!loaded_label && !strcmp (node->name, "Label")) {

      /* We do this song & dance to avoid g_free vs. xmlFree problems. */
      gchar *tmp = xmlNodeGetContent (node->xmlChildrenNode);
      guppi_free (data->label);
      data->label = g_strdup (tmp);
      xmlFree (tmp);

      loaded_label = TRUE;

    } else if (klass->import_xml) {
      /* If we fail, fail silently. */
      klass->import_xml (data, doc, node);
    }
    
    node = node->next;
  }

  return data;
}

/**
 * guppi_data_spew_xml
 * @data: A #GuppiData object.
 *
 * Build an XML tree describing @data and write it out to the console.
 * (This can be useful for debugging purposes.)
 */
void
guppi_data_spew_xml (GuppiData *data)
{
  GuppiXMLDocument *doc;

  g_return_if_fail (GUPPI_IS_DATA (data));

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_data_export_xml (data, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}

/* Subdata handling */

/**
 * guppi_data_has_subdata
 * @data: A #GuppiData object.
 *
 * Returns: %TRUE is @data contains any embedded subdata objects.
 */
gboolean
guppi_data_has_subdata (GuppiData *data)
{
  g_return_val_if_fail (GUPPI_IS_DATA (data), FALSE);
  
  return guppi_data_subdata_count (data) != 0;
}

static void
count_subdata_fn (GuppiData *d, gpointer user_data)
{
  ++*(gint *)user_data;
}

/**
 * guppi_data_subdata_count
 * @data: A #GuppiData object.
 *
 * Returns: the number of embedded subdata objects in @data.
 */
gint
guppi_data_subdata_count (GuppiData *data)
{
  gint count = 0;

  g_return_val_if_fail (GUPPI_IS_DATA (data), 0);
  
  guppi_data_foreach_subdata (data, count_subdata_fn, &count);

  return count;
}

/**
 * guppi_data_foreach_subdata
 * @data: A #GuppiData object.
 * @fn: A function.
 * @user_data: 
 *
 * Iterate over @data's embedded subdata objects, applying the function @fn
 * to each.  For every call, @user_data is passed to @fn as its second argument.
 */

void
guppi_data_foreach_subdata (GuppiData *data, GuppiDataFn fn, gpointer closure)
{
  GuppiDataClass *klass;

  g_return_if_fail (GUPPI_IS_DATA (data));
  g_return_if_fail (fn != NULL);

  klass = GUPPI_DATA_CLASS (G_OBJECT_GET_CLASS (data));

  /* If foreach_subdata isn't defined in the implementation class,
     we assume that that type can never have subdata, and the
     guppi_data_foreach_subdata () call is just a NOP. */
  if (klass->foreach_subdata)
    klass->foreach_subdata (data, fn, closure);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_data_finalize (GObject *obj)
{
  GuppiData *gd = GUPPI_DATA (obj);

  guppi_finalized (obj);

  /* We should never die with pending operations! */
  g_assert (gd->pending_ops == NULL);

  guppi_free0 (gd->label);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static gboolean
validate_class (GuppiDataClass *klass)
{
  gboolean ok = TRUE;

  if (klass->copy == NULL) {
    g_printerr ("Method GuppiData::copy not defined.\n");
    ok = FALSE;
  }

  if (klass->get_size_in_bytes == NULL) {
    g_printerr ("Method GuppiData::get_size_in_bytes not defined.\n");
    ok = FALSE;
  }

  if (klass->get_size_info == NULL) {
    g_printerr ("Method GuppiData::get_size_info not defined.\n");
    ok = FALSE;
  }
  
  return ok;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_data_class_init (GuppiDataClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  klass->changed = changed;
  klass->changed_label = changed_label;
  klass->changed_subdata = changed_subdata;

  klass->validate_class = validate_class;

  data_signals[CHANGED] =
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GuppiDataClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);

  data_signals[CHANGED_LABEL] =
    g_signal_new ("changed_label",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GuppiDataClass, changed_label),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);

  data_signals[CHANGED_SUBDATA] =
    g_signal_new ("changed_subdata",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GuppiDataClass, changed_subdata),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);

  object_class->finalize = guppi_data_finalize;
}

static void
guppi_data_init (GuppiData *gd)
{
  gd->id = guppi_unique_id ();
  gd->label = guppi_strdup (_("Unlabled"));
  gd->read_only = FALSE;
}

GType 
guppi_data_get_type (void)
{
  static GType guppi_data_type = 0;

  if (!guppi_data_type) {
    static const GTypeInfo guppi_data_info = {
      sizeof (GuppiDataClass),
      NULL, NULL,
      (GClassInitFunc) guppi_data_class_init,
      NULL, NULL,
      sizeof (GuppiData),
      0,
      (GInstanceInitFunc) guppi_data_init
    };

    guppi_data_type = g_type_register_static (G_TYPE_OBJECT, "GuppiData", &guppi_data_info, 0);
  }

  return guppi_data_type;
}


/* $Id$ */

