/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-struct.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_STRUCT_H
#define _INC_GUPPI_STRUCT_H

/* #include <gtk/gtk.h> */

#include <libguppi/data/guppi-data.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiStruct GuppiStruct;
typedef struct _GuppiStructClass GuppiStructClass;

typedef void (*GuppiStructFn) (const gchar *name, GuppiData *data, gpointer closure);

struct _GuppiStruct {
  GuppiData parent;
};

struct _GuppiStructClass {
  GuppiDataClass parent_class;

  gboolean   (*add_field)  (GuppiStruct *, const gchar *, GtkType);
  GtkType    (*field_type) (GuppiStruct *, const gchar *);
  GuppiData *(*get)        (GuppiStruct *, const gchar *);
  gboolean   (*set)        (GuppiStruct *, const gchar *, GuppiData *);
  void       (*foreach)    (GuppiStruct *, GuppiStructFn, gpointer);
};

#define GUPPI_TYPE_STRUCT (guppi_struct_get_type())
#define GUPPI_STRUCT(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_STRUCT,GuppiStruct))
#define GUPPI_STRUCT0(obj) ((obj) ? (GUPPI_STRUCT(obj)) : NULL)
#define GUPPI_STRUCT_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_STRUCT,GuppiStructClass))
#define GUPPI_IS_STRUCT(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_STRUCT))
#define GUPPI_IS_STRUCT0(obj) (((obj) == NULL) || (GUPPI_IS_STRUCT(obj)))
#define GUPPI_IS_STRUCT_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_STRUCT))

GtkType guppi_struct_get_type (void);

GtkType guppi_struct_get_field_type (const GuppiStruct *, const gchar *);

GuppiData *guppi_struct_get (const GuppiStruct *, const gchar *);
void guppi_struct_set (GuppiStruct *, const gchar *, GuppiData *);

void guppi_struct_add_field (GuppiStruct *, const gchar *, GtkType);
void guppi_struct_add_free_field (GuppiStruct *, const gchar *);

void guppi_struct_foreach_subdata (GuppiStruct *,
				   void (*fn) (const gchar *, GuppiData *,
					       gpointer),
				   gpointer user_data);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_STRUCT_H */

/* $Id$ */
