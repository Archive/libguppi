/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-integer.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2002 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_INTEGER_H
#define _INC_GUPPI_SEQ_INTEGER_H

#include <libguppi/data/guppi-seq.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiSeqInteger GuppiSeqInteger;
typedef struct _GuppiSeqIntegerIface GuppiSeqIntegerIface;

struct _GuppiSeqIntegerIface {
  GTypeInterface interface;

  void (*range)       (GuppiSeqInteger *, int *min, int *max);
  int  (*frequency)   (GuppiSeqInteger *, int);
  int  (*get)         (GuppiSeqInteger *, int);
  void (*set_many)    (GuppiSeqInteger *, int i, const int *, int stride, gsize N);
  void (*insert_many) (GuppiSeqInteger *, int i, const int *ptr, int stride, gsize N);
};

#define GUPPI_TYPE_SEQ_INTEGER           (guppi_seq_integer_get_type())
#define GUPPI_SEQ_INTEGER(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_SEQ_INTEGER, GuppiSeqInteger))
#define GUPPI_IS_SEQ_INTEGER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_SEQ_INTEGER))
#define GUPPI_SEQ_INTEGER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_SEQ_INTEGER, GuppiSeqIntegerIface))

GType guppi_seq_integer_get_type (void);

gboolean guppi_seq_integer_is_read_only (GuppiSeqInteger *);
gboolean guppi_seq_integer_can_change   (GuppiSeqInteger *);

int guppi_seq_integer_get (GuppiSeqInteger *, int);

void guppi_seq_integer_set (GuppiSeqInteger *, int i, int val);
void guppi_seq_integer_set_many (GuppiSeqInteger *, int i, const int *, int stride, gsize N);

void guppi_seq_integer_prepend (GuppiSeqInteger *, int);
void guppi_seq_integer_prepend_many (GuppiSeqInteger *, const int *, int stride, gsize N);
void guppi_seq_integer_prepend_repeating (GuppiSeqInteger *, int val, gsize N);

void guppi_seq_integer_append (GuppiSeqInteger *, int);
void guppi_seq_integer_append_many (GuppiSeqInteger *, const int *, int stride, gsize N);
void guppi_seq_integer_append_repeating (GuppiSeqInteger *, int val, gsize N);

void guppi_seq_integer_insert (GuppiSeqInteger *, int i, int val);
void guppi_seq_integer_insert_many (GuppiSeqInteger *, int i, const int *, int stride, gsize N);

int guppi_seq_integer_min (GuppiSeqInteger *);
int guppi_seq_integer_max (GuppiSeqInteger *);

int guppi_seq_integer_frequency (GuppiSeqInteger *, int);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_INTEGER_H */

/* $Id$ */
