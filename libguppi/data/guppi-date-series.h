/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-date-series.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATE_SERIES_H
#define _INC_GUPPI_DATE_SERIES_H

/* #include <gnome.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/data/guppi-date-indexed.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiDateSeries GuppiDateSeries;
typedef struct _GuppiDateSeriesClass GuppiDateSeriesClass;

struct _GuppiDateSeries {
  GuppiDateIndexed parent;
};

struct _GuppiDateSeriesClass {
  GuppiDateIndexedClass parent_class;

  double   (*get)        (GuppiDateSeries *, const GDate *);
  void     (*set)        (GuppiDateSeries *, const GDate *, double);
  void     (*unset)      (GuppiDateSeries *, const GDate *);
  gboolean (*get_many)   (GuppiDateSeries *, const GDate *, gint, double *,
			  gint *retval);
  gboolean (*get_range)  (GuppiDateSeries *, const GDate *, const GDate *,
			  double *timecode, double *buffer, gint bufsize,
			  gint *retval);
  gboolean (*get_bounds) (GuppiDateSeries *, const GDate *, const GDate *,
			  double *, double *,
			  gboolean *retval);
};

#define GUPPI_TYPE_DATE_SERIES (guppi_date_series_get_type ())
#define GUPPI_DATE_SERIES(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATE_SERIES,GuppiDateSeries))
#define GUPPI_DATE_SERIES0(obj) ((obj) ? (GUPPI_DATE_SERIES(obj)) : NULL)
#define GUPPI_DATE_SERIES_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATE_SERIES,GuppiDateSeriesClass))
#define GUPPI_IS_DATE_SERIES(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATE_SERIES))
#define GUPPI_IS_DATE_SERIES0(obj) (((obj) == NULL) || (GUPPI_IS_DATE_SERIES(obj)))
#define GUPPI_IS_DATE_SERIES_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATE_SERIES))

GtkType guppi_date_series_get_type (void);

double guppi_date_series_get (GuppiDateSeries *ser, const GDate *date);
void guppi_date_series_set (GuppiDateSeries *ser, const GDate *date, double value);
void guppi_date_series_unset (GuppiDateSeries *ser, const GDate *date);

gint guppi_date_series_get_many (GuppiDateSeries *ser,
				 const GDate *base_date, gint count,
				 double *buffer);
gint guppi_date_series_get_range (GuppiDateSeries *ser,
				  const GDate *start_date, const GDate *end_date,
				  double *buffer, gint buffer_size);

gint guppi_date_series_get_range_timecoded (GuppiDateSeries *ser,
					    const GDate *start_date, const GDate *end_date,
					    double *timecodes,
					    double *buffer,
					    gint buffer_size);

gboolean guppi_date_series_get_bounds (GuppiDateSeries *ser,
				       const GDate *start_date, const GDate *end_date,
				       double *min, double *max);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATE_SERIES_H */

/* $Id$ */
