/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-data.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_H
#define _INC_GUPPI_DATA_H

#include <glib-object.h>
#include <libguppi/useful/guppi-unique-id.h>
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/useful/guppi-xml.h>
#include <libguppi/data/guppi-data-change.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiData GuppiData;
typedef struct _GuppiDataClass GuppiDataClass;
typedef struct _GuppiDataOp GuppiDataOp;

struct _GuppiDataOp {
  void (*op) (GuppiData *, GuppiDataOp *);
  GuppiDataChange *change;
};

typedef void (*GuppiDataFn) (GuppiData *data, gpointer closure);

struct _GuppiData {
  GObject parent;

  guppi_uniq_t id;
  gchar *label;
  gboolean read_only;

  gpointer pending_ops;
};

struct _GuppiDataClass {
  GObjectClass parent_class;

  gboolean read_only;

  /* virtual functions */
  GuppiData *(*copy) (GuppiData *);
  gint       (*get_size_in_bytes) (GuppiData *);
  gchar     *(*get_size_info) (GuppiData *);
  void       (*foreach_subdata) (GuppiData *, GuppiDataFn, gpointer closure);
  void       (*export_xml) (GuppiData *, GuppiXMLDocument *doc, xmlNodePtr node);
  gboolean   (*import_xml) (GuppiData *, GuppiXMLDocument *doc, xmlNodePtr node);

  gboolean class_validated;
  gboolean (*validate_class) (GuppiDataClass *);

  gchar *plug_in_code;

  /* signals */
  void (*changed) (GuppiData *, GuppiDataChange *);
  void (*changed_label) (GuppiData *, const char *new_label);
  void (*changed_subdata) (GuppiData *, GuppiDataChange *);
};

#define GUPPI_TYPE_DATA            (guppi_data_get_type())
#define GUPPI_DATA(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_DATA, GuppiData))
#define GUPPI_DATA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_DATA, GuppiDataClass))
#define GUPPI_IS_DATA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_DATA))
#define GUPPI_IS_DATA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA))

GType guppi_data_get_type (void);

GuppiData *guppi_data_new         (GType type);
GuppiData *guppi_data_new_by_name (const char *type_name);

guppi_uniq_t guppi_data_unique_id (GuppiData *data);

const gchar *guppi_data_get_label(GuppiData *data);
void         guppi_data_set_label (GuppiData *data, const gchar *label);

gboolean guppi_data_is_read_only (GuppiData *data);
gboolean guppi_data_can_change (GuppiData *data);

GuppiData *guppi_data_copy              (GuppiData *data);
gint       guppi_data_get_size_in_bytes (GuppiData *data);
gchar     *guppi_data_get_size_info     (GuppiData *data);


xmlNodePtr guppi_data_export_xml (GuppiData *data, GuppiXMLDocument *doc);
GuppiData *guppi_data_import_xml (GuppiXMLDocument *doc, xmlNodePtr node);
void       guppi_data_spew_xml   (GuppiData *data);


gboolean guppi_data_has_subdata     (GuppiData *data);
gint     guppi_data_subdata_count   (GuppiData *data);
void     guppi_data_foreach_subdata (GuppiData *data,
				     GuppiDataFn fn,
				     gpointer user_data);

/* Generate synthetic "changed" or "changed_subdata" events. */
void guppi_data_changed (GuppiData *data);
void guppi_data_changed_subdata (GuppiData *data);


/* Don't call this unless you _really_ know what you are doing */
void guppi_data_execute_op         (GuppiData *data, GuppiDataOp *op);
void guppi_data_execute_subdata_op (GuppiData *data, GuppiDataOp *op);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATA_H */

/* $Id$ */
