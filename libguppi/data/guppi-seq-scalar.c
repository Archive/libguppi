/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-scalar.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/data/guppi-seq-scalar.h>

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-string.h>

/* #include <gnome.h> */

static void
guppi_seq_scalar_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  while (! initialized) {
    /* Foo */
    initialized = TRUE;
  }
}

GType
guppi_seq_scalar_get_type (void)
{
  static GType seq_scalar_type = 0;

  if (! seq_scalar_type) {
    static const GTypeInfo info = {
      sizeof (GuppiSeqScalarIface),
      guppi_seq_scalar_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL
    };
    seq_scalar_type = g_type_register_static (G_TYPE_INTERFACE, "GuppiSeqScalar", &info, 0);
#ifdef WORKING_INTERFACE_PREREQS
    g_type_interface_add_prerequisite (seq_scalar_type, GUPPI_TYPE_SEQ);
#endif
  }

  return seq_scalar_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_seq_scalar_is_read_only (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), TRUE);
  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);

  return iface->set_many == NULL || iface->insert_many == NULL;
}

gboolean
guppi_seq_scalar_can_change (GuppiSeqScalar *seq)
{
  return ! guppi_seq_scalar_is_read_only (seq);
}
/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/**
 * guppi_seq_scalar_get
 * @seq: The scalar sequence
 * @i: The position index
 *
 * Returns the value of the @i-th element of @seq.
 * @i must lie within @seq's bounds.
 */
double
guppi_seq_scalar_get (GuppiSeqScalar *seq, gint i)
{
  GuppiSeqScalarIface *iface;
  
  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->get);

  return iface->get (seq, i);
}

/**
 * guppi_seq_scalar_set
 * @seq: The scalar sequence
 * @i: The position index
 * @val: A scalar value
 *
 * Sets the @i-th element of @seq equal to @val.
 * @seq must be writeable, and @i must lie within the
 * sequence's bounds.
 */
void
guppi_seq_scalar_set (GuppiSeqScalar *seq, gint i, double val)
{
  GuppiSeqScalarIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_SCALAR (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_scalar_can_change (seq));
  g_return_if_fail (guppi_seq_in_bounds (GUPPI_SEQ (seq), i));

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->set_many);

  iface->set_many (seq, i, &val, 0, 1);
}

/**
 * guppi_seq_scalar_set_many
 * @seq: The scalar sequence
 * @start: The first position to be set in @seq
 * @vals: A pointer to a scalar value
 * @stride: The stride to use while iterating across the memory
 * @N: The number of scalar values to copy from @vals to @seq
 *
 * Copies scalar values from memory into @seq, overwriting the
 * existing values.  If all @N values don't fit within the sequence's
 * existing bounds, it will grow to accomodate the data.
 * @sec must be writeable.
 */
void
guppi_seq_scalar_set_many (GuppiSeqScalar *seq, gint start,
			   const double *vals, gint stride, gsize N)
{
  GuppiSeqScalarIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_SCALAR (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_scalar_can_change (seq));

  if (N == 0)
    return;

  g_return_if_fail (vals != NULL);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->set_many);

  iface->set_many (seq, start, vals, stride, N);
}

/**
 * guppi_seq_scalar_prepend
 * @seq: The scalar sequence
 * @val: A scalar value
 *
 * Prepend the value @val to @seq.
 * The sequence's lower bound will grow downward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_prepend (GuppiSeqScalar *seq, double val)
{
  gint first;

  g_return_if_fail (GUPPI_IS_SEQ (seq));

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_scalar_insert (seq, first, val);
}

/**
 * guppi_seq_scalar_prepend_many
 * @seq: The scalar sequence
 * @vals: A pointer to the first value to prepend
 * @stride: The stride to use while interating across the memory
 * @N: The number of scalar values to read from memory and prepend to @seq.
 *
 * Prepend a sequences of @N scalar values starting at @ptr to @seq.
 * The sequence's lower bound will grow downward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_prepend_many (GuppiSeqScalar *seq,
			       const double *vals, gint stride, gsize N)
{
  gint first;

  g_return_if_fail (GUPPI_IS_SEQ (seq));

  first = guppi_seq_min_index (GUPPI_SEQ (seq));
  guppi_seq_scalar_insert_many (seq, first, vals, stride, N);
}

/**
 * guppi_seq_scalar_prepend_repeating
 * @seq: The scalar sequence
 * @val: A scalar value
 * @N: A count
 *
 * Prepend the sequences of @N occurances of @val to @seq.
 * The sequence's lower bound will grow downward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_prepend_repeating (GuppiSeqScalar *seq, double val, gsize N)
{
  guppi_seq_scalar_prepend_many (seq, &val, 0, N);
}

/**
 * guppi_seq_scalar_append
 * @seq: The scalar sequence
 * @val: A scalar value
 *
 * Append the value @val to @seq.
 * The sequence's upper bound will grow upward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_append (GuppiSeqScalar *seq, double val)
{
  gint last;
  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_scalar_insert (seq, last + 1, val);
}

/**
 * guppi_seq_scalar_append_many
 * @seq: The scalar sequence
 * @vals: A pointer to the first value to append
 * @stride: The stride to use while interating across the memory
 * @N: The number of scalar values to read from memory and prepend to @seq.
 *
 * Append the sequences of @N scalar values starting at @ptr to @seq.
 * The sequence's upper bound will grow upward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_append_many (GuppiSeqScalar *seq,
			      const double *vals, gint stride, gsize N)
{
  gint last;

  g_return_if_fail (GUPPI_IS_SEQ (seq));

  last = guppi_seq_max_index (GUPPI_SEQ (seq));
  guppi_seq_scalar_insert_many (seq, last + 1, vals, stride, N);
}

/**
 * guppi_seq_scalar_append_repeating
 * @seq: The scalar sequence
 * @val: A scalar value
 * @N: A count
 *
 * Append a sequences of @N occurances of @val to @seq.
 * The sequence's upper bound will grow upward.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_append_repeating (GuppiSeqScalar *seq, double val, gsize N)
{
  guppi_seq_scalar_append_many (seq, &val, 0, N);
}

/**
 * guppi_seq_scalar_insert
 * @seq: The scalar sequence
 * @i: The position index to insert at
 * @val: The value to insert
 *
 * Inserts the element @val into @seq at position @i.
 */
void
guppi_seq_scalar_insert (GuppiSeqScalar *seq, gint i, double val)
{
  GuppiSeqScalarIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_SCALAR (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_scalar_can_change (seq));

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->insert_many);

  iface->insert_many (seq, i, &val, 0, 1);
}

/**
 * guppi_seq_scalar_insert_many
 * @seq: The scalar sequence
 * @i: The position the first element is to be inserted at
 * @vals: A pointer to the first value to insert
 * @stride: The stride to use while interating across the memory
 * @N: The number of scalar values to read from memory and inserted into @seq.
 *
 * Insert the sequence of @N scalar values starting at @ptr into @seq.
 * The first element of the inserted sequence becomes the @i-th element of @seq.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_insert_many (GuppiSeqScalar *seq, gint i,
			      const double *vals, gint stride, gsize N)
{
  GuppiSeqScalarIface *iface;

  g_return_if_fail (GUPPI_IS_SEQ_SCALAR (seq));
  g_return_if_fail (guppi_data_can_change (GUPPI_DATA (seq)));
  g_return_if_fail (guppi_seq_scalar_can_change (seq));

  if (N == 0)
    return;

  g_return_if_fail (vals != NULL);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->insert_many);

  iface->insert_many (seq, i, vals, stride, N);
}

/**
 * guppi_seq_scalar_insert_repeating
 * @seq: The scalar sequence
 * @i: The position to begin inserting @val at
 * @val: A scalar value
 * @N: A count
 *
 * Insert @N occurances of @val into @seq,
 * starting at position @i.
 * @seq must be writeable.
 */
void
guppi_seq_scalar_insert_repeating (GuppiSeqScalar *seq, gint i,
				   double x, gsize N)
{
  guppi_seq_scalar_insert_many (seq, i, &x, 0, N);
}

/**
 * guppi_seq_scalar_min
 * @seq: A scalar sequence
 *
 * Finds the minimum value of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_min() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the smallest scalar element of @seq.
 */
double
guppi_seq_scalar_min (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;
  double min;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->range);

  iface->range (seq, &min, NULL);

  return min;
}

/**
 * guppi_seq_scalar_max
 * @seq: A scalar sequence
 *
 * Finds the maximum value of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_max() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the largest scalar element of @seq.
 */
double
guppi_seq_scalar_max (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;
  double max;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->range);

  iface->range (seq, NULL, &max);

  return max;
}

/**
 * guppi_seq_scalar_sum
 * @seq: A scalar sequence
 *
 * Finds the sum of all of the values in the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_sum() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the sum of the elements of @seq.
 */
double
guppi_seq_scalar_sum (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);

  if (guppi_seq_empty (GUPPI_SEQ (seq)))
    return 0;

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->sum);

  return iface->sum (seq);
}

/**
 * guppi_seq_scalar_sum_abs
 * @seq: A scalar sequence
 *
 * Finds the sum of the absolute values of all of the elements of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_sum_abs() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the sum of the absolute values of the elements of @seq.
 */
double
guppi_seq_scalar_sum_abs (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);

  if (guppi_seq_empty (GUPPI_SEQ (seq)))
    return 0;

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->sum_abs);

  return iface->sum_abs (seq);
}

/**
 * guppi_seq_scalar_mean
 * @seq: A scalar sequence
 *
 * Finds the mean (or average) of all of the values in the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_mean() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the mean of the elements of @seq.
 */
double
guppi_seq_scalar_mean (GuppiSeqScalar *seq)
{
  gsize n;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);

  n = guppi_seq_size (GUPPI_SEQ (seq));
  g_return_val_if_fail (n > 0, 0);

  return guppi_seq_scalar_sum (seq) / n;
}

/**
 * guppi_seq_scalar_var
 * @seq: A scalar sequence
 *
 * Finds the population variance of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_var() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the population variance of the elements of @seq.
 */
double
guppi_seq_scalar_var (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->var);

  return iface->var (seq);
}

/**
 * guppi_seq_scalar_vars
 * @seq: A scalar sequence
 *
 * Finds the sample variance of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_vars() repeatedly.
 * @seq must contain at least two elements.
 *
 * Returns the population variance of the elements of @seq.
 */
double
guppi_seq_scalar_vars (GuppiSeqScalar *seq)
{
  double v;
  gsize n;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);

  n = guppi_seq_size (GUPPI_SEQ (seq));
  g_return_val_if_fail (n > 1, 0);

  v = guppi_seq_scalar_var (seq);
  return (n * v) / (n - 1);
}

/**
 * guppi_seq_scalar_sdev
 * @seq: A scalar sequence
 *
 * Finds the population standard deviation of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_sdev() repeatedly.
 * @seq must be non-empty.
 *
 * Returns the population standard deviation of the elements of @seq.
 */
double
guppi_seq_scalar_sdev (GuppiSeqScalar *seq)
{
  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  return sqrt (guppi_seq_scalar_var (seq));
}

/**
 * guppi_seq_scalar_sdevs
 * @seq: A scalar sequence
 *
 * Finds the sample standard deviation of the sequence.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_sdevs() repeatedly.
 * @seq must contain at least two elements.
 *
 * Returns the population standard deviation of the elements of @seq.
 */
double
guppi_seq_scalar_sdevs (GuppiSeqScalar *seq)
{
  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  return sqrt (guppi_seq_scalar_vars (seq));
}

/**
 * guppi_seq_scalar_q1
 * @seq: A scalar sequence
 *
 * Computes the first quartile (Q1) of the elements of @seq.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_q1() repeatedly.
 * @seq must be non-empty.
 *
 * Returns: Q1 of @seq.
 */
double
guppi_seq_scalar_q1 (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;
  double q1;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->quartiles);

  iface->quartiles (seq, &q1, NULL, NULL);
  return q1;
}

/**
 * guppi_seq_scalar_median
 * @seq: A scalar sequence
 *
 * Computes the median of the elements of @seq.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_median() repeatedly.
 * @seq must be non-empty.
 *
 * Returns: the median of @seq.
 */
double
guppi_seq_scalar_median (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;
  double med;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->quartiles);

  iface->quartiles (seq, NULL, &med, NULL);
  return med;
}

/**
 * guppi_seq_scalar_q3
 * @seq: A scalar sequence
 *
 * Computes the third quartile (Q3) of the elements of @seq.
 * Missing values are ignored.
 * The value is cached, so it is not inefficient to call 
 * guppi_seq_scalar_q3() repeatedly.
 * @seq must be non-empty.
 *
 * Returns: Q3 of @seq.
 */
double
guppi_seq_scalar_q3 (GuppiSeqScalar *seq)
{
  GuppiSeqScalarIface *iface;
  double q3;
  
  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->quartiles);

  iface->quartiles (seq, NULL, NULL, &q3);
  return q3;
}

/**
 * guppi_seq_scalar_percentile
 * @seq: A scalar sequence
 * @p: A percentile value between 0 and 1.
 *
 * Computes the value at the @p-th percentile of the elements of @seq,
 * interpolating between elements as necessary.
 * Missing values are ignored.
 * @seq must be non-empty.
 *
 * Returns: the @p-th percentile of @seq.
 */
double
guppi_seq_scalar_percentile (GuppiSeqScalar *seq, double p)
{
  GuppiSeqScalarIface *iface;
  double x;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), 0);
  g_return_val_if_fail (guppi_seq_nonempty (GUPPI_SEQ (seq)), 0);
  g_return_val_if_fail (0 <= p && p <= 1, 0);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);
  g_assert (iface->percentile);

  iface->percentile (seq, p, &x);

  return x;
}

/**
 * guppi_seq_scalar_raw
 * @seq: A scalar sequence
 * @stride: A variable to store the required memory stride
 *
 * For certain #GuppiSeqScalar implementations, the data will be
 * stored in an array in the machine's memory.  If this is the case
 * for @seq, guppi_seq_scalar_raw() provides a pointer and a stride
 * that can be used to read the data directly, which is much more
 * efficiently that using repeatedly calling guppi_seq_scalar_get().
 *
 * The returned pointer and the value in @stride can be combined and
 * used to find the value of a particular element of @seq by
 * using guppi_seq_scalar_raw_get().
 *
 * If this feature is not supported by @seq's particular implementation,
 * %NULL is returned.
 *
 * Returns: a pointer into memory that can be used to retrieve
 * sequence values directly.
 */
const double *
guppi_seq_scalar_raw (GuppiSeqScalar *seq, gint *stride)
{
  GuppiSeqScalarIface *iface;

  g_return_val_if_fail (GUPPI_IS_SEQ_SCALAR (seq), NULL);
  g_return_val_if_fail (stride != NULL, NULL);

  iface = GUPPI_SEQ_SCALAR_GET_IFACE (seq);

  if (iface->raw_access == NULL)
    return NULL;

  return iface->raw_access (seq, stride);
}

/* This is kind of funny, embedding the docs for the macro here... */

/**
 * guppi_seq_scalar_raw_get
 * @ptr: The pointer returns by guppi_seq_scalar_raw()
 * @str: The stride returned (in an argument) by guppi_seq_scalar_raw()
 * @i: The position index
 *
 * This macro can be used to extract the elements of a sequence from
 * memory very efficiently, using the data return from a call to
 * guppi_seq_scalar_raw().
 */

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* $Id$ */
