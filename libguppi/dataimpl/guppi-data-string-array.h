/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-string-array.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_STRING_ARRAY_H__
#define __GUPPI_DATA_STRING_ARRAY_H__

#include <libguppi/dataimpl/guppi-data-generic-array.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataStringArray      GuppiDataStringArray;
typedef struct _GuppiDataStringArrayClass GuppiDataStringArrayClass;

struct _GuppiDataStringArray {
  GuppiDataGenericArray parent;
};

struct _GuppiDataStringArrayClass {
  GuppiDataGenericArrayClass parent_class;
};

#define GUPPI_TYPE_DATA_STRING_ARRAY            (guppi_data_string_array_get_type())
#define GUPPI_DATA_STRING_ARRAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_DATA_STRING_ARRAY, GuppiDataStringArray))
#define GUPPI_DATA_STRING_ARRAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_DATA_STRING_ARRAY, GuppiDataStringArrayClass))
#define GUPPI_IS_DATA_STRING_ARRAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_DATA_STRING_ARRAY))
#define GUPPI_IS_DATA_STRING_ARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_STRING_ARRAY))

GType guppi_data_string_array_get_type (void);

END_GUPPI_DECLS;

#endif /* __GUPPI_DATA_STRING_ARRAY_H__ */

