/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-generic-seq.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-data-generic-array.h>

#include <string.h>
#include <libguppi/data/guppi-seq.h>
#include <libguppi/data/guppi-seq-change.h>

static GObjectClass *parent_class;
/* An implementation of the GuppiSeq interface. */

static void
size_hint (GuppiSeq *seq, gsize N)
{
  GuppiDataGenericArray *array = GUPPI_DATA_GENERIC_ARRAY (seq);
  gsize orig_len = array->data->len;
  
  if (orig_len < N) {
    g_array_set_size (array->data, N);
    g_array_set_size (array->data, orig_len);
  }
}

static void
get_bounds (GuppiSeq *seq, int *min, int *max)
{
  GuppiDataGenericArray *array = GUPPI_DATA_GENERIC_ARRAY (seq);

  if (min)
    *min = array->index_basis;
  
  if (max)
    *max = array->index_basis + (array->data ? array->data->len : 0) - 1;
}

typedef struct _GuppiDataOp_Seq GuppiDataOp_Seq;
struct _GuppiDataOp_Seq {
  GuppiDataOp op;
  int i;
  gsize N;
};

static void
shift_indices_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataGenericArray *array = GUPPI_DATA_GENERIC_ARRAY (data);
  GuppiDataOp_Seq *seq_op = (GuppiDataOp_Seq *) op;

  array->index_basis += seq_op->i;
}

static void
shift_indices (GuppiSeq *seq, int delta)
{
  GuppiDataOp_Seq op;
  GuppiSeqChange_Shift change;

  guppi_seq_change_shift_init (&change);
  change.shift = delta;

  op.op.op = shift_indices_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = delta;
  
  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static void
insert_generic_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataOp_Seq *seq_op = (GuppiDataOp_Seq *) op;
  guppi_data_generic_array_insert (GUPPI_DATA_GENERIC_ARRAY (data), seq_op->i, seq_op->N, TRUE);
}

static void
insert_generic (GuppiSeq *seq, int i, gsize N)
{
  GuppiDataOp_Seq op;
  GuppiSeqChange_Insert change;

  guppi_seq_change_insert_init (&change);
  change.i = i;
  change.N = N;

  op.op.op = insert_generic_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.N = N;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);  
}

static void
delete_many_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataOp_Seq *seq_op = (GuppiDataOp_Seq *) op;
  guppi_data_generic_array_delete (GUPPI_DATA_GENERIC_ARRAY (data), seq_op->i, seq_op->N);
}

static void
delete_many (GuppiSeq *seq, int i, gsize N)
{
  GuppiDataOp_Seq op;
  GuppiSeqChange_Delete change;

  guppi_seq_change_delete_init (&change);
  change.i = i;
  change.N = N;

  op.op.op = delete_many_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.N = N;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static xmlNodePtr
export_xml_element (GuppiSeq *seq, int i, GuppiXMLDocument *doc)
{
  g_assert_not_reached ();

  return NULL;
}

static gboolean
import_xml_element (GuppiSeq *seq, GuppiXMLDocument *doc, xmlNodePtr node)
{
  g_assert_not_reached ();

  return FALSE;
}

static void
gdga_seq_init (GuppiSeqIface *iface)
{
  iface->size_hint = size_hint;
  iface->get_bounds = get_bounds;
  iface->shift_indices = shift_indices;
  iface->insert_generic = insert_generic;
  iface->delete_many = delete_many;

  iface->export_xml_element = export_xml_element;
  iface->import_xml_element = import_xml_element;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static int
get_size_in_bytes (GuppiData *data)
{
  GuppiDataGenericArray *array = (GuppiDataGenericArray *) data;
  GuppiDataGenericArrayClass *klass;
  int total = 0;

  if (array->data) {
    klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (data);
    total +=  klass->elt_size * array->data->len;
  }
  
  total += sizeof (GuppiDataGenericArray);
  total += sizeof (GArray);

  return total;
}

static char *
get_size_info (GuppiData *data)
{
  int min, max;
  get_bounds (GUPPI_SEQ (data), &min, &max);

  if (min <= max)
    return g_strdup_printf ("%d : %d", min, max);
  else
    return g_strdup ("(empty)");
}

static void
guppi_data_generic_array_finalize (GObject *obj)
{
  GuppiDataGenericArray *array = (GuppiDataGenericArray *) obj;
  GuppiDataGenericArrayClass *klass;

  klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (obj);

  if (array->data) {

    if (klass->destroy_elts)
      klass->destroy_elts (array->data->data, array->data->len);

    g_array_free (array->data, TRUE);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

void
guppi_data_generic_array_class_init (GuppiDataGenericArrayClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiDataClass *data_class = (GuppiDataClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  data_class->get_size_in_bytes = get_size_in_bytes;
  data_class->get_size_info = get_size_info;

  object_class->finalize = guppi_data_generic_array_finalize;
}

void
guppi_data_generic_array_init (GuppiDataGenericArray *array)
{

}

GType
guppi_data_generic_array_get_type (void)
{
  static GType gdga_type = 0;

  if (!gdga_type) {
    
    static const GTypeInfo gdga_info = {
      sizeof(GuppiDataGenericArrayClass),
      NULL, NULL,
      (GClassInitFunc) guppi_data_generic_array_class_init,
      NULL, NULL,
      sizeof (GuppiDataGenericArray),
      0,
      (GInstanceInitFunc) guppi_data_generic_array_init
    };

    static const GInterfaceInfo seq_info = {
      (GInterfaceInitFunc) gdga_seq_init,
      NULL, NULL
    };
    

    gdga_type = g_type_register_static (GUPPI_TYPE_DATA, "GuppiDataGenericArray",
					&gdga_info, 0);

    g_type_add_interface_static (gdga_type, GUPPI_TYPE_SEQ, &seq_info);

  }


  return gdga_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
init_array_data (GuppiDataGenericArray *array)
{
  GuppiDataGenericArrayClass *klass;

  if (array->data == NULL) {
    klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (array);
    array->data = g_array_sized_new (FALSE, FALSE, klass->elt_size, 16);
  }
}

void
guppi_data_generic_array_insert (GuppiDataGenericArray *array, int i, gsize N,
				 gboolean initialize)
{
  GuppiDataGenericArrayClass *klass;
  gsize elt_size;
  gpointer p1, p2;
  gboolean in_bounds;

  g_return_if_fail (GUPPI_IS_DATA_GENERIC_ARRAY (array));

  if (N == 0)
    return;
  
  klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (array);
  elt_size = klass->elt_size;

  init_array_data (array);

  in_bounds = array->data->len
    && array->index_basis <= i
    && i < array->index_basis + array->data->len;

  g_array_set_size (array->data, array->data->len + N);

  p1 = array->data->data + (i - array->index_basis) * elt_size;
  p2 = array->data->data + (i + N - array->index_basis) * elt_size;

  if (in_bounds) {
    memmove (p2, p1, (array->data->len - N - (i - array->index_basis)) * elt_size);
  }

  if (initialize) {
    if (klass->put_generic) {
      klass->put_generic (p1, N);
    } else {
      memset (p1, 0, N * elt_size);
    }
  }
}

void
guppi_data_generic_array_delete (GuppiDataGenericArray *array, int i, gsize N)
{
  GuppiDataGenericArrayClass *klass;
  gsize elt_size;
  gpointer p1, p2;

  g_return_if_fail (GUPPI_IS_DATA_GENERIC_ARRAY (array));

  if (N == 0 || array->data == NULL)
    return;
  
  klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (array);
  elt_size = klass->elt_size;

  p1 = array->data->data + (i - array->index_basis) * elt_size;
  p2 = array->data->data + (i + N - array->index_basis) * elt_size;

  if (klass->destroy_elts)
    klass->destroy_elts (p1, N);

  memmove (p1, p2, (array->data->len - N - (i - array->index_basis)) * elt_size);
  g_array_set_size (array->data, array->data->len - N);
}

void
guppi_data_generic_array_copy (GuppiDataGenericArray *dest, GuppiDataGenericArray *src)
{
  GuppiDataGenericArrayClass *klass;

  g_return_if_fail (GUPPI_IS_DATA_GENERIC_ARRAY (dest));
  g_return_if_fail (GUPPI_IS_DATA_GENERIC_ARRAY (src));
  g_return_if_fail (G_OBJECT_GET_CLASS (dest) == G_OBJECT_GET_CLASS (src));

  if (dest == src)
    return;

  klass = (GuppiDataGenericArrayClass *) G_OBJECT_GET_CLASS (dest);

  if (dest->data && dest->data->len && klass->destroy_elts) {
    klass->destroy_elts (dest->data->data, dest->data->len);
  }

  init_array_data (dest);

  dest->index_basis = src->index_basis;
  g_array_set_size (dest->data, src->data ? src->data->len : 0);

  if (src->data && src->data->len) {

    if (klass->copy_elts) {
      klass->copy_elts (dest->data->data, src->data->data, dest->data->len);
    } else {
      memcpy (dest->data->data, src->data->data, dest->data->len * klass->elt_size);
    }
  }
}
