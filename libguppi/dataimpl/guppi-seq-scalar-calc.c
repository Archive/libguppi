/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-scalar-calc-impl.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-seq-scalar-calc.h>

#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0,
  ARG_FN,
  ARG_FN_C,
  ARG_FN_C_USER_DATA,
  ARG_MIN_INDEX,
  ARG_MAX_INDEX
};

static void
guppi_seq_scalar_calc_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_seq_scalar_calc_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  GuppiSeqScalarCalc *calc = GUPPI_SEQ_SCALAR_CALC (obj);
  GuppiFnWrapper *fn_wrap;
  double (*fn)(gint, gpointer);
  gint i;


  switch (arg_id) {

  case ARG_FN:
    fn_wrap = GUPPI_FN_WRAPPER (GTK_VALUE_POINTER (*arg));
    if (fn_wrap != calc->fn_wrapper) {
      guppi_refcounting_assign (calc->fn_wrapper, fn_wrap);
      guppi_data_changed (GUPPI_DATA (obj));
    }
    break;
    
  case ARG_FN_C:
    fn = (double(*)(gint,gpointer)) GTK_VALUE_POINTER (*arg);
    if (calc->fn != fn) {
      calc->fn = fn;
      guppi_data_changed (GUPPI_DATA (obj));
    }
    break;

  case ARG_FN_C_USER_DATA:
    if (calc->user_data != GTK_VALUE_POINTER (*arg)) {
      calc->user_data = GTK_VALUE_POINTER (*arg);
      guppi_data_changed (GUPPI_DATA (obj));
    }
    break;
    
  case ARG_MIN_INDEX:
    i = GTK_VALUE_INT (*arg);
    if (calc->i0 != i) {
      calc->i0 = i;
      guppi_data_changed (GUPPI_DATA (obj));
    }
    break;

  case ARG_MAX_INDEX:
    i = GTK_VALUE_INT (*arg);
    if (calc->i1 != i) {
      calc->i1 = i;
      guppi_data_changed (GUPPI_DATA (obj));
    }
    break;

  default:
    break;
  };
}

static void
guppi_seq_scalar_calc_finalize (GtkObject *obj)
{
  GuppiSeqScalarCalc *calc = GUPPI_SEQ_SCALAR_CALC (obj);

  guppi_unref0 (calc->fn_wrapper)

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

/* no range */

/* no sum */

/* no var */

/* no quartiles */

static double
v_seq_scalar_get (GuppiSeqScalar *impl, gint i)
{
  GuppiSeqScalarCalc *calc = GUPPI_SEQ_SCALAR_CALC (impl);

  if (calc->fn_wrapper) 
    return guppi_fn_wrapper_eval_d__i (calc->fn_wrapper, i);
  else if (calc->fn)
    return calc->fn (i, calc->user_data);

  g_assert_not_reached ();

  return 0;
}

/* no set */

/* no insert or insert_many */

static gint
v_seq_scalar_range_query (GuppiSeqScalar *impl,
			  GuppiSeqBoolean *seq_bool,
			  double min, double max, gboolean do_and)
{
  g_assert_not_reached ();
  return 0;
}

/* no raw access */

/* no set */

static void
v_seq_size_hint (GuppiSeq *impl, gsize N)
{
  /* A total no-op */
}

static void
v_seq_get_bounds (GuppiSeq *impl, gint *i0, gint *i1)
{
  GuppiSeqScalarCalc *calc = GUPPI_SEQ_SCALAR_CALC (impl);

  if (i0)
    *i0 = calc->i0;

  if (i1)
    *i1 = calc->i1;
}

/* no delete or delete many */

/* no grow-to-include */

static GuppiData *
v_data_copy (GuppiData *impl)
{
  /* Unimplemented. */
  g_assert_not_reached ();
  return NULL;
}

static gint
v_data_size_in_bytes (GuppiData *impl)
{
  return sizeof (GuppiSeqScalarCalc);
}

#define add_arg(str, t, symb) \
gtk_object_add_arg_type("GuppiSeqScalarCalc::" str, t, GTK_ARG_WRITABLE, symb)

static void
guppi_seq_scalar_calc_class_init (GuppiSeqScalarCalcClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiSeqScalarClass *seq_scalar_class = GUPPI_SEQ_SCALAR_CLASS (klass);
  GuppiSeqClass *seq_class = GUPPI_SEQ_CLASS (klass);
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_SEQ_SCALAR);

  object_class->get_arg = guppi_seq_scalar_calc_get_arg;
  object_class->set_arg = guppi_seq_scalar_calc_set_arg;
  object_class->finalize = guppi_seq_scalar_calc_finalize;

  seq_scalar_class->get = v_seq_scalar_get;
  seq_scalar_class->range_query = v_seq_scalar_range_query;

  seq_class->size_hint = v_seq_size_hint;
  seq_class->get_bounds = v_seq_get_bounds;
  seq_class->has_missing = NULL;
  seq_class->missing = NULL;
  
  /* Override default sequence import/export... no replacement yet. */
  data_class->import_xml = NULL;
  data_class->export_xml = NULL;
  guppi_FIXME ();

  data_class->read_only = TRUE;
  data_class->copy = v_data_copy;
  data_class->get_size_in_bytes = v_data_size_in_bytes;

  add_arg ("function", GTK_TYPE_POINTER, ARG_FN);
  add_arg ("function_C", GTK_TYPE_POINTER, ARG_FN_C);
  add_arg ("function_data", GTK_TYPE_POINTER, ARG_FN_C_USER_DATA);
  add_arg ("min_index", GTK_TYPE_INT, ARG_MIN_INDEX);
  add_arg ("max_index", GTK_TYPE_INT, ARG_MAX_INDEX);
}

static void
guppi_seq_scalar_calc_init (GuppiSeqScalarCalc *obj)
{

}

GtkType
guppi_seq_scalar_calc_get_type (void)
{
  static GtkType guppi_seq_scalar_calc_type = 0;
  if (!guppi_seq_scalar_calc_type) {
    static const GtkTypeInfo guppi_seq_scalar_calc_info = {
      "GuppiSeqScalarCalc",
      sizeof (GuppiSeqScalarCalc),
      sizeof (GuppiSeqScalarCalcClass),
      (GtkClassInitFunc) guppi_seq_scalar_calc_class_init,
      (GtkObjectInitFunc) guppi_seq_scalar_calc_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_seq_scalar_calc_type =
      gtk_type_unique (GUPPI_TYPE_SEQ_SCALAR,
		       &guppi_seq_scalar_calc_info);
  }
  return guppi_seq_scalar_calc_type;
}

GuppiSeqScalar *
guppi_seq_scalar_calc_new (void)
{
  return GUPPI_SEQ_SCALAR (guppi_type_new (guppi_seq_scalar_calc_get_type ()));
}

/* $Id$ */
