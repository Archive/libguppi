/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-integer-array.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-data-integer-array.h>

#include <stdlib.h>
#include <math.h>

#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/data/guppi-seq-change.h>
#include <libguppi/data/guppi-seq-integer.h>
#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/data/guppi-seq-string.h>

static GObjectClass *parent_class = NULL;

#define PTR_STEP(x, n) ((int *)(((guint8 *)x)+n))

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Data Operations */

typedef struct _GuppiDataOp_SeqInteger GuppiDataOp_SeqInteger;
struct _GuppiDataOp_SeqInteger {
  GuppiDataOp op;

  int i;
  gsize N;
  
  int x;
  const int *in_ptr;
  int in_stride;
};

/* FIXME: Should be cached */
static void
seq_integer_range (GuppiSeqInteger *seq, int *min, int *max)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  int run_min = G_MAXINT, run_max = G_MININT;
  int i;
  
  for (i = 0; i < generic->data->len; ++i) {
    int x = g_array_index (generic->data, int, i);
    if (x < run_min)
      run_min = x;
    if (x > run_max)
      run_max = x;
  }

  if (min)
    *min = run_min;
  if (max)
    *max = run_max;
}

static int
seq_integer_frequency (GuppiSeqInteger *seq, int val)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  int count = 0;
  int i;
  
  for (i = 0; i < generic->data->len; ++i) {
    int x = g_array_index (generic->data, int, i);

    if (x == val)
      ++count;
  }

  return count;
}

static int
seq_integer_get (GuppiSeqInteger *seq, int i)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  int x = g_array_index (generic->data, int, i - generic->index_basis);
  return x;
}

static void
set_many_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) data;
  GuppiDataOp_SeqInteger *seq_op = (GuppiDataOp_SeqInteger *) op;

  gsize i;
  int *my_ptr;
  const int *ptr;

  my_ptr = ((int *) generic->data->data) + (seq_op->i - generic->index_basis);
  ptr = seq_op->in_ptr;

  for (i = 0; i < seq_op->N; ++i) {
    int x = *ptr;
    /* int old_value = *my_ptr; */

    *my_ptr = x;
    ++my_ptr;
    ptr = PTR_STEP (ptr, seq_op->in_stride);
  }
}

static void
seq_integer_set_many (GuppiSeqInteger *seq,
		      int start, 
		      const int *ptr, 
		      int stride, 
		      gsize N)
{
  GuppiDataOp_SeqInteger op;
  GuppiSeqChange_Set change;

  guppi_seq_change_set_init (&change);
  change.i0 = start;
  change.i1 = start+N-1;

  op.op.op = set_many_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = start;
  op.N = N;
  op.in_ptr = ptr;
  op.in_stride = stride;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static void
insert_many_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) data;
  GuppiDataOp_SeqInteger *seq_op = (GuppiDataOp_SeqInteger *) op;

  gsize i;
  int *my_ptr;
  const int *ptr;

  guppi_data_generic_array_insert (generic, seq_op->i, seq_op->N, FALSE);

  my_ptr = ((int *) generic->data->data) + (seq_op->i - generic->index_basis);
  ptr = seq_op->in_ptr;

  for (i = 0; i < seq_op->N; ++i) {
    /* FIXME */
  }
}

static void
seq_integer_insert_many (GuppiSeqInteger *seq,
			 int i, 
			 const int *ptr, 
			 int stride, 
			 gsize N)
{
  GuppiDataOp_SeqInteger op;
  GuppiSeqChange_Insert change;

  guppi_seq_change_insert_init (&change);
  change.i = i;
  change.N = N;

  op.op.op = insert_many_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.N = N;
  op.in_ptr = ptr;
  op.in_stride = stride;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static void
seq_integer_init (GuppiSeqIntegerIface *iface)
{
  iface->range       = seq_integer_range;
  iface->frequency   = seq_integer_frequency;
  iface->get         = seq_integer_get;
  iface->set_many    = seq_integer_set_many;
  iface->insert_many = seq_integer_insert_many;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* A read-only GuppiSeqScalar interface */

static void
seq_scalar_range (GuppiSeqScalar *seq, double *min, double *max)
{
  int imin, imax;
  seq_integer_range ((GuppiSeqInteger *) seq, &imin, &imax);
  
  if (min)
    *min = imin;
  if (max)
    *max = imax;
}

static double
seq_scalar_sum (GuppiSeqScalar *seq)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  int sum = 0;
  int i;
  int *p = (int *) generic->data->data;
  
  for (i = 0; i < generic->data->len; ++i) {
    sum += p[i];
  }

  return sum;
}

static double
seq_scalar_sum_abs (GuppiSeqScalar *seq)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  int sum = 0;
  int i;
  int *p = (int *) generic->data->data;
  
  for (i = 0; i < generic->data->len; ++i) {
    sum += abs (p[i]);
  }

  return sum;
}

static double
seq_scalar_var (GuppiSeqScalar *seq)
{ 
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  double x, om, mean = 0, sumsq = 0;
  gsize i, N = generic->data->len;
  int *p = (int *) generic->data->data;

  for (i = 0; i < N; ++i) {
    x = p[i];
    om = mean;
    mean += (x - mean) / (double) (i+1);
    if (i > 0)
      sumsq += (x - mean) * (x - om);
  }

  return sumsq / N;
}

static gboolean
seq_scalar_quartiles (GuppiSeqScalar *seq, double *q1, double *med, double *q3)
{
  guppi_FIXME ();
  return FALSE;
}

static gboolean
seq_scalar_percentile (GuppiSeqScalar *seq, double p, double *x)
{
  guppi_FIXME ();
  return FALSE;
}

static double
seq_scalar_get (GuppiSeqScalar *seq, int i)
{
  return seq_integer_get ((GuppiSeqInteger *) seq, i);
}

static void
seq_scalar_init (GuppiSeqScalarIface *iface)
{
  iface->range       = seq_scalar_range;
  iface->sum         = seq_scalar_sum;
  iface->sum_abs     = seq_scalar_sum_abs;
  iface->var         = seq_scalar_var;
  iface->quartiles   = seq_scalar_quartiles;
  iface->percentile  = seq_scalar_percentile;
  
  iface->get         = seq_scalar_get;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static const char *
seq_string_get (GuppiSeqString *seq, int i)
{
  return NULL;
}

static void
seq_string_set (GuppiSeqString *seq, int i, char *str)
{

}

static void
seq_string_insert (GuppiSeqString *seq, int i, char *str)
{

}

static void
seq_string_init (GuppiSeqStringIface *iface)
{
  iface->get = seq_string_get;
  iface->set = seq_string_set;
  iface->insert = seq_string_insert;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_data_integer_array_class_init (GuppiDataIntegerArrayClass *klass)
{
  GuppiDataGenericArrayClass *generic_array_class= (GuppiDataGenericArrayClass *) klass;

  parent_class = g_type_class_peek_parent (klass);
  
  generic_array_class->elt_size = sizeof (int);
  generic_array_class->put_generic = NULL;
}

static void
guppi_data_integer_array_init (GuppiDataIntegerArray *array)
{

}

GType
guppi_data_integer_array_get_type (void)
{
  static GType type = 0;

  if (!type) {
    
    static const GTypeInfo info = {
      sizeof(GuppiDataIntegerArrayClass),
      NULL, NULL,
      (GClassInitFunc) guppi_data_integer_array_class_init,
      NULL, NULL,
      sizeof (GuppiDataIntegerArray),
      0,
      (GInstanceInitFunc) guppi_data_integer_array_init
    };

    static const GInterfaceInfo seq_integer_info = {
      (GInterfaceInitFunc) seq_integer_init,
      NULL, NULL
    };

    static const GInterfaceInfo seq_scalar_info = {
      (GInterfaceInitFunc) seq_scalar_init,
      NULL, NULL
    };

    static const GInterfaceInfo seq_string_info = {
      (GInterfaceInitFunc) seq_string_init,
      NULL, NULL
    };
    
    type = g_type_register_static (GUPPI_TYPE_DATA_GENERIC_ARRAY, "GuppiDataIntegerArray",
				   &info, 0);

    g_type_add_interface_static (type, GUPPI_TYPE_SEQ_INTEGER, &seq_integer_info);    
    g_type_add_interface_static (type, GUPPI_TYPE_SEQ_SCALAR, &seq_scalar_info);    
    g_type_add_interface_static (type, GUPPI_TYPE_SEQ_STRING, &seq_string_info);


  }


  return type;
}


