/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-table-core.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATA_TABLE_CORE_H
#define _INC_GUPPI_DATA_TABLE_CORE_H

#include <gnome.h>
#include <libguppi/useful/guppi-defs.h>
#include <guppi-garray.h>
#include <libguppi/data/guppi-data-table.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataTableCore GuppiDataTableCore;
typedef struct _GuppiDataTableCoreClass GuppiDataTableCoreClass;

struct _GuppiDataTableCore {
  GuppiDataTable parent;

  gint R, C;
  GuppiGArray *data;
  GuppiGArray *row_labels;
  GuppiGArray *col_labels;
};

struct _GuppiDataTableCoreClass {
  GuppiDataTableClass parent_class;
};

#define GUPPI_TYPE_DATA_TABLE_CORE (guppi_data_table_core_get_type ())
#define GUPPI_DATA_TABLE_CORE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATA_TABLE_CORE,GuppiDataTableCore))
#define GUPPI_DATA_TABLE_CORE0(obj) ((obj) ? (GUPPI_DATA_TABLE_CORE(obj)) : NULL)
#define GUPPI_DATA_TABLE_CORE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATA_TABLE_CORE,GuppiDataTableCoreClass))
#define GUPPI_IS_DATA_TABLE_CORE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATA_TABLE_CORE))
#define GUPPI_IS_DATA_TABLE_CORE0(obj) (((obj) == NULL) || (GUPPI_IS_DATA_TABLE_CORE(obj)))
#define GUPPI_IS_DATA_TABLE_CORE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_TABLE_CORE))

GtkType guppi_data_table_core_get_type (void);

GuppiDataTable *guppi_data_table_core_new (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_DATA_TABLE_CORE_H */

/* $Id$ */
