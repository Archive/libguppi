/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-integer-core-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_INTEGER_CORE_H
#define _INC_GUPPI_SEQ_INTEGER_CORE_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <guppi-garray.h>
#include <libguppi/data/guppi-seq-integer.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiSeqIntegerCore GuppiSeqIntegerCore;
typedef struct _GuppiSeqIntegerCoreClass GuppiSeqIntegerCoreClass;

struct _GuppiSeqIntegerCore {
  GuppiSeqInteger parent;

  gint index_basis;
  gsize size;
  GuppiGArray *garray;

  gboolean cached_minmax;
  gint min, max;
};

struct _GuppiSeqIntegerCoreClass {
  GuppiSeqIntegerClass parent_class;
};

#define GUPPI_TYPE_SEQ_INTEGER_CORE (guppi_seq_integer_core_get_type())
#define GUPPI_SEQ_INTEGER_CORE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_INTEGER_CORE,GuppiSeqIntegerCore))
#define GUPPI_SEQ_INTEGER_CORE0(obj) ((obj) ? (GUPPI_SEQ_INTEGER_CORE(obj)) : NULL)
#define GUPPI_SEQ_INTEGER_CORE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_INTEGER_CORE,GuppiSeqIntegerCoreClass))
#define GUPPI_IS_SEQ_INTEGER_CORE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_INTEGER_CORE))
#define GUPPI_IS_SEQ_INTEGER_CORE0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_INTEGER_CORE(obj)))
#define GUPPI_IS_SEQ_INTEGER_CORE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_INTEGER_CORE))

GtkType guppi_seq_integer_core_get_type (void);

GuppiSeqInteger *guppi_seq_integer_core_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_INTEGER_CORE_H */

/* $Id$ */
