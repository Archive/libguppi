/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-price-series-core-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PRICE_SERIES_CORE_IMPL_H
#define _INC_GUPPI_PRICE_SERIES_CORE_IMPL_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <guppi-garray.h>
#include <libguppi/data/guppi-price-series.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiPriceSeriesCore GuppiPriceSeriesCore;
typedef struct _GuppiPriceSeriesCoreClass GuppiPriceSeriesCoreClass;

struct _GuppiPriceSeriesCore {
  GuppiPriceSeries parent;

  GDate start_date, end_date;
  gsize size;
  GuppiGArray *garray;
};

struct _GuppiPriceSeriesCoreClass {
  GuppiPriceSeriesClass parent_class;
};

#define GUPPI_TYPE_PRICE_SERIES_CORE (guppi_price_series_core_get_type ())
#define GUPPI_PRICE_SERIES_CORE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PRICE_SERIES_CORE,GuppiPriceSeriesCore))
#define GUPPI_PRICE_SERIES_CORE0(obj) ((obj) ? (GUPPI_PRICE_SERIES_CORE(obj)) : NULL)
#define GUPPI_PRICE_SERIES_CORE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PRICE_SERIES_CORE,GuppiPriceSeriesCoreClass))
#define GUPPI_IS_PRICE_SERIES_CORE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PRICE_SERIES_CORE))
#define GUPPI_IS_PRICE_SERIES_CORE0(obj) (((obj) == NULL) || (GUPPI_IS_PRICE_SERIES_CORE(obj)))
#define GUPPI_IS_PRICE_SERIES_CORE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PRICE_SERIES_CORE))

GtkType guppi_price_series_core_get_type (void);

GuppiPriceSeries *guppi_price_series_core_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PRICE_SERIES_CORE_H */

/* $Id$ */
