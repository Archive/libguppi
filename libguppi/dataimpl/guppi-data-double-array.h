/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-double-array.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_DOUBLE_ARRAY_H__
#define __GUPPI_DATA_DOUBLE_ARRAY_H__

#include <libguppi/dataimpl/guppi-data-generic-array.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataDoubleArray      GuppiDataDoubleArray;
typedef struct _GuppiDataDoubleArrayClass GuppiDataDoubleArrayClass;

struct _GuppiDataDoubleArray {
  GuppiDataGenericArray parent;

  gint ordering;
  double min, max;
  double sum, sum_abs;
  double var;
  double q1, median, q3;

  gpointer sorted_copy;

  guint have_ordering : 1;
  guint have_minmax : 1;
  guint have_sum : 1;
  guint have_sum_abs : 1;
  guint have_var : 1;
  guint have_quartiles : 1;

  guint save_ordering : 1;
  guint save_minmax : 1;
  guint save_sum : 1;
  guint save_sum_abs : 1;
};

struct _GuppiDataDoubleArrayClass {
  GuppiDataGenericArrayClass parent_class;
};

#define GUPPI_TYPE_DATA_DOUBLE_ARRAY            (guppi_data_double_array_get_type())
#define GUPPI_DATA_DOUBLE_ARRAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_DATA_DOUBLE_ARRAY, GuppiDataDoubleArray))
#define GUPPI_DATA_DOUBLE_ARRAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_DATA_DOUBLE_ARRAY, GuppiDataDoubleArrayClass))
#define GUPPI_IS_DATA_DOUBLE_ARRAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_DATA_DOUBLE_ARRAY))
#define GUPPI_IS_DATA_DOUBLE_ARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_DOUBLE_ARRAY))

GType guppi_data_double_array_get_type (void);

END_GUPPI_DECLS;

#endif /* __GUPPI_DATA_DOUBLE_ARRAY_H__ */

