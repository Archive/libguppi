/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-date-series-calc-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_DATE_SERIES_CALC_H
#define _INC_GUPPI_DATE_SERIES_CALC_H

/* #include <gnome.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/data/guppi-date-series.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiDateSeriesCalc GuppiDateSeriesCalc;
typedef struct _GuppiDateSeriesCalcClass GuppiDateSeriesCalcClass;

struct _GuppiDateSeriesCalc {
  GuppiDateSeries parent;

  void (*bounds) (GDate *start, GDate *end, gpointer user_data);
  gboolean (*valid) (const GDate *dt, gpointer user_data);
  double (*get) (const GDate *, gpointer user_data);

  gint (*get_many) (const GDate *dt, gint count, double *buf, gpointer user_data);
  gint (*get_range) (const GDate *sd, const GDate *ed, double *tbuf, double *buf, gint bufsize,
		     gpointer user_data);
  gboolean (*get_bounds) (const GDate *sd, const GDate *ed, double *min, double *max,
			  gpointer user_data);
  
  void (*user_data_destroy_fn) (gpointer);
  gpointer user_data;

  gboolean using_cache, hinted_cache;
  GuppiDateSeries *cache;
};

struct _GuppiDateSeriesCalcClass {
  GuppiDateSeriesClass parent_class;
};

#define GUPPI_TYPE_DATE_SERIES_CALC (guppi_date_series_calc_get_type ())
#define GUPPI_DATE_SERIES_CALC(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_DATE_SERIES_CALC,GuppiDateSeriesCalc))
#define GUPPI_DATE_SERIES_CALC0(obj) ((obj) ? (GUPPI_DATE_SERIES_CALC(obj)) : NULL)
#define GUPPI_DATE_SERIES_CALC_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_DATE_SERIES_CALC,GuppiDateSeriesCalcClass))
#define GUPPI_IS_DATE_SERIES_CALC(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_DATE_SERIES_CALC))
#define GUPPI_IS_DATE_SERIES_CALC0(obj) (((obj) == NULL) || (GUPPI_IS_DATE_SERIES_CALC(obj)))
#define GUPPI_IS_DATE_SERIES_CALC_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATE_SERIES_CALC))

GtkType guppi_date_series_calc_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_DATE_SERIES_CALC_H */

/* $Id$ */
