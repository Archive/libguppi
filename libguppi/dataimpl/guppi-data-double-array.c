/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-double-array.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-data-double-array.h>

#include <stdlib.h>
#include <math.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/data/guppi-seq-change.h>
#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/data/guppi-seq-string.h>

static GObjectClass *parent_class = NULL;

#define PTR_STEP(x, n) ((double *)(((guint8 *)x)+n))

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Data Operations */

typedef struct _GuppiDataOp_SeqScalar GuppiDataOp_SeqScalar;
struct _GuppiDataOp_SeqScalar {
  GuppiDataOp op;
  
  gint i;
  gsize N;

  double x;

  const double *in_ptr;
  gint in_stride;
};

/* Sorted Copy */

typedef struct _SortPair SortPair;
struct _SortPair {
  double x;
  int i;
};

static int
sort_pair_cmp (const void *a, const void *b)
{
  double x = ((const SortPair *) a)->x;
  double y = ((const SortPair *) b)->x;

  return (x > y) - (x < y);
}

static void
assemble_sorted_copy (GuppiDataDoubleArray *array)
{
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) array;
  gsize i, N = generic->data->len;
  double *p;
  SortPair *sorted;

  if (array->sorted_copy)
    return;
  
  p = (double *) generic->data->data;
  sorted = guppi_new (SortPair, N);

  for (i = 0; i < N; ++i) {
    sorted[i].x = p[i];
    sorted[i].i = i - generic->index_basis;
  }

  qsort (sorted, N, sizeof (SortPair), sort_pair_cmp);

  array->sorted_copy = sorted;
}


/* Implementation of the SeqScalar interface */

static void
calc_minmax (GuppiDataDoubleArray *array)
{
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) array;
  gsize i, N = generic->data->len;
  double *p = (double *) generic->data->data;
  double x, min, max;

  min = max = p[0];
  for (i = 1; i < N; ++i) {
    x = p[i];
    if (x < min)
      min = x;
    if (x > max)
      max = x;
  }

  array->min = min;
  array->max = max;
  array->have_minmax = TRUE;
}

static void
seq_scalar_range (GuppiSeqScalar *seq, double *min, double *max)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);
  
  if (! array->have_minmax)
    calc_minmax (array);

  if (min)
    *min = array->min;
  if (max)
    *max = array->max;
}

static double
seq_scalar_sum (GuppiSeqScalar *seq)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) array;

  if (! array->have_sum) {
    double run = 0;
    gsize i;
    double *p = (double *) generic->data->data;
    for (i = 0; i < generic->data->len; ++i) {
      run += p[i];
    }

    array->sum = run;
    array->have_sum = TRUE;
  }

  return array->sum;
}

static double
seq_scalar_sum_abs (GuppiSeqScalar *seq)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);

  if (! array->have_sum_abs) {
    double run = 0;
    gsize i;
    double *p = (double *) generic->data->data;
    for (i = 0; i < generic->data->len; ++i) {
      run += fabs (p[i]);
    }

    array->sum_abs = run;
    array->have_sum_abs = TRUE;
  }

  return array->sum_abs;
}

static double
seq_scalar_var (GuppiSeqScalar *seq)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);

  if (! array->have_var) {
    double x, om, mean = 0, sumsq = 0;
    gsize i, N = generic->data->len;
    double *p = (double *) generic->data->data;

    for (i = 0; i < N; ++i) {
      x = p[i];
      om = mean;
      mean += (x - mean) / (i+1);
      if (i > 0)
	sumsq += (x - mean) * (x - om);
    }

    array->var = sumsq / N;
    array->have_var = TRUE;
  }

  return array->var;
}

static void
calc_quartiles (GuppiDataDoubleArray *array)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (array);
  SortPair *sorted_copy;
  gsize N = generic->data->len;
  int i;
  double t;
  
  if (array->sorted_copy == NULL)
    assemble_sorted_copy (array);
  sorted_copy = array->sorted_copy;

  t = 0.25 * (N - 1);
  i = (int) t;
  array->q1 = (i + 1 - t) * sorted_copy[i].x + (t - i) * sorted_copy[i+1].x;

  t = 0.50 * (N - 1);
  i = (int) t;
  array->median = (i + 1 - t) * sorted_copy[i].x + (t - i) * sorted_copy[i+1].x;

  t = 0.75 * (N - 1);
  i = (int) t;
  array->q3 = (i + 1 - t) * sorted_copy[i].x + (t - i) * sorted_copy[i+1].x;
  
  array->have_quartiles = TRUE;
}

static gboolean
seq_scalar_quartiles (GuppiSeqScalar *seq, double *q1, double *med, double *q3)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);

  if (! array->have_quartiles)
    calc_quartiles (array);

  if (! array->have_quartiles)
    return FALSE;

  if (q1)
    *q1 = array->q1;

  if (med)
    *med = array->median;

  if (q3)
    *q3 = array->q3;

  return TRUE;
}

static gboolean
seq_scalar_percentile (GuppiSeqScalar *seq, double p, double *x)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (seq);
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  SortPair *sorted_copy;
  gsize N = generic->data->len;
  int i;
  double t;

  if (x == NULL)
    return FALSE;

  assemble_sorted_copy (array);
  sorted_copy = array->sorted_copy;

  t = p  * (N - 1);
  i = (int) t;

  *x = (i + 1 - t) * sorted_copy[i].x + (t - i) * sorted_copy[i+1].x;

  return TRUE;
}

static double
seq_scalar_get (GuppiSeqScalar *seq,
		int i)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  double x = g_array_index (generic->data, double, i - generic->index_basis);
  return x;
}

static void
set_many_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataDoubleArray *array = (GuppiDataDoubleArray *) data;
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) data;
  GuppiDataOp_SeqScalar *seq_op = (GuppiDataOp_SeqScalar *) op;

  gsize i;
  double *my_ptr;
  const double *ptr;

  my_ptr = ((double *) generic->data->data) + (seq_op->i - generic->index_basis);
  ptr = seq_op->in_ptr;

  for (i = 0; i < seq_op->N; ++i) {
    double x = *ptr;
    double old_value = *my_ptr;

    *my_ptr = x;

    if (array->have_sum) {
      array->sum += x - old_value;
      array->save_sum = TRUE;
    }

    if (array->have_sum_abs) {
      array->sum_abs += fabs (x) - fabs (old_value);
      array->save_sum_abs = TRUE;
    }

    if (array->have_minmax) {
      
      if (array->min == array->max) {
	
	/* We could probably do something in this case. */
	
      } else if (old_value == array->min && x <= old_value) {
	array->min = x;
	array->save_minmax = TRUE;
      } else if (old_value == array->max && x >= old_value) {
	array->max = x;
	array->save_minmax = TRUE;
      } else if (x < array->min) {
	array->min = x;
	array->save_minmax = TRUE;
      } else if (x > array->max) {
	array->max = x;
	array->save_minmax = TRUE;
      } else if (array->min <= x && x <= array->max) {
	array->save_minmax = TRUE;
      }
    }
    
    ++my_ptr;
    ptr = PTR_STEP (ptr, seq_op->in_stride);
  }
}

static void
seq_scalar_set_many (GuppiSeqScalar *seq,
		     int start,
		     const double *ptr, 
		     int stride,
		     gsize N)
{
  GuppiDataOp_SeqScalar op;
  GuppiSeqChange_Set change;

  guppi_seq_change_set_init (&change);
  change.i0 = start;
  change.i1 = start+N-1;
 
  op.op.op = set_many_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = start;
  op.N = N;
  op.in_ptr = ptr;
  op.in_stride = stride;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static void
insert_many_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataDoubleArray *array = (GuppiDataDoubleArray *) data;
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) data;
  GuppiDataOp_SeqScalar *seq_op = (GuppiDataOp_SeqScalar *) op;

  gsize i;
  double *my_ptr;
  const double *ptr;
  
  guppi_data_generic_array_insert (generic, seq_op->i, seq_op->N, FALSE);

  my_ptr = ((double *) generic->data->data) + (seq_op->i - generic->index_basis);
  ptr = seq_op->in_ptr;

  for (i = 0; i < seq_op->N; ++i) {
    double x = *ptr;

    *my_ptr = x;

    if (array->have_minmax) {
      if (x < array->min)
	array->min = x;
      if (x > array->max)
	array->max = x;
      array->save_minmax = TRUE;
    }

    if (array->have_sum) {
      array->save_sum = TRUE;
    }

    if (array->have_sum_abs) {
      array->sum_abs += fabs(x);
      array->save_sum_abs = TRUE;
    }

    ++my_ptr;
    ptr = PTR_STEP (ptr, seq_op->in_stride);
  }
}

static void
seq_scalar_insert_many (GuppiSeqScalar *seq,
			int i,
			const double *ptr,
			int stride,
			gsize N)
{
  GuppiDataOp_SeqScalar op;
  GuppiSeqChange_Insert change;

  guppi_seq_change_insert_init (&change);
  change.i = i;
  change.N = N;
 
  op.op.op = insert_many_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.N = N;
  op.in_ptr = ptr;
  op.in_stride = stride;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static double *
seq_scalar_raw_access (GuppiSeqScalar *seq, int *stride)
{

  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) seq;

  if (stride)
    *stride = sizeof (double);

  return (double *) generic->data->data;
}

static void
seq_scalar_init (GuppiSeqScalarIface *iface)
{
  iface->range       = seq_scalar_range;
  iface->sum         = seq_scalar_sum;
  iface->sum_abs     = seq_scalar_sum_abs;
  iface->var         = seq_scalar_var;
  iface->quartiles   = seq_scalar_quartiles;
  iface->percentile  = seq_scalar_percentile;
  
  iface->get         = seq_scalar_get;
  iface->set_many    = seq_scalar_set_many;
  iface->insert_many = seq_scalar_insert_many;

  iface->raw_access  = seq_scalar_raw_access;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Implementation of the SeqString interface. */

static const char *
seq_string_get (GuppiSeqString *seq, int i)
{
  GuppiDataGenericArray *generic = (GuppiDataGenericArray *) seq;
  double x = g_array_index (generic->data, double, i - generic->index_basis);
  static char *str = NULL;

  g_free (str);
  
  str = g_strdup_printf ("%g", x);
  
  return str;
}

static void
seq_string_set (GuppiSeqString *seq, int i, char *str)
{

}

static void
seq_string_insert (GuppiSeqString *seq, int i, char *str)
{

}

static void
seq_string_init (GuppiSeqStringIface *iface)
{
  iface->get = seq_string_get;
  iface->set = seq_string_set;
  iface->insert = seq_string_insert;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* GObject-ish and GuppiDataGenericArray-ish stuff */

static void
put_generic (gpointer ptr, gsize N)
{
  double *p = ptr;
  gsize i;
  for (i = 0; i < N; ++i)
    p[i] = 0.0;
}

static void
changed (GuppiData *data, GuppiDataChange *change)
{
  GuppiDataDoubleArray *array = GUPPI_DATA_DOUBLE_ARRAY (data);

  if (GUPPI_DATA_CLASS (parent_class)->changed)
    GUPPI_DATA_CLASS (parent_class)->changed (data, change);

  array->have_ordering = array->save_ordering;
  array->have_minmax = array->save_minmax;
  array->have_sum = array->save_sum;
  array->have_sum_abs = array->save_sum_abs;
  array->have_var = FALSE;
  array->have_var = FALSE;

  array->save_ordering = FALSE;
  array->save_minmax = FALSE;
  array->save_sum = FALSE;
  array->save_sum_abs = FALSE;
}

static void
guppi_data_double_array_class_init (GuppiDataDoubleArrayClass *klass)
{
  GuppiDataGenericArrayClass *generic_array_class = (GuppiDataGenericArrayClass *) klass;
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);
  
  parent_class = g_type_class_peek_parent (klass);

  generic_array_class->elt_size = sizeof (double);
  generic_array_class->put_generic = put_generic;

  data_class->changed = changed;
}

static void
guppi_data_double_array_init (GuppiDataDoubleArray *array)
{

}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GType
guppi_data_double_array_get_type (void)
{
  static GType gd_double_a_type = 0;

  if (!gd_double_a_type) {
    
    static const GTypeInfo info = {
      sizeof(GuppiDataDoubleArrayClass),
      NULL, NULL,
      (GClassInitFunc) guppi_data_double_array_class_init,
      NULL, NULL,
      sizeof (GuppiDataDoubleArray),
      0,
      (GInstanceInitFunc) guppi_data_double_array_init
    };

    static const GInterfaceInfo seq_scalar_info = {
      (GInterfaceInitFunc) seq_scalar_init,
      NULL, NULL
    };

    static const GInterfaceInfo seq_string_info = {
      (GInterfaceInitFunc) seq_string_init,
      NULL, NULL
    };
    
    gd_double_a_type = g_type_register_static (GUPPI_TYPE_DATA_GENERIC_ARRAY, "GuppiDataDoubleArray",
					       &info, 0);
    
    g_type_add_interface_static (gd_double_a_type, GUPPI_TYPE_SEQ_SCALAR, &seq_scalar_info);    
    g_type_add_interface_static (gd_double_a_type, GUPPI_TYPE_SEQ_STRING, &seq_string_info);


  }

  return gd_double_a_type;
}
