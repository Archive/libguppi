/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-string-array.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-data-string-array.h>

#include <libguppi/useful/guppi-memory.h>
#include <libguppi/data/guppi-seq-string.h>
#include <libguppi/data/guppi-seq-change.h>

static GObjectClass *parent_class = NULL;

static void
put_generic (gpointer ptr, gsize N)
{
  char **p = ptr;
  gsize i;

  for (i = 0; i < N; ++i)
    p[i] = NULL;
}

static void
copy_elts (gpointer dest, gpointer src, gsize N)
{
  char **p_dest = dest;
  char **p_src = src;
  gsize i;

  for (i = 0; i < N; ++i) {
    p_dest[i] = guppi_strdup (p_src[i]);
  }
}

static void
destroy_elts (gpointer ptr, gsize N)
{
  char **p = ptr;
  gsize i;

  for (i = 0; i < N; ++i)
    guppi_free (p[i]);
}

static GuppiData *
copy (GuppiData *data)
{
  GuppiDataStringArray *cpy = (GuppiDataStringArray *) guppi_data_new (GUPPI_TYPE_DATA_STRING_ARRAY);

  guppi_data_generic_array_copy ((GuppiDataGenericArray *) cpy,
				 GUPPI_DATA_GENERIC_ARRAY (data));

  return (GuppiData *) cpy;
}

static void
guppi_data_string_array_class_init (GuppiDataStringArrayClass *klass)
{
  GuppiDataGenericArrayClass *generic_array_class = (GuppiDataGenericArrayClass *) klass;
  GuppiDataClass *data_class = (GuppiDataClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  generic_array_class->elt_size = sizeof (char *);
  generic_array_class->put_generic = put_generic;
  generic_array_class->copy_elts = copy_elts;
  generic_array_class->destroy_elts = destroy_elts;

  data_class->copy = copy;
}

static void
guppi_data_string_array_init (GuppiDataStringArray *array)
{
}

static const char *
get (GuppiSeqString *seq, int i)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (seq);
  char *p;
  
  p = g_array_index (generic->data,
		     char *,
		     i - generic->index_basis);
  return p;
}

typedef struct _GuppiDataOp_SeqString GuppiDataOp_SeqString;
struct _GuppiDataOp_SeqString {
  GuppiDataOp op;
  int i;
  char *str;
};

static void
set_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (data);
  GuppiDataOp_SeqString *seq_op = (GuppiDataOp_SeqString *) op;
  char **p;
  
  p = &g_array_index (generic->data,
		      char *,
		      seq_op->i - generic->index_basis);
  *p = seq_op->str;
}

static void
set (GuppiSeqString *seq, int i, char *str)
{
  GuppiDataOp_SeqString op;
  GuppiSeqChange_Set change;

  guppi_seq_change_set_init (&change);
  change.i0 = i;
  change.i1 = i;

  op.op.op = set_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.str = str;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}

static void
insert_op (GuppiData *data, GuppiDataOp *op)
{
  GuppiDataGenericArray *generic = GUPPI_DATA_GENERIC_ARRAY (data);
  GuppiDataOp_SeqString *seq_op = (GuppiDataOp_SeqString *) op;
  char **p;

  if (generic->data == NULL || generic->data->len == 0) {
    generic->index_basis = seq_op->i;
  }
  
  guppi_data_generic_array_insert (generic, seq_op->i, 1, FALSE);
  p = &g_array_index (generic->data, char *, seq_op->i - generic->index_basis);
  *p = seq_op->str;
}

static void
insert (GuppiSeqString *seq, int i, char *str)
{
  GuppiDataOp_SeqString op;
  GuppiSeqChange_Insert change;

  guppi_seq_change_insert_init (&change);
  change.i = i;
  change.N = 1;

  op.op.op = insert_op;
  op.op.change = (GuppiDataChange *) &change;
  op.i = i;
  op.str = str;

  guppi_data_execute_op (GUPPI_DATA (seq), (GuppiDataOp *) &op);
}


static void
seq_string_init (GuppiSeqStringIface *iface)
{
  iface->get = get;
  iface->set = set;
  iface->insert = insert;
}

GType
guppi_data_string_array_get_type (void)
{
  static GType type = 0;

  if (!type) {
    
    static const GTypeInfo info = {
      sizeof(GuppiDataStringArrayClass),
      NULL, NULL,
      (GClassInitFunc) guppi_data_string_array_class_init,
      NULL, NULL,
      sizeof (GuppiDataStringArray),
      0,
      (GInstanceInitFunc) guppi_data_string_array_init
    };

    static const GInterfaceInfo seq_string_info = {
      (GInterfaceInitFunc) seq_string_init,
      NULL, NULL
    };
    

    type = g_type_register_static (GUPPI_TYPE_DATA_GENERIC_ARRAY, "GuppiDataStringArray",
				   &info, 0);
    
    g_type_add_interface_static (type, GUPPI_TYPE_SEQ_STRING, &seq_string_info);

  }


  return type;
}
