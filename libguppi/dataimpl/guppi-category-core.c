/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-category-core-impl.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-category-core.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/data/guppi-data-plug-in.h>


static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_category_core_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_category_core_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_category_core_destroy (GtkObject *obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}



static void
guppi_category_core_finalize (GtkObject *obj)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (obj);

  g_hash_table_foreach (core->name2code, guppi_free_hash_key, NULL);

  g_hash_table_destroy (core->name2code);
  g_hash_table_destroy (core->code2name);

  core->name2code = core->code2name = NULL;

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiData *
v_copy (GuppiData *impl_to_copy)
{
  g_assert_not_reached ();
  return NULL;
}

static gint
v_get_size_in_bytes (GuppiData *d)
{
  gint sz = 0;

  sz += sizeof (GuppiCategoryCore);

  if (GUPPI_DATA_CLASS (parent_class)->get_size_in_bytes)
    sz += GUPPI_DATA_CLASS (parent_class)->get_size_in_bytes (d);

  return sz;
}

static gchar *
v_get_size_info (GuppiData *d)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (d);
  return g_strdup_printf ("%d", g_hash_table_size (core->code2name));
}

static gsize
v_size (GuppiCategory *cat)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);

  return g_hash_table_size (core->code2name);
}

static void
v_codes (GuppiCategory *cat, code_t *min, code_t *max, code_t *unused)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);

  if (min)
    *min = core->min_code;

  if (max)
    *max = core->max_code;

  if (unused)
    *unused = core->max_code == GUPPI_INVALID_CODE ? 0 : core->max_code + 1;
}

static void
v_define (GuppiCategory *cat, gchar *str, code_t c)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);
  gpointer old_key;
  gpointer cptr = GUPPI_CATEGORY_CODE_TO_POINTER (c);

  /* The hashes could get badly out of sync if this function is called
     w/o sufficient checks. */

  /* Make sure we don't leak any old keys. */
  if (g_hash_table_lookup_extended (core->code2name, cptr, NULL, &old_key))
    guppi_free (old_key);

  g_hash_table_remove (core->name2code, str);
  g_hash_table_remove (core->code2name, cptr);

  g_hash_table_insert (core->name2code, str, cptr);
  g_hash_table_insert (core->code2name, cptr, str);

  if (core->min_code == GUPPI_INVALID_CODE || core->min_code > c)
    core->min_code = c;

  if (core->max_code == GUPPI_INVALID_CODE || core->max_code < c)
    core->max_code = c;
}

static const gchar *
v_code2name (GuppiCategory *cat, code_t c)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);
  gpointer cptr = GUPPI_CATEGORY_CODE_TO_POINTER (c);

  return (const gchar *) g_hash_table_lookup (core->code2name, cptr);
}

static code_t
v_name2code (GuppiCategory *cat, const gchar *str)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);
  gpointer cptr;

  if (g_hash_table_lookup_extended (core->name2code, str, NULL, &cptr))
    return GUPPI_CATEGORY_POINTER_TO_CODE (cptr);
  else
    return GUPPI_INVALID_CODE;
}

struct category_foreach {
  GuppiCategoryFn fn;
  gpointer user_data;
};

static void
foreach_handler (gpointer key, gpointer val, gpointer data)
{
  struct category_foreach *foo = (struct category_foreach *) data;

  foo->fn ((const gchar *) key,
	   GUPPI_CATEGORY_POINTER_TO_CODE (val), foo->user_data);
}

static void
v_foreach (GuppiCategory *cat, GuppiCategoryFn fn, gpointer user_data)
{
  GuppiCategoryCore *core = GUPPI_CATEGORY_CORE (cat);
  struct category_foreach foo = { fn, user_data };

  g_hash_table_foreach (core->name2code, foreach_handler, &foo);
}

/***************************************************************************/

static void
guppi_category_core_class_init (GuppiCategoryCoreClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);
  GuppiCategoryClass *category_class = GUPPI_CATEGORY_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CATEGORY);

  object_class->get_arg = guppi_category_core_get_arg;
  object_class->set_arg = guppi_category_core_set_arg;
  object_class->destroy = guppi_category_core_destroy;
  object_class->finalize = guppi_category_core_finalize;

  data_class->copy = v_copy;
  data_class->get_size_in_bytes = v_get_size_in_bytes;
  data_class->get_size_info = v_get_size_info;
  data_class->is_leaf_type = TRUE;

  category_class->size = v_size;
  category_class->codes = v_codes;
  category_class->define = v_define;
  category_class->code2name = v_code2name;
  category_class->name2code = v_name2code;
  category_class->foreach = v_foreach;

}

static void
guppi_category_core_init (GuppiCategoryCore *obj)
{
  obj->min_code = GUPPI_INVALID_CODE;
  obj->max_code = GUPPI_INVALID_CODE;

  obj->name2code = g_hash_table_new (g_str_hash, g_str_equal);
  obj->code2name = g_hash_table_new (NULL, NULL);
}

GtkType guppi_category_core_get_type (void)
{
  static GtkType guppi_category_core_type = 0;
  if (!guppi_category_core_type) {
    static const GtkTypeInfo guppi_category_core_info = {
      "GuppiCategoryCore",
      sizeof (GuppiCategoryCore),
      sizeof (GuppiCategoryCoreClass),
      (GtkClassInitFunc) guppi_category_core_class_init,
      (GtkObjectInitFunc) guppi_category_core_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_category_core_type =
      gtk_type_unique (GUPPI_TYPE_CATEGORY, &guppi_category_core_info);
  }
  return guppi_category_core_type;
}

GuppiCategory *
guppi_category_core_new (void)
{
  return GUPPI_CATEGORY (guppi_type_new (guppi_category_core_get_type ()));
}

/* $Id$ */
