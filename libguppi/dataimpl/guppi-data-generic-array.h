/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-generic-array.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_DATA_GENERIC_ARRAY_H__
#define __GUPPI_DATA_GENERIC_ARRAY_H__

#include <libguppi/data/guppi-data.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiDataGenericArray      GuppiDataGenericArray;
typedef struct _GuppiDataGenericArrayClass GuppiDataGenericArrayClass;

struct _GuppiDataGenericArray {
  GuppiData parent;

  int index_basis;
  GArray *data;
};

struct _GuppiDataGenericArrayClass {
  GuppiDataClass parent_class;

  gsize elt_size;
  void (*put_generic)  (gpointer, gsize count);
  void (*copy_elts)    (gpointer dest, gpointer src, gsize count);
  void (*destroy_elts) (gpointer, gsize count);
};

#define GUPPI_TYPE_DATA_GENERIC_ARRAY            (guppi_data_generic_array_get_type())
#define GUPPI_DATA_GENERIC_ARRAY(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_DATA_GENERIC_ARRAY, GuppiDataGenericArray))
#define GUPPI_DATA_GENERIC_ARRAY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_DATA_GENERIC_ARRAY, GuppiDataGenericArrayClass))
#define GUPPI_IS_DATA_GENERIC_ARRAY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_DATA_GENERIC_ARRAY))
#define GUPPI_IS_DATA_GENERIC_ARRAY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_DATA_GENERIC_ARRAY))

GType guppi_data_generic_array_get_type (void);

void guppi_data_generic_array_insert (GuppiDataGenericArray *array, int i, gsize N, gboolean initialize);
void guppi_data_generic_array_delete (GuppiDataGenericArray *array, int i, gsize N);
void guppi_data_generic_array_copy   (GuppiDataGenericArray *dest, GuppiDataGenericArray *src);

END_GUPPI_DECLS;


#endif /* __GUPPI_DATA_GENERIC_ARRAY_H__ */

