/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-data-table-core.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/dataimpl/guppi-data-table-core.h>

#include <libguppi/useful/guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

static void
clear_labels (GuppiDataTableCore *core)
{
  gint i;
  gchar **underlying;

  underlying = (gchar **) guppi_garray_data (core->row_labels);
  for (i = 0; i < core->R; ++i) {
    guppi_free0 (underlying[i]);
  }

  underlying = (gchar **) guppi_garray_data (core->col_labels);
  for (i = 0; i < core->C; ++i) {
    guppi_free0 (underlying[i]);
  }
}

static void
guppi_data_table_core_finalize (GtkObject *obj)
{
  GuppiDataTableCore *x = GUPPI_DATA_TABLE_CORE(obj);

  guppi_unref0 (x->data);
  guppi_unref0 (x->row_labels);
  guppi_unref0 (x->col_labels);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static gboolean
get_bounds (GuppiDataTable *tab, gint *r, gint *c)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  
  if (r)
    *r = core->R;

  if (c)
    *c = core->C;

  return TRUE;
}

static void
set_bounds (GuppiDataTable *tab, gint r, gint c)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  double *underlying;
  gchar **labels;
  gint i, N;

  clear_labels (core);

  core->R = r;
  core->C = c;
  N = r * c;

  guppi_garray_set_size (core->data, N);
  underlying = (double *) guppi_garray_data (core->data);
  for (i = 0; i < N; ++i)
    underlying[i] = 0.0;

  guppi_garray_set_size (core->row_labels, r);
  labels = (gchar **) guppi_garray_data (core->row_labels);
  for (i = 0; i < r; ++i)
    labels[i] = NULL;

  guppi_garray_set_size (core->col_labels, c);
  labels = (gchar **) guppi_garray_data (core->col_labels);
  for (i = 0; i < c; ++i)
    labels[i] = NULL;
}

static double
get_entry (GuppiDataTable *tab, gint r, gint c)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  double *underlying = (double *) guppi_garray_data (core->data);
  return underlying[core->C * r + c];
}

static void
set_entry (GuppiDataTable *tab, gint r, gint c, double x)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  double *underlying = (double *) guppi_garray_data (core->data);
  underlying[core->C * r + c] = x;
}

static const gchar *
get_label (GuppiDataTable *tab, GuppiDataTableSpan span, gint i)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  gchar **label;

  label = (gchar **) guppi_garray_data (span == GUPPI_TABLE_ROW ? core->row_labels : core->col_labels);
  return label[i];
}

static void
set_label (GuppiDataTable *tab, GuppiDataTableSpan span, gint i, const gchar *str)
{
  GuppiDataTableCore *core = GUPPI_DATA_TABLE_CORE (tab);
  gchar **label;

  label = (gchar **) guppi_garray_data (span == GUPPI_TABLE_ROW ? core->row_labels : core->col_labels);
  
  if (label[i] != str) {
    guppi_free (label[i]);
    label[i] = guppi_strdup (str);
  }
}

static gboolean
get_stats (GuppiDataTable *tab, GuppiDataTableSpan span, gint i,
	   double *sum, double *sum_abs, double *min, double *max)
{
  return FALSE;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_data_table_core_class_init (GuppiDataTableCoreClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiDataTableClass *table_class = GUPPI_DATA_TABLE_CLASS (klass);
  GuppiDataClass *data_class = GUPPI_DATA_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_DATA_TABLE);

  object_class->finalize = guppi_data_table_core_finalize;

  table_class->get_bounds = get_bounds;
  table_class->set_bounds = set_bounds;
  table_class->get_entry  = get_entry;
  table_class->set_entry  = set_entry;
  table_class->get_label  = get_label;
  table_class->set_label  = set_label;
  table_class->get_stats  = get_stats;

  data_class->is_leaf_type = TRUE;
}

static void
guppi_data_table_core_init (GuppiDataTableCore *obj)
{
  obj->data = guppi_garray_new (sizeof (double));
  obj->row_labels = guppi_garray_new (sizeof (gchar *));
  obj->col_labels = guppi_garray_new (sizeof (gchar *));
}

GtkType
guppi_data_table_core_get_type (void)
{
  static GtkType guppi_data_table_core_type = 0;
  if (!guppi_data_table_core_type) {
    static const GtkTypeInfo guppi_data_table_core_info = {
      "GuppiDataTableCore",
      sizeof (GuppiDataTableCore),
      sizeof (GuppiDataTableCoreClass),
      (GtkClassInitFunc)guppi_data_table_core_class_init,
      (GtkObjectInitFunc)guppi_data_table_core_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_data_table_core_type = gtk_type_unique (GUPPI_TYPE_DATA_TABLE, &guppi_data_table_core_info);
  }
  return guppi_data_table_core_type;
}

GuppiDataTable *
guppi_data_table_core_new (void)
{
  return GUPPI_DATA_TABLE (guppi_type_new (guppi_data_table_core_get_type ()));
}

