/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-seq-scalar-calc-impl.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SEQ_SCALAR_CALC_H
#define _INC_GUPPI_SEQ_SCALAR_CALC_H

/* #include <gtk/gtk.h> */
#include <libguppi/useful/guppi-defs.h>
#include <libguppi/useful/guppi-fn-wrapper.h>
#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/data/guppi-seq-scalar.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiSeqScalarCalc GuppiSeqScalarCalc;
typedef struct _GuppiSeqScalarCalcClass GuppiSeqScalarCalcClass;

struct _GuppiSeqScalarCalc {
  GuppiSeqScalar parent;

  gint i0, i1;

  double (*fn) (gint, gpointer);
  gpointer user_data;

  GuppiFnWrapper *fn_wrapper;
};

struct _GuppiSeqScalarCalcClass {
  GuppiSeqScalarClass parent_class;
};

#define GUPPI_TYPE_SEQ_SCALAR_CALC (guppi_seq_scalar_calc_get_type())
#define GUPPI_SEQ_SCALAR_CALC(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SEQ_SCALAR_CALC,GuppiSeqScalarCalc))
#define GUPPI_SEQ_SCALAR_CALC0(obj) ((obj) ? (GUPPI_SEQ_SCALAR_CALC(obj)) : NULL)
#define GUPPI_SEQ_SCALAR_CALC_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SEQ_SCALAR_CALC,GuppiSeqScalarCalcClass))
#define GUPPI_IS_SEQ_SCALAR_CALC(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SEQ_SCALAR_CALC))
#define GUPPI_IS_SEQ_SCALAR_CALC0(obj) (((obj) == NULL) || (GUPPI_IS_SEQ_SCALAR_CALC(obj)))
#define GUPPI_IS_SEQ_SCALAR_CALC_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SEQ_SCALAR_CALC))

GtkType guppi_seq_scalar_calc_get_type (void);

GuppiSeqScalar *guppi_seq_scalar_calc_new (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SEQ_SCALAR_CALC_H */

/* $Id$ */
