/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-engine.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/layout/guppi-layout-engine.h>

#include <math.h>
#include <stdlib.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/math/guppi-matrix.h>
#include <libguppi/useful/guppi-marshal.h>

#define LAYOUT_DEBUG 0

static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  DIRTY,
  LAST_SIGNAL
};

guint guppi_layout_engine_signals[LAST_SIGNAL] = { 0 };

typedef struct _ItemInfo ItemInfo;
struct _ItemInfo {
  GuppiLayoutItem *item;
  guint changed_request_handler;
  gint id;
  gint refs; /* number of times this geometry appears in all rules */
};

typedef struct _GuppiLayoutEnginePrivate GuppiLayoutEnginePrivate;
struct _GuppiLayoutEnginePrivate {
  GList *items;
  ItemInfo *last_item_info;

  GList *rules;

  gint freeze_count;
  gboolean dirty, resolved;
};

static ItemInfo *item_info_new  (GuppiLayoutEngine *engine, GuppiLayoutItem *geom);
static void      item_info_free (ItemInfo *info);
static gint      get_item_id    (GuppiLayoutEngine *, GuppiLayoutItem *);


static void
guppi_layout_engine_finalize (GObject *obj)
{
  GuppiLayoutEngine *engine = GUPPI_LAYOUT_ENGINE (obj);
  GuppiLayoutEnginePrivate *p = engine->priv;

  guppi_finalized (obj);

  g_list_foreach (p->items, (GFunc) item_info_free, NULL);
  g_list_free (p->items);

  g_list_foreach (p->rules, (GFunc) guppi_layout_rule_unref, NULL);
  g_list_free (p->rules);

  guppi_free0 (engine->priv);
}

static void
guppi_layout_engine_class_init (GuppiLayoutEngineClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_layout_engine_finalize;

  guppi_layout_engine_signals[CHANGED] = 
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiLayoutEngineClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

  guppi_layout_engine_signals[DIRTY] =
    g_signal_new ("dirty",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiLayoutEngineClass, dirty),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);
}

static void
guppi_layout_engine_init (GuppiLayoutEngine *engine)
{
  engine->priv = guppi_new0 (GuppiLayoutEnginePrivate, 1);
}

GType
guppi_layout_engine_get_type (void)
{
  static GType guppi_layout_engine_type = 0;

  if (!guppi_layout_engine_type) {

    static const GTypeInfo guppi_layout_engine_info = {
      sizeof (GuppiLayoutEngineClass),
      NULL, NULL,
      (GClassInitFunc) guppi_layout_engine_class_init,
      NULL, NULL,
      sizeof (GuppiLayoutEngine),
      0,
      (GInstanceInitFunc) guppi_layout_engine_init,
    };

    guppi_layout_engine_type = g_type_register_static (G_TYPE_OBJECT, 
						       "GuppiLayoutEngine",
						       &guppi_layout_engine_info,
						       0);
  }

  return guppi_layout_engine_type;
}

GuppiLayoutEngine *
guppi_layout_engine_new (void)
{
  return GUPPI_LAYOUT_ENGINE (guppi_object_new (guppi_layout_engine_get_type ()));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_layout_engine_make_dirty (GuppiLayoutEngine *engine)
{
  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));

  if (engine->priv->dirty)
    return;

  engine->priv->dirty = TRUE;
  if (engine->priv->freeze_count == 0) {
    g_signal_emit (engine, guppi_layout_engine_signals[DIRTY], 0);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gint
constraint_count (GuppiLayoutEngine *engine)
{
  GList *iter;
  gint count = 0;
  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    count += guppi_layout_rule_constraint_count ((GuppiLayoutRule *) iter->data);
  }

  return count;
}

typedef struct _BuildMatrixInfo BuildMatrixInfo;
struct _BuildMatrixInfo {
  GuppiLayoutEngine *engine;
  GuppiMatrix *m;
  ArtDRect *bbox;
  gint r;
};

static void
build_matrix_term_fn (GuppiLayoutConstraintTermType tt, double factor, GuppiLayoutItem *item, gpointer closure)
{
  BuildMatrixInfo *info = closure;
  gint c = 0;

  if (item) {
    c = 4 * get_item_id (info->engine, item);
    switch (tt) {
    case GLC_LEFT:
      /* c += 0; */
      break;
    case GLC_RIGHT:
      c += 1;
      break;
    case GLC_TOP:
      c += 2;
      break;
    case GLC_BOTTOM:
      c += 3;
      break;
    default:
      g_message ("Uh oh %d", (gint)tt);
      g_assert_not_reached ();
    }
    ++c; /* constaints are in column zero */
  }

  guppi_matrix_entry (info->m, info->r, c) += factor;
}

static void
build_matrix_constraint_fn (GuppiLayoutConstraint *ctn, gpointer closure)
{
  BuildMatrixInfo *info = closure;

  guppi_layout_constraint_foreach_with_region (ctn,
					       info->bbox,
					       build_matrix_term_fn, info);

  ++info->r;
}

static void
natural_size_contraint_fn (GuppiLayoutEngine *engine,
			   GuppiLayoutItem   *item,
			   gpointer           closure)
{
  BuildMatrixInfo *info = closure;

  if (guppi_layout_item_has_width_request (item)) {
    GuppiLayoutConstraint *ctn = guppi_layout_constraint_new ();

    guppi_layout_constraint_add_terms (ctn,
				       GLC_RIGHT, 1.0, item,
				       GLC_LEFT, -1.0, item,
				       GLC_WIDTH, -1.0, item,
				       GLC_LAST);

    build_matrix_constraint_fn (ctn, info);

    guppi_layout_constraint_unref (ctn);
  }

  if (guppi_layout_item_has_height_request (item)) {
    GuppiLayoutConstraint *ctn = guppi_layout_constraint_new ();

    guppi_layout_constraint_add_terms (ctn, 
				       GLC_TOP, 1.0, item,
				       GLC_BOTTOM, -1.0, item,
				       GLC_HEIGHT, -1.0, item,
				       GLC_LAST);

    build_matrix_constraint_fn (ctn, info);

    guppi_layout_constraint_unref (ctn);
  }
}

static void
build_simplified_rule_system (GuppiLayoutEngine *engine,
			      GuppiMatrix **matrix, 
			      GuppiVector **vector,
			      ArtDRect *bbox)
{
  gint rows = constraint_count (engine) + 2 * g_list_length (engine->priv->items);
  gint cols = 4 * g_list_length (engine->priv->items) + 1;
  GuppiMatrix *m = guppi_matrix_new (rows, cols);
  BuildMatrixInfo info;
  GList *iter;
  gint r, i, j, nonzero_rows;

  if (rows <= 0 || cols <= 0) {
    g_message ("layout %p is empty", engine);
    return;
  }

  info.engine = engine;
  info.m      = m;
  info.bbox   = bbox;
  info.r      = 0;

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    guppi_layout_rule_foreach (rule, build_matrix_constraint_fn, &info);
  }

  /* Handle elements w/ natural width/height */
  guppi_layout_engine_foreach_item (engine, natural_size_contraint_fn, &info);

#if LAYOUT_DEBUG
  guppi_matrix_spew (m);
#endif

  /* Now we use a Grahm-Schmidt style process to simplify the matrix. */
  nonzero_rows = 0;
  for (r = 0; r < info.r; ++r) {
    for (j=0; j < r; ++j) {
      double sc = guppi_matrix_row_dot (m, r, j);
      guppi_matrix_subtract_scaled_row_from_row (m, sc, j, r);
    }
    if (guppi_matrix_row_is_nonzero (m, r)) {
      guppi_matrix_normalize_row (m, r);
      ++nonzero_rows;
    }
  }

  *matrix = guppi_matrix_new (nonzero_rows, cols-1);
  *vector = guppi_vector_new (nonzero_rows);
  
  j = 0;
  for (r = 0; r < info.r; ++r) {
    if (guppi_matrix_row_is_nonzero (m, r)) {
      double *m_p;
      double *matrix_p;

      guppi_vector_entry (*vector, j) = - guppi_matrix_entry (m, r, 0);

      m_p = guppi_matrix_ptr (m, r, 1);
      matrix_p = guppi_matrix_ptr (*matrix, j, 0);
      for (i = 0; i < cols-1; ++i) {
	*matrix_p = *m_p;
	m_p = guppi_matrix_ptr_col_incr (m, m_p);
	matrix_p = guppi_matrix_ptr_col_incr (*matrix, matrix_p);
      }
      ++j;
    }
  }
  g_assert (nonzero_rows == j);

  guppi_matrix_free (m);
}

struct CustomLUInfo {
  double x0, y0, x1, y1;
};

static gboolean
custom_solve_fallback (GuppiMatrix *m, GuppiVector *vec, gint i, gpointer user_data)
{
  struct CustomLUInfo *info = user_data;
  double q = 0;

  switch (i % 4) {
  case 0:
    q = info->x0;
    break;
  case 1:
    q = info->x1;
    break;
  case 2:
    q = info->y1;
    break;
  case 3:
    q = info->y0;
    break;
  }

  guppi_vector_entry (vec, i) = q;
  
  return TRUE;
}

static double
evil_clean (double x)
{
  const double evil_clean_constant = 1e-10;
  double x0, x1;

  x0 = floor (x);
  x1 = ceil (x);

  if (fabs (x - x0) < evil_clean_constant)
    return x0;
  if (fabs (x - x1) < evil_clean_constant)
    return x1;
  return x;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
changed_request_cb (GuppiLayoutItem *geom, gpointer closure)
{
  GuppiLayoutEngine *engine = GUPPI_LAYOUT_ENGINE (closure);

  guppi_layout_engine_make_dirty (engine);
}

static ItemInfo *
item_info_new (GuppiLayoutEngine *engine, GuppiLayoutItem *item)
{
  ItemInfo *info;

  info = guppi_new0 (ItemInfo, 1);
  info->item = item;
  guppi_ref (info->item);
  info->refs = 1;

  info->changed_request_handler = g_signal_connect (item,
						    "changed_request",
						    (GCallback) changed_request_cb,
						    engine);
  return info;
}

static void
item_info_free (ItemInfo *info)
{
  if (info == NULL)
    return;

  g_signal_handler_disconnect (info->item, info->changed_request_handler);
  guppi_unref (info->item);
  guppi_free (info);
}

static ItemInfo *
find_item_info (GuppiLayoutEngine *engine, GuppiLayoutItem *item)
{
  GList *iter;

  if (engine->priv->last_item_info && engine->priv->last_item_info->item == item)
    return engine->priv->last_item_info;

  /* Sucky linear search. */
  for (iter = engine->priv->items; iter != NULL; iter = g_list_next (iter)) {
    if ( ((ItemInfo *) iter->data)->item == item ) {
      engine->priv->last_item_info = iter->data;
      return iter->data;
    }
  }
  return NULL;
}

static void
assign_ids (GuppiLayoutEngine *engine)
{
  GList *iter;
  gint id = 0;

  for (iter = engine->priv->items; iter != NULL; iter = g_list_next (iter)) {
    ((ItemInfo *) iter->data)->id = id;
    ++id;
  }
}

static gint
get_item_id (GuppiLayoutEngine *engine, GuppiLayoutItem *geom)
{
  ItemInfo *info = find_item_info (engine, geom);
  return info ? info->id : -1;
}

static void
add_item (GuppiLayoutEngine *engine, GuppiLayoutItem *geom)
{
  ItemInfo *info;
  
  info = find_item_info (engine, geom);
  if (info != NULL) {
    ++info->refs;
    return;
  }

  info = item_info_new (engine, geom);

  engine->priv->items = g_list_append (engine->priv->items, info);
  engine->priv->last_item_info = info;

  assign_ids (engine);
}

static void
remove_item (GuppiLayoutEngine *engine, GuppiLayoutItem *geom)
{
  ItemInfo *info;
  GList *node;

  info = find_item_info (engine, geom);
  g_return_if_fail (info != NULL);

  g_assert (info->refs > 0);
  --info->refs;
  if (info->refs > 0)
    return;

  if (engine->priv->last_item_info == info)
    engine->priv->last_item_info = NULL;

  node = g_list_find (engine->priv->items, info);
  engine->priv->items = g_list_remove_link (engine->priv->items, node);
  g_list_free_1 (node);

  item_info_free (info);
  
  assign_ids (engine);
}

static void
add_rule_foreach_term_fn (GuppiLayoutConstraintTermType term_type,
			  double factor, GuppiLayoutItem *geom,
			  gpointer closure)
{
  if (geom != NULL)
    add_item ((GuppiLayoutEngine *) closure, geom);
}

static void
add_rule_foreach_constraint_fn (GuppiLayoutConstraint *cnt, gpointer closure)
{
  guppi_layout_constraint_foreach (cnt, add_rule_foreach_term_fn, closure);
}

void
guppi_layout_engine_add_rule (GuppiLayoutEngine *engine, GuppiLayoutRule *rule)
{
  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (rule != NULL);

#if LAYOUT_DEBUG
  g_print ("Adding \"%s\" (%d)\n",
	   guppi_layout_rule_name (rule),
	   guppi_layout_rule_constraint_count (rule));
#endif

  guppi_layout_rule_ref (rule);
  engine->priv->rules = g_list_append (engine->priv->rules, rule);
  guppi_layout_rule_foreach (rule, add_rule_foreach_constraint_fn, engine);

  guppi_layout_engine_make_dirty (engine);
  g_signal_emit (engine, guppi_layout_engine_signals[CHANGED], 0);
}

void
guppi_layout_engine_add_rules (GuppiLayoutEngine *engine,
			       ...)
{
  va_list args;
  GuppiLayoutRule *rule;

  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));

  va_start (args, engine);

  while ( (rule = va_arg (args, GuppiLayoutRule *)) != NULL) {
    guppi_layout_engine_add_rule (engine, rule);
  }
  
  va_end (args);
}

static void
remove_rule_foreach_term_fn (GuppiLayoutConstraintTermType term_type,
			     double factor, GuppiLayoutItem *geom,
			     gpointer closure)
{
  if (geom != NULL)
    remove_item ((GuppiLayoutEngine *) closure, geom);
}

static void
remove_rule_foreach_constraint_fn (GuppiLayoutConstraint *cnt, gpointer closure)
{
  guppi_layout_constraint_foreach (cnt, remove_rule_foreach_term_fn, closure);
}

void
guppi_layout_engine_remove_rule (GuppiLayoutEngine *engine, GuppiLayoutRule *rule)
{
  GList *node;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (rule != NULL);

  node = g_list_find (engine->priv->rules, rule);
  if (node == NULL)
    return;

  engine->priv->rules = g_list_remove_link (engine->priv->rules, node);
  g_list_free_1 (node);

  guppi_layout_rule_foreach (rule, remove_rule_foreach_constraint_fn, engine);
  guppi_layout_rule_unref (rule);

  guppi_layout_engine_make_dirty (engine);
  g_signal_emit (engine, guppi_layout_engine_signals[CHANGED], 0);
}

void
guppi_layout_engine_remove_item_rules (GuppiLayoutEngine *engine, GuppiLayoutItem *item)
{
  GList *rules_copy, *iter;

  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (item));

  /* Find and remove any rules that mention geom */

  rules_copy = g_list_copy (engine->priv->rules);

  for (iter = rules_copy; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    if (guppi_layout_rule_contains (rule, item))
      guppi_layout_engine_remove_rule (engine, rule);
  }

  g_list_free (rules_copy);

  guppi_layout_engine_make_dirty (engine);
  g_signal_emit (engine, guppi_layout_engine_signals[CHANGED], 0);
}

void
guppi_layout_engine_remove_item (GuppiLayoutEngine *engine, GuppiLayoutItem *item)
{
  ItemInfo *info;

  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (item));

  guppi_layout_engine_remove_item (engine, item);

  /* Remove our main reference */
  remove_item (engine, item);

  info = find_item_info (engine, item);
  g_assert (info == NULL);

  guppi_layout_engine_make_dirty (engine);
  g_signal_emit (engine, guppi_layout_engine_signals[CHANGED], 0);
}

void
guppi_layout_engine_replace_item (GuppiLayoutEngine *engine, GuppiLayoutItem *old, GuppiLayoutItem *nuevo)
{
  GList *iter;
  ItemInfo *info;

  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (old));
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (nuevo));

  if (old == nuevo)
    return;

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    GuppiLayoutRule *rule = iter->data;
    
    guppi_layout_rule_replace (rule, old, nuevo);
  }

  info = find_item_info (engine, old);
  if (info) {
    g_signal_handler_disconnect (info->item, info->changed_request_handler);
    guppi_refcounting_assign (info->item, nuevo);
    info->changed_request_handler = g_signal_connect_object (nuevo,
							     "changed_request",
							     (GCallback) guppi_layout_engine_make_dirty,
							     engine,
							     G_CONNECT_SWAPPED); /* FIXME? */
  }

  guppi_layout_engine_make_dirty (engine);
  g_signal_emit (engine, guppi_layout_engine_signals[CHANGED], 0);
}

void
guppi_layout_engine_foreach_rule (GuppiLayoutEngine *engine, GuppiLayoutEngineRuleFn fn, gpointer closure)
{
  GList *iter;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (fn != NULL);

  for (iter = engine->priv->rules; iter != NULL; iter = g_list_next (iter)) {
    fn (engine, (GuppiLayoutRule *) iter->data, closure);
  }
}

void
guppi_layout_engine_foreach_item (GuppiLayoutEngine *engine, GuppiLayoutEngineItemFn fn, gpointer closure)
{
  GList *iter;

  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (fn != NULL);

  for (iter = engine->priv->items; iter != NULL; iter = g_list_next (iter)) {
    fn (engine, ((ItemInfo *) iter->data)->item, closure);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

gboolean
guppi_layout_engine_is_dirty (GuppiLayoutEngine *engine)
{
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine), FALSE);

  return engine->priv->dirty;
}

void
guppi_layout_engine_freeze (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (engine->priv->freeze_count >= 0);
  ++engine->priv->freeze_count;
}

void
guppi_layout_engine_thaw (GuppiLayoutEngine *engine)
{
  g_return_if_fail (engine && GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (engine->priv->freeze_count > 0);

  --engine->priv->freeze_count;
  if (engine->priv->freeze_count == 0 && engine->priv->dirty) {
    g_signal_emit (engine, guppi_layout_engine_signals[DIRTY], 0);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_layout_engine_do_layout (GuppiLayoutEngine *engine,
			       ArtDRect *bbox)
{
  GuppiMatrix *layout_matrix;
  GuppiVector *layout_vector;
  GuppiVector *soln_vector;
  struct CustomLUInfo custom_info;

  g_return_if_fail (GUPPI_IS_LAYOUT_ENGINE (engine));
  g_return_if_fail (bbox != NULL);

  build_simplified_rule_system (engine, &layout_matrix, &layout_vector, bbox);

  custom_info.x0 = bbox->x0;
  custom_info.y0 = bbox->y0;
  custom_info.x1 = bbox->x1;
  custom_info.y1 = bbox->y1;

#if LAYOUT_DEBUG
  g_printerr ("bounds: %g:%g %g:%g\n", bbox->x0, bbox->x1, bbox->y0, bbox->y1);
  g_printerr ("collapsed system: rows=%d cols=%d\n", guppi_matrix_rows (layout_matrix), guppi_matrix_cols (layout_matrix));

  {
    gint i, j;
    for (i = 0; i < guppi_vector_dim (layout_vector); ++i)
      g_message ("  %2d %g", i, guppi_vector_entry (layout_vector, i));

    for (i = 0; i < guppi_matrix_rows (layout_matrix); ++i) {
      for (j = 0; j < guppi_matrix_cols (layout_matrix); ++j) {
	g_print ("%9.5f ", guppi_matrix_entry (layout_matrix, i, j));
      }
      g_print ("\n");
    }
  }
#endif

  soln_vector = guppi_matrix_solve_with_fallback (layout_matrix,
						  layout_vector,
						  custom_solve_fallback,
						  &custom_info);
#if LAYOUT_DEBUG
  if (soln_vector) {
    gint i;
    for (i = 0; i < guppi_vector_dim (soln_vector); ++i)
      g_message ("soln  %2d %g", i, guppi_vector_entry (soln_vector, i));
  }
#endif
  
  if (soln_vector) {
    GList *iter;
    gint i = 0;

    for (iter = engine->priv->items; iter != NULL; iter = g_list_next (iter)) {
      ItemInfo *info = iter->data;
      ArtDRect rect;
      rect.x0 = evil_clean (guppi_vector_entry (soln_vector, i));
      rect.x1 = evil_clean (guppi_vector_entry (soln_vector, i+1));
      rect.y0 = evil_clean (guppi_vector_entry (soln_vector, i+3)); /* bottom */
      rect.y1 = evil_clean (guppi_vector_entry (soln_vector, i+2)); /* top */

      rect.x0 = MAX (rect.x0, bbox->x0);
      rect.y0 = MAX (rect.y0, bbox->y0);
      rect.x1 = MIN (rect.x1, bbox->x1);
      rect.y1 = MIN (rect.y1, bbox->y1);
      
      guppi_layout_item_set_allocation (info->item, &rect);

      guppi_debug_v ("Setting to %g:%g %g:%g", rect.x0, rect.x1, rect.y0, rect.y1);
      
      i += 4;
    }

    engine->priv->resolved = TRUE;

  } else {

    g_message ("layout failed");
    engine->priv->resolved = FALSE;

  }

  guppi_matrix_free (layout_matrix);
  guppi_vector_free (layout_vector);
  guppi_vector_free (soln_vector);

  engine->priv->dirty = FALSE;
}

