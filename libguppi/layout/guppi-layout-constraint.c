/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-constraint.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/layout/guppi-layout-constraint.h>

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <libxml/xmlmemory.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>

typedef struct _TermInfo TermInfo;
struct _TermInfo {
  GuppiLayoutConstraintTermType type;
  double factor;
  GuppiLayoutItem *item;
};

struct _GuppiLayoutConstraint {
  gint ref;
  GList *terms;
  gboolean locked;
};

#if 0
static const gchar *term_type_names[GLC_LAST] = {
  "left", "right", "top", "bottom", "width", "height", "horizontal_center", "vertical_center",
  "region_left", "region_right", "region_top", "region_bottom", "region_width", "region_height",
  "region_horizontal_center", "region_vertical_center", "fixed"
};
#endif

static TermInfo *
term_info_new (GuppiLayoutConstraintTermType type,
	       double factor,
	       GuppiLayoutItem *item)
{
  TermInfo *ti;

  g_return_val_if_fail (type >= GLC_REGION_LEFT ? item == NULL : item != NULL, NULL);
  g_return_val_if_fail (item == NULL || GUPPI_IS_LAYOUT_ITEM (item), NULL);

  ti = guppi_new (TermInfo, 1);
  ti->type = type;
  ti->factor = factor;
  ti->item = item;
  guppi_ref (ti->item);

  return ti;
}

static void
term_info_free (TermInfo *ti)
{
  if (ti) {
    guppi_unref (ti->item);
    guppi_free (ti);
  }
}

GuppiLayoutConstraint *
guppi_layout_constraint_new (void)
{
  GuppiLayoutConstraint *glc = guppi_new (GuppiLayoutConstraint, 1);
  glc->ref = 1;
  glc->terms = NULL;
  glc->locked = FALSE;
  
  return glc;
}

void
guppi_layout_constraint_ref (GuppiLayoutConstraint *glc)
{
  if (glc) {
    g_assert (glc->ref > 0);
    ++glc->ref;
  }
}

void
guppi_layout_constraint_unref (GuppiLayoutConstraint *glc)
{
  if (glc) {
    g_assert (glc->ref > 0);
    --glc->ref;

    if (glc->ref == 0) {
      g_list_foreach (glc->terms, (GFunc) term_info_free, NULL);
      g_list_free (glc->terms);
      glc->terms = NULL;

      guppi_free (glc);
    }

  }
}

void
guppi_layout_constraint_lock (GuppiLayoutConstraint *glc)
{
  g_return_if_fail (glc != NULL);

  glc->locked = TRUE;
}

void
guppi_layout_constraint_add_term (GuppiLayoutConstraint *glc,
				  GuppiLayoutConstraintTermType type,
				  double factor,
				  GuppiLayoutItem *item)
{
  TermInfo *ti;
  g_return_if_fail (glc != NULL);

  g_assert (! glc->locked);

  if (fabs (factor) < 1e-12)
    return;

  switch (type) {
    
  case GLC_HORIZONTAL_CENTER:
    guppi_layout_constraint_add_term (glc, GLC_RIGHT, factor/2, item);
    guppi_layout_constraint_add_term (glc, GLC_LEFT,  factor/2, item);
    return;

  case GLC_VERTICAL_CENTER:
    guppi_layout_constraint_add_term (glc, GLC_TOP,    factor/2, item);
    guppi_layout_constraint_add_term (glc, GLC_BOTTOM, factor/2, item);
    return;

  case GLC_REGION_WIDTH:
    guppi_layout_constraint_add_term (glc, GLC_REGION_RIGHT,  factor, item);
    guppi_layout_constraint_add_term (glc, GLC_REGION_LEFT,  -factor, item);
    return;
    
  case GLC_REGION_HEIGHT:
    guppi_layout_constraint_add_term (glc, GLC_REGION_TOP,     factor, item);
    guppi_layout_constraint_add_term (glc, GLC_REGION_BOTTOM, -factor, item);
    return;

  default:
    /* Fall through, do nothing. */
  }

  ti = term_info_new (type, factor, item);
  g_return_if_fail (ti != NULL);

  glc->terms = g_list_append (glc->terms, ti);
}

void
guppi_layout_constraint_add_terms (GuppiLayoutConstraint *glc, ...)
{
  va_list args;
  GuppiLayoutConstraintTermType type;
  double factor;
  GuppiLayoutItem *item;

  va_start (args, glc);

  while ( (type = (GuppiLayoutConstraintTermType) va_arg (args, gint)) != GLC_LAST ) {
    factor = va_arg (args, double);
    item = type >= GLC_REGION_LEFT ? NULL : va_arg (args, GuppiLayoutItem *);
    guppi_layout_constraint_add_term (glc, type, factor, item);
  }
  
  va_end (args);
}

void
guppi_layout_constraint_foreach (GuppiLayoutConstraint *glc,
				 GuppiLayoutConstraintTermFn fn,
				 gpointer closure)
{
  GList *iter;

  g_return_if_fail (glc != NULL);
  g_return_if_fail (fn != NULL);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *ti = iter->data;
    fn (ti->type, ti->factor, ti->item, closure);
  }
}

void
guppi_layout_constraint_foreach_with_region (GuppiLayoutConstraint *glc,
					     ArtDRect *region,
					     GuppiLayoutConstraintTermFn fn,
					     gpointer closure)
{
  GList *iter;

  g_return_if_fail (glc != NULL);
  g_return_if_fail (region != NULL);
  g_return_if_fail (fn != NULL);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *ti = iter->data;

    switch (ti->type) {

    case GLC_WIDTH:
      if (guppi_layout_item_has_width_request (ti->item)) {

	double natural = guppi_layout_item_get_width_request (ti->item);
	fn (GLC_FIXED, ti->factor * natural, NULL, closure);

      } else {

	fn (GLC_RIGHT,  ti->factor, ti->item, closure);
	fn (GLC_LEFT,  -ti->factor, ti->item, closure);

      }
      break;

    case GLC_HEIGHT:
      if (guppi_layout_item_has_height_request (ti->item)) {

	double natural = guppi_layout_item_get_height_request (ti->item);
	fn (GLC_FIXED, ti->factor * natural, NULL, closure);

      } else {

	fn (GLC_TOP,     ti->factor, ti->item, closure);
	fn (GLC_BOTTOM, -ti->factor, ti->item, closure);

      }
      break;

    case GLC_HORIZONTAL_CENTER:
      fn (GLC_RIGHT, ti->factor / 2, ti->item, closure);
      fn (GLC_LEFT,  ti->factor / 2, ti->item, closure);
      break;

    case GLC_VERTICAL_CENTER:
      fn (GLC_BOTTOM, ti->factor / 2, ti->item, closure);
      fn (GLC_TOP,    ti->factor / 2, ti->item, closure);
      break;

    case GLC_REGION_LEFT:
      fn (GLC_FIXED, region->x0 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_RIGHT:
      fn (GLC_FIXED, region->x1 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_BOTTOM:
      fn (GLC_FIXED, region->y0 * ti->factor, NULL, closure);
      break;
      
    case GLC_REGION_TOP:
      fn (GLC_FIXED, region->y1 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_WIDTH:
      fn (GLC_FIXED, (region->x1 - region->x0) * ti->factor, NULL, closure);
      break;

    case GLC_REGION_HEIGHT:
      fn (GLC_FIXED, (region->y1 - region->y0) * ti->factor, NULL, closure);
      break;

    case GLC_REGION_HORIZONTAL_CENTER:
      fn (GLC_FIXED, (region->x0 + region->x1)/2 * ti->factor, NULL, closure);
      break;

    case GLC_REGION_VERTICAL_CENTER:
      fn (GLC_FIXED, (region->y0 + region->y1)/2 * ti->factor, NULL, closure);
      break;
   
    default:

      fn (ti->type, ti->factor, ti->item, closure);
    }

  }
}

gboolean
guppi_layout_constraint_contains (GuppiLayoutConstraint *glc, GuppiLayoutItem *item)
{
  GList *iter;

  g_return_val_if_fail (glc != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (item), FALSE);

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *term = iter->data;
    if (term->item == item)
      return TRUE;
  }
  return FALSE;
}

gboolean
guppi_layout_constraint_replace (GuppiLayoutConstraint *glc,
				 GuppiLayoutItem *old,
				 GuppiLayoutItem *nuevo)
{
  GList *iter;
  gboolean did_something = FALSE;

  g_return_val_if_fail (glc != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (old), FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (nuevo), FALSE);

  if (old == nuevo)
    return FALSE;

  for (iter = glc->terms; iter != NULL; iter = g_list_next (iter)) {
    TermInfo *term = iter->data;
    
    if (term->item == old) {
      guppi_refcounting_assign (term->item, nuevo);
      did_something = TRUE;
    }
  }
  
  return did_something;
}

