/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-item.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_ITEM_H__
#define __GUPPI_LAYOUT_ITEM_H__

#include <glib-object.h>
#include <libart_lgpl/libart.h>

typedef struct _GuppiLayoutItem GuppiLayoutItem;
typedef struct _GuppiLayoutItemIface GuppiLayoutItemIface;

struct _GuppiLayoutItemIface {
  GTypeInterface interface;

  /* vtable */
  void     (*get_request) (GuppiLayoutItem *layout_item,
			   double          *width,
			   double          *height);
  
  gboolean (*get_allocation) (GuppiLayoutItem *layout_item,
			      ArtDRect        *allocation);

  void     (*set_allocation) (GuppiLayoutItem *layout_item,
			      ArtDRect        *allocation);

  /* signals */
  void (*changed_request)    (GuppiLayoutItem *layout_item);
  void (*changed_allocation) (GuppiLayoutItem *layout_item);
};

#define GUPPI_TYPE_LAYOUT_ITEM           (guppi_layout_item_get_type ())
#define GUPPI_LAYOUT_ITEM(obj)           (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_LAYOUT_ITEM, GuppiLayoutItem))
#define GUPPI_IS_LAYOUT_ITEM(obj)        (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_LAYOUT_ITEM))
#define GUPPI_LAYOUT_ITEM_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE((obj), GUPPI_TYPE_LAYOUT_ITEM, GuppiLayoutItemIface))

GType    guppi_layout_item_get_type           (void);

/*** Request-related functions ***/

void     guppi_layout_item_get_request        (GuppiLayoutItem *layout_item,
					       double          *width,
					       double          *height);

gboolean guppi_layout_item_has_width_request  (GuppiLayoutItem *layout_item);

gboolean guppi_layout_item_has_height_request (GuppiLayoutItem *layout_item);

double   guppi_layout_item_get_width_request  (GuppiLayoutItem *layout_item);

double   guppi_layout_item_get_height_request (GuppiLayoutItem *layout_item);

void     guppi_layout_item_changed_request    (GuppiLayoutItem *layout_item);


/*** Allocation-related functions ***/

void     guppi_layout_item_set_allocation     (GuppiLayoutItem *layout_item,
					       ArtDRect        *allocation);

gboolean guppi_layout_item_get_allocation     (GuppiLayoutItem *layout_item,
					       ArtDRect        *allocation);

gboolean guppi_layout_item_has_allocation     (GuppiLayoutItem *layout_item);



#endif /* __GUPPI_LAYOUT_ITEM_H__ */



