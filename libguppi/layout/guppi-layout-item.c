/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-item.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/layout/guppi-layout-item.h>

#include <libguppi/useful/guppi-marshal.h>

static void
guppi_layout_item_base_init (gpointer g_class)
{
  static gboolean initialized = FALSE;

  if (! initialized) {

    g_signal_new ("changed_request",
		  GUPPI_TYPE_LAYOUT_ITEM,
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GuppiLayoutItemIface, changed_request),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

    g_signal_new ("changed_allocation",
		  GUPPI_TYPE_LAYOUT_ITEM,
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GuppiLayoutItemIface, changed_allocation),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

    initialized = TRUE;
  }
}

GType
guppi_layout_item_get_type (void)
{
  static GType type = 0;

  if (! type) {

    static const GTypeInfo info = {
      sizeof (GuppiLayoutItemIface),
      guppi_layout_item_base_init,
      NULL, NULL, NULL, NULL, 0, 0, NULL, NULL
    };

    type = g_type_register_static (G_TYPE_INTERFACE,
				   "GuppiLayoutItem",
				   &info,
				   0);
  }

  return type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*** Request-related functions ***/

void
guppi_layout_item_get_request (GuppiLayoutItem *layout_item,
			       double          *width,
			       double          *height)
{
  GuppiLayoutItemIface *iface;

  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item));

  iface = GUPPI_LAYOUT_ITEM_GET_IFACE (layout_item);

  g_assert (iface->get_request);
  iface->get_request (layout_item, width, height);
}

gboolean
guppi_layout_item_has_width_request (GuppiLayoutItem *layout_item)
{
  return guppi_layout_item_get_width_request (layout_item) >= 0;
}

gboolean
guppi_layout_item_has_height_request (GuppiLayoutItem *layout_item)
{
  return guppi_layout_item_get_height_request (layout_item) >= 0;
}

double
guppi_layout_item_get_width_request (GuppiLayoutItem *layout_item)
{
  double w;
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item), -1);
  guppi_layout_item_get_request (layout_item, &w, NULL);
  return w;
}

double
guppi_layout_item_get_height_request (GuppiLayoutItem *layout_item)
{
  double h;
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item), -1);
  guppi_layout_item_get_request (layout_item, NULL, &h);
  return h;
}

void
guppi_layout_item_changed_request (GuppiLayoutItem *layout_item)
{
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item));

  g_signal_emit_by_name (layout_item, "changed_request");
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*** Allocation-related functions ***/

void
guppi_layout_item_set_allocation (GuppiLayoutItem *layout_item,
				  ArtDRect        *allocation)
{
  GuppiLayoutItemIface *iface;

  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item));
  g_return_if_fail (allocation != NULL);

  iface = GUPPI_LAYOUT_ITEM_GET_IFACE (layout_item);

  g_assert (iface->set_allocation);
  iface->set_allocation (layout_item, allocation);

  g_signal_emit_by_name (layout_item, "changed_allocation");
}

gboolean
guppi_layout_item_get_allocation (GuppiLayoutItem *layout_item,
				  ArtDRect        *allocation)
{
  GuppiLayoutItemIface *iface;

  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item), FALSE);
  g_return_val_if_fail (allocation != NULL, FALSE);

  iface = GUPPI_LAYOUT_ITEM_GET_IFACE (layout_item);

  g_assert (iface->get_allocation);
  return iface->get_allocation (layout_item, allocation);
}

gboolean
guppi_layout_item_has_allocation (GuppiLayoutItem *layout_item)
{
  ArtDRect rect;
  return guppi_layout_item_get_allocation (layout_item, &rect);
}
