/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-rule.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/layout/guppi-layout-rule.h>

#include <string.h>
#include <libxml/xmlmemory.h>
#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-debug.h>

struct _GuppiLayoutRule {
  gint ref;
  gchar *name;
  GList *constraints;
  gboolean locked;
};

GuppiLayoutRule *
guppi_layout_rule_new (const gchar *name)
{
  GuppiLayoutRule *rule = guppi_new (GuppiLayoutRule, 1);
  
  rule->ref = 1;
  rule->name = guppi_strdup (name);
  rule->constraints = NULL;
  rule->locked = FALSE;

  return rule;
}

void
guppi_layout_rule_ref (GuppiLayoutRule *rule)
{
  if (rule) {
    g_assert (rule->ref > 0);
    ++rule->ref;
  }
}

void
guppi_layout_rule_unref (GuppiLayoutRule *rule)
{
  if (rule) {
    g_assert (rule->ref > 0);
    --rule->ref;
    if (rule->ref == 0) {
      
      guppi_free (rule->name);
      
      g_list_foreach (rule->constraints, (GFunc) guppi_layout_constraint_unref, NULL);
      g_list_free (rule->constraints);

      guppi_free (rule);
      
    }
  }
}

const gchar *
guppi_layout_rule_name (GuppiLayoutRule *rule)
{
  g_return_val_if_fail (rule != NULL, NULL);
  return rule->name;
}

void
guppi_layout_rule_lock (GuppiLayoutRule *rule)
{
  g_return_if_fail (rule != NULL);
  rule->locked = TRUE;
  guppi_layout_rule_foreach (rule, (GuppiLayoutRuleConstraintFn) guppi_layout_constraint_lock, NULL);
}

void
guppi_layout_rule_add_constraint (GuppiLayoutRule *rule, GuppiLayoutConstraint *cst)
{
  g_return_if_fail (rule != NULL);
  g_return_if_fail (cst != NULL);
  g_assert (! rule->locked);

  rule->constraints = g_list_append (rule->constraints, cst);
  guppi_layout_constraint_ref (cst);
}

GuppiLayoutConstraint *
guppi_layout_rule_new_constraint (GuppiLayoutRule *rule)
{
  GuppiLayoutConstraint *cst;

  g_return_val_if_fail (rule != NULL, NULL);
  g_return_val_if_fail (! rule->locked, NULL);

  cst = guppi_layout_constraint_new ();
  guppi_layout_rule_add_constraint (rule, cst);
  guppi_layout_constraint_unref (cst);

  return cst;
}

gint
guppi_layout_rule_constraint_count (GuppiLayoutRule *rule)
{
  g_return_val_if_fail (rule != NULL, 0);
  
  return g_list_length (rule->constraints);
}

void
guppi_layout_rule_foreach (GuppiLayoutRule *rule,
			   GuppiLayoutRuleConstraintFn fn,
			   gpointer closure)
{
  g_return_if_fail (rule != NULL);
  g_return_if_fail (fn != NULL);

  g_list_foreach (rule->constraints, (GFunc) fn, closure);
}

static void
merge_cb (GuppiLayoutConstraint *cnt, gpointer closure)
{
  GuppiLayoutRule *target = closure;
  guppi_layout_rule_add_constraint (target, cnt);
}

void
guppi_layout_rule_merge (GuppiLayoutRule *target,
			 GuppiLayoutRule *source)
{
  g_return_if_fail (target != NULL);
  g_return_if_fail (source != NULL);
  
  guppi_layout_rule_foreach (source, merge_cb, target);
}

gboolean
guppi_layout_rule_contains (GuppiLayoutRule *rule,
			    GuppiLayoutItem *layout_item)
{
  GList *iter;

  g_return_val_if_fail (rule != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (layout_item), FALSE);

  for (iter = rule->constraints; iter != NULL; iter = g_list_next (iter)) {
    if (guppi_layout_constraint_contains ((GuppiLayoutConstraint *) iter->data, layout_item))
      return TRUE;
  }

  return FALSE;
}

gboolean
guppi_layout_rule_replace (GuppiLayoutRule *rule,
			   GuppiLayoutItem *old,
			   GuppiLayoutItem *nuevo)
{
  GList *iter;
  gboolean did_something = FALSE;

  g_return_val_if_fail (rule != NULL, FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (old), FALSE);
  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM (nuevo), FALSE);

  for (iter = rule->constraints; iter != NULL; iter = g_list_next (iter)) {
    if (guppi_layout_constraint_replace ((GuppiLayoutConstraint *) iter->data, old, nuevo))
      did_something = TRUE;
  }

  return did_something;
}
