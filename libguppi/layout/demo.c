/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * demo.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <libguppi/layout/guppi-layout-item-trivial.h>
#include <libguppi/layout/guppi-layout-engine.h>

static void
add_rules (GuppiLayoutEngine *engine)
{
  GuppiLayoutItem *a = guppi_layout_item_trivial_new ("a");
  GuppiLayoutItem *b = guppi_layout_item_trivial_new ("b");

  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_flush_left (a, 0));
  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_flush_right (b, 0));
  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_fill_vertically (a, 0, 0));
  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_fill_vertically (b, 0, 0));
  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_horizontally_adjacent (a, b, 0));
  guppi_layout_engine_add_rule (engine, guppi_layout_rule_new_same_size (a, b));
}

static int
do_layout (gpointer closure)
{
  GuppiLayoutEngine *engine = closure;
  ArtDRect bbox;

  bbox.x0 = 0;
  bbox.x1 = 100;
  bbox.y0 = 0;
  bbox.y1 = 100;

  guppi_layout_engine_do_layout (engine, &bbox);

  return 0;
}

static void
schedule_layout (GuppiLayoutEngine *engine, gpointer closure)
{
  g_timeout_add (100, do_layout, engine);
}

int
main (int argc, char *argv[])
{
  GuppiLayoutEngine *engine;
  GMainLoop *main_loop;

  g_type_init ();

  engine = guppi_layout_engine_new ();
  g_signal_connect (engine,
		    "dirty",
		    (GCallback) schedule_layout,
		    NULL);

  add_rules (engine);

  main_loop = g_main_loop_new (NULL, TRUE);

  g_main_loop_run (main_loop);

  return 0;
}

