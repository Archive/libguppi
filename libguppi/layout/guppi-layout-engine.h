/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-engine.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_ENGINE_H__
#define __GUPPI_LAYOUT_ENGINE_H__

#include <glib-object.h>
#include <libguppi/layout/guppi-layout-item.h>
#include <libguppi/layout/guppi-layout-rule.h>
#include <libguppi/layout/guppi-layout-rule-predef.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiLayoutEngine GuppiLayoutEngine;
typedef struct _GuppiLayoutEngineClass GuppiLayoutEngineClass;
struct _GuppiLayoutEnginePrivate;

typedef void (*GuppiLayoutEngineRuleFn) (GuppiLayoutEngine *engine, GuppiLayoutRule *rule, gpointer closure);
typedef void (*GuppiLayoutEngineItemFn) (GuppiLayoutEngine *engine, GuppiLayoutItem *item, gpointer closure);

struct _GuppiLayoutEngine {
  GObject parent;
  struct _GuppiLayoutEnginePrivate *priv;
};

struct _GuppiLayoutEngineClass {
  GObjectClass parent_class;

  void (*changed)     (GuppiLayoutEngine *);
  void (*dirty)       (GuppiLayoutEngine *);
};

#define GUPPI_TYPE_LAYOUT_ENGINE            (guppi_layout_engine_get_type())
#define GUPPI_LAYOUT_ENGINE(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_LAYOUT_ENGINE,GuppiLayoutEngine))
#define GUPPI_LAYOUT_ENGINE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LAYOUT_ENGINE,GuppiLayoutEngineClass))
#define GUPPI_IS_LAYOUT_ENGINE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_LAYOUT_ENGINE))
#define GUPPI_IS_LAYOUT_ENGINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LAYOUT_ENGINE))

GType guppi_layout_engine_get_type (void);

GuppiLayoutEngine *guppi_layout_engine_new (void);

void guppi_layout_engine_add_rule             (GuppiLayoutEngine *engine,
					       GuppiLayoutRule *rule);

void guppi_layout_engine_add_rules            (GuppiLayoutEngine *engine, ...);

void guppi_layout_engine_remove_rule          (GuppiLayoutEngine *engine,
					       GuppiLayoutRule *rule);

void guppi_layout_engine_remove_rules_by_item (GuppiLayoutEngine *engine,
					       GuppiLayoutItem *item);

void guppi_layout_engine_remove_item          (GuppiLayoutEngine *engine,
					       GuppiLayoutItem *item);

void guppi_layout_engine_replace_item         (GuppiLayoutEngine *engine,
					       GuppiLayoutItem *old,
					       GuppiLayoutItem *nuevo);


void guppi_layout_engine_foreach_rule (GuppiLayoutEngine *engine,
				       GuppiLayoutEngineRuleFn fn,
				       gpointer closure);
void guppi_layout_engine_foreach_item (GuppiLayoutEngine *engine,
				       GuppiLayoutEngineItemFn fn,
				       gpointer closure);


gboolean guppi_layout_engine_is_dirty (GuppiLayoutEngine *engine);

void     guppi_layout_engine_freeze   (GuppiLayoutEngine *engine);

void     guppi_layout_engine_thaw     (GuppiLayoutEngine *engine);


void     guppi_layout_engine_do_layout (GuppiLayoutEngine *engine,
					ArtDRect *bbox);



END_GUPPI_DECLS;

#endif /* __GUPPI_LAYOUT_ENGINE_H__ */

