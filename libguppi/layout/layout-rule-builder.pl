#!/usr/bin/perl -w

use strict;

my $mode = shift;


if (! ($mode eq "-c" || $mode eq "-h")) {
    die "Unknown mode";
}



my $filename = shift;

open(IN, $filename) || die "Can't open '$filename'";

if ($mode eq "-h") {
    h_pre ();
} else {
    c_pre ();
}

while (defined (my $line = <IN>)) {

    $line =~ s/\#.*//g; # strip out comments
    chomp $line;

    next unless $line =~ /\S/; # skip whitespace-only lines

    my ($name, $argstr) = $line =~ /^(\S+):\s*(.*)$/;
    my @args = split (/,\s*/, $argstr);

    my @parts = ();

    while (defined($line = <IN>) && $line =~ /\S/) {
	chomp $line;
	$line =~ s/^\s*//g;
	$line =~ s/\s*$//g;
	push (@parts, $line);
    }

    if ($mode eq "-h") {
	h_code ($name, [@args], [@parts]);
    } else {
	c_code ($name, [@args], [@parts]);
    }
}

if ($mode eq "-h") {
    h_post ();
} else {
    c_post ();
}

###########################################################

sub arg2decl {
    my $str = shift;

    if ($str =~ /^@/) {
	return "GuppiLayoutItem *" . substr ($str, 1);
    }
    
    return "double $str";
}

###########################################################

###
### Generate .h file
###

sub h_pre {

    print <<EOF;

/* This code is auto-generated.  EDIT AT YOUR OWN PERIL! */

#ifndef __GUPPI_LAYOUT_RULE_PREDEF_H__
#define __GUPPI_LAYOUT_RULE_PREDEF_H__

#include <libguppi/layout/guppi-layout-rule.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS

EOF

}

sub h_post {

    print <<EOF;

END_GUPPI_DECLS

#endif /* __GUPPI_LAYOUT_RULE_PREDEF_H__ */

/* This code is auto-generated.  EDIT AT YOUR OWN PERIL! */

EOF

}

sub h_code {
    my $name = shift;
    my $arg_ptr = shift;
    my $parts_ptr = shift;

    my $args = join (", ", map { arg2decl ($_) } @$arg_ptr);

    print "GuppiLayoutRule *guppi_layout_rule_new_$name ($args);\n\n";
}

###########################################################

###
### Generate .c file
###

sub c_pre {
    print <<EOF;

/* This code is auto-generated.  EDIT AT YOUR OWN PERIL! */

#include <libguppi/layout/guppi-layout-rule-predef.h>
#include <libguppi/useful/guppi-i18n.h>

EOF
}

sub c_post {
    print <<EOF;

/* This code is auto-generated.  EDIT AT YOUR OWN PERIL! */

EOF
}

sub c_code {
    my $name = shift;
    my $arg_ptr = shift;
    my $parts_ptr = shift;

    my $args = join (", ", map { arg2decl ($_) } @$arg_ptr);

    my $nice_name = $name;
    $nice_name =~ s/_/ /g;
    $nice_name =~ s/(\s|^)([a-z])/$1.uc($2)/eg;

    print "/* This code is auto-generated.  EDIT AT YOUR OWN PERIL! */\n";
    print "GuppiLayoutRule *\n";
    print "guppi_layout_rule_new_$name ($args)\n";
    print "{\n";
    print "  GuppiLayoutRule *rule;\n";

    foreach my $a (@$arg_ptr) {
	if ($a =~ /^@/) {
	    my $aa = substr ($a, 1);
	    print "  g_return_val_if_fail (GUPPI_IS_LAYOUT_ITEM ($aa), NULL);\n";
	}
    }

    print "  rule = guppi_layout_rule_new (_(\"$nice_name\"));\n";

    foreach my $p (@$parts_ptr) {
	if ($p =~ /^merge\s+([^\(]+)\s*\((.*)\)$/) {
	    print "  {\n";
	    print "    GuppiLayoutRule *merge = guppi_layout_rule_new_$1 ($2);\n";
	    print "    guppi_layout_rule_merge (rule, merge);\n";
	    print "    guppi_layout_rule_unref (merge);\n";
	    print "  }\n";

	} else {
	    print "  guppi_layout_constraint_add_terms (guppi_layout_rule_new_constraint (rule),\n";

	    my @terms = split (' ', $p);
	    foreach my $t (@terms) {
		my @subterms = split (':', $t);
		my $factor = $subterms[0];
		my $type = "GLC_" . uc $subterms[1];
		print "                                     $type, $factor,";
		if ($subterms[2]) {
		    print " ", $subterms[2], ",";
		}
		print "\n";
	    }
	    print "                                     GLC_LAST);\n";
	}
    }

    print "  guppi_layout_rule_lock (rule);\n";
    print "  return rule;\n";
    print "}\n\n";
    
    
}
