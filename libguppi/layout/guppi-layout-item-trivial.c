/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-item-trivial.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <libguppi/layout/guppi-layout-item-trivial.h>

#include <libguppi/layout/guppi-layout-item.h>
#include <libguppi/useful/guppi-memory.h>

static GObjectClass *parent_class;

static void
guppi_layout_item_trivial_get_request (GuppiLayoutItem *item,
				       double          *width,
				       double          *height)
{
  GuppiLayoutItemTrivial *triv = GUPPI_LAYOUT_ITEM_TRIVIAL (item);

  if (width)
    *width = triv->width_request;

  if (height)
    *height = triv->height_request;
}

static gboolean
guppi_layout_item_trivial_get_allocation (GuppiLayoutItem *item,
					  ArtDRect *allocation)
{
  GuppiLayoutItemTrivial *triv = GUPPI_LAYOUT_ITEM_TRIVIAL (item);

  if (! triv->has_allocation)
    return FALSE;

  if (allocation)
    *allocation = triv->allocation;

  return TRUE;
}

static void
guppi_layout_item_trivial_set_allocation (GuppiLayoutItem *item,
					  ArtDRect *allocation)
{
  GuppiLayoutItemTrivial *triv = GUPPI_LAYOUT_ITEM_TRIVIAL (item);

  triv->has_allocation = TRUE;
  triv->allocation = *allocation;

  g_print ("%s: %g:%g %g:%g\n",
	   triv->label,
	   triv->allocation.x0,
	   triv->allocation.x1,
	   triv->allocation.y0,
	   triv->allocation.y1);
}

static void
guppi_layout_item_trivial_guppi_layout_item_init (GuppiLayoutItemIface *iface)
{
  iface->get_request = guppi_layout_item_trivial_get_request;
  iface->get_allocation = guppi_layout_item_trivial_get_allocation;
  iface->set_allocation = guppi_layout_item_trivial_set_allocation;
}

static void
guppi_layout_item_trivial_class_init (GuppiLayoutItemTrivialClass *klass)
{
  parent_class = g_type_class_peek_parent (klass);
}

static void
guppi_layout_item_trivial_init (GuppiLayoutItemTrivial *obj)
{
  obj->width_request = -1;
  obj->height_request = -1;
  obj->has_allocation = FALSE;
}

GType
guppi_layout_item_trivial_get_type (void)
{
  static GType type = 0;

  if (! type) {
    
    static const GTypeInfo info = {
      sizeof (GuppiLayoutItemTrivialClass),
      NULL, NULL,
      (GClassInitFunc) guppi_layout_item_trivial_class_init,
      NULL, NULL,
      sizeof (GuppiLayoutItemTrivial),
      0,
      (GInstanceInitFunc) guppi_layout_item_trivial_init
    };

    static const GInterfaceInfo guppi_layout_item_info = {
      (GInterfaceInitFunc) guppi_layout_item_trivial_guppi_layout_item_init,
      NULL, NULL
    };

    type = g_type_register_static (G_TYPE_OBJECT,
				   "GuppiLayoutItemTrivial",
				   &info,
				   0);

    g_type_add_interface_static (type,
				 GUPPI_TYPE_LAYOUT_ITEM,
				 &guppi_layout_item_info);

  }

  return type;
}

GuppiLayoutItem *
guppi_layout_item_trivial_new (const char *label)
{
  GuppiLayoutItemTrivial *triv;
  static int foo = 1;

  triv = guppi_object_new (GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL);
  if (label && *label) {
    triv->label = g_strdup (label);
  } else {
    triv->label = g_strdup_printf ("Unlabelled #%d", foo);
    ++foo;
  }

  return (GuppiLayoutItem *) triv;
}

void
guppi_layout_item_trivial_set_request (GuppiLayoutItemTrivial *trivial,
				       double width_request,
				       double height_request)
{
  g_return_if_fail (GUPPI_IS_LAYOUT_ITEM_TRIVIAL (trivial));

  if (trivial->width_request != width_request
      || trivial->height_request != height_request) {
    trivial->width_request = width_request;
    trivial->height_request = height_request;

    guppi_layout_item_changed_request (GUPPI_LAYOUT_ITEM (trivial));
  }
}
