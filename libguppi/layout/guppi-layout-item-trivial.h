/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-layout-item-trivial.h
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_LAYOUT_ITEM_TRIVIAL_H__
#define __GUPPI_LAYOUT_ITEM_TRIVIAL_H__

#include <glib-object.h>
#include <libart_lgpl/libart.h>
#include <libguppi/layout/guppi-layout-item.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiLayoutItemTrivial GuppiLayoutItemTrivial;
typedef struct _GuppiLayoutItemTrivialClass GuppiLayoutItemTrivialClass;

struct _GuppiLayoutItemTrivial {
  GObject parent;

  char *label;

  double width_request, height_request;

  gboolean has_allocation;
  ArtDRect allocation;
};

struct _GuppiLayoutItemTrivialClass {
  GObjectClass parent_class;
};

#define GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL            (guppi_layout_item_trivial_get_type ())
#define GUPPI_LAYOUT_ITEM_TRIVIAL(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL, GuppiLayoutItemTrivial))
#define GUPPI_LAYOUT_ITEM_TRIVIAL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass), GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL, GuppiLayoutItemTrivialClass))
#define GUPPI_IS_LAYOUT_ITEM_TRIVIAL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL))
#define GUPPI_IS_LAYOUT_ITEM_TRIVIAL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LAYOUT_ITEM_TRIVIAL))

GType guppi_layout_item_trivial_get_type (void);

GuppiLayoutItem *guppi_layout_item_trivial_new (const char *label);

void guppi_layout_item_trivial_set_request (GuppiLayoutItemTrivial *trivial,
					    double                   width_request,
					    double                   height_request);

END_GUPPI_DECLS;


#endif /* __GUPPI_LAYOUT_ITEM_TRIVIAL_H__ */

