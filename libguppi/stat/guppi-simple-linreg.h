/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-simple-linreg.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SIMPLE_LINREG_H
#define _INC_GUPPI_SIMPLE_LINREG_H

/* #include <gtk/gtk.h> */

#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/data/guppi-seq-boolean.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiSimpleLinreg GuppiSimpleLinreg;
typedef struct _GuppiSimpleLinregClass GuppiSimpleLinregClass;

struct _GuppiSimpleLinreg {
  GtkObject parent;

  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  GuppiSeqBoolean *mask_data;

  /* Results of regression calculation */
  gboolean valid;
  guint count;
  double slope, intercept;
  double residual_sdev;
  double slope_serr, intercept_serr;
  double slope_t, intercept_t;
  double slope_p, intercept_p;
  double ess, rss, tss_x, tss_y;
  double mean_x, mean_y;
  double F;
  double p;
  double R, Rsq, adj_Rsq;
};

struct _GuppiSimpleLinregClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiSimpleLinreg *);
};

#define GUPPI_TYPE_SIMPLE_LINREG (guppi_simple_linreg_get_type())
#define GUPPI_SIMPLE_LINREG(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SIMPLE_LINREG,GuppiSimpleLinreg))
#define GUPPI_SIMPLE_LINREG0(obj) ((obj) ? (GUPPI_SIMPLE_LINREG(obj)) : NULL)
#define GUPPI_SIMPLE_LINREG_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SIMPLE_LINREG,GuppiSimpleLinregClass))
#define GUPPI_IS_SIMPLE_LINREG(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SIMPLE_LINREG))
#define GUPPI_IS_SIMPLE_LINREG0(obj) (((obj) == NULL) || (GUPPI_IS_SIMPLE_LINREG(obj)))
#define GUPPI_IS_SIMPLE_LINREG_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SIMPLE_LINREG))

GtkType guppi_simple_linreg_get_type (void);

GuppiSimpleLinreg *guppi_simple_linreg_new (void);

void guppi_simple_linreg_set_x_data (GuppiSimpleLinreg *, GuppiSeqScalar *);
void guppi_simple_linreg_set_y_data (GuppiSimpleLinreg *, GuppiSeqScalar *);
void guppi_simple_linreg_set_mask (GuppiSimpleLinreg *, GuppiSeqBoolean *);

double guppi_simple_linreg_slope (const GuppiSimpleLinreg *);
double guppi_simple_linreg_intercept (const GuppiSimpleLinreg *);
double guppi_simple_linreg_R (const GuppiSimpleLinreg *);
double guppi_simple_linreg_R_squared (const GuppiSimpleLinreg *);

double guppi_simple_linreg_residual_sdev (const GuppiSimpleLinreg *);

double guppi_simple_linreg_slope_serr (const GuppiSimpleLinreg *);
double guppi_simple_linreg_intercept_serr (const GuppiSimpleLinreg *);

double guppi_simple_linreg_slope_t (const GuppiSimpleLinreg *);
double guppi_simple_linreg_intercept_t (const GuppiSimpleLinreg *);

double guppi_simple_linreg_slope_p (const GuppiSimpleLinreg *);
double guppi_simple_linreg_intercept_p (const GuppiSimpleLinreg *);

double guppi_simple_linreg_F (const GuppiSimpleLinreg *);
double guppi_simple_linreg_F_p (const GuppiSimpleLinreg *);


END_GUPPI_DECLS

#endif /* _INC_GUPPI_SIMPLE_LINREG_H */

/* $Id$ */
