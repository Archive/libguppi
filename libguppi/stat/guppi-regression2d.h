/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression2d.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_REGRESSION2D_H
#define _INC_GUPPI_REGRESSION2D_H

#include <gnome.h>
#include <libguppi/useful/guppi-defs.h>

#include <libguppi/data/guppi-seq-scalar.h>
#include <libguppi/data/guppi-seq-boolean.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiRegression2D GuppiRegression2D;
typedef struct _GuppiRegression2DClass GuppiRegression2DClass;
typedef enum {
  GUPPI_REGRESSION2D_X_DATA  = 1<<0,
  GUPPI_REGRESSION2D_Y_DATA  = 1<<1,
  GUPPI_REGRESSION2D_MASK    = 1<<2,
  GUPPI_REGRESSION2D_WEIGHTS = 1<<3,
  GUPPI_REGRESSION2D_OTHER   = 1<<4
} GuppiRegression2DElement;

struct _GuppiRegression2D {
  GtkObject parent;
  gpointer opaque_internals;
};

struct _GuppiRegression2DClass {
  GtkObjectClass parent_class;

  void (*changed) (GuppiRegression2D *, guint elements);

  gboolean allow_mask, allow_weights;
};

#define GUPPI_TYPE_REGRESSION2D (guppi_regression2d_get_type ())
#define GUPPI_REGRESSION2D(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_REGRESSION2D,GuppiRegression2D))
#define GUPPI_REGRESSION2D0(obj) ((obj) ? (GUPPI_REGRESSION2D(obj)) : NULL)
#define GUPPI_REGRESSION2D_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_REGRESSION2D,GuppiRegression2DClass))
#define GUPPI_IS_REGRESSION2D(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_REGRESSION2D))
#define GUPPI_IS_REGRESSION2D0(obj) (((obj) == NULL) || (GUPPI_IS_REGRESSION2D(obj)))
#define GUPPI_IS_REGRESSION2D_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_REGRESSION2D))

GtkType guppi_regression2d_get_type (void);

/* GtkObject *guppi_regression2d_new (void); */

void guppi_regression2d_changed (GuppiRegression2D *);

gboolean guppi_regression2d_allow_mask    (GuppiRegression2D *);
gboolean guppi_regression2d_allow_weights (GuppiRegression2D *);

GuppiSeqScalar  *guppi_regression2d_x_data  (GuppiRegression2D *);
GuppiSeqScalar  *guppi_regression2d_y_data  (GuppiRegression2D *);
GuppiSeqBoolean *guppi_regression2d_mask    (GuppiRegression2D *);
GuppiSeqScalar  *guppi_regression2d_weights (GuppiRegression2D *);

void guppi_regression2d_set_x_data  (GuppiRegression2D *, GuppiSeqScalar *);
void guppi_regression2d_set_y_data  (GuppiRegression2D *, GuppiSeqScalar *);
void guppi_regression2d_set_mask    (GuppiRegression2D *, GuppiSeqBoolean *);
void guppi_regression2d_set_weights (GuppiRegression2D *, GuppiSeqScalar *);

void guppi_regression2d_freeze (GuppiRegression2D *);
void guppi_regression2d_thaw   (GuppiRegression2D *);




END_GUPPI_DECLS;

#endif /* _INC_GUPPI_REGRESSION2D_H */

/* $Id$ */
