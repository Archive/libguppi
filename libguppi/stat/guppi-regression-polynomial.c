/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-regression-polynomial.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/math/guppi-matrix.h>
#include <libguppi/stat/guppi-regression-polynomial.h>

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

typedef struct _GuppiRegressionPolynomialPrivate GuppiRegressionPolynomialPrivate;
struct _GuppiRegressionPolynomialPrivate {
  gint degree;
  double *coeff;
  gboolean valid;
};

#define priv(x) ((GuppiRegressionPolynomialPrivate*)(GUPPI_REGRESSION_POLYNOMIAL((x))->opaque_internals))

static void guppi_regression_polynomial_recalc (GuppiRegression2D *, guint);

static void
guppi_regression_polynomial_get_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_regression_polynomial_set_arg (GtkObject *obj, GtkArg *arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_regression_polynomial_finalize (GtkObject *obj)
{
  GuppiRegressionPolynomial *x = GUPPI_REGRESSION_POLYNOMIAL(obj);
  GuppiRegressionPolynomialPrivate *p = priv (x);

  guppi_free0 (p->coeff);

  g_free (x->opaque_internals);
  x->opaque_internals = NULL;

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_regression_polynomial_class_init (GuppiRegressionPolynomialClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiRegression2DClass *twod_class = GUPPI_REGRESSION2D_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_REGRESSION2D);

  twod_class->changed = guppi_regression_polynomial_recalc;

  object_class->get_arg = guppi_regression_polynomial_get_arg;
  object_class->set_arg = guppi_regression_polynomial_set_arg;
  object_class->finalize = guppi_regression_polynomial_finalize;

}

static void
guppi_regression_polynomial_init (GuppiRegressionPolynomial *obj)
{
  GuppiRegressionPolynomialPrivate *p = g_new0 (GuppiRegressionPolynomialPrivate, 1);
  obj->opaque_internals = p;

  p->degree = -1;
  p->coeff = NULL;

}

GtkType
guppi_regression_polynomial_get_type (void)
{
  static GtkType guppi_regression_polynomial_type = 0;
  if (!guppi_regression_polynomial_type) {
    static const GtkTypeInfo guppi_regression_polynomial_info = {
      "GuppiRegressionPolynomial",
      sizeof (GuppiRegressionPolynomial),
      sizeof (GuppiRegressionPolynomialClass),
      (GtkClassInitFunc)guppi_regression_polynomial_class_init,
      (GtkObjectInitFunc)guppi_regression_polynomial_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_regression_polynomial_type = gtk_type_unique (GUPPI_TYPE_REGRESSION2D, &guppi_regression_polynomial_info);
  }
  return guppi_regression_polynomial_type;
}

GuppiRegression2D *
guppi_regression_polynomial_new (void)
{
  return GUPPI_REGRESSION2D (guppi_type_new (guppi_regression_polynomial_get_type ()));
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_regression_polynomial_set_degree (GuppiRegressionPolynomial *reg, gint n)
{
  GuppiRegressionPolynomialPrivate *p;
  gint i;

  g_return_if_fail (GUPPI_IS_REGRESSION_POLYNOMIAL (reg));
  g_return_if_fail (n >= 0);

  p = priv (reg);

  if (n == p->degree)
    return;

  p->degree = n;
  guppi_free (p->coeff);
  p->coeff = guppi_new (double, n+1);
  for (i = 0; i <= n; ++i)
    p->coeff[i] = 0;

  guppi_regression2d_changed (GUPPI_REGRESSION2D (reg));
}

gint
guppi_regression_polynomial_degree (GuppiRegressionPolynomial *reg)
{
  g_return_val_if_fail (GUPPI_IS_REGRESSION_POLYNOMIAL (reg), -1);

  return priv (reg)->degree;
}

GuppiPolynomial *
guppi_regression_polynomial_get_polynomial (GuppiRegressionPolynomial *reg)
{
  GuppiRegressionPolynomialPrivate *p;

  g_return_val_if_fail (GUPPI_IS_REGRESSION_POLYNOMIAL (reg), NULL);
  p = priv (reg);

  return p->coeff ? guppi_polynomial_newv (p->degree, p->coeff) : NULL;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_regression_polynomial_recalc (GuppiRegression2D *reg, guint elements)
{
  GuppiRegressionPolynomial *poly;
  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  gboolean has_missing;
  gint deg;

  gconstpointer x_raw, y_raw;
  gint x_stride=0, y_stride=0, i, i0, i1, j, k;
  GuppiVector *vec, *b;
  GuppiMatrix *M, *Mi;

  g_return_if_fail (GUPPI_IS_REGRESSION_POLYNOMIAL (reg));
  poly = GUPPI_REGRESSION_POLYNOMIAL (reg);
  priv (poly)->valid = FALSE;

  x_data = guppi_regression2d_x_data (reg);
  y_data = guppi_regression2d_y_data (reg);
  deg = guppi_regression_polynomial_degree (GUPPI_REGRESSION_POLYNOMIAL (reg));

  if (x_data == NULL || y_data == NULL || deg < 0)
    return;

  has_missing = guppi_seq_has_missing (GUPPI_SEQ (x_data)) || guppi_seq_has_missing (GUPPI_SEQ (y_data));
  guppi_seq_common_bounds (GUPPI_SEQ (x_data), GUPPI_SEQ (y_data), &i0, &i1);

  x_raw = guppi_seq_scalar_raw (x_data, &x_stride);
  y_raw = guppi_seq_scalar_raw (y_data, &y_stride);

  vec = guppi_vector_new (deg+1);

  M = guppi_matrix_new (deg+1, deg+1);
  guppi_matrix_set_constant (M, 0);

  for (i = i0; i <= i1; ++i) {
    double x, y, q, xi, xij, xijk;

    if (x_raw)
      x = guppi_seq_scalar_raw_get (x_raw, x_stride, i);
    else
      x = guppi_seq_scalar_get (x_data, i);

    if (y_raw)
      y = guppi_seq_scalar_raw_get (y_raw, y_stride, i);
    else
      y = guppi_seq_scalar_get (y_data, i);

    q = 1.0;
    for (j = 0; j <= deg; ++j) {
      guppi_vector_entry (vec, j) += q * y;
      q *= x;
    }

    xi = x;
    xij = 1;
    
    for (j = 0; j <= deg; ++j) {
      xijk = xij;
      for (k = 0; k <= deg; ++k) {
	guppi_matrix_entry (M, j, k) += xijk;
	xijk *= xi;
      }
      xij *= xi;
    }
  }

  Mi = guppi_matrix_invert (M);
  b = guppi_matrix_apply (Mi, vec);
  for (i = 0; i <= deg; ++i) {
    priv (poly)->coeff[i] = guppi_vector_entry (b, i);
  }
  
  guppi_matrix_free (M);
  guppi_matrix_free (Mi);
  guppi_vector_free (vec);
  guppi_vector_free (b);
  
}
