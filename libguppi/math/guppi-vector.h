/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-vector.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

/* 
   WARNING!   WARNING!   WARNING!   WARNING!   WARNING!   WARNING!

   This implementation is not suitable for any sort of heavy-duty
   (or even medium-duty) linear algebra computations.  It is only
   a convenience to be used by Guppi internally, and shouldn't be
   directly exposed to the user in any way.

   If you want to do real linear algebra calculations, use a real
   linear algebra package --- not this hacked-up mess.

   WARNING!   WARNING!   WARNING!   WARNING!   WARNING!   WARNING!
*/

#ifndef __GUPPI_VECTOR_H__
#define __GUPPI_VECTOR_H__

#include <glib.h>

typedef struct _GuppiVector GuppiVector;
struct _GuppiVector {
  gint n;
  double *v;
  double epsilon;
};

GuppiVector *guppi_vector_new       (gint n);
GuppiVector *guppi_vector_new_basis (gint n, gint i);

GuppiVector *guppi_vector_copy (GuppiVector *);
void         guppi_vector_free (GuppiVector *);

#define guppi_vector_dim(x) ((x)->n)
#define guppi_vector_data(x) ((x)->v)
#define guppi_vector_entry(x,i) (*((x)->v+(i)))

double   guppi_vector_norm_sq  (GuppiVector *);
double   guppi_vector_norm     (GuppiVector *);
gboolean guppi_vector_is_zero  (GuppiVector *);
gboolean guppi_vector_same_dim (GuppiVector *, GuppiVector *);
gboolean guppi_vector_equal    (GuppiVector *, GuppiVector *);

void     guppi_vector_scalar_multiply (GuppiVector *v, double c);
double   guppi_vector_dot_product     (GuppiVector *v, GuppiVector *w);

void     guppi_vector_normalize (GuppiVector *);

void     guppi_vector_spew (GuppiVector *);

#endif /* __GUPPI_VECTOR_H__ */

