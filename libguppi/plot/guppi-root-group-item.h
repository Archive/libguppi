/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ROOT_GROUP_ITEM_H
#define _INC_GUPPI_ROOT_GROUP_ITEM_H

/* #include <gnome.h> */
#include <libguppi/data/guppi-data.h>
#include <libguppi/plot/guppi-canvas-group.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

enum {
  ROOT_GROUP_RESIZE_NONE,
  ROOT_GROUP_RESIZE_FIT_BEST,
  ROOT_GROUP_RESIZE_FIT_HORIZONTAL,
  ROOT_GROUP_RESIZE_FIT_VERTICAL,
  ROOT_GROUP_RESIZE_FIT_BEST_GROW_ONLY,
  ROOT_GROUP_RESIZE_FIT_BEST_SHRINK_ONLY,
  ROOT_GROUP_RESIZE_FILL_SPACE
};

typedef struct _GuppiRootGroupItem GuppiRootGroupItem;
typedef struct _GuppiRootGroupItemClass GuppiRootGroupItemClass;

struct _GuppiRootGroupItem {
  GuppiCanvasGroup parent;
  GnomeCanvasItem *background;

  guint pending_button_press;
  GuppiCanvasItem *pending_item;
  GuppiPlotTool *pending_tool;
  gboolean pending_released;
  gint pending_skips;

  guint last_key, last_key_state;
  guint pending_key_release;

  gint pending_c_x, pending_c_y;

  GuppiPlotTool *active_tool;

  guint kp_sig, kr_sig;
  GtkWidget *key_event_source;

  GuppiCanvasItem *dnd_highlighted_item;
  GnomeCanvasItem *dnd_highlight;
  GuppiData *dnd_dropped_data;
  guint drag_leave_sig, drag_motion_sig, drag_drop_sig, drag_data_recv_sig;

  guint resize_sig;
  gint resize_semantics;
};

struct _GuppiRootGroupItemClass {
  GuppiCanvasGroupClass parent_class;
};

#define GUPPI_TYPE_ROOT_GROUP_ITEM (guppi_root_group_item_get_type())
#define GUPPI_ROOT_GROUP_ITEM(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_ROOT_GROUP_ITEM,GuppiRootGroupItem))
#define GUPPI_ROOT_GROUP_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ROOT_GROUP_ITEM,GuppiRootGroupItemClass))
#define GUPPI_IS_ROOT_GROUP_ITEM(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_ROOT_GROUP_ITEM))
#define GUPPI_IS_ROOT_GROUP_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ROOT_GROUP_ITEM))

GType guppi_root_group_item_get_type (void);

GObject *guppi_root_group_item_new (void);

double guppi_root_group_item_horizontal_fit_scale (GuppiRootGroupItem *);
double guppi_root_group_item_vertical_fit_scale (GuppiRootGroupItem *);
double guppi_root_group_item_best_fit_scale (GuppiRootGroupItem *);

void guppi_root_group_item_horizontal_fit (GuppiRootGroupItem *);
void guppi_root_group_item_vertical_fit (GuppiRootGroupItem *);
void guppi_root_group_item_best_fit (GuppiRootGroupItem *);

void guppi_root_group_item_set_resize_semantics (GuppiRootGroupItem *, gint);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_ROOT_GROUP_ITEM_H */

/* $Id$ */
