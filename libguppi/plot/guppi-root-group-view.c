/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-root-group-view.h>

#include <string.h>
#include <stdlib.h>

#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/plot/guppi-root-group-state.h>

/* Harmonious defaults */
#define DEFAULT_WIDTH  (6*72)
#define DEFAULT_HEIGHT (6*72/1.61803398874989484820)

static GObjectClass *parent_class = NULL;

static void
guppi_root_group_view_finalize (GObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
view_init (GuppiElementView *view)
{

}

static void
xml_export (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr root_node)
{
  GuppiRootGroupView *rgv = GUPPI_ROOT_GROUP_VIEW (view);
  xmlNodePtr rgv_node;
  gchar buf[64];

  rgv_node = xmlNewNode (doc->ns, "RootGroupView_size");
  
  g_snprintf (buf, 64, "%g", rgv->width);
  xmlNewProp (rgv_node, "width", buf);

  g_snprintf (buf, 64, "%g", rgv->height);
  xmlNewProp (rgv_node, "height", buf);

  xmlAddChild (root_node, rgv_node);

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_export)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_export (view, doc, root_node);
}

static gboolean
xml_import (GuppiElementView *view, GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiRootGroupView *rgv = GUPPI_ROOT_GROUP_VIEW (view);
  gchar *s;

  if (!strcmp (node->name, "RootGroupView_size")) {
    s = xmlGetProp (node, "width");
    rgv->width = s ? atof (s) : DEFAULT_WIDTH;
    xmlFree (s);

    s = xmlGetProp (node, "height");
    rgv->height = s ? atof (s) : DEFAULT_HEIGHT;
    xmlFree (s);
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_import)
    return GUPPI_ELEMENT_VIEW_CLASS (parent_class)->xml_import (view, doc, node);

  return TRUE;
}

static void
guppi_root_group_view_class_init (GuppiRootGroupViewClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_root_group_view_finalize;

  view_class->view_init = view_init;

  view_class->xml_export = xml_export;
  view_class->xml_import = xml_import;
}

/***************************************************************************/

static void
guppi_root_group_view_init (GuppiRootGroupView *obj)
{
  obj->width = DEFAULT_WIDTH;
  obj->height = DEFAULT_HEIGHT;
}

GType 
guppi_root_group_view_get_type (void)
{
  static GType guppi_root_group_view_type = 0;

  if (!guppi_root_group_view_type) {

    static const GTypeInfo guppi_root_group_view_info = {
      sizeof (GuppiRootGroupViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_root_group_view_class_init,
      NULL, NULL,
      sizeof (GuppiRootGroupView),
      0,
      (GInstanceInitFunc) guppi_root_group_view_init,
    };

    guppi_root_group_view_type =
      g_type_register_static (GUPPI_TYPE_GROUP_VIEW, 
			      "GuppiRootGroupView",
			      &guppi_root_group_view_info,
			      0);
  }

  return guppi_root_group_view_type;
}

GuppiRootGroupView *
guppi_root_group_view_new (void)
{
  GuppiElementState *state;
  GuppiElementView *view;

  state = guppi_root_group_state_new ();
  view = guppi_element_state_make_view (state);
  guppi_unref (state);

  return GUPPI_ROOT_GROUP_VIEW (view);
}

void
guppi_root_group_view_set_size (GuppiRootGroupView *root,
				double width, double height)
{
  ArtDRect rect;

  g_return_if_fail (GUPPI_IS_ROOT_GROUP_VIEW (root));
  g_return_if_fail (width > 0);
  g_return_if_fail (height > 0);

  rect.x0 = 0;
  rect.x1 = width;
  rect.y0 = 0;
  rect.y1 = height;

  guppi_layout_item_set_allocation (GUPPI_LAYOUT_ITEM (root), &rect);
}

/* $Id$ */
