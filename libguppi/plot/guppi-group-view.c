/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-group-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-group-view.h>

#include <string.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-marshal.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/plot/guppi-group-state.h>

enum {
  VIEW_ADD,
  VIEW_REMOVE,
  VIEW_REPLACE,
  LAST_SIGNAL
};

guint guppi_group_view_signals[LAST_SIGNAL] = { 0 };


typedef struct _GuppiGroupViewPrivate GuppiGroupViewPrivate;
struct _GuppiGroupViewPrivate {
  GList *elements;

  GuppiLayoutEngine *layout;
  guint pending_layout;

  gint block_changed_propogation_to_children;

  gboolean touched;
};

#define priv(x) (GUPPI_GROUP_VIEW(x)->priv)

static GObjectClass *parent_class = NULL;

static void
guppi_group_view_finalize (GObject *obj)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (obj);
  GuppiGroupViewPrivate *p = priv (grp);

  if (p->pending_layout)
    g_source_remove (p->pending_layout);

  guppi_unref0 (p->layout);
  g_list_foreach (p->elements, guppi_unref2, NULL);
  g_list_free (p->elements);

  guppi_free0 (grp->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static gboolean
find (GuppiElementView *view, const gchar *label, GuppiElementState **es, GuppiElementView **ev)
{
  GuppiElementViewClass *klass;
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);
  GList *iter;

  for (iter = priv (grp)->elements; iter != NULL; iter = g_list_next (iter)) {
    GuppiElementView *subview = iter->data;
    klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (subview));
    if (klass->find (subview, label, es, ev))
      return TRUE;
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->find)
    return GUPPI_ELEMENT_VIEW_CLASS (parent_class)->find (view, label, es, ev);

  return FALSE;
}

/**************************************************************************/

static void
freeze (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);

  guppi_layout_engine_freeze (priv (grp)->layout);
}

static void
thaw (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);

  guppi_layout_engine_thaw (priv (grp)->layout);
}

struct RenderInfo {
  GuppiCanvas *canvas;
  ArtDRect *bbox;
};

static void
render_iterator (GuppiElementView *view,
		 gpointer user_data)
{
  struct RenderInfo *info = user_data;

  guppi_element_view_render (view, info->bbox, info->canvas);
}

static void
render (GuppiElementView *view,
	ArtDRect         *bbox,
	GuppiCanvas      *canvas)
{
  struct RenderInfo info;

  info.canvas = canvas;
  info.bbox = bbox;

  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view),
			    render_iterator,
			    &info);
}

static void
set_allocation (GuppiElementView *view)
{
  GuppiGroupView *grp = GUPPI_GROUP_VIEW (view);
  ArtDRect bbox;

  if (guppi_layout_item_get_allocation (GUPPI_LAYOUT_ITEM (view), &bbox)) {
    guppi_layout_engine_do_layout (priv (grp)->layout, &bbox);
  }
}

/**************************************************************************/

static void
changed (GuppiElementView *view)
{
  GuppiGroupView *grp = (GuppiGroupView *) view;

  if (grp->priv->block_changed_propogation_to_children == 0) {
    guppi_group_view_foreach (grp,
			      (void (*)(GuppiElementView *, gpointer)) guppi_element_view_changed,
			      NULL);
  }

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed (view);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static gboolean
do_layout_cb (gpointer user_data)
{
  GuppiGroupView *grp = user_data;

  if (guppi_layout_engine_is_dirty (grp->priv->layout)) {
    ArtDRect bbox;
    if (guppi_layout_item_get_allocation (GUPPI_LAYOUT_ITEM (grp), &bbox)) {
      guppi_layout_engine_do_layout (priv (grp)->layout, &bbox);
    }
  }

  grp->priv->pending_layout = 0;

  return FALSE;
}

static void
dirty_layout_cb (GuppiLayoutEngine *lay,
		 gpointer           user_data)
{
  GuppiGroupView *grp = user_data;

  if (grp->priv->pending_layout)
    return;

  grp->priv->pending_layout = g_idle_add (do_layout_cb, grp);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_group_view_class_init (GuppiGroupViewClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_group_view_finalize;

  view_class->freeze            = freeze;
  view_class->thaw              = thaw;
  view_class->render            = render;
  view_class->set_allocation    = set_allocation;
  view_class->find              = find;
  view_class->changed           = changed;

  guppi_group_view_signals[VIEW_ADD] = 
    g_signal_new ("view_add",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiGroupViewClass, view_add),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);
  
  guppi_group_view_signals[VIEW_REMOVE] = 
    g_signal_new ("view_remove",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiGroupViewClass, view_remove),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER,
		  G_TYPE_NONE, 1, G_TYPE_POINTER);

  guppi_group_view_signals[VIEW_REPLACE] = 
    g_signal_new ("view_replace",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiGroupViewClass, view_replace),
		  NULL, NULL,
		  guppi_marshal_VOID__POINTER_POINTER,
		  G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
}

static void
guppi_group_view_init (GuppiGroupView *obj)
{
  GuppiGroupViewPrivate *p;
  p = guppi_new0 (GuppiGroupViewPrivate, 1);
  obj->priv = p;

  p->layout = guppi_layout_engine_new ();
  g_signal_connect (p->layout,
		    "dirty",
		    (GCallback) dirty_layout_cb,
		    obj);

  //guppi_element_view_set_clipped ((GuppiElementView *) obj, FALSE);
}

GType
guppi_group_view_get_type (void)
{
  static GType guppi_group_view_type = 0;

  if (!guppi_group_view_type) {

    static const GTypeInfo guppi_group_view_info = {
      sizeof (GuppiGroupViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_group_view_class_init,
      NULL, NULL,
      sizeof (GuppiGroupView),
      0,
      (GInstanceInitFunc) guppi_group_view_init,
    };

    guppi_group_view_type =
      g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW,
			      "GuppiGroupView",
			      &guppi_group_view_info,
			      0);
  }

  return guppi_group_view_type;
}

GuppiGroupView *
guppi_group_view_new (void)
{
  GuppiElementState *state = guppi_group_state_new ();
  GuppiElementView *view = guppi_element_view_new (state, NULL);
  guppi_unref (state);
  return GUPPI_GROUP_VIEW (view);
}

GuppiLayoutEngine *
guppi_group_view_layout (GuppiGroupView *view)
{
  g_return_val_if_fail (GUPPI_IS_GROUP_VIEW (view), NULL);
  return priv (view)->layout;
}

/**************************************************************************/

gboolean 
guppi_group_view_has (GuppiGroupView *grp, GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_GROUP_VIEW (grp), FALSE);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  return g_list_find (priv (grp)->elements, view) != NULL;
}

static void
changed_proxy (GuppiElementView *view,
	       gpointer user_data)
{
  GuppiGroupView *grp = user_data;

  ++grp->priv->block_changed_propogation_to_children;
  guppi_element_view_changed ((GuppiElementView *) grp);
  --grp->priv->block_changed_propogation_to_children;
}

void
guppi_group_view_add (GuppiGroupView *grp, GuppiElementView *view)
{
  GuppiGroupViewPrivate *p;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (grp);

  if (!guppi_group_view_has (grp, view)) {
    GuppiGroupViewClass *klass;
    klass = GUPPI_GROUP_VIEW_CLASS (G_OBJECT_GET_CLASS (grp));

    p->elements = g_list_append (p->elements, view);

    if (klass->add_hook)
      klass->add_hook (grp, view);

    /* FIXME: we need to disconnect these signals! */

    g_signal_connect_swapped (view,
			     "changed_structure",
			     (GCallback) guppi_element_view_changed_structure,
			      grp);

    g_signal_connect (view,
		      "changed",
		      (GCallback) changed_proxy,
		      grp);

    guppi_ref (view);

    g_signal_emit (grp, guppi_group_view_signals[VIEW_ADD], 0, view);
    guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
  }
}

void
guppi_group_view_remove (GuppiGroupView *grp, GuppiElementView *view)
{
  GuppiGroupViewPrivate *p;
  GList *node;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (grp);

  node = g_list_find (p->elements, view);
  g_return_if_fail (node != NULL);

  guppi_layout_engine_remove_item (p->layout, GUPPI_LAYOUT_ITEM (view));

  g_signal_handlers_disconnect_by_func (view,
					(GCallback) guppi_element_view_changed_structure,
					grp);

  guppi_unref (view);
  p->elements = g_list_remove_link (p->elements, node);
  g_list_free_1 (node);

  g_signal_emit (grp, guppi_group_view_signals[VIEW_REMOVE], 0, view);
  guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
}

void
guppi_group_view_replace (GuppiGroupView *grp,
			  GuppiElementView *old, GuppiElementView *nuevo)
{
  GuppiGroupViewPrivate *p;
  GList *old_node;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (old));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (nuevo));
  
  if (old == nuevo)
    return;

  p = priv (grp);

  old_node = g_list_find (p->elements, old);
  g_return_if_fail (old_node != NULL);
  g_return_if_fail (g_list_find (p->elements, nuevo) == NULL);

  guppi_layout_engine_replace_item (p->layout, 
				    GUPPI_LAYOUT_ITEM (old),
				    GUPPI_LAYOUT_ITEM (nuevo));

  guppi_ref (old);
  guppi_refcounting_assign (old_node->data, nuevo);
  g_signal_emit (grp, guppi_group_view_signals[VIEW_REPLACE], 0, old);
  guppi_unref (old);

  guppi_element_view_changed_structure (GUPPI_ELEMENT_VIEW (grp));
}

void
guppi_group_view_foreach (GuppiGroupView *grp,
			  void (*fn) (GuppiElementView *, gpointer),
			  gpointer user_data)
{
  GuppiGroupViewPrivate *p;
  GList *iter;

  g_return_if_fail (grp != NULL && GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (fn != NULL);

  p = priv (grp);

  for (iter = p->elements; iter != NULL; iter = g_list_next (iter))
    fn (GUPPI_ELEMENT_VIEW (iter->data), user_data);
}

gint
guppi_group_view_compare_z (GuppiGroupView *grp, GuppiElementView *a, GuppiElementView *b)
{
  GList *iter;
  gint seen_a, seen_b, count;

  g_return_val_if_fail (GUPPI_IS_GROUP_VIEW (grp), 0);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (a), 0);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (b), 0);

  seen_a = -1;
  seen_b = -1;
  count  = 0;
  
  iter = priv (grp)->elements;
  while (iter != NULL && (seen_a == -1 || seen_b == -1)) {
    if (seen_a == -1 && iter->data == a)
      seen_a = count;
    if (seen_b == -1 && iter->data == b)
      seen_b = count;
    ++count;
    iter = g_list_next (iter);
  }

  if (seen_a == -1 || seen_b == -1)
    return 0;

  return seen_a - seen_b;
}

void
guppi_group_view_raise (GuppiGroupView *grp, GuppiElementView *a, GuppiElementView *b)
{
  GList *iter, *node_a = NULL, *node_b = NULL;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (b));

  iter = priv (grp)->elements;
  while (iter && !(node_a && node_b)) {
    if (!node_a && iter->data == a) {
      node_a = iter;
    }
    if (!node_b && iter->data == b) {
      if (node_a == NULL)
	return;
      node_b = iter;
    }
    iter = g_list_next (iter);
  }

  if (!(node_a && node_b))
    return;

  priv (grp)->elements = g_list_remove_link (priv (grp)->elements, node_b);
  if (node_a->prev)
    node_a->prev->next = node_b;
  node_b->prev = node_a->prev;  
  node_b->next = node_a;
  node_a->prev = node_b;
}

void
guppi_group_view_raise_to_top (GuppiGroupView *grp, GuppiElementView *a)
{
  GList *iter;
  GuppiGroupViewPrivate *p;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));

  p = priv (grp);

  iter = p->elements;
  while (iter) {
    if (iter->data == a)
      break;
    iter = g_list_next (iter);
  }

  if (iter) {
    p->elements = g_list_remove_link (p->elements, iter);
    p->elements = g_list_append (p->elements, iter->data);
    g_list_free_1 (iter);
  }
}

void
guppi_group_view_sink_to_bottom (GuppiGroupView *grp, GuppiElementView *a)
{
  GList *iter;
  GuppiGroupViewPrivate *p;

  g_return_if_fail (GUPPI_IS_GROUP_VIEW (grp));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (a));

  p = priv (grp);

  iter = p->elements;
  while (iter) {
    if (iter->data == a)
      break;
    iter = g_list_next (iter);
  }

  if (iter) {
    p->elements = g_list_remove_link (p->elements, iter);
    p->elements = g_list_prepend (p->elements, iter->data);
    g_list_free_1 (iter);
  }
}

/* $Id$ */
