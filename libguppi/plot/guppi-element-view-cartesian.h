
/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001-2002 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_VIEW_CARTESIAN_H
#define _INC_GUPPI_ELEMENT_VIEW_CARTESIAN_H

#include <libguppi/useful/guppi-enums.h>
#include <libguppi/plot/guppi-element-view.h>
#include <libguppi/plot/guppi-view-interval.h>
#include <libguppi/plot/guppi-axis-markers.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiElementViewCartesian GuppiElementViewCartesian;
typedef struct _GuppiElementViewCartesianClass GuppiElementViewCartesianClass;
typedef struct _GuppiElementViewCartesianPrivate GuppiElementViewCartesianPrivate;

struct _GuppiElementViewCartesian {
  GuppiElementView parent;
  GuppiElementViewCartesianPrivate *priv;
};

struct _GuppiElementViewCartesianClass {
  GuppiElementViewClass parent_class;

  void (*update_axis_markers) (GuppiElementViewCartesian *cart,
			       guppi_axis_t               axis,
			       GuppiAxisMarkers          *markers,
			       double                     range_min,
			       double                     range_max);

  gboolean (*preferred_range) (GuppiElementViewCartesian *cart,
			       guppi_axis_t               axis,
			       double                    *range_min,
			       double                    *range_max);

};

#define GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN (guppi_element_view_cartesian_get_type())
#define GUPPI_ELEMENT_VIEW_CARTESIAN(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN,GuppiElementViewCartesian))
#define GUPPI_ELEMENT_VIEW_CARTESIAN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN,GuppiElementViewCartesianClass))
#define GUPPI_IS_ELEMENT_VIEW_CARTESIAN(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN))
#define GUPPI_IS_ELEMENT_VIEW_CARTESIAN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_VIEW_CARTESIAN))

GType guppi_element_view_cartesian_get_type (void);

/* View Intervals */

void               guppi_element_view_cartesian_add_view_interval (GuppiElementViewCartesian *cart,
								   guppi_axis_t axis);

GuppiViewInterval *guppi_element_view_cartesian_get_view_interval (GuppiElementViewCartesian *cart,
								   guppi_axis_t axis);

void               guppi_element_view_cartesian_connect_view_intervals (GuppiElementViewCartesian *cart1,
									guppi_axis_t axis1,
									GuppiElementViewCartesian *cart2,
									guppi_axis_t axis2);

void guppi_element_view_cartesian_set_preferred_view (GuppiElementViewCartesian *cart,
						      guppi_axis_t axis);

void guppi_element_view_cartesian_set_preferred_view_all (GuppiElementViewCartesian *cart);

void guppi_element_view_cartesian_force_preferred_view (GuppiElementViewCartesian *cart,
							guppi_axis_t axis,
							gboolean);


/* Axis Markers */

void              guppi_element_view_cartesian_add_axis_markers     (GuppiElementViewCartesian *cart,
								     guppi_axis_t               axis);

gint              guppi_element_view_carterian_get_axis_marker_type (GuppiElementViewCartesian *cart,
								     guppi_axis_t               axis);

void              guppi_element_view_cartesian_set_axis_marker_type (GuppiElementViewCartesian *cart,
								     guppi_axis_t               axis,
								     gint                       code);

GuppiAxisMarkers *guppi_element_view_cartesian_get_axis_markers     (GuppiElementViewCartesian *cart,
								     guppi_axis_t               axis);

void              guppi_element_view_cartesian_connect_axis_markers (GuppiElementViewCartesian *cart1,
								     guppi_axis_t               axis1,
								     GuppiElementViewCartesian *view2,
								     guppi_axis_t               axis2);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_ELEMENT_VIEW_CARTESIAN_H */

/* $Id$ */
