/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-window.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-plot-window.h>

/* #include <gnome.h> */
#include <time.h>

#include <libgnome/gnome-paper.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-print-dialog.h>
#include <libgnomeprint/gnome-print-master.h>
#include <libgnomeprint/gnome-print-preview.h>
#include <libgnomeprint/gnome-print-master-preview.h>

#include <libguppi/useful/guppi-useful.h>

#include <libguppi/plot/guppi-basic-tools.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_plot_window_destroy (GtkObject * obj)
{
  GuppiPlotWindow *win = GUPPI_PLOT_WINDOW (obj);

#if 0
  if (win->properties_window) {
    gtk_widget_destroy (win->properties_window);
    win->properties_window = NULL;
  }
#endif

  guppi_unref0 (win->root_view);

  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_plot_window_finalize (GtkObject * obj)
{
  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_plot_window_class_init (GuppiPlotWindowClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;

  parent_class = gtk_type_class (gnome_app_get_type ());

  object_class->destroy = guppi_plot_window_destroy;
  object_class->finalize = guppi_plot_window_finalize;

}

/*************************************************************************/

/* Menu Callbacks */

static void
new_linked_view_cb (GtkWidget * w, gpointer data)
{
  GuppiPlotWindow *win = GUPPI_PLOT_WINDOW (data);
  g_return_if_fail (win != NULL);

  g_assert_not_reached ();
}

static void
new_view_cb (GtkWidget * w, gpointer data)
{
  GuppiPlotWindow *win = GUPPI_PLOT_WINDOW (data);

  static gboolean warned_about_brokenness = FALSE;

  g_return_if_fail (win != NULL);

  if (!warned_about_brokenness) {
    guppi_warning_dialog ("Unlinked views are sort of broken right now...");
    warned_about_brokenness = TRUE;
  }

}

static void
save_cb (GtkWidget * w, gpointer data)
{
  guppi_unimplemented_function_dialog ("Save");
}

static void
saveas_cb (GtkWidget * w, gpointer data)
{
  guppi_unimplemented_function_dialog ("Save As");
}

static void
do_print_cb (GtkWidget * w, gint n, gpointer data)
{
  GnomePrintDialog *pd = GNOME_PRINT_DIALOG (w);
  GuppiPlotWindow *win = GUPPI_PLOT_WINDOW (data);
  GnomePrintContext *pc = NULL;
  GnomePrintMaster *gpm = NULL;
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (win->root_view);
  GuppiGeometry *geom;
  const GnomePaper *paper;
  double page_width, page_height, plot_width, plot_height;

  gpm = gnome_print_master_new_from_dialog (pd);
  pc = gnome_print_master_get_context (gpm);
#ifdef USING_OLD_FONT_API
  paper = gpm->paper;
#else
  paper = gnome_print_master_get_paper (gpm);
#endif

  page_width = gnome_paper_pswidth (paper);
  page_height = gnome_paper_psheight (paper);

  geom = guppi_element_view_geometry (view);
  plot_width = guppi_geometry_width (geom);
  plot_height = guppi_geometry_height (geom);

  gnome_print_translate (pc,
			 (page_width - plot_width) / 2,
			 (page_height - plot_height) / 2);

  guppi_element_view_print (view, pc);

  gnome_print_showpage (pc);
  gnome_print_context_close (pc);

  if (n == GNOME_PRINT_PRINT) {

    gnome_print_master_print (gpm);

  } else if (n == GNOME_PRINT_PREVIEW) {
    GnomePrintMasterPreview *pmp;

    pmp = gnome_print_master_preview_new_with_orientation
      (gpm, _("Print preview"), FALSE);

    gtk_widget_show (GTK_WIDGET (pmp));
  }

  gtk_widget_destroy (w);
}

static void
print_cb (GtkWidget * w, gpointer data)
{
  GtkWidget *print_dialog;

  print_dialog = gnome_print_dialog_new (_("Print"), 0);

  gtk_signal_connect (GTK_OBJECT (print_dialog),
		      "clicked", GTK_SIGNAL_FUNC (do_print_cb), data);

  gtk_widget_show_all (print_dialog);
}

static void
close_cb (GtkWidget * w, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
}

static void
exit_cb (GtkWidget * w, gpointer data)
{
  gtk_widget_destroy (GTK_WIDGET (data));
  guppi_exit ();
}

static void
edit_prop_cb (GtkWidget * w, gpointer data)
{
  GuppiPlotWindow *win = GUPPI_PLOT_WINDOW (data);

  if (win->root_view)
    guppi_root_group_view_show_configure_window (win->root_view);
}

/**********************************************************************/

static void
zoom_cb (GtkWidget * w, gpointer data)
{
  GtkEntry *entry = GTK_ENTRY (w);
  GuppiPlotWindow *gpw = GUPPI_PLOT_WINDOW (data);
  double z;
  gchar *c;
  gboolean seen_perc = FALSE;

  g_return_if_fail (entry != NULL);
  g_return_if_fail (gpw != NULL);

  c = gtk_entry_get_text (entry);
  while (*c) {
    if (*c == '%')
      seen_perc = TRUE;
    ++c;
  }

  if (!seen_perc)
    return;

  z = atof (gtk_entry_get_text (entry));
  if (z <= 0) {
    gtk_entry_set_text (entry, "100%");
    guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (gpw->root_item), 1.0);
  } else {
    z /= 100;
    guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (gpw->root_item), z);
  }
}

static void
zoom_changed_cb (GuppiCanvasItem *item, double scale, GuppiPlotWindow * win)
{
  gchar buf[64];
  g_snprintf (buffer, 64, "%.0f%%", scale * 100);
  gtk_entry_set_text (GTK_ENTRY (win->zoom_combo->entry), buffer);
}

/**********************************************************************/
#if 0
static void
shrinkwrap_cb (GtkButton * b, GuppiPlotCanvas * canv)
{
  guppi_plot_canvas_fit_to_plot (canv);
}
#endif

/*************************************************************************/

static void
guppi_plot_window_build_menus (GuppiPlotWindow * gpw)
{
  GnomeApp *app;

  static GnomeUIInfo file_menu[] = {
    {GNOME_APP_UI_ITEM,
     N_("New View"), N_("Create a New View Window For This Plot"),
     new_view_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    {GNOME_APP_UI_ITEM,
     N_("New Linked View"),
     N_("Create a New Linked View Window For This Plot"),
     new_linked_view_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_SEPARATOR,
    GNOMEUIINFO_MENU_SAVE_ITEM (save_cb, NULL),
    GNOMEUIINFO_MENU_SAVE_AS_ITEM (saveas_cb, NULL),
    GNOMEUIINFO_MENU_PRINT_ITEM (print_cb, NULL),
    GNOMEUIINFO_MENU_CLOSE_ITEM (close_cb, NULL),
    GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
    GNOMEUIINFO_END
  };

  static GnomeUIInfo edit_menu[] = {
    {GNOME_APP_UI_ITEM,
     N_("Configure"), N_("Configure This Plot"),
     edit_prop_cb, NULL, NULL,
     GNOME_APP_PIXMAP_NONE, NULL,
     0, (GdkModifierType) 0, NULL},
    GNOMEUIINFO_END
  };

  static GnomeUIInfo plotwin_menus[] = {
    GNOMEUIINFO_MENU_FILE_TREE (file_menu),
    GNOMEUIINFO_MENU_EDIT_TREE (edit_menu),
    GNOMEUIINFO_END
  };

  app = GNOME_APP (gpw);
  g_return_if_fail (app != NULL);


  gnome_app_create_menus_with_data (app, plotwin_menus, gpw);
}

static void
guppi_plot_window_build_toolbar (GuppiPlotWindow * gpw)
{
  static GnomeUIInfo toolbar_uiinfo[] = {
    GNOMEUIINFO_ITEM_STOCK (N_("Print"), N_("Print"), print_cb,
			    GNOME_STOCK_PIXMAP_PRINT),
    GNOMEUIINFO_END
  };

  g_return_if_fail (gpw != NULL);

  gpw->toolbar = GTK_TOOLBAR (gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
					       GTK_TOOLBAR_ICONS));
  gnome_app_fill_toolbar_with_data (gpw->toolbar,
				    toolbar_uiinfo,
				    GNOME_APP (gpw)->accel_group, gpw);


  gnome_app_set_toolbar (GNOME_APP (gpw), gpw->toolbar);
}

/*************************************************************************/

static void
toolkit_select_cb (GuppiPlotToolkit * tk, gpointer item)
{
  g_return_if_fail (tk != NULL && GUPPI_IS_PLOT_TOOLKIT (tk));
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));

  guppi_canvas_item_set_local_toolkit (GUPPI_CANVAS_ITEM (item), tk);
}

struct toolbar_build_info {
  GtkToolbar *toolbar;
  GHashTable *hash;
  gpointer cb_user_data;
  gint tool_count;
};

static void
add_item_tools_iter_fn (GuppiPlotToolkit * tk, gpointer ptr)
{
  struct toolbar_build_info *info = (struct toolbar_build_info *) ptr;
  guint sig;

  sig = guppi_plot_toolkit_signature (tk);

  if (g_hash_table_lookup (info->hash, GUINT_TO_POINTER (sig)))
    return;

  guppi_plot_toolkit_add_to_toolbar (tk, info->toolbar, toolkit_select_cb, NULL,	/* unselect cb */
				     info->cb_user_data);

  guppi_ref (tk);

  gtk_signal_connect_object (GTK_OBJECT (info->toolbar),
			     "destroy",
			     GTK_SIGNAL_FUNC (guppi_unref_fn),
			     GTK_OBJECT (tk));

  g_hash_table_insert (info->hash,
		       GUINT_TO_POINTER (sig), GUINT_TO_POINTER (sig));

  ++info->tool_count;
}

static void
guppi_plot_window_build_tool_palette (GuppiPlotWindow * gpw)
{
  GtkToolbar *toolkit_toolbar;
  struct toolbar_build_info info;

  g_return_if_fail (gpw != NULL && GUPPI_IS_PLOT_WINDOW (gpw));

  toolkit_toolbar = GTK_TOOLBAR (gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
						  GTK_TOOLBAR_ICONS));

  /*
   * Add item-specific tools.
   */

  info.toolbar = toolkit_toolbar;
  info.hash = g_hash_table_new (NULL, NULL);
  info.cb_user_data = gpw->root_item;
  info.tool_count = 0;

  guppi_canvas_item_foreach_class_toolkit (GUPPI_CANVAS_ITEM (gpw->root_item),
					   add_item_tools_iter_fn, &info);

  g_hash_table_destroy (info.hash);

  if (info.tool_count == 0)
    return;

  gnome_app_add_toolbar (GNOME_APP (gpw),
			 toolkit_toolbar,
			 _("Tools"),
			 GNOME_DOCK_ITEM_BEH_NORMAL, GNOME_DOCK_TOP, 2, 0, 0);
}

/*************************************************************************/

static void
guppi_plot_window_init (GuppiPlotWindow * obj)
{
  obj->canv = NULL;
}

GtkType guppi_plot_window_get_type (void)
{
  static GtkType guppi_plot_window_type = 0;
  if (!guppi_plot_window_type) {
    static const GtkTypeInfo guppi_plot_window_info = {
      "GuppiPlotWindow",
      sizeof (GuppiPlotWindow),
      sizeof (GuppiPlotWindowClass),
      (GtkClassInitFunc) guppi_plot_window_class_init,
      (GtkObjectInitFunc) guppi_plot_window_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_plot_window_type = gtk_type_unique (gnome_app_get_type (),
					      &guppi_plot_window_info);
  }
  return guppi_plot_window_type;
}

void
guppi_plot_window_construct (GuppiPlotWindow * gpw,
			     GuppiRootGroupView * root_view)
{
  const gchar *zoom_values[] = { "200%", "150%", "125%", "100%",
    "75%", "50%", "25%", NULL
  };
  GList *zoom_list = NULL;
  GtkWidget *swin;
  GtkWidget *box;
  GtkWidget *b;
  GtkWidget *w;
  gchar *path;
  gint i;

  g_return_if_fail (gpw != NULL && GUPPI_IS_PLOT_WINDOW (gpw));
  g_return_if_fail (root_view && GUPPI_IS_ROOT_GROUP_VIEW (root_view));
  g_return_if_fail (gpw->canv == NULL);

  gnome_app_construct (GNOME_APP (gpw), PACKAGE, "Plot");

  gpw->root_view = root_view;
  guppi_ref (gpw->root_view);

  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  gpw->canv = GNOME_CANVAS (gnome_canvas_new_aa ());
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  gpw->root_item =
    GUPPI_ROOT_GROUP_ITEM (guppi_element_view_make_canvas_item
			   (GUPPI_ELEMENT_VIEW (gpw->root_view), gpw->canv,
			    gnome_canvas_root (gpw->canv)));

  guppi_root_group_item_set_resize_semantics (gpw->root_item,
					      ROOT_GROUP_RESIZE_FIT_BEST_GROW_ONLY);

  guppi_plot_window_build_menus (gpw);
  guppi_plot_window_build_toolbar (gpw);
  guppi_plot_window_build_tool_palette (gpw);

  gpw->status_bar = GNOME_APPBAR (gnome_appbar_new (FALSE, TRUE,
						    GNOME_PREFERENCES_USER));
  gnome_app_set_statusbar (GNOME_APP (gpw), GTK_WIDGET (gpw->status_bar));

  gtk_toolbar_append_space (gpw->toolbar);

  /* Add horizontal-fit button. */
  b = gtk_button_new ();
  if ((path = guppi_find_pixmap ("fill-horiz.png"))) {
    w = gnome_pixmap_new_from_file (path);
    guppi_free (path);
  } else {
    w = gtk_label_new (_("Fit Horizontally"));
  }
  gtk_container_add (GTK_CONTAINER (b), w);

  gtk_signal_connect_object (GTK_OBJECT (b),
			     "clicked",
			     GTK_SIGNAL_FUNC
			     (guppi_root_group_item_horizontal_fit),
			     GTK_OBJECT (gpw->root_item));

  gtk_toolbar_append_widget (gpw->toolbar, b,
			     _("Zoom to Fit Horizontally"),
			     _("Zoom to Fit Horizontally"));


  /* Add vertical-fit button. */
  b = gtk_button_new ();
  if ((path = guppi_find_pixmap ("fill-vert.png"))) {
    w = gnome_pixmap_new_from_file (path);
    guppi_free (path);
  } else {
    w = gtk_label_new (_("Fit Vertically"));
  }
  gtk_container_add (GTK_CONTAINER (b), w);

  gtk_signal_connect_object (GTK_OBJECT (b),
			     "clicked",
			     GTK_SIGNAL_FUNC
			     (guppi_root_group_item_vertical_fit),
			     GTK_OBJECT (gpw->root_item));

  gtk_toolbar_append_widget (gpw->toolbar, b,
			     _("Zoom to Fit Vertically"),
			     _("Zoom to Fit Vertically"));


  /* Add best-fit button. */
  b = gtk_button_new ();
  if ((path = guppi_find_pixmap ("fill-best.png"))) {
    w = gnome_pixmap_new_from_file (path);
    guppi_free (path);
  } else {
    w = gtk_label_new (_("Fit Best"));
  }
  gtk_container_add (GTK_CONTAINER (b), w);

  gtk_signal_connect_object (GTK_OBJECT (b),
			     "clicked",
			     GTK_SIGNAL_FUNC (guppi_root_group_item_best_fit),
			     GTK_OBJECT (gpw->root_item));

  gtk_toolbar_append_widget (gpw->toolbar, b,
			     _("Zoom to Best Fit"), _("Zoom to Best Fit"));


  /* Add resize w/ window button. */
  /* I hate the semantics of this thing. */
#if 0
  b = gtk_button_new ();
  if ((path = guppi_find_pixmap ("resize-to-win.png"))) {
    w = gnome_pixmap_new_from_file (path);
    guppi_free (path);
  } else {
    w = gtk_label_new (_("Resize to Window"));
  }
  gtk_container_add (GTK_CONTAINER (b), w);

  gtk_signal_connect_object (GTK_OBJECT (b),
			     "clicked",
			     GTK_SIGNAL_FUNC
			     (guppi_plot_canvas_resize_to_window),
			     GTK_OBJECT (gpw->canv));

  gtk_toolbar_append_widget (gpw->toolbar, b,
			     _("Resize Plot to Window Size"),
			     _("Resize Plot to Window Size"));
#endif


  /* Add zoom % combo box. */
  gpw->zoom_combo = GTK_COMBO (gtk_combo_new ());
  for (i = 0; zoom_values[i]; ++i)
    zoom_list = g_list_append (zoom_list, (gchar *) zoom_values[i]);
  gtk_combo_set_popdown_strings (gpw->zoom_combo, zoom_list);
  gtk_combo_set_value_in_list (gpw->zoom_combo, FALSE, FALSE);
  gtk_combo_disable_activate (gpw->zoom_combo);
  gtk_widget_set_usize (gpw->zoom_combo->entry, 50, -1);
  gtk_signal_connect (GTK_OBJECT (gpw->zoom_combo->entry),
		      "changed", GTK_SIGNAL_FUNC (zoom_cb), gpw);

  gtk_signal_connect (GTK_OBJECT (gpw->root_item),
		      "changed_scale",
		      GTK_SIGNAL_FUNC (zoom_changed_cb),
		      gpw);

  box = gtk_hbox_new (FALSE, 2);
  /* Add an empty label for spacing.  This is bad. */
  gtk_box_pack_start (GTK_BOX (box), gtk_label_new (""), FALSE, FALSE, 5);
  gtk_box_pack_start (GTK_BOX (box), gtk_label_new (_("Zoom:")),
		      FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (box), GTK_WIDGET (gpw->zoom_combo),
		    FALSE, FALSE, 1);

  gtk_toolbar_append_widget (gpw->toolbar, box, _("Zoom"), _("Zoom"));



  /* Add shrinkwrap button. */
#if 0
  w = gtk_button_new_with_label ("Foo");
  gtk_signal_connect (GTK_OBJECT (w), "clicked",
		      GTK_SIGNAL_FUNC (shrinkwrap_cb), gpw->canv);
  gtk_toolbar_append_widget (gpw->toolbar, w,
			     _("Shrink Wrap"), _("Shrink Wrap"));
#endif



  gtk_widget_show_all (GTK_WIDGET (gpw->toolbar));

  swin = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (swin), GTK_WIDGET (gpw->canv));
  gtk_widget_show_all (GTK_WIDGET (gpw->canv));
  gnome_app_set_contents (GNOME_APP (gpw), swin);

}

static gboolean
win_delete_event (GtkWidget *w, GdkEventAny *ev, gpointer user_data)
{
  guppi_root_group_view_hide_configure_window (GUPPI_ROOT_GROUP_VIEW (user_data));
  return FALSE;
}

GtkWidget *
guppi_plot_window_new (GuppiRootGroupView *root_view)
{
  GtkWidget *win;

  g_return_val_if_fail (root_view
			&& GUPPI_IS_ROOT_GROUP_VIEW (root_view), NULL);

  win = GTK_WIDGET (guppi_type_new (guppi_plot_window_get_type ()));

  guppi_plot_window_construct (GUPPI_PLOT_WINDOW (win), root_view);

  gtk_signal_connect (GTK_OBJECT (win),
		      "delete_event",
		      GTK_SIGNAL_FUNC (win_delete_event),
		      root_view);

  return win;
}



/* $Id$ */
