/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-plug-in.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_PLUG_IN_H
#define _INC_GUPPI_PLOT_PLUG_IN_H

#include <libguppi/useful/guppi-plug-in.h>
#include <libguppi/plot/guppi-element-state.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPlotPlugIn GuppiPlotPlugIn;

struct _GuppiPlotPlugIn {
  GuppiPlugIn parent;

  GuppiElementState *(*element_constructor) (void);
};

GuppiPlugIn       *guppi_plot_plug_in_new (void);
GuppiElementState *guppi_plot_plug_in_create_element (GuppiPlotPlugIn *);



END_GUPPI_DECLS

#endif /* _INC_GUPPI_PLOT_PLUG_IN_H */

/* $Id$ */
