/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-color-palette.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-color-palette.h>

#include <math.h>
#include <string.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-rgb.h>
#include <libxml/tree.h>


static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  LAST_SIGNAL
};

static guint guppi_color_palette_signals[LAST_SIGNAL] = { 0 };

typedef enum {
  PALETTE_CUSTOM,
  PALETTE_MONOCHROME,
  PALETTE_STOCK,
  PALETTE_ALIEN_STOCK,
  PALETTE_TRANSITION,
  PALETTE_FADE,
  PALETTE_FIRE,
  PALETTE_ICE,
  PALETTE_THERMAL,
  PALETTE_SPECTRUM,
  PALETTE_INVALID,
  PALETTE_LAST
} PaletteStyle;

typedef struct {
  PaletteStyle code;
  const gchar *meta;
  gboolean parameterless;
} PaletteInfo;

static const PaletteInfo palette_info[PALETTE_LAST] = {

  { PALETTE_CUSTOM,      "custom",      FALSE },
  { PALETTE_MONOCHROME,  "monochrome",  FALSE },
  { PALETTE_STOCK,       "stock",       TRUE  },
  { PALETTE_ALIEN_STOCK, "alien_stock", TRUE  },
  { PALETTE_TRANSITION,  "transition",  FALSE },
  { PALETTE_FADE,        "fade",        FALSE },
  { PALETTE_FIRE,        "fire",        TRUE  },
  { PALETTE_ICE,         "ice",         TRUE  },
  { PALETTE_THERMAL,     "thermal",     TRUE  },
  { PALETTE_SPECTRUM,    "spectrum",    TRUE  },
  { PALETTE_INVALID,     NULL,          FALSE }
};

GuppiColorPalette *
guppi_color_palette_new (void)
{
  GuppiColorPalette *pal = GUPPI_COLOR_PALETTE (guppi_object_new (guppi_color_palette_get_type ()));
  guppi_color_palette_set_stock (pal);

  return pal;
}

GuppiColorPalette *
guppi_color_palette_copy (GuppiColorPalette *pal)
{
  GuppiColorPalette *new_pal = GUPPI_COLOR_PALETTE (guppi_object_new (guppi_color_palette_get_type ()));

  new_pal->meta      = guppi_strdup (pal->meta);
  new_pal->N         = pal->N;
  new_pal->offset    = pal->offset;
  new_pal->intensity = pal->intensity;
  new_pal->alpha     = pal->alpha;
  new_pal->flip      = pal->flip;
  new_pal->own_nodes = pal->own_nodes;

  if (new_pal->own_nodes) {
    new_pal->nodes = guppi_new (guint32, pal->N);
    memcpy (new_pal->nodes, pal->nodes, sizeof (guint32) * pal->N);
  } else {
    new_pal->nodes = pal->nodes;
  }

  return new_pal;
}

gint
guppi_color_palette_size (GuppiColorPalette *pal)
{
  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), -1);

  return pal->N;
}

guint32
guppi_color_palette_get (GuppiColorPalette *pal, gint i)
{
  guint32 c, r, g, b, a;

  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), 0);
  
  if (pal->N < 1) {
    return 0;
  }

  if (pal->N > 1) {
    i = (i + pal->offset) % pal->N;
    if (i < 0)
      i += pal->N;

    if (pal->flip)
      i = pal->N - 1 - i;
  } else {
    i = 0;
  }

  c = pal->nodes[i];

  if (c == 0 || pal->intensity == 0 || pal->alpha == 0)
    return 0;

  if (pal->intensity == 255 || pal->alpha == 255)
    return c;

  UINT_TO_RGBA (c, &r, &g, &b, &a);

  r = (r * pal->intensity + 0x80) >> 8;
  g = (g * pal->intensity + 0x80) >> 8;
  b = (b * pal->intensity + 0x80) >> 8;
  a = (a * pal->alpha     + 0x80) >> 8;

  return RGBA_TO_UINT (r, g, b, a);
}

guint32
guppi_color_palette_interpolate (GuppiColorPalette *pal, double t)
{
  guint32 c1, c2;
  gint i, f1, f2;
  gint r1, g1, b1, a1;
  gint r2, g2, b2, a2;

  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), 0);

  if (pal->N < 1) {
    return 0;
  } else if (pal->N == 1) {
    return guppi_color_palette_get (pal, 0);
  }

  i = (gint) floor (t);
  f2 = (gint) floor (256 * (t - i));
  f1 = 256 - f2;

  c1 = guppi_color_palette_get (pal, i);
  c2 = guppi_color_palette_get (pal, i+1);

  if (c1 == c2 || f2 == 0)
    return c1;

  UINT_TO_RGBA (c1, &r1, &g1, &b1, &a1);
  UINT_TO_RGBA (c2, &r2, &g2, &b2, &a2);

  if (r1 != r2)
    r1 = (f1 * r1 + f2 * r2) >> 8;

  if (g1 != g2)
    g1 = (f1 * g1 + f2 * g2) >> 8;

  if (b1 != b2)
    b1 = (f1 * b1 + f2 * b2) >> 8;

  if (a1 != a2)
    a1 = (f1 * a1 + f2 * a2) >> 8;

  return RGBA_TO_UINT (r1, g1, b1, a1);
}

void
guppi_color_palette_set (GuppiColorPalette *pal, gint i, guint32 col)
{
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  if (pal->N < 1)
    return;

  /* Map index onto palette */
  if (pal->N > 1) {
    i = (i + pal->offset) % pal->N;
    if (i < 0)
      i += pal->N;

    if (pal->flip)
      i = pal->N - 1 - i;
  } else {
    i = 0;
  }

  if (pal->nodes[i] == col)
    return;

  if (! pal->own_nodes) {
    guint32 *nodes = pal->nodes;
    pal->nodes = guppi_new (guint32, pal->N);
    memcpy (pal->nodes, nodes, sizeof (guint32) * pal->N);
    pal->own_nodes = TRUE;
  }

  pal->nodes[i] = col;
  guppi_free (pal->meta);
  pal->meta = guppi_strdup (palette_info[PALETTE_CUSTOM].meta);

  g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
}

gint
guppi_color_palette_get_offset (GuppiColorPalette *pal)
{
  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), 0);
  return pal->offset;
}

void
guppi_color_palette_set_offset (GuppiColorPalette *pal, gint offset)
{
  gint shift;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  shift = offset - pal->offset;
  pal->offset = offset;

  if (pal->N > 1) {
    shift = shift % pal->N;
    if (shift < 0)
      shift += pal->N;
    if (shift != 0)
      g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
  }
}

gint
guppi_color_palette_get_alpha (GuppiColorPalette *pal)
{
  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), -1);
  return pal->alpha;
}

void
guppi_color_palette_set_alpha (GuppiColorPalette *pal, gint alpha)
{
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  g_return_if_fail (0 <= alpha && alpha <= 255);

  if (pal->alpha != alpha) {
    pal->alpha = alpha;
    g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
  }
}

gint
guppi_color_palette_get_intensity (GuppiColorPalette *pal)
{
  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), -1);
  return pal->intensity;
}

void
guppi_color_palette_set_intensity (GuppiColorPalette *pal, gint intensity)
{
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  g_return_if_fail (0 <= intensity && intensity <= 255);
  if (pal->intensity != intensity) {
    pal->intensity = intensity;
    g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
  }
}

gboolean
guppi_color_palette_get_flipped (GuppiColorPalette *pal)
{
  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), FALSE);
  return pal->flip;
}

void
guppi_color_palette_set_flipped (GuppiColorPalette *pal, gboolean f)
{
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  if (pal->flip != f) {
    pal->flip = f;
    g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_color_palette_set_raw (GuppiColorPalette *pal, const gchar *meta, guint32 *nodes, gint N, gboolean owned)
{
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  g_return_if_fail (nodes != NULL);
  g_return_if_fail (N > 0);

  if (pal->meta && !strcmp (meta, pal->meta) && pal->nodes == nodes && pal->N == N)
    return;

  if (pal->own_nodes)
    guppi_free (pal->nodes);

  guppi_free (pal->meta);
  pal->meta = guppi_strdup (meta);

  pal->nodes = nodes;
  pal->N = N;
  pal->own_nodes = owned;

  g_signal_emit (pal, guppi_color_palette_signals[CHANGED], 0);
}

void
guppi_color_palette_set_stock (GuppiColorPalette *pal)
{
  static guint32 stock_colors[] = {
    0xff3000ff, 0x80ff00ff, 0x00ffcfff, 0x2000ffff,
    0xff008fff, 0xffbf00ff, 0x00ff10ff, 0x009fffff,
    0xaf00ffff, 0xff0000ff, 0xafff00ff, 0x00ff9fff,
    0x0010ffff, 0xff00bfff, 0xff8f00ff, 0x20ff00ff,
    0x00cfffff, 0x8000ffff, 0xff0030ff, 0xdfff00ff,
    0x00ff70ff, 0x0040ffff, 0xff00efff, 0xff6000ff,
    0x50ff00ff, 0x00ffffff, 0x5000ffff, 0xff0060ff,
    0xffef00ff, 0x00ff40ff, 0x0070ffff, 0xdf00ffff
  };

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_STOCK].meta,
			       stock_colors,
			       sizeof (stock_colors) / sizeof (guint32),
			       FALSE);
}

void
guppi_color_palette_set_alien_stock (GuppiColorPalette *pal)
{
  static guint32 alien_stock_colors[] = {
    0x9c9cffff, 0x9c3163ff, 0xffffceff, 0xceffffff,
    0x630063ff, 0xff8484ff, 0x0063ceff, 0xceceffff,
    0x000084ff, 0xff00ffff, 0xffff00ff, 0x00ffffff,
    0x840084ff, 0x840000ff, 0x008484ff, 0x0000ffff,
    0x00ceffff, 0xceffffff, 0xceffceff, 0xffff9cff,
    0x9cceffff, 0xff9cceff, 0xce9cffff, 0xffce9cff,
    0x3163ffff, 0x31ceceff, 0x9cce00ff, 0xffce00ff,
    0xff9c00ff, 0xff6300ff, 0x63639cff, 0x949494ff,
    0x003163ff, 0x319c63ff, 0x003100ff, 0x313100ff,
    0x9c3100ff, 0x9c3163ff, 0x31319cff, 0x313131ff,
    0xffffffff, 0xff0000ff, 0x00ff00ff, 0x0000ffff,
    0xffff00ff, 0xff00ffff, 0x00ffffff, 0x840000ff,
    0x008400ff, 0x000084ff, 0x848400ff, 0x840084ff,
    0x008484ff, 0xc6c6c6ff, 0x848484ff
  };

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_ALIEN_STOCK].meta,
			       alien_stock_colors,
			       sizeof (alien_stock_colors) / sizeof (guint32),
			       FALSE);
}

void 
guppi_color_palette_set_transition (GuppiColorPalette *pal, guint32 c1, guint32 c2)
{
  guint32 *nodes;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  nodes = guppi_new (guint32, 2);
  nodes[0] = c1;
  nodes[1] = c2;

  guppi_color_palette_set_raw (pal, palette_info[PALETTE_TRANSITION].meta, nodes, 2, TRUE);
}

void
guppi_color_palette_set_fade (GuppiColorPalette *pal, guint32 c)
{
  guint32 *nodes;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  nodes = guppi_new (guint32, 2);
  nodes[0] = 0;
  nodes[1] = c;

  guppi_color_palette_set_raw (pal, palette_info[PALETTE_FADE].meta, nodes, 2, TRUE);
}

void
guppi_color_palette_set_fire (GuppiColorPalette *pal)
{
  static guint32 fire_colors[] = {
    0x000000ff, 0xff0000ff, 0xffff00ff
  };

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_FIRE].meta,
			       fire_colors,
			       sizeof (fire_colors) / sizeof (guint32),
			       FALSE);

}

void
guppi_color_palette_set_ice (GuppiColorPalette *pal)
{
  static guint32 ice_colors[] = {
    0x000000ff, 0xff0000ff, 0xffff00ff
  };

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_ICE].meta,
			       ice_colors,
			       sizeof (ice_colors) / sizeof (guint32),
			       FALSE);

}

void
guppi_color_palette_set_thermal (GuppiColorPalette *pal)
{
  static guint32 thermal_colors[] = {
    0x00ffffff, 0x0000ffff, 0x800080ff, 0xff0000ff, 0xffff00ff
  };
    
  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_THERMAL].meta,
			       thermal_colors,
			       sizeof (thermal_colors) / sizeof (guint32),
			       FALSE);
}


void
guppi_color_palette_set_spectrum (GuppiColorPalette *pal)
{
  static guint32 spectrum_colors[] = {
    0xff0000ff, 0xff8000ff, 0xffff00ff, 0x33cc33ff, 0x0080ffff, 0xff80ffff
  };

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  guppi_color_palette_set_raw (pal,
			       palette_info[PALETTE_SPECTRUM].meta,
			       spectrum_colors,
			       sizeof (spectrum_colors) / sizeof (guint32),
			       FALSE);
}

void
guppi_color_palette_set_monochrome (GuppiColorPalette *pal, guint32 c)
{
  guint32 *cc;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));

  cc = guppi_new (guint32, 1);
  *cc = c;
  guppi_color_palette_set_raw (pal, 
			       palette_info[PALETTE_MONOCHROME].meta,
			       cc,
			       1,
			       TRUE);
}


xmlNodePtr
guppi_color_palette_export_xml (GuppiColorPalette *pal, GuppiXMLDocument *doc)
{
  xmlNodePtr node;
  gint i;
  PaletteStyle code = PALETTE_INVALID;

  g_return_val_if_fail (GUPPI_IS_COLOR_PALETTE (pal), NULL);

  for (i = 0; palette_info[i].code != PALETTE_INVALID; ++i) {
    if (!strcmp (pal->meta, palette_info[i].meta)) {
      code = i;
      break;
    }
  }

  if (code == PALETTE_INVALID)
    return NULL;

  node = xmlNewNode (doc->ns, "ColorPalette");

  guppi_xml_set_property (node, "meta", pal->meta);
  guppi_xml_set_property_bool (node, "flip", pal->flip);
  guppi_xml_set_property_int (node, "offset", pal->offset);
  guppi_xml_set_property_int (node, "intensity", pal->intensity);
  guppi_xml_set_property_int (node, "alpha", pal->alpha);
  
  if (!palette_info[i].parameterless && pal->N > 0) {
    
    guppi_xml_set_propertyf (node, "N", "%d", pal->N);
    for (i = 0; i < pal->N; ++i) {
      gint r, g, b, a;
      xmlNodePtr subnode;
      UINT_TO_RGBA (pal->nodes[i], &r, &g, &b, &a);
      subnode = xmlNewNode (doc->ns, "color");
      guppi_xml_set_propertyf (subnode, "rgba", "#%02x%02x%02x%02x", r, g, b, a);
      xmlAddChild (node, subnode);
    }

  }
  
  return node;
}

GuppiColorPalette *
guppi_color_palette_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiColorPalette *pal = NULL;
  gint offset, intensity, alpha;
  gchar *meta;
  gboolean flip;
  PaletteStyle code = PALETTE_INVALID;
  guint32 *colors;
  gint i, N;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ColorPalette"))
    return NULL;

  meta      = guppi_xml_get_property (node, "meta");
  offset    = guppi_xml_get_property_int (node, "offset", 0);
  intensity = guppi_xml_get_property_int (node, "intensity", 0xff);
  alpha     = guppi_xml_get_property_int (node, "alpha", 0xff);
  flip      = guppi_xml_get_property_bool (node, "flip", FALSE);

  for (i = 0; palette_info[i].code != PALETTE_INVALID; ++i) {
    if (!strcmp (meta, palette_info[i].meta)) {
      code = i;
      break;
    }
  }

  if (code == PALETTE_INVALID)
    goto finished;

  pal = guppi_color_palette_new ();

  if (palette_info[code].parameterless) {
    switch (code) {

    case PALETTE_STOCK:
      guppi_color_palette_set_stock (pal);
      break;

    case PALETTE_ALIEN_STOCK:
      guppi_color_palette_set_alien_stock (pal);
      break;

    case PALETTE_FIRE:
      guppi_color_palette_set_fire (pal);
      break;

    case PALETTE_ICE:
      guppi_color_palette_set_ice (pal);
      break;

    case PALETTE_THERMAL:
      guppi_color_palette_set_thermal (pal);
      break;

    case PALETTE_SPECTRUM:
      guppi_color_palette_set_spectrum (pal);
      break;

    default:
      g_assert_not_reached ();
    }

    goto finished;
  }

  N = guppi_xml_get_property_int (node, "N", 0);
  colors = guppi_new0 (guint32, N);
  i = 0;

  node = node->xmlChildrenNode;
  while (node != NULL && i < N) {
    if (!strcmp (node->name, "color")) {
      gchar *str = guppi_xml_get_property (node, "rgba");
      gint r, g, b, a;
      if (sscanf (str, "#%2x%2x%2x%2x", &r, &g, &b, &a) == 4) {
	colors[i] = RGBA_TO_UINT (r, g, b, a);
	++i;
      }
      g_free (str);
    }
    node = node->next;
  }

  guppi_color_palette_set_raw (pal, meta, colors, N, TRUE);

  guppi_color_palette_set_offset (pal, offset);
  guppi_color_palette_set_intensity (pal, intensity);
  guppi_color_palette_set_alpha (pal, alpha);
  guppi_color_palette_set_flipped (pal, flip);

 finished:
  g_free (meta);
  return pal;
}

void
guppi_color_palette_set_custom (GuppiColorPalette *pal, gint N, guint32 *color)
{
  guint32 *color_cpy;
  gint i;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  g_return_if_fail (N > 0);

  /* If color is passed in as NULL, set the palette to all black. */

  color_cpy = guppi_new (guint32, N);
  for (i = 0; i < N; ++i)
    color_cpy[i] = color ? color[i] : RGBA_BLACK;

  guppi_color_palette_set_raw (pal,
			       N > 1 ? palette_info[PALETTE_CUSTOM].meta : palette_info[PALETTE_MONOCHROME].meta,
			       color_cpy, N, TRUE);
}

void
guppi_color_palette_set_vcustom (GuppiColorPalette *pal, gint N, guint32 first_color, ...)
{
  guint32 *color;
  gint i;
  va_list args;

  g_return_if_fail (GUPPI_IS_COLOR_PALETTE (pal));
  g_return_if_fail (N > 0);

  color = guppi_new (guint32, N);

  color[0] = first_color;

  i = 1;
  va_start (args, first_color);
  while (i < N) {
    color[i] = (guint32) va_arg (args, gint);
    ++i;
  }
  va_end (args);

  guppi_color_palette_set_raw (pal,
			       N > 1 ? palette_info[PALETTE_CUSTOM].meta : palette_info[PALETTE_MONOCHROME].meta,
			       color, N, TRUE);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static xmlNodePtr
pal_xml_export (GuppiXMLDocument *doc, gpointer ptr)
{
  return guppi_color_palette_export_xml (GUPPI_COLOR_PALETTE (ptr), doc);
}

static gboolean
pal_xml_import (GuppiXMLDocument *doc, xmlNodePtr node, gpointer *ptr)
{
  GuppiColorPalette *pal = guppi_color_palette_import_xml (doc, node);

  if (pal == NULL)
    return FALSE;

  guppi_unref (*ptr);
  *ptr = pal;

  return TRUE;
}

GuppiAttributeFlavor
guppi_attribute_flavor_color_palette (void)
{
  static GuppiAttributeFlavor flavor = 0;

  if (flavor == 0) {
    flavor = guppi_attribute_flavor_register_object_semantics ("color_palette", NULL, NULL, NULL);
    guppi_attribute_flavor_add_xml_serialization (flavor, pal_xml_export, pal_xml_import);
    guppi_attribute_flavor_add_signal_to_forward (flavor, "changed", TRUE);
  }

  return flavor;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_color_palette_finalize (GObject *obj)
{
  GuppiColorPalette *x = GUPPI_COLOR_PALETTE(obj);

  guppi_finalized (obj);

  if (x->own_nodes) {
    guppi_free0 (x->nodes);
  }
  guppi_free0 (x->meta);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_color_palette_class_init (GuppiColorPaletteClass *klass)
{
  GObjectClass *object_class = (GObjectClass *)klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_color_palette_finalize;

  guppi_color_palette_signals[CHANGED] =
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiColorPaletteClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);
}

static void
guppi_color_palette_init (GuppiColorPalette *pal)
{
  pal->meta      = guppi_strdup ("custom");
  pal->alpha     = 0xff;
  pal->intensity = 0xff;
}

GType
guppi_color_palette_get_type (void)
{
  static GType guppi_color_palette_type = 0;
  if (!guppi_color_palette_type) {
    static const GTypeInfo guppi_color_palette_info = {
      sizeof (GuppiColorPaletteClass),
      NULL, NULL,
      (GClassInitFunc) guppi_color_palette_class_init,
      NULL, NULL,
      sizeof (GuppiColorPalette),
      0,
      (GInstanceInitFunc) guppi_color_palette_init
    };

    guppi_color_palette_type = g_type_register_static (G_TYPE_OBJECT,
						       "GuppiColorPalette",
						       &guppi_color_palette_info,
						       0);
  }
  return guppi_color_palette_type;
}

