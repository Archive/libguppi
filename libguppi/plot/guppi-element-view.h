
/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_VIEW_H
#define _INC_GUPPI_ELEMENT_VIEW_H

#include <libguppi/useful/guppi-enums.h>
#include <libguppi/canvas/guppi-canvas.h>
#include <libguppi/plot/guppi-element-state.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiElementView GuppiElementView;
typedef struct _GuppiElementViewClass GuppiElementViewClass;
struct _GuppiElementViewPrivate;

struct _GuppiElementView {
  GObject parent;
  struct _GuppiElementViewPrivate *priv;
};

struct _GuppiElementViewClass {
  GObjectClass parent_class;

  const gchar *plug_in_code;

  /* VTable */

  void (*render) (GuppiElementView *, ArtDRect *bbox, GuppiCanvas *);
  void (*set_allocation) (GuppiElementView *);

  /* Init function called after view is constructed and has parent state
     attached. */
  void (*view_init) (GuppiElementView *);

  /* Freeze/thaw */

  void (*freeze)       (GuppiElementView *);
  void (*thaw)         (GuppiElementView *);

  gboolean (*find) (GuppiElementView *view, const gchar *label, 
		    GuppiElementState **stateptr, GuppiElementView **viewptr);

  /* XML input & output */
  void     (*xml_export)  (GuppiElementView *, GuppiXMLDocument *, xmlNodePtr root_node);
  gboolean (*xml_import)  (GuppiElementView *, GuppiXMLDocument *, xmlNodePtr);

  /* Signals */
  void (*changed)           (GuppiElementView *);
  void (*changed_structure) (GuppiElementView *);
};

#define GUPPI_TYPE_ELEMENT_VIEW (guppi_element_view_get_type())
#define GUPPI_ELEMENT_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_ELEMENT_VIEW,GuppiElementView))
#define GUPPI_ELEMENT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_VIEW,GuppiElementViewClass))
#define GUPPI_IS_ELEMENT_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_ELEMENT_VIEW))
#define GUPPI_IS_ELEMENT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_VIEW))

GType guppi_element_view_get_type (void);

GuppiElementView *guppi_element_view_new (GuppiElementState *, ...);

GuppiElementState *guppi_element_view_state (GuppiElementView *);

guppi_uniq_t guppi_element_view_unique_id (GuppiElementView *);

const gchar *guppi_element_view_get_label (GuppiElementView *);
void         guppi_element_view_set_label (GuppiElementView *, const gchar *);

gboolean guppi_element_view_get_clipped (GuppiElementView *);
void     guppi_element_view_set_clipped (GuppiElementView *, gboolean);

gboolean guppi_element_view_get (GuppiElementView *, ...);
gboolean guppi_element_view_set (GuppiElementView *, ...);

GuppiElementState *guppi_element_view_find_state (GuppiElementView *, const gchar *label);
GuppiElementView  *guppi_element_view_find_view  (GuppiElementView *, const gchar *label);

void guppi_element_view_set_state (GuppiElementView *, GuppiElementState *);

void guppi_element_view_set_request (GuppiElementView *view,
				     double width, double height);

void guppi_element_view_conv      (GuppiElementView *, const ArtPoint *t, ArtPoint *p);
void guppi_element_view_conv_bulk (GuppiElementView *, const ArtPoint *t, ArtPoint *p, gsize N);

/* If bbox is NULL, renders entire item */
void guppi_element_view_render  (GuppiElementView *, ArtDRect *bbox, GuppiCanvas *canvas);

void guppi_element_view_changed (GuppiElementView *);
void guppi_element_view_freeze  (GuppiElementView *);
void guppi_element_view_thaw    (GuppiElementView *);

void guppi_element_view_changed_structure (GuppiElementView *);

xmlNodePtr        guppi_element_view_export_xml (GuppiElementView *, GuppiXMLDocument *);
GuppiElementView *guppi_element_view_import_xml (GuppiXMLDocument *, xmlNodePtr);
void              guppi_element_view_spew_xml   (GuppiElementView *);

void guppi_element_view_set_debug_bg_color (GuppiElementView *, guint32);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_ELEMENT_VIEW_H */

/* $Id$ */
