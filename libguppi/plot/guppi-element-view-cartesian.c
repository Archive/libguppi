/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001-2002 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-element-view-cartesian.h>

#include <math.h>
#include <string.h>
#include <glade/glade.h>
#include <libart_lgpl/libart.h>

#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-marshal.h>
#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-attribute-bag.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-paths.h>
#include <libguppi/useful/guppi-unique-id.h>

typedef struct _ViewAxisPair ViewAxisPair;
struct _ViewAxisPair {
  GuppiElementViewCartesian *cart;
  guppi_axis_t axis;

  /* We cache the class of the cartesian view */
  GuppiElementViewCartesianClass *klass;
};

struct _GuppiElementViewCartesianPrivate {

  GuppiViewInterval *view_interval[GUPPI_LAST_AXIS];
  guint vi_changed_handler[GUPPI_LAST_AXIS];
  guint vi_prefrange_handler[GUPPI_LAST_AXIS];
  gboolean vi_force_preferred[GUPPI_LAST_AXIS];
  ViewAxisPair *vi_closure[GUPPI_LAST_AXIS];

  gboolean forcing_preferred;
  guint pending_force_tag;

  gint axis_marker_type[GUPPI_LAST_AXIS];
  GuppiAxisMarkers *axis_markers[GUPPI_LAST_AXIS];
  guint am_changed_handler[GUPPI_LAST_AXIS];
};

static GObjectClass *parent_class = NULL;

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
update_axis_markers (GuppiElementViewCartesian *cart,
		     guppi_axis_t               ax,
		     GuppiAxisMarkers          *marks,
		     double                     range_min,
		     double                     range_max)
{
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  if (marks && cart->priv->axis_marker_type[ax] != GUPPI_AXIS_NONE)
    guppi_axis_markers_populate_generic (marks,
					 cart->priv->axis_marker_type[ax],
					 range_min, range_max);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
guppi_element_view_cartesian_finalize (GObject *obj)
{
  GuppiElementViewCartesian *cart;
  GuppiElementViewCartesianPrivate *p;
  gint i;

  cart = GUPPI_ELEMENT_VIEW_CARTESIAN (obj);
  p = cart->priv;

  guppi_finalized (obj);

  /* clean up the view intervals */

  for (i=0; i<GUPPI_LAST_AXIS; ++i) {

    if (p->view_interval[i]) {

      if (p->vi_changed_handler[i])
	g_signal_handler_disconnect (p->view_interval[i], 
				     p->vi_changed_handler[i]);

      if (p->vi_prefrange_handler[i])
	g_signal_handler_disconnect (p->view_interval[i],
				     p->vi_prefrange_handler[i]);

      guppi_unref0 (p->view_interval[i]);
    }
    
    guppi_free0 (p->vi_closure[i]);
  }

  if (p->pending_force_tag)
    g_source_remove (p->pending_force_tag);
  
  /* clean up the axis markers */

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {

    if (p->axis_markers[i]) {

      if (p->am_changed_handler[i])
	g_signal_handler_disconnect (p->axis_markers[i],
				     p->am_changed_handler[i]);

      guppi_unref0 (p->axis_markers[i]);
    }
  }

  guppi_free0 (cart->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_element_view_cartesian_class_init (GuppiElementViewCartesianClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_element_view_cartesian_finalize;
  klass->update_axis_markers = update_axis_markers;
}


static void
guppi_element_view_cartesian_init (GuppiElementViewCartesian *cart)
{
  cart->priv = guppi_new0 (GuppiElementViewCartesianPrivate, 1);
}

GType 
guppi_element_view_cartesian_get_type (void)
{
  static GType guppi_element_view_cartesian_type = 0;

  if (!guppi_element_view_cartesian_type) {
    
    static const GTypeInfo guppi_element_view_cartesian_info = {
      sizeof (GuppiElementViewCartesianClass),
      NULL, NULL,
      (GClassInitFunc) guppi_element_view_cartesian_class_init,
      NULL, NULL,
      sizeof (GuppiElementViewCartesian),
      0,
      (GInstanceInitFunc) guppi_element_view_cartesian_init
    };
    
    guppi_element_view_cartesian_type = g_type_register_static (GUPPI_TYPE_ELEMENT_VIEW,
								"GuppiElementViewCartesian",
								&guppi_element_view_cartesian_info,
								0);
  }
  return guppi_element_view_cartesian_type;
}


/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
compute_markers (GuppiElementViewCartesian *cart,
		 guppi_axis_t               ax)
{
  GuppiElementViewCartesianClass *klass;
  double a, b;

  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  klass = GUPPI_ELEMENT_VIEW_CARTESIAN_CLASS (G_OBJECT_GET_CLASS (cart));

  if (cart->priv->axis_markers[ax] != NULL && klass->update_axis_markers != NULL) {

    GuppiViewInterval *vi = guppi_element_view_cartesian_get_view_interval (cart, ax);
    GuppiAxisMarkers *am = cart->priv->axis_markers[ax];

    if (am) {
      guppi_view_interval_range (vi, &a, &b);
      klass->update_axis_markers (cart, ax, am, a, b);
    }
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* 
 *
 * GuppiViewInterval gadgetry
 *
 */

static gboolean
force_all_preferred_idle (gpointer ptr)
{
  GuppiElementViewCartesian *cart = ptr;
  gint i;

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    if (cart->priv->view_interval[i] && cart->priv->vi_force_preferred[i])
      guppi_element_view_cartesian_set_preferred_view (cart, i);
  }
  cart->priv->pending_force_tag = 0;

  return FALSE;
}

static void
vi_changed (GuppiViewInterval *vi,
	    ViewAxisPair      *pair)
{
  GuppiElementViewCartesian *cart = pair->cart;
  guppi_axis_t ax = pair->axis;

  GuppiElementViewCartesianPrivate *p = cart->priv;

  guppi_element_view_freeze ((GuppiElementView *) cart);

  if (p->vi_force_preferred[ax]) {

    if (p->vi_changed_handler[ax])
      g_signal_handler_block (vi, p->vi_changed_handler[ax]);

    guppi_element_view_cartesian_set_preferred_view (cart, ax);
    
    if (p->vi_changed_handler[ax])
      g_signal_handler_unblock (vi, p->vi_changed_handler[ax]);
    
  } else if (p->forcing_preferred && p->pending_force_tag == 0) {
    
    p->pending_force_tag = g_idle_add (force_all_preferred_idle, cart);

  }
  
  if (p->axis_markers[ax] != NULL)
    compute_markers (cart, ax);

  guppi_element_view_changed (GUPPI_ELEMENT_VIEW (cart));

  guppi_element_view_thaw ((GuppiElementView *) cart);
}

static void
vi_preferred (GuppiViewInterval *vi, ViewAxisPair *pair)
{
  GuppiElementViewCartesian *cart = pair->cart;
  guppi_axis_t ax = pair->axis;
  double a, b;
  
  if (pair->klass == NULL) {
    pair->klass = GUPPI_ELEMENT_VIEW_CARTESIAN_CLASS (G_OBJECT_GET_CLASS (cart));
    g_assert (pair->klass);
  }

  /* Our 'changed' signals are automatically blocked during view
     interval range requests, so we don't need to worry about
     freeze/thaw. */

  if (pair->klass->preferred_range
      && pair->klass->preferred_range (cart, ax, &a, &b)) {
      guppi_view_interval_grow_to (vi, a, b);
  }
}

static void
set_view_interval (GuppiElementViewCartesian *cart,
		   guppi_axis_t               ax,
		   GuppiViewInterval         *vi)
{
  GuppiElementViewCartesianPrivate *p = cart->priv;
  gint i = (int) ax;

  g_assert (0 <= i && i < GUPPI_LAST_AXIS);
  
  if (p->view_interval[i] == vi)
    return;
  
  if (p->view_interval[i] && p->vi_changed_handler[i]) {
    g_signal_handler_disconnect (p->view_interval[i], p->vi_changed_handler[i]);
    p->vi_changed_handler[i] = 0;
  }

  if (p->vi_prefrange_handler[i]) {
    g_signal_handler_disconnect (p->view_interval[i], p->vi_prefrange_handler[i]);
    p->vi_prefrange_handler[i] = 0;
  }

  guppi_refcounting_assign (p->view_interval[i], vi);

  if (vi != NULL) {
    
    if (p->vi_closure[i] == NULL) {
      p->vi_closure[i] = guppi_new0 (ViewAxisPair, GUPPI_LAST_AXIS);
      p->vi_closure[i]->cart = cart;
      p->vi_closure[i]->axis = ax;
    }

    p->vi_changed_handler[i] = g_signal_connect (p->view_interval[i],
						 "changed",
						 (GCallback) vi_changed,
						 p->vi_closure[i]);
    
    p->vi_prefrange_handler[i] = g_signal_connect (p->view_interval[i],
						   "preferred_range_request",
						   (GCallback) vi_preferred,
						   p->vi_closure[i]);

    compute_markers (cart, ax);
  }
}

/*** ViewInterval-related API calls ***/

void
guppi_element_view_cartesian_add_view_interval (GuppiElementViewCartesian *cart,
						guppi_axis_t               ax)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  if (cart->priv->view_interval[ax] == NULL) {
    GuppiViewInterval *vi = guppi_view_interval_new ();
    set_view_interval (cart, ax, vi);
    guppi_view_interval_request_preferred_range (vi);
    guppi_unref (vi);
  }
}

GuppiViewInterval *
guppi_element_view_cartesian_get_view_interval (GuppiElementViewCartesian *cart,
						guppi_axis_t               ax)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart), NULL);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  return cart->priv->view_interval[ax];
}

void
guppi_element_view_cartesian_connect_view_intervals (GuppiElementViewCartesian *cart1,
						     guppi_axis_t               axis1,
						     GuppiElementViewCartesian *cart2,
						     guppi_axis_t               axis2)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart1));
  g_return_if_fail (0 <= axis1 && axis1 < GUPPI_LAST_AXIS);
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart2));
  g_return_if_fail (0 <= axis2 && axis2 < GUPPI_LAST_AXIS);

  if (axis1 == axis2 && cart1 == cart2)
    return;

  set_view_interval (cart2, axis2,
		     guppi_element_view_cartesian_get_view_interval (cart1, axis1));

  guppi_element_view_changed (GUPPI_ELEMENT_VIEW (cart2));
}

/***************************************************************************/

void
guppi_element_view_cartesian_set_preferred_view (GuppiElementViewCartesian *cart,
						 guppi_axis_t               ax)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  if (cart->priv->view_interval[ax] != NULL) 
    guppi_view_interval_request_preferred_range (cart->priv->view_interval[ax]);
}

void
guppi_element_view_set_preferred_view_all (GuppiElementViewCartesian *cart)
{
  gint i;
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));

  /* Grossly inefficient, and wrong */

  for (i = 0; i < GUPPI_LAST_AXIS; ++i) {
    if (cart->priv->view_interval[i] != NULL)
      guppi_element_view_cartesian_set_preferred_view (cart, (guppi_axis_t) i);
  }
}

void
guppi_element_view_cartesian_force_preferred_view (GuppiElementViewCartesian *cart,
						   guppi_axis_t               ax,
						   gboolean                   force)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  cart->priv->vi_force_preferred[ax] = force;

  if (force)
    guppi_element_view_cartesian_set_preferred_view (cart, ax);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*
 *
 * GuppiAxisMarkers gadgetry
 *
 */

static void
am_changed (GuppiAxisMarkers *gam,
	    ViewAxisPair     *pair)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (pair->cart);

  guppi_element_view_changed (view);
}

static void
set_axis_markers (GuppiElementViewCartesian *cart,
		  guppi_axis_t               ax,
		  GuppiAxisMarkers          *mark)
{
  GuppiElementViewCartesianPrivate *p;

  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  p = cart->priv;

  if (p->axis_markers[ax] != NULL) {
    g_signal_handler_disconnect (p->axis_markers[ax], p->am_changed_handler[ax]);
    p->am_changed_handler[ax] = 0;
  }

  guppi_refcounting_assign (p->axis_markers[ax], mark);

  if (mark != NULL) {

    if (p->vi_closure[ax] == NULL) {
      p->vi_closure[ax] = guppi_new0 (ViewAxisPair, 1);
      p->vi_closure[ax]->cart = cart;
      p->vi_closure[ax]->axis = ax;
    }

    p->am_changed_handler[ax] = g_signal_connect (p->axis_markers[ax],
						  "changed",
						  (GCallback) am_changed,
						  p->vi_closure[ax]);
  }
}

/*** GuppiAxisMarker-related API calls */

void
guppi_element_view_cartesian_add_axis_markers (GuppiElementViewCartesian *cart,
					       guppi_axis_t               ax)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);
 
  if (cart->priv->axis_markers[ax] == NULL) {
    GuppiAxisMarkers *am = guppi_axis_markers_new ();
    set_axis_markers (cart, ax, am);
    guppi_unref (am);
  }
}

gint
guppi_element_view_cartesian_get_axis_marker_type (GuppiElementViewCartesian *cart,
						   guppi_axis_t               ax)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart), -1);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  return cart->priv->axis_marker_type[ax];
}

void
guppi_element_view_cartesian_set_axis_marker_type (GuppiElementViewCartesian *cart,
						   guppi_axis_t               ax,
						   gint                       code)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart));
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);
  
  cart->priv->axis_marker_type[ax] = code;
  guppi_element_view_cartesian_add_axis_markers (cart, ax);
  compute_markers (cart, ax);
}

GuppiAxisMarkers *
guppi_element_view_cartesian_get_axis_markers (GuppiElementViewCartesian *cart,
					       guppi_axis_t               ax)
{
  GuppiAxisMarkers *gam;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart), NULL);
  g_assert (0 <= ax && ax < GUPPI_LAST_AXIS);

  gam = cart->priv->axis_markers[ax];
  if (gam)
    guppi_axis_markers_sort (gam);
  
  return gam;
}

void
guppi_element_view_cartesian_connect_axis_markers (GuppiElementViewCartesian *cart1,
						   guppi_axis_t               ax1,
						   GuppiElementViewCartesian *cart2,
						   guppi_axis_t               ax2)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart1));
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW_CARTESIAN (cart2));
  g_assert (0 <= ax1 && ax1 < GUPPI_LAST_AXIS);
  g_assert (0 <= ax2 && ax2 < GUPPI_LAST_AXIS);

  guppi_element_view_freeze ((GuppiElementView *) cart2);

  set_axis_markers (cart2, ax2,
		    guppi_element_view_cartesian_get_axis_markers (cart1, ax1));

  guppi_element_view_cartesian_connect_view_intervals (cart1, ax1, cart2, ax2);

  guppi_element_view_changed ((GuppiElementView *) cart2);
  guppi_element_view_thaw ((GuppiElementView *) cart2);
}


/* $Id$ */
