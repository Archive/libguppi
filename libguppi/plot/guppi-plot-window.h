/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-window.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_WINDOW_H
#define _INC_GUPPI_PLOT_WINDOW_H

/* #include <gnome.h> */
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-stock.h>
#include <libgnomeui/gnome-appbar.h>

#include <libguppi/plot/guppi-root-group-view.h>
#include <libguppi/plot/guppi-root-group-item.h>
#include <libguppi/plot/guppi-canvas-item.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiPlotWindow GuppiPlotWindow;
typedef struct _GuppiPlotWindowClass GuppiPlotWindowClass;

struct _GuppiPlotWindow {
  GnomeApp parent;

  GtkToolbar *toolbar;
  GtkCombo *zoom_combo;
  GnomeAppBar *status_bar;

#if 0
  GuppiProperties *properties;
  GtkWidget *properties_window;
#endif

  GnomeCanvas *canv;
  GuppiRootGroupView *root_view;
  GuppiRootGroupItem *root_item;
};

struct _GuppiPlotWindowClass {
  GnomeAppClass parent_class;
};

#define GUPPI_TYPE_PLOT_WINDOW (guppi_plot_window_get_type())
#define GUPPI_PLOT_WINDOW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PLOT_WINDOW,GuppiPlotWindow))
#define GUPPI_PLOT_WINDOW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_WINDOW,GuppiPlotWindowClass))
#define GUPPI_IS_PLOT_WINDOW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PLOT_WINDOW))
#define GUPPI_IS_PLOT_WINDOW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_WINDOW))

GtkType guppi_plot_window_get_type (void);

void guppi_plot_window_construct (GuppiPlotWindow *, GuppiRootGroupView *);
GtkWidget *guppi_plot_window_new (GuppiRootGroupView * root_view);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PLOT_WINDOW_H */

/* $Id$ */
