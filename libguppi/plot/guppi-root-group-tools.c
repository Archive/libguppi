/* $Id$ */

/*
 * guppi-root-group-tools.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-root-group-tools.h>

#include <gdk/gdkkeysyms.h>

#include <libguppi/useful/guppi-i18n.h>

#include <libguppi/plot/guppi-root-group-view.h>

#include <libguppi/plot/guppi-plot-toolhelp.h>

static void
start_help_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  GtkWidget *win;
  GtkWidget *frame;
  GtkWidget *help;

  help = guppi_plot_toolhelp_new (item, tool->pt_x, tool->pt_y);
  if (help == NULL)
    return;

  win = gtk_window_new (GTK_WINDOW_POPUP);
  frame = gtk_frame_new (NULL);

  gtk_container_add (GTK_CONTAINER (win), frame);
  gtk_container_add (GTK_CONTAINER (frame), help);
  gtk_window_set_position (GTK_WINDOW (win), GTK_WIN_POS_MOUSE);

  gtk_widget_show_all (win);
  tool->ptr_arg1 = win;
}

static void
end_help_cb (GuppiPlotTool * tool, GuppiCanvasItem * item)
{
  if (tool->ptr_arg1)
    gtk_widget_destroy (GTK_WIDGET (tool->ptr_arg1));
  tool->ptr_arg1 = NULL;
}

GuppiPlotTool *
guppi_root_group_tool_new_help (void)
{
  GuppiPlotTool *tool;

  tool = guppi_plot_tool_new ();
  guppi_plot_tool_set_name (tool, _("Pop-up Help"));

  tool->supported_type = GUPPI_TYPE_ROOT_GROUP_ITEM;
  tool->first = start_help_cb;
  tool->last = end_help_cb;

  return tool;
}

/**************************************************************************/

GuppiPlotToolkit *
guppi_root_group_toolkit_configure (void)
{
  GuppiPlotToolkit *tk;

  tk = guppi_plot_toolkit_new (_("Configure"));

  guppi_plot_toolkit_set_key_tool (tk, GDK_question, GDK_SHIFT_MASK,
				   guppi_root_group_tool_new_help ());

  return tk;
}



/* $Id$ */
