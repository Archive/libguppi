/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-view-interval.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-view-interval.h>

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-marshal.h>


static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  PREFERRED_RANGE_REQUEST,
  LAST_SIGNAL
};
static guint gvi_signals[LAST_SIGNAL] = { 0 };

static void
guppi_view_interval_finalize (GObject * obj)
{
  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_view_interval_class_init (GuppiViewIntervalClass * klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_view_interval_finalize;

  gvi_signals[CHANGED] =
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiViewIntervalClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

  gvi_signals[PREFERRED_RANGE_REQUEST] =
    g_signal_new ("preferred_range_request",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiViewIntervalClass, preferred_range_request),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);
}

static void
guppi_view_interval_init (GuppiViewInterval * obj)
{
  obj->id = guppi_unique_id ();
  obj->type = GUPPI_VIEW_NORMAL;
  obj->t0 = 0;
  obj->t1 = 1;
  obj->min = -HUGE_VAL;
  obj->max = HUGE_VAL;
  obj->min_width = 0;

  obj->include_min = TRUE;
  obj->include_max = TRUE;
}

GType guppi_view_interval_get_type (void)
{
  static GType guppi_view_interval_type = 0;
  if (!guppi_view_interval_type) {
    static const GTypeInfo guppi_view_interval_info = {
      sizeof (GuppiViewIntervalClass),
      NULL, NULL,
      (GClassInitFunc) guppi_view_interval_class_init,
      NULL, NULL,
      sizeof (GuppiViewInterval),
      0,
      (GInstanceInitFunc) guppi_view_interval_init
    };
    guppi_view_interval_type =
      g_type_register_static (G_TYPE_OBJECT, "GuppiViewInterval", &guppi_view_interval_info, 0);
  }
  return guppi_view_interval_type;
}

GuppiViewInterval *
guppi_view_interval_new (void)
{
  return GUPPI_VIEW_INTERVAL (guppi_object_new (guppi_view_interval_get_type ()));
}

static void
changed (GuppiViewInterval *v)
{
  if (!v->block_changed_signals)
    g_signal_emit (v, gvi_signals[CHANGED], 0);
}

void
guppi_view_interval_set (GuppiViewInterval *v, double a, double b)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  guppi_2sort (&a, &b);
  if (a < v->min)
    a = v->min;
  if (b > v->max)
    b = v->max;

  if (b - a < v->min_width)
    return;

  if (guppi_view_interval_is_logarithmic (v)) {
    if (b <= 0)
      b = 1;
    if (a <= 0)
      a = b / 1e+10;
  }

  if (v->t0 != a || v->t1 != b) {
    v->t0 = a;
    v->t1 = b;
    changed (v);
  }
}

void
guppi_view_interval_grow_to (GuppiViewInterval *v, double a, double b)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (a > b) {
    double t = a;
    a = b;
    b = t;
  }

  if (v->t0 <= v->t1) {
    guppi_view_interval_set (v, MIN (a, v->t0), MAX (b, v->t1));
  } else {
    guppi_view_interval_set (v, a, b);
  }
}

void
guppi_view_interval_range (GuppiViewInterval *v, double *a, double *b)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (a)
    *a = v->t0;
  if (b)
    *b = v->t1;
}

void
guppi_view_interval_set_bounds (GuppiViewInterval *v, double a, double b)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  guppi_2sort (&a, &b);

  v->min = a;
  v->max = b;
}

void
guppi_view_interval_clear_bounds (GuppiViewInterval *v)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  v->min = -HUGE_VAL;
  v->max = HUGE_VAL;
}

void
guppi_view_interval_set_min_width (GuppiViewInterval *v, double mw)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  v->min_width = mw;
}

gboolean guppi_view_interval_valid_fn (GuppiViewInterval *v, double x)
{
  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (v), FALSE);

  switch (v->type) {

  case GUPPI_VIEW_LOG:
    return x > 0;

#if 0
  case GUPPI_VIEW_RECIPROCAL:
    return x != 0;
#endif

  default:
  }

  return TRUE;
}

double
guppi_view_interval_conv_fn (GuppiViewInterval *v, double x)
{
  double t0, t1;

  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (v), 0);

  t0 = v->t0;
  t1 = v->t1;

  switch (v->type) {

  case GUPPI_VIEW_NORMAL:
    /* do nothing */
    break;

  case GUPPI_VIEW_LOG:

    return log (x / t0) / log (t1 / t0);

#if 0
  case GUPPI_VIEW_RECIPROCAL:
    x = x ? 1 / x : 0;
    t0 = t0 ? 1 / t0 : 0;
    t1 = t1 ? 1 / t1 : 0;
    break;
#endif

  default:
    g_assert_not_reached ();

  }

  return (x - t0) / (t1 - t0);
}

double
guppi_view_interval_unconv_fn (GuppiViewInterval *v, double x)
{
  double t0, t1;

  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (v), 0);

  t0 = v->t0;
  t1 = v->t1;

  switch (v->type) {

  case GUPPI_VIEW_NORMAL:
    return t0 + x * (t1 - t0);
    break;

  case GUPPI_VIEW_LOG:
    return t0 * pow (t1 / t0, x);

  default:
    g_assert_not_reached ();
  }

  return 0;
}

void
guppi_view_interval_conv_bulk (GuppiViewInterval *v,
			       const double *in_data,
			       double *out_data, gsize N)
{
  double t0, t1, tsize, x, y = 0, c = 0;
  gsize i;
  gint type;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));
  g_return_if_fail (out_data != NULL);
  g_return_if_fail (N == 0 || in_data != NULL);

  if (N == 0)
    return;

  t0 = v->t0;
  t1 = v->t1;
  tsize = t1 - t0;
  type = v->type;

  if (type == GUPPI_VIEW_LOG)
    c = log (t1 / t0);

  for (i = 0; i < N; ++i) {
    x = in_data[i];

    if (type == GUPPI_VIEW_NORMAL) {
      y = (x - t0) / tsize;
    } else if (type == GUPPI_VIEW_LOG) {
      y = log (x / t0) / c;
    } else {
      g_assert_not_reached ();
    }

    out_data[i] = y;
  }
}

void
guppi_view_interval_unconv_bulk (GuppiViewInterval *v,
				 const double *in_data,
				 double *out_data, gsize N)
{
  double t0, t1, tsize, x, y = 0, c = 0;
  gsize i;
  gint type;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));
  g_return_if_fail (out_data != NULL);
  g_return_if_fail (N == 0 || in_data != NULL);

  if (N == 0)
    return;

  t0 = v->t0;
  t1 = v->t1;
  tsize = t1 - t0;
  type = v->type;
  if (type == GUPPI_VIEW_LOG)
    c = t1 / t0;

  for (i = 0; i < N; ++i) {
    x = in_data[i];

    if (type == GUPPI_VIEW_NORMAL)
      y = t0 + x * (t1 - t0);
    else if (type == GUPPI_VIEW_LOG)
      y = t0 * pow (c, x);
    else
      g_assert_not_reached ();

    out_data[i] = y;
  }
}

void
guppi_view_interval_rescale_around_point (GuppiViewInterval *v,
					  double x, double s)
{
  double a, b;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (s < 0)
    s = -s;

  if (s != 1) {

    x = guppi_view_interval_conv (v, x);

    /* I do this to be explicit: we are transforming the conv-coordinate
       edge-points of the interval. */
    a = s * (0 - x) + x;
    b = s * (1 - x) + x;

    a = guppi_view_interval_unconv (v, a);
    b = guppi_view_interval_unconv (v, b);

    guppi_view_interval_set (v, a, b);
  }
}

void
guppi_view_interval_recenter_around_point (GuppiViewInterval *v, double x)
{
  double a, b, c;
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  guppi_view_interval_range (v, &a, &b);
  c = (a + b) / 2;
  if (c != x)
    guppi_view_interval_translate (v, x - c);
}

void
guppi_view_interval_translate (GuppiViewInterval *v, double dx)
{
  double a, b;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (dx != 0  && v->min <= a + dx && b + dx <= v->max) {
    guppi_view_interval_range (v, &a, &b);
    guppi_view_interval_set (v, a + dx, b + dx);
  }
}

void
guppi_view_interval_conv_translate (GuppiViewInterval *v, double x)
{
  double a, b;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (x == 0)
    return;

  a = x;
  b = 1 + x;

  if (!(guppi_view_interval_is_logarithmic (v) && v->t0 <= 0)) {

    a = guppi_view_interval_unconv (v, a);

  } else {

    a = v->t0;

  }

  b = guppi_view_interval_unconv (v, b);

  guppi_2sort (&a, &b);

  if (v->min <= a && b <= v->max)
    guppi_view_interval_set (v, a, b);
}

void
guppi_view_interval_request_preferred_range (GuppiViewInterval *v)
{
  double p0, p1;

  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  p0 = v->t0;
  p1 = v->t1;

  v->block_changed_signals = TRUE;

  v->t0 = 0;
  v->t1 = -1;

  g_signal_emit (v, gvi_signals[PREFERRED_RANGE_REQUEST], 0);

  if (v->t0 > v->t1)
    guppi_view_interval_set (v, -0.05, 1.05);

  v->block_changed_signals = FALSE;

  if (v->t0 != p0 || v->t1 != p1)
    changed (v);
}

/**************************************************************************/

void
guppi_view_interval_scale_linearly (GuppiViewInterval *v)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (v->type != GUPPI_VIEW_NORMAL) {
    v->type = GUPPI_VIEW_NORMAL;
    changed (v);
  }
}

void
guppi_view_interval_scale_logarithmically (GuppiViewInterval *v, double base)
{
  g_return_if_fail (GUPPI_IS_VIEW_INTERVAL (v));

  if (v->type != GUPPI_VIEW_LOG) {
    v->type = GUPPI_VIEW_LOG;
    v->type_arg = base;
    changed (v);
  }
}

gboolean guppi_view_interval_is_logarithmic (const GuppiViewInterval *v)
{
  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (v), FALSE);

  return v->type == GUPPI_VIEW_LOG;
}

double
guppi_view_interval_logarithm_base (const GuppiViewInterval *v)
{
  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (v), 0);
  g_return_val_if_fail (guppi_view_interval_is_logarithmic (v), 0);

  return v->type_arg;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

xmlNodePtr
guppi_view_interval_export_xml (GuppiViewInterval *vi, GuppiXMLDocument *doc)
{
  xmlNodePtr node;
  gchar buf[64];
  gchar *s;

  g_return_val_if_fail (GUPPI_IS_VIEW_INTERVAL (vi), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  node = xmlNewNode (doc->ns, "ViewInterval");

  s = guppi_uniq2str (vi->id);
  xmlNewProp (node, "UID", s);
  guppi_free (s);

  if (! guppi_xml_document_has_cached (doc, vi->id)) {
    
    g_snprintf (buf, 64, "%g", vi->t0);
    xmlNewProp (node, "t0", buf);

    g_snprintf (buf, 64, "%g", vi->t1);
    xmlNewProp (node, "t1", buf);

    g_snprintf (buf, 64, "%d", vi->type);
    xmlNewProp (node, "type", buf);

    g_snprintf (buf, 64, "%g", vi->type_arg);
    xmlNewProp (node, "type_arg", buf);

    guppi_ref (vi);
    guppi_xml_document_cache_full (doc, vi->id, vi, guppi_unref_fn);
  }

  return node;
}

GuppiViewInterval *
guppi_view_interval_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiViewInterval *vi;
  guppi_uniq_t id;
  gchar *s;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ViewInterval"))
    return NULL;

  s = xmlGetProp (node, "UID");
  id = guppi_str2uniq (s);
  xmlFree (s);

  vi = guppi_xml_document_lookup (doc, id);

  if (vi && GUPPI_IS_VIEW_INTERVAL (vi))
    return vi;

  vi = guppi_view_interval_new ();
  vi->id = id;
  
  s = xmlGetProp (node, "t0");
  vi->t0 = s ? atof (s) : 0;
  xmlFree (s);

  s = xmlGetProp (node, "t1");
  vi->t1 = s ? atof (s) : 1;
  xmlFree (s);

  s = xmlGetProp (node, "type");
  vi->type = s ? atoi(s) : GUPPI_VIEW_NORMAL;
  xmlFree (s);

  s = xmlGetProp (node, "type_arg");
  vi->type_arg = s ? atof (s) : 0;
  xmlFree (s);
		
  guppi_xml_document_cache_full (doc, vi->id, vi, guppi_unref_fn);

  return vi;
}


/* $Id$ */
