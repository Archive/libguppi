/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-element-view.h>

#include <math.h>
#include <string.h>
#include <glade/glade.h>
#include <libart_lgpl/libart.h>
#include <libxml/xmlmemory.h>

#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-marshal.h>
#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-attribute-bag.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-paths.h>
#include <libguppi/useful/guppi-unique-id.h>
#include <libguppi/layout/guppi-layout-item.h>

typedef struct _GuppiElementViewPrivate GuppiElementViewPrivate;
struct _GuppiElementViewPrivate {

  guppi_uniq_t id;
  gchar *label;

  double width_request;
  double height_request;
  ArtDRect bbox;

  guint32 debug_bg_color;

  GuppiElementState *state;
  guint state_changed_handler;
  guint state_changed_request_handler;

  GuppiAttributeBag *attr_bag;

  gint freeze_count;

  guint hide           : 1;
  guint clipped        : 1;
  guint have_bbox      : 1;
  guint pending_change : 1;
};

#define priv(x) ((x)->priv)

static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  CHANGED_STRUCTURE,
  LAST_SIGNAL
};

static guint view_signals[LAST_SIGNAL] = { 0 };

static void
guppi_element_view_finalize (GObject *obj)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (obj);
  GuppiElementViewPrivate *p = priv (view);

  guppi_finalized (obj);

  /* clean up from state */

  if (p->state_changed_handler)
    g_signal_handler_disconnect (p->state, p->state_changed_handler);
  if (p->state_changed_request_handler)
    g_signal_handler_disconnect (p->state, p->state_changed_request_handler);
  guppi_unref0 (p->state);

  guppi_unref0 (p->attr_bag);

  guppi_free0 (view->priv);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiElementView *
guppi_element_view_new (GuppiElementState *state, ...)
{
  GuppiElementView *view;
  va_list args;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);

  view = guppi_element_state_make_view (state);
  va_start (args, state);
  guppi_attribute_bag_vset (priv (view)->attr_bag, &args);
  va_end (args);

  return view;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

const gchar *
guppi_element_view_get_label (GuppiElementView *view)
{
  const gchar *label = NULL;
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);

  guppi_attribute_bag_get1 (view->priv->attr_bag, "label::raw", &label);
  return label;
}

void
guppi_element_view_set_label (GuppiElementView *view, const gchar *label)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  guppi_attribute_bag_set (view->priv->attr_bag, "label", label, NULL);
}

gboolean
guppi_element_view_get_clipped (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);
  return view->priv->clipped;
}

void
guppi_element_view_set_clipped (GuppiElementView *view, gboolean x)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  view->priv->clipped = x;
}

guppi_uniq_t
guppi_element_view_unique_id (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), 0);
  return priv (view)->id;
}

static GuppiAttributeBag *
resolve_dot (GuppiElementView *view, const gchar *key, gchar **true_key)
{
  GuppiElementViewClass *klass;
  GuppiElementState *es = NULL;
  GuppiElementView *ev = NULL;
  GuppiAttributeBag *ret_bag = NULL;
  gchar *target_label;
  gchar *dot = strchr (key, '.');

  if (dot == NULL) {
    *true_key = (gchar *) key;
    return view->priv->attr_bag;
  }

  target_label = guppi_strndup (key, dot-key);
  *true_key = (gchar *) dot+1;

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));
  if (klass->find (view, target_label, &es, &ev)) {
    if (es != NULL) {
      ret_bag = guppi_element_state_attribute_bag (es);
    } else {
      ret_bag = ev->priv->attr_bag;
    }
  }

  guppi_free (target_label);
  return ret_bag;
}

gboolean
guppi_element_view_get (GuppiElementView *view, ...)
{
  va_list args;
  GuppiAttributeBag *bag;
  const gchar *key, *true_key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  va_start (args, view);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {
      bag = resolve_dot (view, key, (gchar **) &true_key);
      if (bag) {
	guppi_attribute_bag_get1 (bag, true_key, va_arg (args, gpointer));
      } else {
	g_warning ("problem! (%s)", key);
	key = NULL;
	rv = FALSE;
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

gboolean
guppi_element_view_set (GuppiElementView *view, ...)
{
  va_list args;
  GuppiAttributeBag *bag;
  const gchar *key, *true_key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), FALSE);

  va_start (args, view);
  do {
    key = va_arg (args, const gchar *);
    
    if (key != NULL) {
      bag = resolve_dot (view, key, (gchar **) &true_key);
      if (bag) {
	guppi_attribute_bag_vset1 (bag, true_key, &args);
      } else {
	g_warning ("problem setting (%s)", key);
	key = NULL;
	rv = FALSE;
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/*
 *
 * State-related operations
 *
 */

static void
state_changed_proxy_fn (GuppiElementState *state, gpointer view)
{
  guppi_element_view_changed (view);
}

static void
state_changed_request_proxy_fn (GuppiElementState *state, double width, double height, gpointer closure)
{
  /* FIXME: change request here */
  /* guppi_element_view_changed_size (GUPPI_ELEMENT_VIEW (closure), width, height); */
}

void
guppi_element_view_set_state (GuppiElementView *view,
			      GuppiElementState *state)
{
  GuppiElementViewClass *klass;
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (state));

  p = priv (view);
  g_return_if_fail (p->state == NULL);

  p->state = state;
  guppi_ref (p->state);

  /* Forward state 'changed' signals as 'changed_state' */
  p->state_changed_handler = g_signal_connect (state,
					       "changed",
					       (GCallback) state_changed_proxy_fn,
					       view);

  /* Forward the state's 'changed_request' signals to the view. */
  p->state_changed_request_handler = g_signal_connect (state,
						       "changed_request",
						       (GCallback) state_changed_request_proxy_fn,
						       view);

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));
  if (klass->view_init)
    klass->view_init (view);
}

GuppiElementState *
guppi_element_view_state (GuppiElementView *view)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  return priv (view)->state;
}

void
guppi_element_view_set_request (GuppiElementView *view,
				double width, double height)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  if (width < 0)
    width = -1;

  if (height < 0)
    height = -1;
  
  if (view->priv->width_request != width
      || view->priv->height_request != height) {
      
    view->priv->width_request = width;
    view->priv->height_request = height;

    guppi_layout_item_changed_request (GUPPI_LAYOUT_ITEM (view));
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_element_view_conv (GuppiElementView *view,
			 const ArtPoint   *t,
			 ArtPoint         *p)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (t != NULL);
  g_return_if_fail (p != NULL);

  p->x = t->x * (view->priv->bbox.x1 - view->priv->bbox.x0) + view->priv->bbox.x0;
  p->y = t->y * (view->priv->bbox.y1 - view->priv->bbox.y0) + view->priv->bbox.y0;
}

void
guppi_element_view_conv_bulk (GuppiElementView *view,
			      const ArtPoint   *t,
			      ArtPoint         *p,
			      gsize             N)
{
  double x0, y0, w, h;
  gsize i;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (t != NULL);
  g_return_if_fail (p != NULL);

  x0 = view->priv->bbox.x0;
  y0 = view->priv->bbox.y0;
  w = view->priv->bbox.x1 - view->priv->bbox.x0;
  h = view->priv->bbox.y1 - view->priv->bbox.y0;

  for (i = 0; i < N; ++i) {
    p[i].x = t[i].x * w + x0;
    p[i].y = t[i].y * h + y0;
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_element_view_render (GuppiElementView *view,
			   ArtDRect *bbox,
			   GuppiCanvas *canvas)
{
  GuppiElementViewClass *klass;
  ArtDRect render_bbox;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_return_if_fail (GUPPI_IS_CANVAS (canvas));

  if (! view->priv->have_bbox) {
    g_warning ("Blocking pre-allocation render!");
    return;
  }

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  g_assert (klass->render);

  render_bbox = view->priv->bbox;
  if (bbox) {
    art_drect_intersect (&render_bbox, &render_bbox, bbox);
  }

  if (! art_drect_empty (&render_bbox)) {
    guppi_canvas_push_attributes (canvas);

    if (view->priv->clipped)
      guppi_canvas_set_clipping_rectangle (canvas, &render_bbox);

    if (view->priv->debug_bg_color) {
      guppi_canvas_push_attributes (canvas);
      guppi_canvas_set_color_rgba (canvas, view->priv->debug_bg_color);
      guppi_canvas_draw_rectangle (canvas, &render_bbox, TRUE, FALSE);
      guppi_canvas_pop_attributes (canvas);
    }

    klass->render (view, &render_bbox, canvas);
    guppi_canvas_pop_attributes (canvas);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_element_view_changed (GuppiElementView *view)
{
  GuppiElementViewPrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  p = priv (view);

  if (p->freeze_count > 0)
    p->pending_change = TRUE;
  else {
    g_signal_emit (view, view_signals[CHANGED], 0);
    p->pending_change = FALSE;
  }
}

void
guppi_element_view_freeze (GuppiElementView *view)
{
  GuppiElementViewClass *klass;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  g_return_if_fail (view->priv->freeze_count >= 0);
  ++view->priv->freeze_count;

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));
  if (klass->freeze)
    klass->freeze (view);
}

void
guppi_element_view_thaw (GuppiElementView *view)
{
  GuppiElementViewClass *klass;

  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  g_return_if_fail (view->priv->freeze_count > 0);
  --view->priv->freeze_count;

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));
  if (klass->thaw)
    klass->thaw (view);

  if (view->priv->freeze_count == 0) {

    if (view->priv->pending_change) 
      guppi_element_view_changed (view);
  }
}

void
guppi_element_view_changed_structure (GuppiElementView *view)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  g_signal_emit (view, view_signals[CHANGED_STRUCTURE], 0);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

xmlNodePtr
guppi_element_view_export_xml (GuppiElementView *view, GuppiXMLDocument *doc)
{
  GuppiElementViewClass *klass;
  xmlNodePtr view_node = NULL;
  xmlNodePtr state_node = NULL;
  xmlNodePtr attr_node = NULL;
  gchar *uid_str;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  view_node = xmlNewNode (doc->ns, "ElementView");

  uid_str = guppi_uniq2str (priv (view)->id);
  xmlNewProp (view_node, "UID", uid_str);
  guppi_free (uid_str);

  /* It is very important that the first child of the view is the state! */
  state_node = guppi_element_state_export_xml (guppi_element_view_state (view), doc);
  if (state_node)
    xmlAddChild (view_node, state_node);

  attr_node = guppi_attribute_bag_export_xml (priv (view)->attr_bag, doc);
  if (attr_node) {
    if (attr_node->xmlChildrenNode != NULL) {
      xmlAddChild (view_node, attr_node);
    } else {
      xmlFreeNode (attr_node);
    }
  }

  if (klass->xml_export) {
    klass->xml_export (view, doc, view_node);
  }

  return view_node;
}

GuppiElementView *
guppi_element_view_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiElementState *state;
  GuppiElementView *view;
  GuppiElementViewClass *klass;
  gchar *uid_str = NULL;
  gboolean loaded_attr_bag = FALSE;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ElementView"))
    return NULL;

  uid_str = xmlGetProp (node, "UID");

  node = node->xmlChildrenNode;

  state = guppi_element_state_import_xml (doc, node);
  if (state == NULL)
    return NULL;
  view = guppi_element_view_new (state, NULL);
  guppi_unref0 (state);

  view->priv->id = guppi_str2uniq (uid_str);
  xmlFree (uid_str);
  
  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  node = node->next;

  while (node != NULL) {

    if (!loaded_attr_bag && guppi_attribute_bag_import_xml (view->priv->attr_bag, doc, node)) {

      loaded_attr_bag = TRUE;

    } else if (klass->xml_import) {

      klass->xml_import (view, doc, node);

    }
    
    node = node->next;

  }

  return view;
}

void
guppi_element_view_spew_xml (GuppiElementView *view)
{
  GuppiXMLDocument *doc;
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_view_export_xml (view, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

void
guppi_element_view_set_debug_bg_color (GuppiElementView *view,
				       guint32 color)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_VIEW (view));
  view->priv->debug_bg_color = color;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* Default handler for find method */

static gboolean
find (GuppiElementView *view, const gchar *label, GuppiElementState **stateptr, GuppiElementView **viewptr)
{
  const gchar *our_label;

  if ((stateptr == NULL && viewptr == NULL) || label == NULL)
    return FALSE;

  if (viewptr) {
    our_label = guppi_element_view_get_label (view);

    if (our_label && !strcmp (label, our_label)) {
      *viewptr = view;
      return TRUE;
    }
  }

  if (stateptr) {
    GuppiElementState *state;
  
    state = guppi_element_view_state (view);
    our_label = guppi_element_state_get_label (state);
    
    if (our_label && !strcmp (label, our_label)) {
      *stateptr = state;
      return TRUE;
    }
  }

  return FALSE;
}

GuppiElementState *
guppi_element_view_find_state (GuppiElementView *view, const gchar *label)
{
  GuppiElementViewClass *klass;
  GuppiElementState *state = NULL;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  if (klass->find) {
    klass->find (view, label, &state, NULL);
  }

  return state;
}

GuppiElementView *
guppi_element_view_find_view (GuppiElementView *view, const gchar *label)
{
  GuppiElementViewClass *klass;
  GuppiElementView *subview = NULL;
  
  g_return_val_if_fail (GUPPI_IS_ELEMENT_VIEW (view), NULL);
  g_return_val_if_fail (label != NULL, NULL);

  klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  if (klass->find) {
    klass->find (view, label, NULL, &subview);
  }

  return subview;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

/* GuppiLayoutItem interface */

static void
guppi_element_view_get_request (GuppiLayoutItem *item,
				double          *width,
				double          *height)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (item);

  if (width)
    *width = view->priv->width_request;

  if (height)
    *height = view->priv->height_request;
}

static gboolean
guppi_element_view_get_allocation (GuppiLayoutItem *item,
				   ArtDRect        *allocation)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (item);

  *allocation = view->priv->bbox;

  return view->priv->have_bbox;
}

static void
guppi_element_view_set_allocation (GuppiLayoutItem *item,
				   ArtDRect        *allocation)
{
  GuppiElementView *view = GUPPI_ELEMENT_VIEW (item);
  GuppiElementViewClass *klass = GUPPI_ELEMENT_VIEW_CLASS (G_OBJECT_GET_CLASS (view));

  view->priv->bbox = *allocation;
  view->priv->have_bbox = TRUE;

#if 0
  g_print ("%s (%g,%g) (%g,%g) w=%g h=%g\n",
	   g_type_name (G_OBJECT_TYPE (view)),
	   allocation->x0, allocation->y0,
	   allocation->x1, allocation->y1,
	   allocation->x1 - allocation->x0,
	   allocation->y1 - allocation->y0);
#endif

  if (klass->set_allocation)
    klass->set_allocation (view);
}

static void
guppi_element_view_guppi_layout_item_init (GuppiLayoutItemIface *iface)
{
  iface->get_request = guppi_element_view_get_request;
  iface->get_allocation = guppi_element_view_get_allocation;
  iface->set_allocation = guppi_element_view_set_allocation;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static void
view_init (GuppiElementView *view)
{

}

static void
guppi_element_view_class_init (GuppiElementViewClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_element_view_finalize;

  klass->find = find;

  klass->view_init = view_init;

  view_signals[CHANGED] =
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiElementViewClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

  view_signals[CHANGED_STRUCTURE] =
    g_signal_new ("changed_structure",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiElementViewClass, changed_structure),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

}

static void
guppi_element_view_init (GuppiElementView *view)
{
  GuppiElementViewPrivate *p;

  p = view->priv = guppi_new0 (GuppiElementViewPrivate, 1);

  p->id = guppi_unique_id ();

  p->attr_bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add_with_default (p->attr_bag, GUPPI_ATTR_STRING, "label", NULL, _("Unlabelled"));

  p->width_request = -1;
  p->height_request = -1;

  p->clipped = TRUE;
}

GType 
guppi_element_view_get_type (void)
{
  static GType guppi_element_view_type = 0;

  if (!guppi_element_view_type) {

    static const GTypeInfo guppi_element_view_info = {
      sizeof (GuppiElementViewClass),
      NULL, NULL,
      (GClassInitFunc) guppi_element_view_class_init,
      NULL, NULL,
      sizeof (GuppiElementView),
      0,
      (GInstanceInitFunc) guppi_element_view_init
    };

    static const GInterfaceInfo guppi_layout_item_info = {
      (GInterfaceInitFunc) guppi_element_view_guppi_layout_item_init,
      NULL, NULL
    };
    
    guppi_element_view_type = g_type_register_static (G_TYPE_OBJECT,
						      "GuppiElementView",
						      &guppi_element_view_info,
						      0);

    g_type_add_interface_static (guppi_element_view_type,
				 GUPPI_TYPE_LAYOUT_ITEM,
				 &guppi_layout_item_info);
  }

  return guppi_element_view_type;
}


/* $Id$ */
