/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_ELEMENT_STATE_H
#define _INC_GUPPI_ELEMENT_STATE_H

#include <libguppi/useful/guppi-unique-id.h>
#include <libguppi/useful/guppi-attribute-bag.h>
#include <libguppi/useful/guppi-xml.h>
#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS

/* forward defs */
struct _GuppiElementView;

typedef struct _GuppiElementState GuppiElementState;
typedef struct _GuppiElementStateClass GuppiElementStateClass;
struct _GuppiElementStatePrivate;

struct _GuppiElementState {
  GObject parent;
  struct _GuppiElementStatePrivate *priv;
};

struct _GuppiElementStateClass {
  GObjectClass parent_class;

  const gchar *name;
  const gchar *plug_in_code;

  /* --- VTable --- */

  GType view_type;
  struct _GuppiElementView *(*make_view) (GuppiElementState *);

  /* XML input & output */
  void     (*xml_export)  (GuppiElementState *, GuppiXMLDocument *, xmlNodePtr root_node);
  gboolean (*xml_import)  (GuppiElementState *, GuppiXMLDocument *, xmlNodePtr);

  /* Signals */
  void (*changed)         (GuppiElementState *);
  void (*changed_request) (GuppiElementState *, double width, double height);
};

#define GUPPI_TYPE_ELEMENT_STATE (guppi_element_state_get_type())
#define GUPPI_ELEMENT_STATE(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_ELEMENT_STATE,GuppiElementState))
#define GUPPI_ELEMENT_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_ELEMENT_STATE,GuppiElementStateClass))
#define GUPPI_IS_ELEMENT_STATE(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_ELEMENT_STATE))
#define GUPPI_IS_ELEMENT_STATE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_ELEMENT_STATE))

GType guppi_element_state_get_type (void);

void guppi_element_state_register (const gchar *type_name, GType type);

GuppiElementState *guppi_element_state_new (const gchar *type, ...);

guppi_uniq_t       guppi_element_state_unique_id     (GuppiElementState *);
GuppiAttributeBag *guppi_element_state_attribute_bag (GuppiElementState *);

const gchar *guppi_element_state_get_label (GuppiElementState *);
void         guppi_element_state_set_label (GuppiElementState *, const gchar *label);

gboolean guppi_element_state_get (GuppiElementState *, ...);
gboolean guppi_element_state_set (GuppiElementState *, ...);

struct _GuppiElementView *guppi_element_state_make_view (GuppiElementState *);

void         guppi_element_state_changed (GuppiElementState *);
const gchar *guppi_element_state_get_changed_attribute (GuppiElementState *);

void guppi_element_state_set_request (GuppiElementState *, double width, double height);

xmlNodePtr         guppi_element_state_export_xml (GuppiElementState *, GuppiXMLDocument *);
GuppiElementState *guppi_element_state_import_xml (GuppiXMLDocument *, xmlNodePtr);

void               guppi_element_state_spew_xml   (GuppiElementState *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_ELEMENT_STATE_H */

/* $Id$ */
