/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-root-group-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-root-group-item.h>

#include <libgnomecanvas/gnome-canvas-rect-ellipse.h>

#include <math.h>
#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-metrics.h>
#include <libguppi/useful/guppi-rgb.h>
#include <libguppi/plot/guppi-root-group-view.h>

#include <libguppi/plot/guppi-root-group-tools.h>

static GObjectClass *parent_class = NULL;

static void
guppi_root_group_item_destroy (GtkObject *obj)
{
  GuppiRootGroupItem *item = GUPPI_ROOT_GROUP_ITEM (obj);
  GnomeCanvas *canvas = GNOME_CANVAS_ITEM (item)->canvas;

  if (item->pending_button_press) {
    g_source_remove (item->pending_button_press);
    item->pending_button_press = 0;
  }

  if (item->pending_key_release) {
    g_source_remove (item->pending_key_release);
    item->pending_key_release = 0;
  }

  if (item->kp_sig)
    g_signal_handler_disconnect (item->key_event_source, item->kp_sig);

  if (item->kr_sig)
    g_signal_handler_disconnect (item->key_event_source, item->kr_sig);

  if (item->background) {
    g_object_unref (item->background);
    item->background = NULL;
  }

  /* Shut down drag & drop in an orderly manner... */
  gtk_drag_dest_unset (GTK_WIDGET (GNOME_CANVAS_ITEM (item)->canvas));

  if (item->dnd_highlighted_item) {
    g_object_unref (item->dnd_highlighted_item);
    item->dnd_highlighted_item = NULL;
  }

  if (item->drag_leave_sig) {
    g_signal_handler_disconnect (canvas, item->drag_leave_sig);
    item->drag_leave_sig = 0;
  }

  if (item->drag_motion_sig) {
    g_signal_handler_disconnect (canvas, item->drag_motion_sig);
    item->drag_motion_sig = 0;
  }

  if (item->drag_drop_sig) {
    g_signal_handler_disconnect (canvas, item->drag_drop_sig);
    item->drag_drop_sig = 0;
  }

  if (item->drag_data_recv_sig) {
    g_signal_handler_disconnect (canvas, item->drag_data_recv_sig);
    item->drag_data_recv_sig = 0;
  }

  if (item->resize_sig) {
    g_signal_handler_disconnect (canvas, item->resize_sig);
    item->resize_sig = 0;
  }

  guppi_unref (item->background);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    GTK_OBJECT_CLASS (parent_class)->destroy (GTK_OBJECT (obj));
}

static void
guppi_root_group_item_finalize (GObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
set_canvas_size (GuppiRootGroupItem *rg)
{
  GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (rg);
  GnomeCanvas *canv = GNOME_CANVAS_ITEM (rg)->canvas;
  double s = guppi_canvas_item_scale (item);
  double x0, y0, x1, y1;
  gint xsize, ysize;

  guppi_canvas_item_get_bbox_pt (item, &x0, &y0, &x1, &y1);

  xsize = (gint) rint (guppi_x_pt2px ((x1 - x0) * s));
  ysize = (gint) rint (guppi_y_pt2px ((y1 - y0) * s));

  gnome_canvas_set_scroll_region (canv, 0, 0, xsize, ysize);

  if (rg->background == NULL) {
    rg->background =
      gnome_canvas_item_new (GNOME_CANVAS_GROUP
			     (GNOME_CANVAS_ITEM (rg)->parent),
			     gnome_canvas_rect_get_type (), "x1", 0.0, "y1",
			     0.0, "x2", (double) xsize, "y2", (double) ysize,
			     "fill_color_rgba", RGBA_WHITE, NULL);
    /* Add our own reference to it so that it is not destroyed prematurely when
     * the entire canvas is destroyed */
    guppi_ref (rg->background);
  } else {
    gnome_canvas_item_set (rg->background,
			   "x2", (double) xsize, "y2", (double) ysize, NULL);
  }
  gnome_canvas_item_lower_to_bottom (rg->background);

  guppi_canvas_item_set_bbox_c (item, 0, 0, xsize, ysize);
}

static void
changed_scale (GuppiCanvasItem *item, double scale)
{
  static void canv_size_allocate (GtkWidget *, GtkAllocation *, gpointer);
  GuppiRootGroupItem *root_item = GUPPI_ROOT_GROUP_ITEM (item);

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_scale)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->changed_scale (item, scale);

  set_canvas_size (root_item);

  if (root_item->resize_semantics == ROOT_GROUP_RESIZE_FILL_SPACE) {
    canv_size_allocate (GTK_WIDGET (GNOME_CANVAS_ITEM (item)->canvas),
			NULL, root_item);
  }
}

/***************************************************************************/

/*** Button Press/Release Handling */

/*
 * Handle delayed execution of button-press events.
 * By waiting, we are able to disinguish between single-click and
 * double-click events.
 */

/* We require double-clicks to be a bit faster than gtk+ --- a sluggish
   double-click will trigger both a single-click and a double-click
   action. (i.e. the tool will quickly activate & then deactivate
   before the double-click is processed.) */
#define BUTTON_TIMEOUT_LEN 100

static gint
button_press_idle_cb (gpointer ptr)
{
  GuppiRootGroupItem *root;

  g_return_val_if_fail (ptr != NULL && GUPPI_IS_ROOT_GROUP_ITEM (ptr), FALSE);

  root = GUPPI_ROOT_GROUP_ITEM (ptr);

  g_return_val_if_fail (root->pending_tool != NULL, FALSE);
  g_return_val_if_fail (root->pending_item != NULL, FALSE);

  /* Let's wait a bit longer and see if this is a double-click. */
  if (root->pending_released && root->pending_skips == 0) {
    root->pending_skips = 1;
    return TRUE;
  }

  if (root->pending_button_press == 0)
    return FALSE;

  root->pending_button_press = 0;

  root->active_tool = root->pending_tool;

  gnome_canvas_item_grab (GNOME_CANVAS_ITEM (root),
			  GDK_BUTTON_PRESS_MASK |
			  GDK_BUTTON_RELEASE_MASK |
			  GDK_BUTTON_MOTION_MASK |
			  GDK_POINTER_MOTION_MASK |
			  GDK_POINTER_MOTION_HINT_MASK,
			  root->active_tool->cursor, GDK_CURRENT_TIME);

  guppi_plot_tool_first (root->active_tool,
			 root->pending_item,
			 root->pending_c_x, root->pending_c_y);

  root->pending_item = NULL;
  root->pending_tool = NULL;

  if (root->pending_released && root->active_tool) {
    /* This is basically code dup from button_release_event */
    guppi_plot_tool_end (root->active_tool,
			 root->pending_c_x, root->pending_c_y);
    gnome_canvas_item_ungrab (GNOME_CANVAS_ITEM (root), GDK_CURRENT_TIME);
    root->active_tool = NULL;
  }

  return FALSE;
}

static void
add_pending_button_press (GuppiRootGroupItem *root,
			  GuppiCanvasItem *item,
			  GuppiPlotTool *tool, gint c_x, gint c_y)
{
  g_return_if_fail (root != NULL && GUPPI_IS_ROOT_GROUP_ITEM (root));
  g_return_if_fail (item != NULL && GUPPI_IS_CANVAS_ITEM (item));
  g_return_if_fail (tool != NULL && GUPPI_IS_PLOT_TOOL (tool));

  g_return_if_fail (root->pending_button_press == 0);

  root->pending_item = item;
  root->pending_tool = tool;
  root->pending_c_x = c_x;
  root->pending_c_y = c_y;
  root->pending_button_press = g_timeout_add (BUTTON_TIMEOUT_LEN,
						button_press_idle_cb, root);
  root->pending_released = FALSE;
  root->pending_skips = 0;
}

static void
flush_pending_button_press (GuppiRootGroupItem *root)
{
  g_return_if_fail (root != NULL && GUPPI_IS_ROOT_GROUP_ITEM (root));

  if (root->pending_button_press) {
    g_source_remove (root->pending_button_press);
    root->pending_skips = 1;
    button_press_idle_cb (root);
  }
}

static void
remove_pending_button_press (GuppiRootGroupItem *root)
{
  g_return_if_fail (root != NULL && GUPPI_IS_ROOT_GROUP_ITEM (root));

  if (root->pending_button_press) {
    g_source_remove (root->pending_button_press);
    root->pending_item = NULL;
    root->pending_tool = NULL;
    root->pending_button_press = 0;
  }
}

static gint
button_press_event (GuppiRootGroupItem *root, GdkEventButton *ev)
{
  double pt_x, pt_y;
  GuppiPlotTool *tool;
  GuppiCanvasItem *tool_item;
  GuppiCanvasItem *root_item;
  GuppiElementView *view;

  g_return_val_if_fail (root != NULL
			&& GUPPI_IS_ROOT_GROUP_ITEM (root), FALSE);
  g_return_val_if_fail (ev != NULL, FALSE);

  root_item = GUPPI_CANVAS_ITEM (root);
  view = guppi_canvas_item_view (root_item);

  /* We want nothing to do with triple-clicks... */
  if (ev->type == GDK_3BUTTON_PRESS)
    return FALSE;

  /* If another tool is already active, return. */
  if (root->active_tool != NULL)
    return FALSE;

  if (guppi_element_view_tools_are_blocked (view))
    return FALSE;

  /* Convert our button-press into device-indep coordinates. */
  guppi_canvas_item_c2pt (root_item, ev->x, ev->y, &pt_x, &pt_y);

  /* If this is the second piece of a double-click, handle it. */
  if (ev->type == GDK_2BUTTON_PRESS) {
    remove_pending_button_press (root);

    return
      guppi_canvas_item_double_click (root_item,
				      pt_x, pt_y, ev->button, ev->state);
  }

  /* In this case, this must be the second click of a double-click...
     so the right thing to do is to ignore it. */
  if (root->pending_button_press && root->pending_released)
    return TRUE;

  /* Now we try to activate a tool. */
  if (guppi_canvas_item_locate_button_tool (root_item, pt_x, pt_y,
					    ev->button, ev->state,
					    &tool_item, &tool)) {

    if (tool != NULL && tool_item != NULL) {
      add_pending_button_press (root, tool_item, tool, ev->x, ev->y);
      return TRUE;
    }

  }

  return FALSE;
}

static gint
button_release_event (GuppiRootGroupItem *root, GdkEventButton *ev)
{
  GuppiPlotTool *tool;

  g_return_val_if_fail (root != NULL
			&& GUPPI_IS_ROOT_GROUP_ITEM (root), FALSE);
  g_return_val_if_fail (ev != NULL, FALSE);


  if (root->pending_button_press) {
    root->pending_released = TRUE;
    return TRUE;
  }

  tool = root->active_tool;

  if (tool == NULL)
    return FALSE;

  guppi_plot_tool_end (tool, ev->x, ev->y);
  gnome_canvas_item_ungrab (GNOME_CANVAS_ITEM (root), ev->time);
  root->active_tool = NULL;

  return TRUE;
}

static gint
motion_notify_event (GuppiRootGroupItem *root, GdkEventMotion *ev)
{
  GuppiPlotTool *tool;

  g_return_val_if_fail (root != NULL
			&& GUPPI_IS_ROOT_GROUP_ITEM (root), FALSE);
  g_return_val_if_fail (ev != NULL, FALSE);

  flush_pending_button_press (root);

  tool = root->active_tool;

  if (tool == NULL)
    return FALSE;

  guppi_plot_tool_middle (tool, ev->x, ev->y);

  if (ev->is_hint)
    gdk_window_get_pointer (ev->window, NULL, NULL, NULL);

  return TRUE;
}

/*** Key Event Handling */

#define KEY_RELEASE_DELAY 75

static gint
key_release_cb (gpointer ptr)
{
  GuppiRootGroupItem *root;

  g_return_val_if_fail (ptr != NULL && GUPPI_IS_ROOT_GROUP_ITEM (ptr), FALSE);

  root = GUPPI_ROOT_GROUP_ITEM (ptr);

  guppi_plot_tool_end (root->active_tool, root->pending_c_x,
		       root->pending_c_y);
  gdk_keyboard_ungrab (GDK_CURRENT_TIME);
  root->active_tool = NULL;
  root->pending_key_release = 0;
  root->last_key = 0;
  root->last_key_state = 0;
  return FALSE;
}

static void
schedule_key_release (GuppiRootGroupItem *root)
{
  if (root->pending_key_release)
    g_source_remove (root->pending_key_release);

  root->pending_key_release = g_timeout_add (KEY_RELEASE_DELAY,
					       key_release_cb, root);
}

static void
cancel_key_release (GuppiRootGroupItem *root)
{
  if (root->pending_key_release)
    g_source_remove (root->pending_key_release);
  root->pending_key_release = 0;
}

static void
key_press_event (GtkWidget *w, GdkEventKey *ev, gpointer user_data)
{
  GuppiRootGroupItem *root;
  GuppiCanvasItem *root_item;
  GtkWidget *canv_widget;
  GuppiPlotTool *tool;
  GuppiCanvasItem *tool_item;
  gint x, y;
  double c_x, c_y, pt_x, pt_y;

  g_return_if_fail (w != NULL);
  g_return_if_fail (ev != NULL);
  g_return_if_fail (user_data != NULL
		    && GUPPI_IS_ROOT_GROUP_ITEM (user_data));

  root = GUPPI_ROOT_GROUP_ITEM (user_data);
  root_item = GUPPI_CANVAS_ITEM (root);
  canv_widget = GTK_WIDGET (GNOME_CANVAS_ITEM (root)->canvas);

  if (root->active_tool != NULL)
    return;
  if (root->pending_button_press)
    return;

  if (root->pending_key_release) {
    cancel_key_release (root);
    return;
  }

  /* 
   * We are stealing keypress events from the top-level window,
   * so we have to check to make sure that the keypress was actually
   * done while the pointer is above this widget.  If not, return.
   */
  gdk_window_get_pointer (canv_widget->window, &x, &y, NULL);
  if (x < 0 || x >= canv_widget->allocation.width ||
      y < 0 || y >= canv_widget->allocation.height)
    return;

  /* Convert into canvas coordinates. */
  gnome_canvas_window_to_world (GNOME_CANVAS (canv_widget), x, y, &c_x, &c_y);
  guppi_canvas_item_c2pt (root_item, c_x, c_y, &pt_x, &pt_y);

  if (guppi_canvas_item_locate_key_tool (root_item, pt_x, pt_y,
					 ev->keyval, ev->state,
					 &tool_item, &tool)) {
    root->active_tool = tool;
    root->last_key = ev->keyval;
    root->last_key_state = ev->state;
    gdk_keyboard_grab (w->window, TRUE, ev->time);
    guppi_plot_tool_first (tool, tool_item, c_x, c_y);
  }
}

static void
key_release_event (GtkWidget *w, GdkEventKey *ev, gpointer user_data)
{
  GuppiRootGroupItem *root;
  GuppiCanvasItem *root_item;
  GtkWidget *canv_widget;
  gint x, y;
  double c_x, c_y;

  g_return_if_fail (w != NULL);
  g_return_if_fail (ev != NULL);
  g_return_if_fail (user_data != NULL
		    && GUPPI_IS_ROOT_GROUP_ITEM (user_data));

  root = GUPPI_ROOT_GROUP_ITEM (user_data);
  root_item = GUPPI_CANVAS_ITEM (root);
  canv_widget = GTK_WIDGET (GNOME_CANVAS_ITEM (root)->canvas);

  if (root->active_tool == NULL)
    return;

  /* 
   * We are stealing keypress events from the top-level window,
   * so we have to check to make sure that the keypress was actually
   * done while the pointer is above this widget.  If not, return.
   */
  gdk_window_get_pointer (canv_widget->window, &x, &y, NULL);
  if (x < 0 || x >= canv_widget->allocation.width ||
      y < 0 || y >= canv_widget->allocation.height) return;

  /* Convert into canvas coordinates. */
  gnome_canvas_window_to_world (GNOME_CANVAS (canv_widget), x, y, &c_x, &c_y);

  root->pending_c_x = c_x;
  root->pending_c_y = c_y;

  schedule_key_release (root);
}


/*** Event Dispatching */

static gint
event (GnomeCanvasItem *gnoitem, GdkEvent *ev)
{
  GuppiRootGroupItem *root = GUPPI_ROOT_GROUP_ITEM (gnoitem);

  switch (ev->type) {
  case GDK_BUTTON_PRESS:
  case GDK_2BUTTON_PRESS:
    return button_press_event (root, &(ev->button));

  case GDK_BUTTON_RELEASE:
    return button_release_event (root, &(ev->button));

  case GDK_MOTION_NOTIFY:
    return motion_notify_event (root, &(ev->motion));

  default:
  }

  return FALSE;
}

/*** Drag & Drop */

static void
drag_and_drop_init (GuppiRootGroupItem *root)
{
  static GtkTargetEntry drag_types[] = {
    {"guppi/data", 0, 0},
  };
  static gint n_drag_types = sizeof (drag_types) / sizeof (drag_types[0]);

  gtk_drag_dest_set (GTK_WIDGET (GNOME_CANVAS_ITEM (root)->canvas),
		     GTK_DEST_DEFAULT_MOTION | GTK_DEST_DEFAULT_DROP,
		     drag_types, n_drag_types, GDK_ACTION_COPY);
}

static void
drag_unhighlight (GuppiRootGroupItem *root)
{
  if (root->dnd_highlighted_item) {
    gint cx1, cy1, cx2, cy2;

    g_object_unref (root->dnd_highlight);

    guppi_canvas_item_get_bbox_c (root->dnd_highlighted_item,
				  &cx1, &cy1, &cx2, &cy2);

    guppi_canvas_item_request_redraw_c (GUPPI_CANVAS_ITEM (root),
					cx1 - 3, cy1 - 3, cx2 + 3, cy2 + 3);
  }

  root->dnd_highlight = NULL;
  root->dnd_highlighted_item = NULL;
}

static void
drag_highlight (GuppiRootGroupItem *root, GuppiCanvasItem *item)
{
  GnomeCanvas *gnome_canvas;
  gint cx1, cy1, cx2, cy2;

  if (root->dnd_highlighted_item == item)
    return;

  drag_unhighlight (root);

  if (item == NULL)
    return;

  gnome_canvas = GNOME_CANVAS_ITEM (root)->canvas;

  root->dnd_highlighted_item = item;

  guppi_canvas_item_get_bbox_c (item, &cx1, &cy1, &cx2, &cy2);

  root->dnd_highlight =
    gnome_canvas_item_new (gnome_canvas_root (gnome_canvas),
			   gnome_canvas_rect_get_type (), "outline_color",
			   "blue", "fill_color_rgba", 0x0060ff40,
			   "width_pixels", (guint) 1, "x1", (double) cx1,
			   "y1", (double) cy1, "x2", (double) cx2, "y2",
			   (double) cy2, NULL);
  gnome_canvas_request_redraw (gnome_canvas, cx1 - 3, cy1 - 3, cx2 + 3,
			       cy2 + 3);
}

static void
drag_leave (GtkWidget *w, GdkDragContext *context, guint t,
	    gpointer user_data)
{
  drag_unhighlight (GUPPI_ROOT_GROUP_ITEM (user_data));
}

static gboolean
drag_motion (GtkWidget *w, GdkDragContext *context,
	     gint x, gint y, guint t, gpointer user_data)
{
  GuppiRootGroupItem *root = GUPPI_ROOT_GROUP_ITEM (user_data);
  GnomeCanvas *gnome_canvas;
  double c_x, c_y, pt_x, pt_y;
  GuppiCanvasItem *target_item = NULL;

  gnome_canvas = GNOME_CANVAS (w);

  gnome_canvas_window_to_world (gnome_canvas, x, y, &c_x, &c_y);
  guppi_canvas_item_c2pt (GUPPI_CANVAS_ITEM (root), c_x, c_y, &pt_x, &pt_y);

  target_item =
    guppi_canvas_item_data_drop_candidate (GUPPI_CANVAS_ITEM (root), pt_x,
					   pt_y);

  drag_highlight (root, target_item);

  return TRUE;
}

static void
drag_data_received (GtkWidget *w, GdkDragContext *context,
		    gint x, gint y, GtkSelectionData *selection_data,
		    guint info, guint time, gpointer user_data)
{
  GuppiRootGroupItem *root = GUPPI_ROOT_GROUP_ITEM (user_data);

  root->dnd_dropped_data = *(GuppiData **) selection_data->data;
}

static gboolean
drag_drop (GtkWidget *w, GdkDragContext *context, gint x, gint y, guint t,
	   gpointer user_data)
{
  GuppiRootGroupItem *root = GUPPI_ROOT_GROUP_ITEM (user_data);
  GuppiCanvasItem *target_item;
  double c_x, c_y, pt_x, pt_y;
  gboolean done = FALSE;

  gnome_canvas_window_to_world (GNOME_CANVAS (w), x, y, &c_x, &c_y);
  guppi_canvas_item_c2pt (GUPPI_CANVAS_ITEM (root), c_x, c_y, &pt_x, &pt_y);

  target_item =
    guppi_canvas_item_data_drop_candidate (GUPPI_CANVAS_ITEM (root), pt_x,
					   pt_y);

  if (target_item)
    guppi_canvas_item_data_drop (target_item, root->dnd_dropped_data);

  /* 
   * If all of the items refuse our dropped data, we should offer
   * some sort of visual cue (like a snap-back)... but I'm not sure
   * how to do that.
   */

  root->dnd_dropped_data = NULL;

  return done;
}

/*** Handle Resizes Gracefully */

static void
canv_size_allocate (GtkWidget *widget,
		    GtkAllocation *alloc, gpointer user_data)
{
  GuppiRootGroupItem *root_item = GUPPI_ROOT_GROUP_ITEM (user_data);
  double current_scale;
  double opt_scale;
  gboolean please_resize_canvas = FALSE;

  current_scale = guppi_canvas_item_scale (GUPPI_CANVAS_ITEM (root_item));
  opt_scale = -1;

  switch (root_item->resize_semantics) {

  case ROOT_GROUP_RESIZE_FIT_BEST:
    opt_scale = guppi_root_group_item_best_fit_scale (root_item);
    break;

  case ROOT_GROUP_RESIZE_FIT_HORIZONTAL:
    opt_scale = guppi_root_group_item_horizontal_fit_scale (root_item);
    break;

  case ROOT_GROUP_RESIZE_FIT_VERTICAL:
    opt_scale = guppi_root_group_item_vertical_fit_scale (root_item);
    break;

  case ROOT_GROUP_RESIZE_FIT_BEST_GROW_ONLY:
    opt_scale = guppi_root_group_item_best_fit_scale (root_item);
    if (opt_scale <= current_scale)
      opt_scale = -1;
    break;

  case ROOT_GROUP_RESIZE_FIT_BEST_SHRINK_ONLY:
    opt_scale = guppi_root_group_item_best_fit_scale (root_item);
    if (opt_scale >= current_scale)
      opt_scale = -1;
    break;

  case ROOT_GROUP_RESIZE_FILL_SPACE:
    {
      gint w = -1, h = -1;
      double pt_w, pt_h;
      GuppiCanvasItem *item = GUPPI_CANVAS_ITEM (root_item);
      GtkWidget *canv = GTK_WIDGET (GNOME_CANVAS_ITEM (item)->canvas);
      GtkWidget *canv_parent = canv ? canv->parent : NULL;
      GuppiRootGroupView *view = GUPPI_ROOT_GROUP_VIEW (guppi_canvas_item_view (item));

      if (canv_parent) {
	w = canv_parent->allocation.width;
	h = canv_parent->allocation.height;

	/* An ugly hack. */
	
	if (GTK_IS_SCROLLED_WINDOW (canv_parent)) {
	  GtkWidget *hbar = GTK_SCROLLED_WINDOW (canv_parent)->hscrollbar;
	  GtkWidget *vbar = GTK_SCROLLED_WINDOW (canv_parent)->vscrollbar;
	  GtkScrolledWindowClass *klass;
	  klass = GTK_SCROLLED_WINDOW_CLASS (G_OBJECT_CLASS (canv_parent));
	  if (hbar)
	    h -= hbar->allocation.height + klass->scrollbar_spacing + 1;
	  if (vbar)
	    w -= vbar->allocation.width + klass->scrollbar_spacing + 1;
	}


	if (w > 0 && h > 0) {

	  if (current_scale <= 0)
	    current_scale = opt_scale = 1;

	  pt_w = guppi_x_px2pt (w) / current_scale;
	  pt_h = guppi_y_px2pt (h) / current_scale;

	  if (pt_w > 0 && pt_h > 0) {
	    guppi_root_group_view_set_size (view, pt_w, pt_h);
	    please_resize_canvas = TRUE;
	  }
	}
      }
    }

    break;
      

  case ROOT_GROUP_RESIZE_NONE:
  default:
    /* insure we do nothing */
  }

  if (please_resize_canvas)
    set_canvas_size (root_item);

  if (opt_scale > 0)
    guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (root_item), opt_scale);
}


static void
post_realization_init (GuppiCanvasItem *item)
{
  GuppiRootGroupItem *root = GUPPI_ROOT_GROUP_ITEM (item);
  GnomeCanvas *canvas;
  GtkWidget *parent;
  double s;

  if (GUPPI_CANVAS_ITEM_CLASS (parent_class)->post_realization_init)
    GUPPI_CANVAS_ITEM_CLASS (parent_class)->post_realization_init (item);

  /* Initialize the scale & set up the canvas properly. */
  s = 1.0;
  if (root->resize_semantics != ROOT_GROUP_RESIZE_FILL_SPACE)
    s = guppi_root_group_item_best_fit_scale (root);
  guppi_canvas_item_set_scale (item, s);
			       

  /* Steal key press events from our top-level window.  This is evil. */
  parent = GTK_WIDGET (GNOME_CANVAS_ITEM (item)->canvas);
  g_assert (parent != NULL);
  while (parent->parent)
    parent = parent->parent;

  root->key_event_source = parent;

  root->kp_sig =
    g_signal_connect (parent,
		      "key_press_event",
		      (GCallback) key_press_event, 
		      item);

  root->kr_sig =
    g_signal_connect (parent,
		      "key_release_event",
		      (GCallback) key_release_event,
		      item);

  /* Hook up our drag & drop signals.  Quasi-evil. */

  drag_and_drop_init (root);

  canvas = GNOME_CANVAS_ITEM (root)->canvas;

  root->drag_leave_sig =
    g_signal_connect (canvas,
		      "drag_leave",
		      (GCallback) drag_leave, 
		      root);

  root->drag_motion_sig =
    g_signal_connect (canvas, 
		      "drag_motion",
		      (GCallback) drag_motion,
		      root);

  root->drag_drop_sig =
    g_signal_connect (canvas,
		      "drag_drop",
		      (GCallback) drag_drop,
		      root);

  root->drag_data_recv_sig =
    g_signal_connect (canvas,
		      "drag_data_received",
		      (GCallback) drag_data_received,
		      root);

  /* Define our resize semantics. */

  root->resize_sig =
    g_signal_connect (canvas,
		      "size_allocate",
		      (GCallback) canv_size_allocate,
		      root);


  /* Set our initial canvas size. */

  set_canvas_size (root);


}

/***************************************************************************/

static void
guppi_root_group_item_class_init (GuppiRootGroupItemClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GuppiCanvasItemClass *item_class = GUPPI_CANVAS_ITEM_CLASS (klass);
  GnomeCanvasItemClass *gnoitem_class = GNOME_CANVAS_ITEM_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  GTK_OBJECT_CLASS (object_class)->destroy = guppi_root_group_item_destroy;
  object_class->finalize = guppi_root_group_item_finalize;

  gnoitem_class->event = event;

  item_class->post_realization_init = post_realization_init;
  item_class->changed_scale = changed_scale;

#ifdef ACTUALLY_ALLOW_OUR_BROKEN_CONFIG_STUFF_TO_WORK
  guppi_canvas_item_class_set_item_class_toolkit (item_class,
						  guppi_root_group_toolkit_configure ());
#endif

}

static void
guppi_root_group_item_init (GuppiRootGroupItem *obj)
{
  obj->background = NULL;
  obj->resize_semantics = ROOT_GROUP_RESIZE_FIT_BEST;
}

GType
guppi_root_group_item_get_type (void)
{
  static GType guppi_root_group_item_type = 0;

  if (!guppi_root_group_item_type) {

    static const GTypeInfo guppi_root_group_item_info = {
      sizeof (GuppiRootGroupItemClass),
      NULL, NULL,
      (GClassInitFunc) guppi_root_group_item_class_init,
      NULL, NULL,
      sizeof (GuppiRootGroupItem),
      0,
      (GInstanceInitFunc) guppi_root_group_item_init
    };

    guppi_root_group_item_type =
      g_type_register_static (GUPPI_TYPE_CANVAS_GROUP,
			      "GuppiRootGroupItem",
			      &guppi_root_group_item_info,
			      0);
  }

  return guppi_root_group_item_type;
}

GObject *
guppi_root_group_item_new (void)
{
  return guppi_object_new (guppi_root_group_item_get_type ());
}

/***************************************************************************/

double
guppi_root_group_item_horizontal_fit_scale (GuppiRootGroupItem *root)
{
  GuppiRootGroupView *view;
  gint x_alloc;
  double x_size, w;

  g_return_val_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root), 0);

  view =
    GUPPI_ROOT_GROUP_VIEW (guppi_canvas_item_view (GUPPI_CANVAS_ITEM (root)));

  x_alloc = GTK_WIDGET (GNOME_CANVAS_ITEM (root)->canvas)->allocation.width;
  x_size = guppi_x_px2pt (x_alloc);
  w = guppi_root_group_view_width (view);

  return w > 0 ? x_size / w : 1.0;
}

double
guppi_root_group_item_vertical_fit_scale (GuppiRootGroupItem *root)
{
  GuppiRootGroupView *view;
  gint y_alloc;
  double y_size, h;

  g_return_val_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root), 0);

  view =
    GUPPI_ROOT_GROUP_VIEW (guppi_canvas_item_view (GUPPI_CANVAS_ITEM (root)));

  y_alloc = GTK_WIDGET (GNOME_CANVAS_ITEM (root)->canvas)->allocation.height;
  y_size = guppi_y_px2pt (y_alloc);
  h = guppi_root_group_view_height (view);

  return h > 0 ? y_size / h : 1.0;
}

double
guppi_root_group_item_best_fit_scale (GuppiRootGroupItem *root)
{
  g_return_val_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root), 0);

  return MIN (guppi_root_group_item_horizontal_fit_scale (root),
	      guppi_root_group_item_vertical_fit_scale (root));
}

void
guppi_root_group_item_horizontal_fit (GuppiRootGroupItem *root)
{
  double s;
  g_return_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root));
  s = guppi_root_group_item_horizontal_fit_scale (root);
  guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (root), s);
}

void
guppi_root_group_item_vertical_fit (GuppiRootGroupItem *root)
{
  double s;
  g_return_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root));
  s = guppi_root_group_item_vertical_fit_scale (root);
  guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (root), s);
}

void
guppi_root_group_item_best_fit (GuppiRootGroupItem *root)
{
  double s;
  g_return_if_fail (root && GUPPI_IS_ROOT_GROUP_ITEM (root));
  s = guppi_root_group_item_best_fit_scale (root);
  guppi_canvas_item_set_scale (GUPPI_CANVAS_ITEM (root), s);
}

void
guppi_root_group_item_set_resize_semantics (GuppiRootGroupItem *item,
					    gint rs)
{
  g_return_if_fail (item != NULL && GUPPI_IS_ROOT_GROUP_ITEM (item));
  if (item->resize_semantics != rs) {
    item->resize_semantics = rs;
    /* Do an immediate resize to bring the view in line w/ the new scheme. */
    canv_size_allocate (NULL, NULL, item);
  }
}


/* $Id$ */
