/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-group-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_GROUP_VIEW_H
#define _INC_GUPPI_GROUP_VIEW_H

/* #include <gnome.h> */
#include <libguppi/plot/guppi-element-view.h>
#include <libguppi/layout/guppi-layout-engine.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiGroupView GuppiGroupView;
typedef struct _GuppiGroupViewClass GuppiGroupViewClass;
struct _GuppiGroupViewPrivate;

struct _GuppiGroupView {
  GuppiElementView parent;
  struct _GuppiGroupViewPrivate *priv;
};

struct _GuppiGroupViewClass {
  GuppiElementViewClass parent_class;

  void (*add_hook) (GuppiGroupView *, GuppiElementView *);

  /* signals */
  void (*view_add)     (GuppiGroupView *, GuppiElementView *);
  void (*view_remove)  (GuppiGroupView *, GuppiElementView *);
  void (*view_replace) (GuppiGroupView *, GuppiElementView *old, GuppiElementView *neuvo);
};

#define GUPPI_TYPE_GROUP_VIEW (guppi_group_view_get_type())
#define GUPPI_GROUP_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_GROUP_VIEW,GuppiGroupView))
#define GUPPI_GROUP_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_GROUP_VIEW,GuppiGroupViewClass))
#define GUPPI_IS_GROUP_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_GROUP_VIEW))
#define GUPPI_IS_GROUP_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_GROUP_VIEW))

GType guppi_group_view_get_type (void);

GuppiGroupView *guppi_group_view_new (void);

GuppiLayoutEngine *guppi_group_view_layout (GuppiGroupView *view);

/* Element manipulation functions */

gboolean guppi_group_view_has     (GuppiGroupView *, GuppiElementView *);
void     guppi_group_view_add     (GuppiGroupView *, GuppiElementView *);
void     guppi_group_view_remove  (GuppiGroupView *, GuppiElementView *);
void     guppi_group_view_replace (GuppiGroupView *grp,
				   GuppiElementView *old,
				   GuppiElementView *nuevo);

void guppi_group_view_foreach (GuppiGroupView * grp,
			       void (*callback)(GuppiElementView *, gpointer),
			       gpointer);

/* returns < 0 if a is below b */
gint guppi_group_view_compare_z      (GuppiGroupView *grp, GuppiElementView *a, GuppiElementView *b);
void guppi_group_view_raise          (GuppiGroupView *grp, GuppiElementView *raise_this, GuppiElementView *above_this);
void guppi_group_view_raise_to_top   (GuppiGroupView *grp, GuppiElementView *);
void guppi_group_view_sink_to_bottom (GuppiGroupView *grp, GuppiElementView *);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_GROUP_VIEW_H */

/* $Id$ */
