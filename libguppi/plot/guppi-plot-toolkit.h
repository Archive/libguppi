/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-plot-toolkit.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PLOT_TOOLKIT_H
#define _INC_GUPPI_PLOT_TOOLKIT_H

#include <glib-object.h>
#include <gtk/gtk.h>
#include <libguppi/plot/guppi-plot-tool.h>

#include <libguppi/useful/guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiPlotKeystroke GuppiPlotKeystroke;
struct _GuppiPlotKeystroke {
  guint key_code;
  guint state;
  GuppiPlotTool *tool;
};

typedef struct _GuppiPlotToolkit GuppiPlotToolkit;
typedef struct _GuppiPlotToolkitClass GuppiPlotToolkitClass;

#define GUPPI_PLOT_TOOLKIT_BUTTON_MAX 5
struct _GuppiPlotToolkit {
  GObject parent;

  gchar *name;
  const gchar *toolbar_button_image;

  GtkWidget *(*build_toolbar_button) (GuppiPlotToolkit *);

  GtkWidget *(*build_toolbar_gadget) (GuppiPlotToolkit *);

  GuppiPlotTool *button_tool[GUPPI_PLOT_TOOLKIT_BUTTON_MAX];
  GuppiPlotTool *shift_button_tool[GUPPI_PLOT_TOOLKIT_BUTTON_MAX];
  GuppiPlotTool *ctrl_button_tool[GUPPI_PLOT_TOOLKIT_BUTTON_MAX];
  GuppiPlotTool *ctrl_shift_button_tool[GUPPI_PLOT_TOOLKIT_BUTTON_MAX];

  GHashTable *key_tools;
};

struct _GuppiPlotToolkitClass {
  GObjectClass parent_class;
};

#define GUPPI_TYPE_PLOT_TOOLKIT (guppi_plot_toolkit_get_type())
#define GUPPI_PLOT_TOOLKIT(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj),GUPPI_TYPE_PLOT_TOOLKIT,GuppiPlotToolkit))
#define GUPPI_PLOT_TOOLKIT_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PLOT_TOOLKIT,GuppiPlotToolkitClass))
#define GUPPI_IS_PLOT_TOOLKIT(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), GUPPI_TYPE_PLOT_TOOLKIT))
#define GUPPI_IS_PLOT_TOOLKIT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PLOT_TOOLKIT))

GType guppi_plot_toolkit_get_type (void);

GuppiPlotToolkit *guppi_plot_toolkit_new (const gchar * name);

const gchar *guppi_plot_toolkit_name (GuppiPlotToolkit *);

GuppiPlotTool *guppi_plot_toolkit_get_button_tool (GuppiPlotToolkit *,
						   guint button, guint state);

void guppi_plot_toolkit_set_button_tool (GuppiPlotToolkit *,
					 guint button, guint state,
					 GuppiPlotTool *);

GuppiPlotTool *guppi_plot_toolkit_get_key_tool (GuppiPlotToolkit *,
						guint key, guint state);

void guppi_plot_toolkit_set_key_tool (GuppiPlotToolkit *,
				      guint key, guint state,
				      GuppiPlotTool *);

GList *guppi_plot_toolkit_get_all_keys (GuppiPlotToolkit *);

void guppi_plot_toolkit_foreach (GuppiPlotToolkit *,
				 void (*fn) (GuppiPlotTool *, gpointer),
				 gpointer);

/* Return a unique id for each toolkit. */
guint guppi_plot_toolkit_signature (GuppiPlotToolkit *);

void guppi_plot_toolkit_add_to_toolbar (GuppiPlotToolkit *,
					GtkToolbar *,
					void (*select_cb) (GuppiPlotToolkit *, gpointer),
					void (*unselect_cb) (GuppiPlotToolkit *, gpointer),
					gpointer user_data);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_PLOT_TOOLKIT_H */

/* $Id$ */
