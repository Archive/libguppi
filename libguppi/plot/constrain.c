/* $Id$ */

/*
 * constrain.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-layout.h"
#include <libguppi/plot/guppi-geometry.h>

int
main (int argc, char *argv[])
{
  GuppiLayout *lay;
  GuppiGeometry *a;
  GuppiGeometry *b;
  GuppiGeometry *c;
  GuppiGeometry *d;
  gint i;

  gnome_init ("foo", "0.0", argc, argv);

  lay = guppi_layout_new ();
  a = guppi_geometry_new ();
  b = guppi_geometry_new ();
  c = guppi_geometry_new ();
  d = guppi_geometry_new ();

  guppi_layout_horizontally_adjacent (lay, a, b, 10);
  guppi_layout_width_ratio (lay, a, b, 0.25);
  guppi_layout_flush_left (lay, a, 0);
  guppi_layout_flush_right (lay, b, 0);

  guppi_layout_vertical_fill (lay, a, 0, 0);
  guppi_layout_vertical_fill (lay, b, 0, 0);

  guppi_layout_center_horizontally (lay, c);
  guppi_layout_center_vertically (lay, c);
  guppi_layout_width_relative (lay, c, 0.50);
  guppi_layout_height_relative (lay, c, 0.40);

  //  guppi_layout_aligned_right_edge(lay, a, c);


  //  guppi_layout_aligned_bottom_edge(lay, a, c);

  //  guppi_layout_same_place(lay, a, c);
  //  guppi_layout_same_place(lay, a, d);

  //  guppi_layout_vertical_fill(lay, a, 0, 0);
  //  guppi_layout_horizontal_fill(lay, a, 0, 0);

  guppi_layout_calc (lay, 0, 0, 100, 100);


  gtk_object_unref (GTK_OBJECT (a));
  gtk_object_unref (GTK_OBJECT (b));
  gtk_object_unref (GTK_OBJECT (c));
  gtk_object_unref (GTK_OBJECT (d));
  gtk_object_unref (GTK_OBJECT (lay));

  return 0;
}




/* $Id$ */
