/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-element-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <libguppi/plot/guppi-element-state.h>

#include "string.h"

#include <libxml/xmlmemory.h>

#include <libguppi/useful/guppi-debug.h>
#include <libguppi/useful/guppi-i18n.h>
#include <libguppi/useful/guppi-convenient.h>
#include <libguppi/useful/guppi-memory.h>
#include <libguppi/useful/guppi-marshal.h>
#include <libguppi/useful/guppi-unique-id.h>
#include <libguppi/useful/guppi-plug-in-spec.h>
#include <libguppi/plot/guppi-plot-plug-in.h>
#include <libguppi/plot/guppi-element-view.h>
#include <libguppi/plot/guppi-root-group-state.h>

typedef struct _GuppiElementStatePrivate GuppiElementStatePrivate;
struct _GuppiElementStatePrivate {
  guppi_uniq_t id;
  GuppiAttributeBag *attr_bag;

  gboolean doing_attr_change;
  GList *changed_attr;

  double cached_w, cached_h;
};

#define priv(x) ((x)->priv)


static GObjectClass *parent_class = NULL;

enum {
  CHANGED,
  CHANGED_REQUEST,
  LAST_SIGNAL
};

static guint ges_signals[LAST_SIGNAL] = { 0 };

static void
guppi_element_state_finalize (GObject *obj)
{
  GuppiElementState *ges = GUPPI_ELEMENT_STATE (obj);
  GuppiElementStatePrivate *p = priv (ges);

  guppi_finalized (obj);

  guppi_unref0 (p->attr_bag);

  guppi_free0 (ges->priv);

  if (p->changed_attr) {
    g_list_free (p->changed_attr);
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_element_state_class_init (GuppiElementStateClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  parent_class = g_type_class_peek_parent (klass);

  object_class->finalize = guppi_element_state_finalize;

  ges_signals[CHANGED] =
    g_signal_new ("changed",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiElementStateClass, changed),
		  NULL, NULL,
		  guppi_marshal_VOID__VOID,
		  G_TYPE_NONE, 0);

  ges_signals[CHANGED_REQUEST] =
    g_signal_new ("changed_request",
		  G_TYPE_FROM_CLASS (klass),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GuppiElementStateClass, changed_request),
		  NULL, NULL,
		  guppi_marshal_VOID__DOUBLE_DOUBLE,
		  G_TYPE_NONE, 2, G_TYPE_DOUBLE, G_TYPE_DOUBLE);
}

static void push_changed_attr (GuppiElementState *, const gchar *str);

static void
bag_changed_cb (GuppiAttributeBag *bag, const gchar *name, gpointer closure)
{
  GuppiElementState *state = GUPPI_ELEMENT_STATE (closure);
  push_changed_attr (state, name);
  state->priv->doing_attr_change = TRUE;
  guppi_element_state_changed (state);
}

static void
guppi_element_state_init (GuppiElementState *obj)
{
  obj->priv = guppi_new0 (GuppiElementStatePrivate, 1);

  obj->priv->id       = guppi_unique_id ();

  obj->priv->attr_bag = guppi_attribute_bag_new ();
  guppi_attribute_bag_add_with_default (obj->priv->attr_bag, GUPPI_ATTR_STRING, "label", NULL, _("Unlabelled"));

  g_signal_connect (G_OBJECT (obj->priv->attr_bag),
		    "changed",
		    (GCallback) bag_changed_cb,
		    G_OBJECT (obj));

  obj->priv->cached_w = -1;
  obj->priv->cached_h = -1;
}

GType 
guppi_element_state_get_type (void)
{
  static GType guppi_element_state_type = 0;
  if (!guppi_element_state_type) {
    static const GTypeInfo guppi_element_state_info = {
      sizeof (GuppiElementStateClass),
      NULL, NULL,
      (GClassInitFunc) guppi_element_state_class_init,
      NULL, NULL,
      sizeof (GuppiElementState),
      0,
      (GInstanceInitFunc) guppi_element_state_init
    };
    guppi_element_state_type =
      g_type_register_static (G_TYPE_OBJECT,
			      "GuppiElementState",
			      &guppi_element_state_info,
			      0);
  }
  return guppi_element_state_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static GHashTable *state_hash = NULL;

void
guppi_element_state_register (const char *type_name,
			      GType       type)
{
  g_return_if_fail (type_name && *type_name);
  g_return_if_fail (type != 0);
  
  if (state_hash == NULL)
    state_hash = g_hash_table_new (g_str_hash, g_str_equal);

  g_hash_table_insert (state_hash,
		       g_strdup (type_name), 
		       GINT_TO_POINTER (type));
}

static GuppiElementState *
guppi_element_state_create (const char *type_name)
{
  gpointer ptr;
  GType type;
  GuppiElementState *state;

  g_return_val_if_fail (type_name && *type_name, NULL);

  if (state_hash == NULL)
    return NULL;

  ptr = g_hash_table_lookup (state_hash, type_name);
  if (ptr == NULL) {
    g_warning ("Unknown type '%s'", type_name);
    return NULL;
  }

  type = (GType) GPOINTER_TO_INT (ptr);
  state = GUPPI_ELEMENT_STATE (guppi_object_new (type));

  return state;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

GuppiElementState *
guppi_element_state_new (const gchar *type, ...)
{
  GuppiPlugIn *plug_in;
  GuppiPlotPlugIn *plot_plug_in;
  GuppiElementState *state;
  GuppiElementStateClass *klass;
  va_list args;

  g_return_val_if_fail (type && *type, NULL);

  state = guppi_element_state_create (type);

  if (state == NULL) {
    /* Find our plug-in. */
    plug_in = guppi_plug_in_lookup ("plot", type);
    if (plug_in == NULL) {
      g_warning ("Unknown plot plug-in: \"%s\"", type);
      return NULL;
    }
    plot_plug_in = (GuppiPlotPlugIn *) plug_in;
    
    /* Build our plot element. */
    state =  guppi_plot_plug_in_create_element (plot_plug_in);
    g_return_val_if_fail (state != NULL, NULL);

    /* Remember the plug in that created us, as necessary... */
    klass = GUPPI_ELEMENT_STATE_CLASS (G_OBJECT_GET_CLASS (state));
    if (klass->plug_in_code == NULL) {
      klass->plug_in_code = guppi_strdup (guppi_plug_in_code (plug_in));
      guppi_permanent_alloc (klass->plug_in_code);
    }
  }

  /* Apply our args */
  va_start (args, type);
  guppi_attribute_bag_vset (priv (state)->attr_bag, &args);
  va_end (args);

  return state;
}

/**************************************************************************/

guppi_uniq_t
guppi_element_state_unique_id (GuppiElementState *state)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), 0);
  return priv (state)->id;
}

GuppiAttributeBag *
guppi_element_state_attribute_bag (GuppiElementState *state)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);
  return priv (state)->attr_bag;
}

const gchar *
guppi_element_state_get_label (GuppiElementState *state)
{
  const gchar *label = NULL;
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), NULL);

  guppi_attribute_bag_get1 (state->priv->attr_bag, "label::raw", &label);
  return label;
}

void
guppi_element_state_set_label (GuppiElementState *state, const gchar *label)
{
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (state));
  guppi_attribute_bag_set (state->priv->attr_bag, "label", label, NULL);
}

gboolean
guppi_element_state_get (GuppiElementState *state, ...)
{
  va_list args;
  const gchar *key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), FALSE);

  va_start (args, state);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {

      if (!strcmp (key, "label")) {
	gchar **dest = va_arg (args, gchar **);
	*dest = g_strdup (guppi_element_state_get_label (state));
      } else {
	gpointer ptr = va_arg (args, gpointer);
	if (ptr != NULL)
	  guppi_attribute_bag_get1 (priv (state)->attr_bag, key, ptr);
      }
    }

  } while (key != NULL);

  return rv;
}

gboolean
guppi_element_state_set (GuppiElementState *state, ...)
{
  va_list args;
  const gchar *key;
  gboolean rv = TRUE;

  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (state), FALSE);

  va_start (args, state);
  do {
    key = va_arg (args, const gchar *);

    if (key != NULL) {

      if (!strcmp (key, "label")) {
	guppi_element_state_set_label (state, va_arg (args, const gchar *));
      } else {
	guppi_attribute_bag_vset1 (priv (state)->attr_bag, key, &args);
      }
    }
  } while (key != NULL);
  va_end (args);

  return rv;
}

/**************************************************************************/

GuppiElementView *
guppi_element_state_make_view (GuppiElementState *ges)
{
  GuppiElementStateClass *klass;
  GuppiElementView *view = NULL;

  g_return_val_if_fail (ges != NULL, NULL);
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (ges), NULL);

  klass = GUPPI_ELEMENT_STATE_CLASS (G_OBJECT_GET_CLASS (ges));

  if (klass->view_type && klass->make_view) {
    g_warning ("For %s, both a view type and a view constructor are defined.",
	       g_type_name (G_OBJECT_TYPE (ges)));
  }

  if (klass->view_type) {

    view = GUPPI_ELEMENT_VIEW (guppi_object_new (klass->view_type));

  } else if (klass->make_view) {

    view = klass->make_view (ges);

  }

  /* Tweak the insides of our view object. */
  if (view)
    guppi_element_view_set_state (view, ges);

  /* If our state has a size associated with it already, pass it onto the
     view so that the size calculation doesn't need to be repeated. */
  if (ges->priv->cached_w >= 0 || ges->priv->cached_h >= 0) {
    guppi_element_view_set_request (view, ges->priv->cached_w, ges->priv->cached_h);
  }

  return view;
}

/***************************************************************************/

static void
push_changed_attr (GuppiElementState *ges, const gchar *str)
{
  ges->priv->changed_attr = g_list_prepend (ges->priv->changed_attr, (gpointer) str);
}

static void
pop_changed_attr (GuppiElementState *ges)
{
  GList *node = ges->priv->changed_attr;
  ges->priv->changed_attr = g_list_remove_link (ges->priv->changed_attr, ges->priv->changed_attr);
  g_list_free_1 (node);
}

void
guppi_element_state_changed (GuppiElementState *ges)
{
  GuppiElementStatePrivate *p;

  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (ges));

  p = priv (ges);

  if (! ges->priv->doing_attr_change) {
    push_changed_attr (ges, NULL);
    ges->priv->doing_attr_change = FALSE;
  }
  g_signal_emit (ges, ges_signals[CHANGED], 0);
  pop_changed_attr (ges);
}

const gchar *
guppi_element_state_get_changed_attribute (GuppiElementState *ges)
{
  g_return_val_if_fail (GUPPI_IS_ELEMENT_STATE (ges), NULL);

  return ges->priv->changed_attr ? (const gchar *) ges->priv->changed_attr->data : NULL;
}

void
guppi_element_state_set_request (GuppiElementState *ges,
				 double width,
				 double height)
{
  g_return_if_fail (ges != NULL);
  g_return_if_fail (GUPPI_IS_ELEMENT_STATE (ges));

  if (width < 0)
    width = -1;
  if (height < 0)
    height = -1;

  /* Store the width and height so that they can be passed on to any views we might
     create later. */
  if (ges->priv->cached_w != width
      || ges->priv->cached_h != height) {
    ges->priv->cached_w = width;
    ges->priv->cached_h = height;

    g_signal_emit (ges, ges_signals[CHANGED_REQUEST], width, height, 0);
  }
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

xmlNodePtr
guppi_element_state_export_xml (GuppiElementState *state, GuppiXMLDocument *doc)
{
  GuppiElementStateClass *klass;
  xmlNodePtr state_node = NULL;
  xmlNodePtr attr_node = NULL;
  gchar *uid_str;

  g_return_val_if_fail (state && GUPPI_IS_ELEMENT_STATE (state), NULL);
  g_return_val_if_fail (doc != NULL, NULL);

  klass = GUPPI_ELEMENT_STATE_CLASS (G_OBJECT_GET_CLASS (state));

  state_node = xmlNewNode (doc->ns, "ElementState");

  if (klass->plug_in_code) {
    xmlNewProp (state_node, "Type", klass->plug_in_code);
  }
  
  uid_str = guppi_uniq2str (priv (state)->id);
  xmlNewProp (state_node, "UID", uid_str);
  guppi_free (uid_str);

  attr_node = guppi_attribute_bag_export_xml (priv (state)->attr_bag, doc);  
  if (attr_node) {
    if (attr_node->xmlChildrenNode != NULL) {
      xmlAddChild (state_node, attr_node);
    } else {
      xmlFreeNode (attr_node);
    }
  }

  if (klass->xml_export) {
    klass->xml_export (state, doc, state_node);
  }

  return state_node;
}

GuppiElementState *
guppi_element_state_import_xml (GuppiXMLDocument *doc, xmlNodePtr node)
{
  GuppiElementState *state = NULL;
  GuppiElementStateClass *klass;
  gchar *type_str = NULL, *uid_str = NULL;
  gboolean loaded_attr_bag = FALSE;

  g_return_val_if_fail (doc != NULL, NULL);
  g_return_val_if_fail (node != NULL, NULL);

  if (strcmp (node->name, "ElementState"))
    return NULL;

  type_str = xmlGetProp (node, "Type");
  state = guppi_element_state_new (type_str, NULL);
  if (state == NULL) {
    g_warning ("Unknown GuppiElementState type '%s'", type_str);
    goto finished;
  }

  klass = GUPPI_ELEMENT_STATE_CLASS (G_OBJECT_GET_CLASS (state));

  uid_str = xmlGetProp (node, "UID");
  priv (state)->id = guppi_str2uniq (uid_str);

  node = node->xmlChildrenNode;

  while (node != NULL) {

    if (!loaded_attr_bag && guppi_attribute_bag_import_xml (priv (state)->attr_bag, doc, node)) {

      loaded_attr_bag = TRUE;

    } else if (klass->xml_import) {

      klass->xml_import (state, doc, node);

    }

    node = node->next;
  }
  
 finished:
  if (type_str)
    xmlFree (type_str);
  if (uid_str)
    xmlFree (uid_str);

  return state;
}

void
guppi_element_state_spew_xml (GuppiElementState *state)
{
  GuppiXMLDocument *doc;
  g_return_if_fail (state && GUPPI_IS_ELEMENT_STATE (state));

  doc = guppi_xml_document_new ();
  guppi_xml_document_set_root (doc, guppi_element_state_export_xml (state, doc));
  guppi_xml_document_spew (doc);
  guppi_xml_document_free (doc);
}

/* $Id$ */
