/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * common-seq.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "common-seq.h"

static void
test_size (GuppiSeq *seq)
{
  gsize N;

  g_print ("Testing guppi_seq_size, guppi_seq_nonempty, guppi_seq_empty\n");

  N = guppi_seq_size (seq);

  if (N == 0 && guppi_seq_nonempty (seq))
    g_print (">!> Size zero, but non-empty!\n");
  if (N > 0 && guppi_seq_empty (seq))
    g_print (">!> Non-zero size, but empty!\n");
  if (guppi_seq_empty (seq) && guppi_seq_nonempty (seq))
    g_print (">!> Simultaneously empty and non-empty!\n");
}

static void
test_shift (GuppiSeq *seq)
{
  int i0, i1, j0, j1;
  int delta;
  guppi_seq_indices (seq, &i0, &i1);

  g_print ("Testing guppi_seq_shift_indices\n");
  for (delta = -5; delta <= 5; ++delta) {
    i0 += delta;
    i1 += delta;
    guppi_seq_shift_indices (seq, delta);
    guppi_seq_indices (seq, &j0, &j1);
    
    if (i0 != j0 || i1 != j1) {
      g_print (">!> Expected %d:%d after shifting by %d, got %d:%d\n",
	       i0, i1, delta, j0, j1);
    }
  }
}

void
test_seq (GuppiSeq *seq)
{
  g_return_if_fail (GUPPI_IS_SEQ (seq));

  test_size (seq);
  test_shift (seq);
}
