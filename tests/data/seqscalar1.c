/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * scalar1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <math.h>

#include "guppi-test-framework.h"
#include "check-missing.h"
#include "check-xml.h"

#include <guppi-useful.h>
#include <guppi-data-init.h>

#include <guppi-seq-scalar-core.h>

static gboolean 
eq (double x, double y)
{
  return fabs (x-y) < 1e-14;
}

static double
nth_value (gint i)
{
  return sin (128.3277272 * i - 0.12818 * i * i);
}

static GuppiSeqScalar *
build (gint N)
{
  GuppiSeqScalar *ss = GUPPI_SEQ_SCALAR (guppi_seq_scalar_core_new ());
  gint i;

  for (i = 0; i < N; ++i)
    guppi_seq_scalar_append (ss, nth_value (i));

  return ss;
}

static gboolean
plugins (gpointer foo)
{
  guppi_plug_in_path_set ("../../plug-ins/data_impl/seq_boolean");
  guppi_plug_in_path_append ("../../plug-ins/data_impl/seq_scalar");
  guppi_plug_in_spec_find_all ();
  return guppi_plug_in_count () > 0;
}

static gboolean
crdest (gpointer foo)
{
  GuppiSeqScalar *ss;

  ss = build (0);
  if (ss == NULL)
    return FALSE;

  guppi_unref (ss);

  return TRUE;
}

static gboolean
core (gpointer foo)
{
  GuppiSeqScalar *ss = build (100);
  gint i, i0, i1;
  
  guppi_test_subtest ("bounds");
  guppi_seq_bounds (GUPPI_SEQ (ss), &i0, &i1);
  if (i0 != 0 || i1 != 99)
    return FALSE;

  guppi_test_subtest ("get");
  for (i = i0; i <= i1; ++i) {
    double x = nth_value (i);
    if (! eq (guppi_seq_scalar_get (ss, i), x))
      return FALSE;
  }

  guppi_test_subtest ("set/get");
  for (i = i0; i <= i1; ++i) {
    double x = nth_value (i) * 77;
    double y;
    guppi_seq_scalar_set (ss, i, x);
    if (! eq (y = guppi_seq_scalar_get (ss, i), x)) {
      g_print ("%d: %g vs %g (%g)\n", i, y, x, y-x);
      return FALSE;
    }
  }

  guppi_unref (ss);

  return TRUE;
}

static gboolean
missing (gpointer foo)
{
  GuppiSeqScalar *ss = build (200);
  GuppiSeq *seq = GUPPI_SEQ (ss);

  guppi_test_subtest ("initial missing count");
  if (!check_missing (seq, 0))
    return FALSE;

  guppi_test_subtest ("set_missing");
  guppi_seq_set_missing (seq, 4);
  guppi_seq_set_missing (seq, 48);
  guppi_seq_set_missing (seq, 123);
  if (!check_missing (seq, 3, 4, 48, 123))
    return FALSE;

  guppi_test_subtest ("insert_missing");
  guppi_seq_insert_missing (seq, 100);
  if (!check_missing (seq, 4, 4, 48, 100, 124))
    return FALSE;

  guppi_test_subtest ("delete");
  guppi_seq_delete (seq, 77);
  guppi_seq_delete (seq, 99);
  if (!check_missing (seq, 3, 4, 48, 122))
    return FALSE;

  guppi_test_subtest ("insert");
  guppi_seq_scalar_insert (ss, 33, 17.0);
  if (!check_missing (seq, 3, 4, 49, 123))
    return FALSE;

  guppi_test_subtest ("set");
  guppi_seq_scalar_set (ss, 49, 12.391);
  if (!check_missing (seq, 2, 4, 123))
    return FALSE;

  guppi_unref (ss);

  return TRUE;
}

static gboolean
stats (gpointer foo)
{
  GuppiSeqScalar *ss;
  double z;

  guppi_test_subtest ("constructing");
  ss = GUPPI_SEQ_SCALAR (guppi_seq_scalar_core_new ());
  guppi_seq_scalar_append (ss, 2);
  guppi_seq_scalar_append (ss, 1);
  guppi_seq_scalar_append (ss, 4);
  guppi_seq_scalar_append (ss, 5);
  guppi_seq_scalar_append (ss, 3);

  guppi_test_subtest ("min");
  z = guppi_seq_scalar_min (ss);
  if (z != 1.0) {
    g_print ("min is %g, should be 1.0", z);
    return FALSE;
  }

  guppi_test_subtest ("max");
  z = guppi_seq_scalar_max (ss);
  if (z != 5.0) {
    g_print ("max is %g, should be 5.0", z);
    return FALSE;
  }

  guppi_test_subtest ("sum");
  z = guppi_seq_scalar_sum (ss);
  if (z != 15.0) {
    g_print ("sum is %g, should be 15.0", z);
    return FALSE;
  }
  
  guppi_unref (ss);

  return TRUE;
}

static gboolean
check_xml (gpointer unused)
{
  GuppiSeqScalar *ss = build (1000);

  if (! check_data_xml (GUPPI_DATA (ss)))
    return FALSE;

  guppi_unref (ss);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();

  guppi_test_do ("Locate and load relevant plug-ins",        plugins, NULL);
  guppi_test_do ("Create and destroy a GuppiSeqScalarCore",  crdest, NULL);
  guppi_test_do ("Test core functions",                      core, NULL);
  guppi_test_do ("Missing values handling",                  missing, NULL);
  guppi_test_do ("Stats (Simple Test)",                      stats, NULL);
  guppi_test_do ("Checking XML import & export",             check_xml, NULL);

  guppi_test_quit ();
  return 0;
}
