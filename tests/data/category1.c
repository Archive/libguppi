/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * category1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>

#include "guppi-test-framework.h"
#include <guppi-useful.h>
#include <guppi-data-init.h>

#include <guppi-category.h>

static gboolean
plugins (gpointer foo)
{
  guppi_plug_in_path_set ("../../plug-ins/data_impl/category");
  guppi_plug_in_spec_find_all ();
  return guppi_plug_in_count () > 0;
}

static gboolean
construct (gpointer foo)
{
  GuppiData *d;

  guppi_test_subtest ("Create");
  d = guppi_data_new ("GuppiCategoryCore");
  if (d == NULL)
    return FALSE;

  guppi_test_subtest ("Destroy");
  guppi_unref (d);
  
  return TRUE;
}

static gboolean
basic_ops (gpointer foo)
{
  const gchar *str1 = "Frog Face";
  const gchar *str2 = "What Can a Poor Boy Do?";
  const gchar *str3 = "Acres of Vapor";

  GuppiCategory *cat;
  code_t c1, c2, c3;

  cat = GUPPI_CATEGORY (guppi_data_new ("GuppiCategoryCore"));

  guppi_test_subtest ("Add by name");
  c1 = guppi_category_add_by_name (cat, str1);
  if (c1 == GUPPI_INVALID_CODE)
    return FALSE;

  c2 = guppi_category_add_by_name (cat, str2);
  if (c2 == GUPPI_INVALID_CODE)
    return FALSE;

  c3 = guppi_category_add_by_name (cat, str3);
  if (c3 == GUPPI_INVALID_CODE)
    return FALSE;

  if (guppi_category_add_by_name (cat, str2) != GUPPI_INVALID_CODE)
    return FALSE;

  guppi_test_subtest ("Size and bounds");
  if (guppi_category_size (cat) != 3)
    return FALSE;

  if (guppi_category_min_code (cat) != c1)
    return FALSE;

  if (guppi_category_max_code (cat) != c3)
    return FALSE;

  guppi_test_subtest ("Find by name");
  if (guppi_category_find_by_name (cat, str1) != c1)
    return FALSE;

  if (guppi_category_find_by_name (cat, str2) != c2)
    return FALSE;

  if (guppi_category_find_by_name (cat, str3) != c3)
    return FALSE;

  guppi_test_subtest ("Find by code");
  if (strcmp (guppi_category_find_by_code (cat, c1), str1))
    return FALSE;

  if (strcmp (guppi_category_find_by_code (cat, c2), str2))
    return FALSE;

  if (strcmp (guppi_category_find_by_code (cat, c3), str3))
    return FALSE;

  guppi_test_subtest ("String containment checks");
  if (! guppi_category_contains (cat, str1))
    return FALSE;

  if (! guppi_category_contains (cat, str2))
    return FALSE;

  if (! guppi_category_contains (cat, str3))
    return FALSE;

  if (guppi_category_contains (cat, "This is a bad category"))
    return FALSE;

  guppi_test_subtest ("Code containment checks");
  if (! guppi_category_contains_code (cat, c1))
    return FALSE;

  if (! guppi_category_contains_code (cat, c2))
    return FALSE;

  if (! guppi_category_contains_code (cat, c3))
    return FALSE;

  if (guppi_category_contains_code (cat, c3 + 666))
    return FALSE;

  guppi_test_subtest ("unref");
  guppi_unref (cat);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();

  guppi_test_do ("Locate and load category plug-ins",  plugins, NULL);
  guppi_test_do ("Create/destroy a GuppiCategoryCore", construct, NULL);
  guppi_test_do ("Basic category operations",          basic_ops, NULL);

  guppi_test_quit ();
  return 0;
}




