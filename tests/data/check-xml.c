/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * check-xml.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "check-xml.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <guppi-memory.h>
#include <guppi-test-framework.h>

gboolean
check_data_xml (GuppiData *data)
{
  GuppiXMLDocument *doc;
  GuppiData *data2;
  gchar tmp_filename_a[] = "/tmp/guppitest_1_XXXXXX";
  gchar tmp_filename_b[] = "/tmp/guppitest_2_XXXXXX";
  xmlNodePtr node;
  gboolean ok;
  FILE *test;

  g_return_val_if_fail (GUPPI_IS_DATA (data), FALSE);

  mkstemp (tmp_filename_a);
  mkstemp (tmp_filename_b);

  doc = guppi_xml_document_new ();
  node = guppi_data_export_xml (data, doc);
  guppi_xml_document_set_root (doc, node);
  guppi_xml_document_write_file (doc, tmp_filename_a);
  guppi_xml_document_free (doc);
  
  test = fopen (tmp_filename_a, "r");
  if (test == NULL)
    return FALSE;

  doc = guppi_xml_document_read_file (tmp_filename_a);
  data2 = guppi_data_import_xml (doc, guppi_xml_document_get_root (doc));
  guppi_xml_document_free (doc);

  doc = guppi_xml_document_new ();
  node = guppi_data_export_xml (data2, doc);
  guppi_xml_document_set_root (doc, node);
  guppi_xml_document_write_file (doc, tmp_filename_b);
  guppi_xml_document_free (doc);

  ok = guppi_test_check_file_equality (tmp_filename_a, tmp_filename_b);

  guppi_unref (data2);  
  if (ok) {
    unlink (tmp_filename_a);
    unlink (tmp_filename_b);
  }

  return ok;
}
