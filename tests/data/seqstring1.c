/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * string1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <math.h>

#include "guppi-test-framework.h"
/* #include "check-missing.h" */

#include <guppi-useful.h>
#include <guppi-data-init.h>

#include <guppi-data-string-array.h>
#include <guppi-seq-string.h>

static char *
nth_string (int i)
{
  return g_strdup_printf ("--- %d ---", i);
}

static GuppiSeqString *
build (gint N)
{
  GuppiSeqString *ss;
  gint i;

  guppi_data_string_array_get_type ();
  ss = GUPPI_SEQ_STRING (guppi_data_new (GUPPI_TYPE_DATA_STRING_ARRAY));

  for (i = 0; i < N; ++i) {
    gchar *str = nth_string (i);
    guppi_seq_string_append_nc (ss, str);
  }

  return ss;
}

static gboolean
plugins (gpointer foo)
{
  guppi_plug_in_path_set ("../../plug-ins/data_impl/seq_boolean");
  guppi_plug_in_path_append ("../../plug-ins/data_impl/seq_string");
  guppi_plug_in_spec_find_all ();
  return guppi_plug_in_count () > 0;
}

static gboolean
crdest (gpointer foo)
{
  GuppiSeqString *ss;

  ss = build (0);
  if (ss == NULL)
    return FALSE;

  guppi_unref (ss);

  return TRUE;
}

static gboolean
core (gpointer foo)
{
  GuppiSeqString *ss = build (100);
  gint i, i0, i1;
  
  guppi_test_subtest ("bounds");
  guppi_seq_bounds (GUPPI_SEQ (ss), &i0, &i1);
  if (i0 != 0 || i1 != 99)
    return FALSE;

  guppi_test_subtest ("get check");
  for (i = i0; i <= i1; ++i) {
    char *test = nth_string (i);
    const char *str = guppi_seq_string_get (ss, i);
    if (strcmp (test, str))
      return FALSE;
    g_free (test);
  }

  guppi_unref (ss);

  return TRUE;
}

static gboolean
copy (gpointer foo)
{
  GuppiSeqString *ss = build (100);
  GuppiData *cpy = guppi_data_copy (GUPPI_DATA (ss));
  int i, i0, i1, j0, j1;

  if (cpy == NULL)
    return FALSE;

  guppi_seq_indices (GUPPI_SEQ (ss), &i0, &i1);
  guppi_seq_indices (GUPPI_SEQ (cpy), &j0, &j1);

  if (i0 != j0)
    return FALSE;

  if (i1 != j1)
    return FALSE;

  for (i = i0; i <= i1; ++i) {
    const char *s1 = guppi_seq_string_get (ss, i);
    const char *s2 = guppi_seq_string_get (GUPPI_SEQ_STRING (cpy), i);

    if (!(s1 && s2 && !strcmp (s1, s2)))
      return FALSE;
  }

  return TRUE;
}

#if 0
static gboolean
missing (gpointer foo)
{
  GuppiSeqString *ss = build (200);
  GuppiSeq *seq = GUPPI_SEQ (ss);

  guppi_test_subtest ("initial missing count");
  if (!check_missing (seq, 0))
    return FALSE;

  guppi_test_subtest ("set_missing");
  guppi_seq_set_missing (seq, 4);
  guppi_seq_set_missing (seq, 48);
  guppi_seq_set_missing (seq, 123);
  if (!check_missing (seq, 3, 4, 48, 123))
    return FALSE;

  guppi_test_subtest ("insert_missing");
  guppi_seq_insert_missing (seq, 100);
  if (!check_missing (seq, 4, 4, 48, 100, 124))
    return FALSE;

  guppi_test_subtest ("delete");
  guppi_seq_delete (seq, 77);
  guppi_seq_delete (seq, 99);
  if (!check_missing (seq, 3, 4, 48, 122))
    return FALSE;

  guppi_test_subtest ("insert");
  guppi_seq_string_insert (ss, 33, "assbarn");
  if (!check_missing (seq, 3, 4, 49, 123))
    return FALSE;

  guppi_test_subtest ("set");
  guppi_seq_string_set (ss, 49, "frog");
  if (!check_missing (seq, 2, 4, 123))
    return FALSE;

  guppi_unref (ss);

  return TRUE;
}
#endif

int
main (int argc, char *argv[])
{
  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();

  //guppi_test_do ("Locate and load relevant plug-ins",        plugins, NULL);
  guppi_test_do ("Create and destroy a GuppiSeqStringCore",  crdest, NULL);
  guppi_test_do ("Test core functions",                      core, NULL);
  guppi_test_do ("Test copy function",                      copy, NULL);
  /* guppi_test_do ("Missing values handling",                  missing, NULL);*/

  guppi_test_quit ();
  return 0;
}
