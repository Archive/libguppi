/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * string-array.c
 *
 * Copyright (C) 2002 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include "common-seq.h"

#include <guppi-memory.h>
#include <guppi-data-string-array.h>
#include <guppi-seq-string.h>

static void
show (GuppiSeqString *seq)
{
  int i, i0, i1;
  guppi_seq_bounds (GUPPI_SEQ (seq), &i0, &i1);

  if (i0 > i1)
    g_print (">!> (empty)\n");

  for (i = i0; i <= i1; ++i)
    g_print (">!> %d: %s\n", i, guppi_seq_string_get (seq, i));
}

static void
do_tests (GuppiData *data)
{
  g_print ("\n>!> Test Case:\n");
  show (GUPPI_SEQ_STRING (data));
  test_seq (GUPPI_SEQ (data));
}

int
main (int argc, char *argv[])
{
  GuppiDataStringArray *array;
  GuppiDataStringArray *copy;

  g_type_init ();
  guppi_useful_init ();
  guppi_data_init ();

  g_print (">!> Testing GuppiDataStringArray\n");

  array = (GuppiDataStringArray *) guppi_data_new (GUPPI_TYPE_DATA_STRING_ARRAY);
  if (array == NULL) {
    g_print (">!> Couldn't create array\n");
    exit (0);
  }
  do_tests (GUPPI_DATA (array));
  
  guppi_seq_string_append (GUPPI_SEQ_STRING (array), "one");
  do_tests (GUPPI_DATA (array));

  guppi_seq_string_append (GUPPI_SEQ_STRING (array), "two");
  do_tests (GUPPI_DATA (array));

  guppi_seq_string_prepend (GUPPI_SEQ_STRING (array), "zero");
  do_tests (GUPPI_DATA (array));

  guppi_seq_string_insert (GUPPI_SEQ_STRING (array), 2, "one and a half");
  do_tests (GUPPI_DATA (array));

  guppi_seq_delete (GUPPI_SEQ (array), 2);
  do_tests (GUPPI_DATA (array));

  guppi_seq_delete_range (GUPPI_SEQ (array), 0, 1);
  do_tests (GUPPI_DATA (array));

  guppi_seq_grow_to_include (GUPPI_SEQ (array), 5);
  guppi_seq_string_set (GUPPI_SEQ_STRING (array), 1, "1");
  guppi_seq_string_set (GUPPI_SEQ_STRING (array), 2, "2");
  guppi_seq_string_set (GUPPI_SEQ_STRING (array), 3, "3");
  do_tests (GUPPI_DATA (array));

  guppi_seq_grow_to_include (GUPPI_SEQ (array), -5);
  do_tests (GUPPI_DATA (array));

  copy = (GuppiDataStringArray *) guppi_data_copy (GUPPI_DATA (array));
  guppi_unref (array);
  do_tests (copy);

  array = (GuppiDataStringArray *) guppi_data_new (GUPPI_TYPE_DATA_STRING_ARRAY);

  guppi_seq_string_insert (GUPPI_SEQ_STRING (array), 10, "ten!");
  do_tests (GUPPI_DATA (array));

  guppi_seq_string_prepend (GUPPI_SEQ_STRING (array), "pre");
  do_tests (GUPPI_DATA (array));

  guppi_seq_string_append (GUPPI_SEQ_STRING (array), "post");
  do_tests (GUPPI_DATA (array));
}
