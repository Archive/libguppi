#!/bin/bash

if [ -n "$COLORTERM" -o $TERM = "xterm" -o $TERM = "rxvt" -o $TERM = "xterm-color" ]; then
	SETCOLOR_SUCCESS="echo -en \\033[1;32m"
	SETCOLOR_FAILURE="echo -en \\033[1;31m"
	SETCOLOR_WARNING="echo -en \\033[1;33m"
	SETCOLOR_NORMAL="echo -en \\033[0;39m"
else
	SETCOLOR_SUCCESS=
	SETCOLOR_FAILURE=
	SETCOLOR_WARNING=
	SETCOLOR_NORMAL=
fi

function run_test () {
	
	base=$1

	rm -f core

	warning_message=""

	if [[ $warning_message == "" && ! -e $base ]]; then
	    warning_message="no such file"
	fi

	if [[ $warning_message == "" && ! -x $base ]]; then
	    warning_message="not an executable"
	fi

	if [[ $warning_message == "" && ! -r $base.solution ]]; then
	    warning_message="missing solution"
	fi

	if [[ $warning_message != "" ]]; then
	    echo -n "$base: "
	    $SETCOLOR_WARNING
	    echo -n WARNING
	    $SETCOLOR_NORMAL
	    echo , $warning_message
	    warning=$(( $warning + 1 ))
	    continue
	fi

	./$base > $base.mistake
	fgrep '>!>' $base.solution > $base.baseline 2>&1
	fgrep '>!>' $base.mistake > $base.compare 2>&1

	diff -U 500 $base.baseline $base.compare >$base.diff 2>&1

	if [ $? -eq 0 ] ; then
		echo -n "$base: "
		$SETCOLOR_SUCCESS
		echo PASSED
		$SETCOLOR_NORMAL
		rm -f $base.mistake $base.diff
		success=$(( $success + 1 ))
	else
		echo -n "$base: "
		$SETCOLOR_FAILURE
		if [ -e core ] ; then
			echo CRASH
			$SETCOLOR_NORMAL
			crash=$(( $crash + 1 ))
		else
			echo -n FAILED
			$SETCOLOR_NORMAL
			echo , check $base.mistake and $base.diff
			failure=$(( $failure + 1 ))
		fi

	fi

	rm -f $base.compare $base.baseline
}

for i in $@; do

    # If we are given a directory, execute anything inside
    # that directory that looks like a valid test.
    if [ -d $i ] ; then

	for j in `ls $i` ; do
	    file="$i/$j"
	    if [[ -x $file && -f $file && -r $file ]] ; then
		run_test $file
	    fi
	done
    
    else

	run_test $i

    fi
done

echo -n "Summary: "

if [ $success ] ; then
	
	$SETCOLOR_SUCCESS
	if [ $success -eq "1" ] ; then
		echo -n 1 Success
	else
		echo -n $success Successes
	fi
	$SETCOLOR_NORMAL
else
	echo -n No Successes
fi

echo -n ", "

if [ $warning ] ; then

	$SETCOLOR_WARNING
	if [ $warning -eq "1" ] ; then
		echo -n "1 Warning"
	else
		echo -n "$warning Warnings"
	fi
	$SETCOLOR_NORMAL
else
    echo -n No Warnings
fi

echo -n ", "

if [ $failure ] ; then

	$SETCOLOR_FAILURE
	if [ $failure -eq "1" ] ; then
		echo -n 1 Failure
	else
		echo -n $failure Failures
	fi
	$SETCOLOR_NORMAL
else
	echo -n No Failures
fi

echo -n ", "

if [ $crash ] ; then

	$SETCOLOR_FAILURE
	if [ $crash -eq "1" ] ; then
		echo -n 1 Crash
	else
		echo -n $crash Crashes
        fi
	$SETCOLOR_NORMAL
else
    echo -n No Crashes
fi

$SETCOLOR_NORMAL

echo .
