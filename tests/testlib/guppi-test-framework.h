/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-test-framework.h
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#ifndef __GUPPI_TEST_FRAMEWORK_H__
#define __GUPPI_TEST_FRAMEWORK_H__

#include <glib.h>

void guppi_test_init    (gint *argc, gchar **argv[]);
void guppi_test_quit    (void);

void guppi_test_begin   (const gchar *description);
void guppi_test_end     (gboolean successful);

void guppi_test_subtest (const gchar *description);

void guppi_test_do      (const gchar *description, gboolean (*test_fn) (gpointer), gpointer);


/* Utilities */
gboolean guppi_test_check_file_equality (const gchar *a, const gchar *b);

#endif /* __GUPPI_TEST_FRAMEWORK_H__ */

