/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * palette1.c
 *
 * Copyright (C) 2001 The Free Software Foundation, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA.
 */

#include <config.h>
#include <guppi-test-framework.h>
#include <guppi-useful.h>
#include <guppi-data-init.h>
#include <guppi-plot-init.h>

#include <guppi-color-palette.h>

static gboolean
xml_test (gpointer closure)
{
  GuppiColorPalette *pal = GUPPI_COLOR_PALETTE (closure);
  GuppiColorPalette *imported_pal;
  GuppiXMLDocument *doc;
  xmlNodePtr node;
  gboolean equal;
  
  doc = guppi_xml_document_new ();
  node = guppi_color_palette_export_xml (pal, doc);
  guppi_xml_document_set_root (doc, node);
  guppi_xml_document_write_file (doc, "/tmp/pal-xml.1");
  guppi_xml_document_free (doc);

  system ("cat /tmp/pal-xml.1");

  doc= guppi_xml_document_read_file ("/tmp/pal-xml.1");
  imported_pal = guppi_color_palette_import_xml (doc, guppi_xml_document_get_root (doc));
  guppi_xml_document_free (doc);

  doc = guppi_xml_document_new ();
  node = guppi_color_palette_export_xml (imported_pal, doc);
  guppi_xml_document_set_root (doc, node);
  guppi_xml_document_write_file (doc, "/tmp/pal-xml.2");
  guppi_xml_document_free (doc);
  
  equal = guppi_test_check_file_equality ("/tmp/pal-xml.1", "/tmp/pal-xml.2");

  if (! equal) {
    system ("diff /tmp/pal-xml.1 /tmp/pal-xml.2");
  }

  unlink ("/tmp/pal-xml.1");
  unlink ("/tmp/pal-xml.2");
  
  return equal;
}

static void
test_palette (GuppiColorPalette *pal)
{
  gchar *desc;

  desc = guppi_strdup_printf ("%s: XML import/export", pal->meta);
  guppi_test_do (desc, xml_test, pal);
  guppi_free (desc);
}

int
main (int argc, char *argv[])
{
  GuppiColorPalette *pal;

  guppi_test_init (&argc, &argv);

  guppi_useful_init_without_guile ();
  guppi_data_init ();
  guppi_plot_init ();

  pal = guppi_color_palette_new ();
  test_palette (pal);

  guppi_color_palette_set_alien_stock (pal);
  test_palette (pal);

  guppi_color_palette_set_transition (pal, RGBA_RED, RGBA_YELLOW);
  test_palette (pal);

  guppi_color_palette_set_fade (pal, RGBA_BLUE);
  test_palette (pal);

  guppi_color_palette_set_fire (pal);
  test_palette (pal);

  guppi_color_palette_set_ice (pal);
  test_palette (pal);

  guppi_color_palette_set_thermal (pal);
  test_palette (pal);

  guppi_color_palette_set_spectrum (pal);
  test_palette (pal);

  guppi_color_palette_set_vcustom (pal, 5, RGBA_RED, RGBA_WHITE, RGBA_BLUE, RGBA_BLACK, RGBA_YELLOW);
  guppi_color_palette_set_flipped (pal, TRUE);
  guppi_color_palette_set_alpha (pal, 123);
  guppi_color_palette_set_intensity (pal, 99);
  test_palette (pal);

  guppi_color_palette_set_monochrome (pal, RGBA_GREEN);
  test_palette (pal);
    
  guppi_test_quit ();
  return 0;
}
