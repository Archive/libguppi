##
## Take a list of headers and assemble a .h
## file that includes each of them.

import sys;
import os.path;

def canned_warning():
    print
    print "/* This is an automatically-generated file. */"
    print "/* DO NOT EDIT! */"
    print

canned_warning()

common_prefix = ""
omit = []
omit_pending = 0

for a in sys.argv[1:]:

    if omit_pending:
        omit.append(a)
        omit_pending = 0
    elif common_prefix == "?":
        common_prefix = a
    elif a == "--prefix":
        common_prefix = "?"
    elif a == "--omit":
        omit_pending = 1
    else:
        if not a in omit:
            header_filename = os.path.join(common_prefix, a)
            print "#include <" + header_filename + ">"

canned_warning()
