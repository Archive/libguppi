"""
top.py

Copyright (C) 2000 EMC Capital Management, Inc.

Developed by Andrew Chatham <andrew.chatham@duke.edu>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
USA

---------------------------------------------------------------------

A module/class for doing topological sorts. I use this to find the
dependency relationships between classes so I can print the code for
them out in the right order.

Andrew Chatham
"""

import exceptions

class GraphError(exceptions.RuntimeError): pass

class TopSorter:

    '''A class to do a topological sort, used to find depenencies.
    Adadpted from pseudocode in "Introduction to Algorithms" by
    Cormen, Leiserson, and Rivest, 21st printing, page 486'''
    
    def __init__(self,G):
        '''G is a dictionary of dependencies:
        eg: G = {'read_book' : ['learn_to_read', 'open_book'],
                 'learn_to_read' : [],
                 'open_book' : []}'''
        self.color = {}
        self.dist = {}

        # make sure all dependencies are covered; assume no dependency if not
        # explicitly listed
        for dep in G.values():
            for cls in dep:
                if not G.has_key(cls):
                    G[cls] = ()

        for cls in G.keys():
            self.color[cls] = 0
            self.dist[cls] = 0
        self.G = G
        self.t = 0
    
    def __call__(self): return self.do_top()
    
    def dfs(self):
        '''Do a depth-first search, storing only the finish times.'''
        for v in self.G.keys():
            if self.color[v] == 0:
                self.dfs_visit(v)

    def dfs_visit(self, v):
        '''DFS helper function'''
        self.color[v] = 1
        self.t = self.t + 1
        for adj in self.G[v]:
            if self.color[adj] == 0:
                self.dfs_visit(adj)
            elif self.color[adj] == 1:
                raise GraphError, "Cycle detected. Shame on you"
            
        self.color[v] = 2
        self.t = self.t + 1
        self.dist[v] = self.t
            
    def do_top(self):
        '''Perform the topological sort.
        Returns a list of the items in topological order'''
        
        self.dfs()
        #m = list of (distance, name) pairs
        m = map(None, self.dist.values(), self.dist.keys())
        #sort by distance
        m.sort()
        return map(lambda x: x[1], m)
        

