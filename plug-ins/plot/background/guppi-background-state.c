/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-background-state.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-background-state.h"

#include <guppi-memory.h>
#include <guppi-rgb.h>
#include "guppi-background-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_background_state_finalize (GtkObject *obj)
{
  GuppiBackgroundState *x = GUPPI_BACKGROUND_STATE(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_background_state_class_init (GuppiBackgroundStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_background_state_finalize;

  state_class->name = _("Background");
  state_class->view_type = GUPPI_TYPE_BACKGROUND_VIEW;

}

static void
guppi_background_state_init (GuppiBackgroundState *obj)
{
  GuppiAttributeBag *bag;

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA, "color",          NULL, RGBA_WHITE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA, "color_final",    NULL, RGBA_BLUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT,  "gradient_start", NULL, GUPPI_SOUTH);
}

GtkType
guppi_background_state_get_type (void)
{
  static GtkType guppi_background_state_type = 0;
  if (!guppi_background_state_type) {
    static const GtkTypeInfo guppi_background_state_info = {
      "GuppiBackgroundState",
      sizeof (GuppiBackgroundState),
      sizeof (GuppiBackgroundStateClass),
      (GtkClassInitFunc)guppi_background_state_class_init,
      (GtkObjectInitFunc)guppi_background_state_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_background_state_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_background_state_info);
  }
  return guppi_background_state_type;
}

GuppiElementState *
guppi_background_state_new (void)
{
  return (GuppiElementState *) guppi_type_new (guppi_background_state_get_type ());
}

