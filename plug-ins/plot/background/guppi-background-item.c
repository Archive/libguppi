/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-background-item.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-background-item.h"

#include <guppi-memory.h>
#include <guppi-rgb.h>
#include "guppi-background-state.h"
#include "guppi-background-view.h"

static GtkObjectClass *parent_class = NULL;

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *clip_path, gint flags)
{

}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiElementState *state = guppi_canvas_item_state (gci);
  gint cx0, cy0, cx1, cy1, i, j;
  guint32 color, color_final, this_color;
  gint gradient_start;
  gint r, g, b, a;
  double t;

  guppi_element_state_get (state,
			   "color", &color,
			   "color_final", &color_final,
			   "gradient_start", &gradient_start,
			   NULL);

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);


  if (gradient_start == GUPPI_WEST || gradient_start == GUPPI_EAST) {
    
    for (i = buf->rect.x0; i < buf->rect.x1; ++i) {
      guchar *p = BUF_PTR (buf, i, buf->rect.y0);
      t = (i - cx0) / (double) (cx1 - cx0 - 1);
      if (gradient_start == GUPPI_EAST)
	t = 1 - t;
      this_color = UINT_INTERPOLATE (color, color_final, t);
      UINT_TO_RGBA (this_color, &r, &g, &b, &a);
      for (j = buf->rect.y0; j < buf->rect.y1; ++j) {
	PIXEL_RGBA (p, r, g, b, a);
	p += buf->buf_rowstride;
      }
    }

  } else if (gradient_start == GUPPI_NORTH || gradient_start == GUPPI_SOUTH) {

    for (j = buf->rect.y0; j < buf->rect.y1; ++j) {
      guchar *p = BUF_PTR (buf, buf->rect.x0, j);
      t = (j - cy0) / (double) (cy1 - cy0 - 1);
      if (gradient_start == GUPPI_SOUTH)
	t = 1 - t;
      this_color = UINT_INTERPOLATE (color, color_final, t);
      UINT_TO_RGBA (this_color, &r, &g, &b, &a);
      for (i = buf->rect.x0; i < buf->rect.x1; ++i) {
	PIXEL_RGBA (p, r, g, b, a);
	p += 3;
      }
    }
    
  } else {
    
    for (j = buf->rect.y0; j < buf->rect.y1; ++j) {
      guchar *p = BUF_PTR (buf, buf->rect.x0, j);
      UINT_TO_RGBA (color, &r, &g, &b, &a);
      for (i = buf->rect.x0; i < buf->rect.x1; ++i) {
	PIXEL_RGBA (p, r, g, b, a);
	p += 3;
      }
    }

  }
}

static void
guppi_background_item_finalize (GtkObject *obj)
{
  GuppiBackgroundItem *x = GUPPI_BACKGROUND_ITEM(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_background_item_class_init (GuppiBackgroundItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiCanvasItemClass *item_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_background_item_finalize;

  item_class->guppi_update = update;
  item_class->guppi_render = render;
}

static void
guppi_background_item_init (GuppiBackgroundItem *obj)
{

}

GtkType
guppi_background_item_get_type (void)
{
  static GtkType guppi_background_item_type = 0;
  if (!guppi_background_item_type) {
    static const GtkTypeInfo guppi_background_item_info = {
      "GuppiBackgroundItem",
      sizeof (GuppiBackgroundItem),
      sizeof (GuppiBackgroundItemClass),
      (GtkClassInitFunc)guppi_background_item_class_init,
      (GtkObjectInitFunc)guppi_background_item_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_background_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_background_item_info);
  }
  return guppi_background_item_type;
}

GtkObject *
guppi_background_item_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_background_item_get_type ()));
}
