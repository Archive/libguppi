/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-frame-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-frame-view.h"

#include <guppi-memory.h>
#include "guppi-frame-item.h"
#include "guppi-frame-print.h"
#include "guppi-frame-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_frame_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
guppi_frame_view_class_init (GuppiFrameViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_frame_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_FRAME_ITEM;
  view_class->print_type = GUPPI_TYPE_FRAME_PRINT;
}

static void
guppi_frame_view_init (GuppiFrameView * obj)
{

}

GtkType guppi_frame_view_get_type (void)
{
  static GtkType guppi_frame_view_type = 0;
  if (!guppi_frame_view_type) {
    static const GtkTypeInfo guppi_frame_view_info = {
      "GuppiFrameView",
      sizeof (GuppiFrameView),
      sizeof (GuppiFrameViewClass),
      (GtkClassInitFunc) guppi_frame_view_class_init,
      (GtkObjectInitFunc) guppi_frame_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_frame_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_frame_view_info);
  }
  return guppi_frame_view_type;
}

GtkObject *
guppi_frame_view_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_frame_view_get_type ()));
}



/* $Id$ */
