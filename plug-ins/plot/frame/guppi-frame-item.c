/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-frame-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-frame-item.h"

#include <math.h>
#include <guppi-useful.h>
#include <guppi-axis-markers.h>
#include "guppi-frame-view.h"
#include "guppi-frame-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_frame_item_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *svp, gint flags)
{

}


static void
soft_box (GnomeCanvasBuf *buf,
	  double x, double y,
	  double n, double s, double w, double e, guint32 color)
{
  guppi_paint_soft_box (buf, x - w, y - n, x + e, y + s, color);
}



static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiFrameState *state = GUPPI_FRAME_STATE (guppi_canvas_item_state (gci));
  GuppiElementView *view = guppi_canvas_item_view (gci);
  double scale = guppi_canvas_item_scale (gci);
  gboolean show_major_ticks, show_minor_ticks, show_micro_ticks;
  gboolean show_major_rules, show_minor_rules, show_micro_rules;
  double major_tick_thickness, minor_tick_thickness, micro_tick_thickness;
  double major_rule_thickness, minor_rule_thickness, micro_rule_thickness;
  double major_tick_length, minor_tick_length, micro_tick_length;
  guint32 major_tick_color, minor_tick_color, micro_tick_color;
  guint32 major_rule_color, minor_rule_color, micro_rule_color;
  gint frame_flags;
  guint32 frame_color;
  double frame_thickness;
  gint cx0, cy0, cx1, cy1;
  gint j;

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);

  guppi_element_state_get ((GuppiElementState *) state,
			   "show_major_ticks", &show_major_ticks,
			   "show_minor_ticks", &show_minor_ticks,
			   "show_micro_ticks", &show_micro_ticks,
			   "show_major_rules", &show_major_rules,
			   "show_minor_rules", &show_minor_rules,
			   "show_micro_rules", &show_micro_rules,
			   "major_tick_thickness", &major_tick_thickness,
			   "minor_tick_thickness", &minor_tick_thickness,
			   "micro_tick_thickness", &micro_tick_thickness,
			   "major_rule_thickness", &major_rule_thickness,
			   "minor_rule_thickness", &minor_rule_thickness,
			   "micro_rule_thickness", &micro_rule_thickness,
			   "major_tick_length", &major_tick_length,
			   "minor_tick_length", &minor_tick_length,
			   "micro_tick_length", &micro_tick_length,
			   "major_tick_color", &major_tick_color,
			   "minor_tick_color", &minor_tick_color,
			   "micro_tick_color", &micro_tick_color,
			   "major_rule_color", &major_rule_color,
			   "minor_rule_color", &minor_rule_color,
			   "micro_rule_color", &micro_rule_color,
			   "frame_flags", &frame_flags,
			   "frame_color", &frame_color,
			   "frame_thickness", &frame_thickness,
			   NULL);

  /* Render the ticks */

  for (j = 0; j < 2; ++j) {
    GuppiAxisMarkers *marks = NULL;

    marks = guppi_element_view_axis_markers (view, j == 0 ? GUPPI_X_AXIS : GUPPI_Y_AXIS);

    if (marks) {
      gint N = guppi_axis_markers_size (marks);
      gint i;

      gboolean show = FALSE, rule = FALSE;
      double pos, span_n = 0, span_s = 0, span_w = 0, span_e = 0;
      guint32 color = 0;
      double xx, yy;

      const GuppiTick *tick;
      for (i = 0; i < N; ++i) {
	tick = guppi_axis_markers_get (marks, i);

	pos = tick->position;
	show = FALSE;

	switch (tick->type) {

	case GUPPI_TICK_NONE:
	  break;

	case GUPPI_TICK_MAJOR:
	  if (show_major_ticks) {
	    show = TRUE;
	    rule = FALSE;
	    color = major_tick_color;
	    span_w = span_e = major_tick_thickness / 2;
	    span_s = 0;
	    span_n = major_tick_length;
	  }
	  break;

	case GUPPI_TICK_MINOR:
	  if (show_minor_ticks) {
	    show = TRUE;
	    rule = FALSE;
	    color = minor_tick_color;
	    span_w = span_e = minor_tick_thickness / 2;
	    span_s = 0;
	    span_n = minor_tick_length;
	  }
	  break;

	case GUPPI_TICK_MICRO:
	  if (show_micro_ticks) {
	    show = TRUE;
	    rule = FALSE;
	    color = micro_tick_color;
	    span_w = span_e = micro_tick_thickness / 2;
	    span_s = 0;
	    span_n = micro_tick_length;
	  }
	  break;

	case GUPPI_TICK_MAJOR_RULE:
	  if (show_major_rules) {
	    show = TRUE;
	    rule = TRUE;
	    color = major_rule_color;
	    span_w = span_e = major_rule_thickness / 2;
	    span_n = span_s = 0;
	  }
	  break;

	case GUPPI_TICK_MINOR_RULE:
	  if (show_minor_rules) {
	    show = TRUE;
	    rule = TRUE;
	    color = minor_rule_color;
	    span_w = span_e = minor_rule_thickness / 2;
	    span_n = span_s = 0;
	  }
	  break;

	case GUPPI_TICK_MICRO_RULE:
	  if (show_micro_rules) {
	    show = TRUE;
	    rule = TRUE;
	    color = micro_rule_color;
	    span_w = span_e = micro_rule_thickness / 2;
	    span_n = span_s = 0;
	  }
	  break;

	default:
	  g_assert_not_reached ();
	}

	if (show && color != 0) {

	  if (j == 0) {
	    guppi_canvas_item_vp2c_d (gci, pos, 0, &xx, NULL);
	    yy = cy1 - 1;

	  } else if (j == 1) {
	    double t;

	    xx = cx0;
	    guppi_canvas_item_vp2c_d (gci, 0, pos, NULL, &yy);

	    /* Transpose coordinates */
	    t = span_n;
	    span_n = span_e;
	    span_e = t;
	    t = span_s;
	    span_s = span_w;
	    span_w = t;
	  }

	  span_n = guppi_y_pt2px (span_n * scale);
	  span_s = guppi_y_pt2px (span_s * scale);
	  span_w = guppi_x_pt2px (span_w * scale);
	  span_e = guppi_x_pt2px (span_e * scale);

	  if (rule) {
	    if (j == 0)
	      span_n = cy1 - cy0;
	    else
	      span_e = cx1 - cx0;
	  }

	  soft_box (buf, xx, yy, span_n, span_s, span_w, span_e, color);

	}

      }
    }
  }


  /* Render the frame. */

  if (frame_flags && (frame_color & 0xff) && frame_thickness > 0) {
    gint fr_int_x, fr_int_y;
    double fr_scaled, fr_scaled_x, fr_scaled_y, fr_frac_x, fr_frac_y;
    guint r, g, b, a;
    gint k0, k1;

    fr_scaled = frame_thickness * scale;
    fr_scaled_x = guppi_x_pt2px (fr_scaled);
    fr_scaled_y = guppi_y_pt2px (fr_scaled);

    fr_int_x = (gint) floor (fr_scaled_x);
    fr_int_y = (gint) floor (fr_scaled_y);
    fr_frac_x = fr_scaled_x - fr_int_x;
    fr_frac_y = fr_scaled_y - fr_int_y;
    if (fr_frac_x < 1e-4)
      fr_frac_x = 0;
    if (fr_frac_y < 1e-4)
      fr_frac_y = 0;

    UINT_TO_RGBA (frame_color, &r, &g, &b, &a);

    if (frame_flags & GUPPI_NORTH) {

      k0 = frame_flags & GUPPI_WEST ? fr_int_x : 0;
      k1 = frame_flags & GUPPI_EAST ? fr_int_x : 0;

      PAINT_BOX (buf, r, g, b, a, cx0 + k0, cy0, cx1 - k1, cy0 + fr_int_y);
      if (fr_frac_y > 0)
	PAINT_HORIZ (buf, r, g, b, (gint) rint (a * fr_frac_y),
		     cx0 + k0, cx1 - k1, cy0 + fr_int_y);
    }

    if (frame_flags & GUPPI_SOUTH) {

      k0 = frame_flags & GUPPI_WEST ? fr_int_x : 0;
      k1 = frame_flags & GUPPI_EAST ? fr_int_x : 0;

      PAINT_BOX (buf, r, g, b, a, cx0 + k0, cy1 - fr_int_y, cx1 - k1, cy1);
      if (fr_frac_y > 0)
	PAINT_HORIZ (buf, r, g, b, (gint) rint (a * fr_frac_y),
		     cx0 + k0, cx1 - k1, cy1 - fr_int_y - 1);
    }

    if (frame_flags & GUPPI_WEST) {
      PAINT_BOX (buf, r, g, b, a, cx0, cy0, cx0 + fr_int_x, cy1);
      if (fr_frac_x > 0) {
	k0 = frame_flags & GUPPI_NORTH ? fr_int_y : 0;
	k1 = frame_flags & GUPPI_SOUTH ? fr_int_y : 0;
	PAINT_VERT (buf, r, g, b, (gint) rint (a * fr_frac_x),
		    cx0 + fr_int_x, cy0 + k0, cy1 - k1);
      }
    }

    if (frame_flags & GUPPI_EAST) {
      PAINT_BOX (buf, r, g, b, a, cx1 - fr_int_x, cy0, cx1, cy1);
      if (fr_frac_x > 0) {
	k0 = frame_flags & GUPPI_NORTH ? fr_int_y : 0;
	k1 = frame_flags & GUPPI_SOUTH ? fr_int_y : 0;
	PAINT_VERT (buf, r, g, b, (gint) rint (a * fr_frac_x),
		    cx1 - fr_int_x - 1, cy0 + k0, cy1 - k1);
      }
    }
  }
}

/***************************************************************************/

static void
guppi_frame_item_class_init (GuppiFrameItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_frame_item_finalize;

  gci_class->guppi_update = update;
  gci_class->guppi_render = render;
}

static void
guppi_frame_item_init (GuppiFrameItem *obj)
{

}

GtkType guppi_frame_item_get_type (void)
{
  static GtkType guppi_frame_item_type = 0;
  if (!guppi_frame_item_type) {
    static const GtkTypeInfo guppi_frame_item_info = {
      "GuppiFrameItem",
      sizeof (GuppiFrameItem),
      sizeof (GuppiFrameItemClass),
      (GtkClassInitFunc) guppi_frame_item_class_init,
      (GtkObjectInitFunc) guppi_frame_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_frame_item_type =
      gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_frame_item_info);
  }
  return guppi_frame_item_type;
}

GtkObject *
guppi_frame_item_new (void)
{
  return GTK_OBJECT (guppi_type_new (guppi_frame_item_get_type ()));
}



/* $Id$ */
