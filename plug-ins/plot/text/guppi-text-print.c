/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-text-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-convenient.h>
#include <guppi-useful.h>
#include "guppi-text-print.h"
#include "guppi-text-state.h"
#include "guppi-text-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_text_print_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

typedef struct _print_info print_info;
struct _print_info {
  GuppiElementPrint *ep;
  GnomeFont *last_font;
  gboolean first;
  double fudge;
};

static void
word_cb (const gchar *str,
	 GnomeFont *font,
	 double off_x, double off_y,
	 gpointer user_data)
{
  print_info *info = (print_info *) user_data;

  if (font != info->last_font) {
    guppi_element_print_setfont (info->ep, font);
    info->last_font = font;
  }

  if (info->first) {
    info->fudge = gnome_font_get_ascender (font);
    info->first = FALSE;
  }

  guppi_element_print_moveto (info->ep, off_x, -off_y - info->fudge);
  guppi_element_print_show (info->ep, str);
}

#if 0
static void
draw_bbox (GuppiElementPrint *ep, ArtDRect *bbox, guint32 rgb)
{
  guppi_element_print_gsave (ep);
  
  guppi_element_print_setrgbcolor_uint (ep, rgb);

  guppi_element_print_setlinewidth (ep, 72 / 32.0);
  
  guppi_element_print_newpath (ep);
  guppi_element_print_moveto (ep, bbox->x0, -bbox->y0);
  guppi_element_print_lineto (ep, bbox->x1, -bbox->y0);
  guppi_element_print_lineto (ep, bbox->x1, -bbox->y1);
  guppi_element_print_lineto (ep, bbox->x0, -bbox->y1);
  guppi_element_print_closepath (ep);
  guppi_element_print_stroke (ep);

  guppi_element_print_setlinewidth (ep, 72 / 128.0);
  
  if (bbox->x0 < 0 && 0 < bbox->x1) {
    guppi_element_print_newpath (ep);
    guppi_element_print_moveto (ep, 0, -bbox->y0);
    guppi_element_print_lineto (ep, 0, -bbox->y1);
    guppi_element_print_stroke (ep);
  }

  if (bbox->y0 < 0 && 0 < bbox->y1) {
    guppi_element_print_newpath (ep);
    guppi_element_print_moveto (ep, bbox->x0, 0);
    guppi_element_print_lineto (ep, bbox->x1, 0);
    guppi_element_print_stroke (ep);
  }
  
  guppi_element_print_grestore (ep);
}
#endif

static void
print (GuppiElementPrint *ep)
{
  GuppiTextState *state = GUPPI_TEXT_STATE (guppi_element_print_state (ep));
  print_info info;
  ArtDRect bbox;
  double w, h, w2, h2;
  double top_ha=0;
  double aff[6];

  guppi_text_block_bbox (state->block, &bbox);

  /* Get ascender-height of first line. */
  guppi_text_block_line_dimensions (state->block, 0, NULL, &top_ha, NULL);

  w = bbox.x1 - bbox.x0;
  h = bbox.y1 - bbox.y0;
  w2 = ep->pt_x1 - ep->pt_x0;
  h2 = ep->pt_y1 - ep->pt_y0;

  info.ep = ep;
  info.last_font = NULL;
  info.first = TRUE;
  info.fudge = 0;

  guppi_element_print_gsave (ep);

  art_affine_translate (aff,
  			-bbox.x0 + ep->pt_x0 + (w2-w)/2,
  			bbox.y1 + ep->pt_y0 + (h2-h)/2);
  guppi_element_print_concat (ep, aff);


  art_affine_rotate (aff, -guppi_text_block_angle (state->block));
  guppi_element_print_concat (ep, aff);

  art_affine_translate (aff, 0, top_ha);
  guppi_element_print_concat (ep, aff);

  guppi_text_block_foreach_word (state->block, word_cb, &info);

  guppi_element_print_grestore (ep);
}

/***************************************************************************/

static void
guppi_text_print_class_init (GuppiTextPrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_text_print_finalize;

  ep_class->print = print;
}

static void
guppi_text_print_init (GuppiTextPrint * obj)
{

}

GtkType guppi_text_print_get_type (void)
{
  static GtkType guppi_text_print_type = 0;
  if (!guppi_text_print_type) {
    static const GtkTypeInfo guppi_text_print_info = {
      "GuppiTextPrint",
      sizeof (GuppiTextPrint),
      sizeof (GuppiTextPrintClass),
      (GtkClassInitFunc) guppi_text_print_class_init,
      (GtkObjectInitFunc) guppi_text_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_text_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_text_print_info);
  }
  return guppi_text_print_type;
}

/* $Id$ */
