/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-barchart-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-rgb.h>
#include "guppi-barchart-item.h"
#include "guppi-barchart-view.h"
#include "guppi-barchart-state.h"
#include "guppi-barchart-tools.h"

static GtkObjectClass *parent_class = NULL;

enum {
  CLICKED_BAR,
  LAST_SIGNAL
};

guint bar_signals[LAST_SIGNAL] = { 0 };

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *clip_path, gint flags)
{

}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiElementView *view;
  GuppiBarchartView *bc_view;
  GuppiBarchartState *state;
  gint r, c, R, C;
  double x0, y0, x1, y1, sc, eth;
  guint32 bar_color, edge_color;

  view = guppi_canvas_item_view (gci);
  bc_view = GUPPI_BARCHART_VIEW (view);
  state = GUPPI_BARCHART_STATE (guppi_canvas_item_state (gci));

  sc = guppi_canvas_item_scale (gci);

#if 0
  {
    gint cx0, cy0, cx1, cy1;
    guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);
    g_message ("bbox %d %d %d %d", cx0, cy0, cx1, cy1);
  }
#endif

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "edge_color", &edge_color,
			   "edge_thickness", &eth,
			   NULL);

  eth = guppi_pt2px (eth * sc);

  guppi_barchart_state_table_dimensions (state, &R, &C);

  for (c = 0; c < C; ++c) {
    for (r = 0; r < R; ++r) {

      guppi_barchart_view_bar_position (bc_view, r, c, &x0, &y0, &x1, &y1, &bar_color);

      /* Transform to canvas coordinates */
      guppi_canvas_item_pt2c_d (gci, x0, y0, &x0, &y0);
      guppi_canvas_item_pt2c_d (gci, x1, y1, &x1, &y1);
      guppi_2sort (&x0, &x1); 
      guppi_2sort (&y0, &y1);

#if 0
      g_message ("render bar %d %d: %g %g %g %g %x", r, c, x0, y0, x1, y1, bar_color);
#endif

      /* This is sort of a silly (and inefficient) way to draw a box with a border... */
      guppi_paint_soft_box (buf, x0, y0, x1, y1, edge_color);
      guppi_paint_soft_box (buf,
			    x0 + eth, MIN (y0, y1) + eth,
			    x1 - eth, MAX (y0, y1) - eth, bar_color);

    }
  }
}

static gboolean
double_click (GuppiCanvasItem * gci,
	      guint button, guint state, double pt_x, double pt_y)
{
  GuppiBarchartView *view =
    GUPPI_BARCHART_VIEW (guppi_canvas_item_view (gci));
  double x, y;
  gint r, c;

  guppi_element_view_pt2vp (GUPPI_ELEMENT_VIEW (view), pt_x, pt_y, &x, &y);

  if (guppi_barchart_view_find_bar_at_position (view, x, y, &r, &c)) {

    gtk_signal_emit (GTK_OBJECT (gci), bar_signals[CLICKED_BAR],
		     r, c, button, state);

    return TRUE;
  }

  return FALSE;
}

static void
foreach_class_toolkit (GuppiCanvasItem * item,
		       void (*fn) (GuppiPlotToolkit *, gpointer),
		       gpointer user_data)
{
  GuppiPlotToolkit *tk;

  tk = guppi_barchart_toolkit_style_toggle ();
  fn (tk, user_data);
  guppi_unref (tk);
}

/**************************************************************************/

static void
guppi_barchart_item_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

typedef void (*GuppiSignal_NONE__INT_INT_UINT_UINT) (GtkObject *, gint, gint, guint, guint, gpointer);

static void
guppi_marshal_NONE__INT_INT_UINT_UINT (GtkObject * obj,
				       GtkSignalFunc func,
				       gpointer func_data, GtkArg * args)
{
  GuppiSignal_NONE__INT_INT_UINT_UINT rfunc = (GuppiSignal_NONE__INT_INT_UINT_UINT) func;
  rfunc (obj,
	 GTK_VALUE_INT (args[0]),
	 GTK_VALUE_INT (args[1]),
	 GTK_VALUE_UINT (args[2]),
	 GTK_VALUE_UINT (args[3]),
	 func_data);
}

static void
guppi_barchart_item_class_init (GuppiBarchartItemClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_barchart_item_finalize;

  gci_class->guppi_update = update;
  gci_class->guppi_render = render;
  gci_class->double_click = double_click;
  gci_class->foreach_class_toolkit = foreach_class_toolkit;

  guppi_canvas_item_class_set_item_class_toolkit (gci_class,
						  guppi_barchart_toolkit_new_default ());

  bar_signals[CLICKED_BAR] =
    gtk_signal_new ("clicked_bar",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GuppiBarchartItemClass, clicked_bar),
		    guppi_marshal_NONE__INT_INT_UINT_UINT,
		    GTK_TYPE_NONE, 4,
		    GTK_TYPE_INT, GTK_TYPE_INT, GTK_TYPE_UINT, GTK_TYPE_UINT);

  gtk_object_class_add_signals (object_class, bar_signals, LAST_SIGNAL);
}

static void
guppi_barchart_item_init (GuppiBarchartItem * obj)
{

}

GtkType guppi_barchart_item_get_type (void)
{
  static GtkType guppi_barchart_item_type = 0;
  if (!guppi_barchart_item_type) {
    static const GtkTypeInfo guppi_barchart_item_info = {
      "GuppiBarchartItem",
      sizeof (GuppiBarchartItem),
      sizeof (GuppiBarchartItemClass),
      (GtkClassInitFunc) guppi_barchart_item_class_init,
      (GtkObjectInitFunc) guppi_barchart_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_barchart_item_type =
      gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_barchart_item_info);
  }
  return guppi_barchart_item_type;
}

/* $Id$ */
