/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-barchart-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-barchart-state.h"

#include <guppi-i18n.h>
#include <guppi-rgb.h>
#include <guppi-data-socket.h>
#include <guppi-data-flavor.h>
#include <guppi-data-table.h>
#include <guppi-seq-scalar.h>
#include <guppi-color-palette.h>
#include "guppi-barchart-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_barchart_state_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
make_config_model (GuppiElementState *state, GuppiConfigModel *model)
{
  guppi_config_model_add_glade_file (model,
				     "Bar Chart", "Bar Layout",
				     GUPPI_CONFIG_APPEARANCE,
				     guppi_element_state_attribute_bag (state),
				     "guppi-barchart-state-config.glade", "bar_layout",
				     NULL, NULL, NULL);

  if (GUPPI_ELEMENT_STATE_CLASS (parent_class)->make_config_model)
    GUPPI_ELEMENT_STATE_CLASS (parent_class)->make_config_model (state, model);
}

/***************************************************************************/

static void
guppi_barchart_state_class_init (GuppiBarchartStateClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_barchart_state_finalize;

  state_class->name = _("Bar Chart");

  state_class->view_type = GUPPI_TYPE_BARCHART_VIEW;
  state_class->make_config_model = make_config_model;
}

static void
guppi_barchart_state_init (GuppiBarchartState *obj)
{
  GuppiAttributeBag *bag;
  double inch = guppi_in2pt (1.0);

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "data::socket::adopt",       NULL, guppi_data_socket_new_by_type (GUPPI_TYPE_DATA_TABLE));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,          "edge_color",               NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_COLOR_PALETTE, "bar_colors",               NULL, NULL);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,       "use_stock_colors",         NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,       "fallback_to_stock_colors", NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,          "fallback_color",           NULL, RGBA_RED);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "transpose",              NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "reverse_rows",           NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "reverse_cols",           NULL, FALSE);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "stacked",                NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "normalize_stacks",       NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "vertical_bars",          NULL, TRUE);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,    "bar_base",               NULL, 0.0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "edge_thickness",         NULL, inch / 48);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,    "cluster_margin",         NULL, 0.20);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,    "bar_margin",             NULL, 0.06);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "enforce_preferred_view", NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,    "bar_pos_min",            NULL, 0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,    "bar_pos_max",            NULL, 1);
}

GtkType
guppi_barchart_state_get_type (void)
{
  static GtkType guppi_barchart_state_type = 0;
  if (!guppi_barchart_state_type) {
    static const GtkTypeInfo guppi_barchart_state_info = {
      "GuppiBarchartState",
      sizeof (GuppiBarchartState),
      sizeof (GuppiBarchartStateClass),
      (GtkClassInitFunc) guppi_barchart_state_class_init,
      (GtkObjectInitFunc) guppi_barchart_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_barchart_state_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_barchart_state_info);
  }
  return guppi_barchart_state_type;
}

GuppiElementState *
guppi_barchart_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_barchart_state_get_type ()));
}

/**************************************************************************/

/* Data Access Functions */

gboolean
guppi_barchart_state_table_dimensions (GuppiBarchartState *state,
				       gint *row_count, gint *col_count)
{
  GuppiDataTable *table = NULL;
  gboolean transpose;
  gint R, C;

  g_return_val_if_fail (GUPPI_IS_BARCHART_STATE (state), FALSE);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "data", &table,
			   "transpose", &transpose,
			   NULL);

  if (table == NULL)
    return FALSE;

  guppi_data_table_get_dimensions (table, &R, &C);

  if (transpose) {
    gint x = R;
    R = C;
    C = x;
  }

  if (row_count)
    *row_count = R;
  if (col_count)
    *col_count = C;

  guppi_unref (table);

  return TRUE;
}

gboolean
guppi_barchart_state_bar_info (GuppiBarchartState *state,
			       gint r, gint c,
			       double *min, double *max,
			       guint32 *color)
{
  GuppiDataTable *table = NULL;
  GuppiColorPalette *pal = NULL;
  static GuppiColorPalette *stock_pal = NULL;

  gboolean transpose, reverse_rows, reverse_cols;
  gboolean use_stock_colors, fallback_to_stock_colors;
  gboolean stacked, normalize_stacks;
  guint32 fallback_color;
  gint R, C, i;
  double min_value, max_value;

  g_return_val_if_fail (GUPPI_IS_BARCHART_STATE (state), FALSE);
  if (r < 0 || c < 0)
    return FALSE;

  if (stock_pal == NULL) {
    stock_pal = guppi_color_palette_new ();
    guppi_permanent_alloc (stock_pal);
  }

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "data", &table,
			   "transpose", &transpose,
			   "stacked", &stacked,
			   "normalize_stacks", &normalize_stacks,
			   "reverse_rows", &reverse_rows,
			   "reverse_cols", &reverse_cols,
			   "bar_colors::raw", &pal,
			   "use_stock_colors", &use_stock_colors,
			   "fallback_to_stock_colors", &fallback_to_stock_colors,
			   "fallback_color", &fallback_color,
			   NULL);
  if (table == NULL) 
    return FALSE;

  if (transpose) {
    gint x = r;
    r = c;
    c = x;
  }
  
  guppi_data_table_get_dimensions (table, &R, &C);
  if (r >= R || c >= C) {
    guppi_unref (table);
    return FALSE;
  }

  if (reverse_rows)
    r = R - r - 1;
  if (reverse_cols)
    c = C - c - 1;

  if (stacked) {
    min_value = c > 0 ? guppi_data_table_get_range_abs_sum (table, r, 0, r, c-1) : 0;
    max_value = min_value + fabs (guppi_data_table_get_entry (table, r, c));
  } else {
    double value = guppi_data_table_get_entry (table, r, c);
    min_value = MIN (value, 0);
    max_value = MAX (value, 0);
  }

  if (normalize_stacks) {
    double factor = guppi_data_table_get_range_abs_sum (table, r, 0, r, C-1);
    if (factor > 0) {
      min_value /= factor;
      max_value /= factor;
    }
  }

  guppi_2sort (&min_value, &max_value);

  if (min)
    *min = min_value;
  if (max)
    *max = max_value;

  if (color) {
    i = c; /* color by column # */
    *color = fallback_color;
    if (use_stock_colors || (pal == NULL && fallback_to_stock_colors)) {
      *color = guppi_color_palette_get (stock_pal, i);
    } else if (pal != NULL) {
      *color = guppi_color_palette_get (pal, i);
    }
  }

  guppi_unref (table);

  return TRUE;
}

gboolean
guppi_barchart_state_bar_bounds (GuppiBarchartState *state, double *min, double *max)
{
  gint r, c, R, C;
  double m = 0, M = 0;
  
  g_return_val_if_fail (GUPPI_IS_BARCHART_STATE (state), FALSE);

  guppi_barchart_state_table_dimensions (state, &R, &C);

  /* Obviously, not a wildly efficient implementation */
  for (r = 0; r < R; ++r) {
    for (c = 0; c < C; ++c) {
      double mm, MM;
      if (! guppi_barchart_state_bar_info (state, r, c, &mm, &MM, NULL))
	return FALSE;
      if (r == 0 && c == 0) {
	m = mm;
	M = MM;
      } else {
	if (mm < m) m = mm;
	if (MM > M) M = MM;
      }
    }
  }

  if (min)
    *min = m;
  if (max)
    *max = M;

  return TRUE;
}

/* $Id$ */
