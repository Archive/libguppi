/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-image-item.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-image-item.h"

#include <math.h>
#include <guppi-memory.h>
#include "guppi-image-view.h"
#include "guppi-image-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_image_item_finalize (GtkObject *obj)
{
  GuppiImageItem *x = GUPPI_IMAGE_ITEM(obj);

  guppi_pixbuf_unref (x->scaled_pixbuf);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *svp, gint flags)
{
  GuppiImageState *img_state = GUPPI_IMAGE_STATE (guppi_canvas_item_state (gci));
  GuppiImageItem *img_item = GUPPI_IMAGE_ITEM (gci);
  GdkPixbuf *pixbuf;
  double sc = guppi_canvas_item_scale (gci);
  gint w, h;

  if (img_item->pixbuf_scale >= 0 && fabs (sc - img_item->pixbuf_scale) < 1e-6)
    return;

  guppi_pixbuf_unref (img_item->scaled_pixbuf);
  img_item->scaled_pixbuf = NULL;
  img_item->pixbuf_scale = -1;

  if (img_state->pixbuf == NULL || sc < 1e-6)
    return;

  w = (gint) rint (sc * gdk_pixbuf_get_width (img_state->pixbuf->pixbuf));
  h = (gint) rint (sc * gdk_pixbuf_get_height (img_state->pixbuf->pixbuf));
  pixbuf = gdk_pixbuf_scale_simple (img_state->pixbuf->pixbuf, w, h, GDK_INTERP_BILINEAR);

  if (pixbuf) {
    img_item->scaled_pixbuf = guppi_pixbuf_new (pixbuf);
    img_item->pixbuf_scale = sc;
    gdk_pixbuf_unref (pixbuf);
  }
}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiImageItem *img_item = GUPPI_IMAGE_ITEM (gci);
  gint cx0, cy0;

  if (img_item->scaled_pixbuf == NULL)
    return;

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, NULL, NULL);

  guppi_pixbuf_paste (img_item->scaled_pixbuf, cx0, cy0, 0xff, buf);
}

static void
guppi_image_item_class_init (GuppiImageItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiCanvasItemClass *item_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_image_item_finalize;
  item_class->guppi_render = render;
  item_class->guppi_update = update;
}

static void
guppi_image_item_init (GuppiImageItem *obj)
{
  obj->pixbuf_scale = -1;
}

GtkType
guppi_image_item_get_type (void)
{
  static GtkType guppi_image_item_type = 0;
  if (!guppi_image_item_type) {
    static const GtkTypeInfo guppi_image_item_info = {
      "GuppiImageItem",
      sizeof (GuppiImageItem),
      sizeof (GuppiImageItemClass),
      (GtkClassInitFunc)guppi_image_item_class_init,
      (GtkObjectInitFunc)guppi_image_item_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_image_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_image_item_info);
  }
  return guppi_image_item_type;
}
