/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-image-view.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-image-view.h"

#include <guppi-memory.h>
#include "guppi-image-item.h"
#include "guppi-image-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_image_view_finalize (GtkObject *obj)
{
  GuppiImageView *x = GUPPI_IMAGE_VIEW(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_image_view_class_init (GuppiImageViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_image_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_IMAGE_ITEM;
  view_class->print_type = GUPPI_TYPE_IMAGE_PRINT;
}

static void
guppi_image_view_init (GuppiImageView *obj)
{

}

GtkType
guppi_image_view_get_type (void)
{
  static GtkType guppi_image_view_type = 0;
  if (!guppi_image_view_type) {
    static const GtkTypeInfo guppi_image_view_info = {
      "GuppiImageView",
      sizeof (GuppiImageView),
      sizeof (GuppiImageViewClass),
      (GtkClassInitFunc)guppi_image_view_class_init,
      (GtkObjectInitFunc)guppi_image_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_image_view_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_image_view_info);
  }
  return guppi_image_view_type;
}
