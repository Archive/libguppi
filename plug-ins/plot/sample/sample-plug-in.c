/* $Id$ */

/*
 * sample-plug-in.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 * and Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>

#include <guppi-plug-in.h>

GuppiPlugIn *guppi_plug_in (void);

GuppiPlugIn *
guppi_plug_in (void)
{
  GuppiPlugIn *pi = guppi_plug_in_new ();

  pi->magic_number = GUPPI_PLUG_IN_MAGIC_NUMBER;
  pi->type = "sample";
  pi->external_name = _("Sample Plug-In");
  pi->internal_name = "sample";
  pi->major_version = 3;
  pi->minor_version = 14;
  pi->micro_version = 159;
  pi->copyright = "(C) 2000 The Gratis Software Foundation";
  pi->authors = "A. Programmer <aprog@gsf.org>";

  return pi;
}

/* $Id$ */
