/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-xybox-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_XYBOX_VIEW_H
#define _INC_GUPPI_XYBOX_VIEW_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-group-view.h>

BEGIN_GUPPI_DECLS 

typedef struct _GuppiXYBoxView GuppiXYBoxView;
typedef struct _GuppiXYBoxViewClass GuppiXYBoxViewClass;

struct _GuppiXYBoxView {
  GuppiGroupView parent;
};

struct _GuppiXYBoxViewClass {
  GuppiGroupViewClass parent_class;
};

#define GUPPI_TYPE_XYBOX_VIEW (guppi_xybox_view_get_type ())
#define GUPPI_XYBOX_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_XYBOX_VIEW,GuppiXYBoxView))
#define GUPPI_XYBOX_VIEW0(obj) ((obj) ? (GUPPI_XYBOX_VIEW(obj)) : NULL)
#define GUPPI_XYBOX_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_XYBOX_VIEW,GuppiXYBoxViewClass))
#define GUPPI_IS_XYBOX_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_XYBOX_VIEW))
#define GUPPI_IS_XYBOX_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_XYBOX_VIEW(obj)))
#define GUPPI_IS_XYBOX_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_XYBOX_VIEW))

GtkType guppi_xybox_view_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_XYBOX_VIEW_H */

/* $Id$ */
