/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-xybox-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-xybox-view.h"

#include <guppi-group-view-layout.h>
#include <guppi-memory.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_xybox_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

struct prefdata {
  gboolean ok;
  guppi_axis_t axis;
  double min;
  double max;
};

static void
pref_cb (GuppiElementView *view, gpointer user_data)
{
  GuppiElementViewClass *klass;
  struct prefdata *pd = (struct prefdata *)user_data;
  double a, b;

  klass = GUPPI_ELEMENT_VIEW_CLASS (GTK_OBJECT (view)->klass);

  if (klass->preferred_range (view, pd->axis, &a, &b)) {
    pd->min = pd->ok ? MIN (a, pd->min) : a;
    pd->max = pd->ok ? MAX (b, pd->max) : b;
    pd->ok = TRUE;
  }
}

static gboolean
preferred_range (GuppiElementView *view, guppi_axis_t ax, double *a, double *b)
{
  struct prefdata pd;

  pd.ok = FALSE;
  pd.axis = ax;

  guppi_group_view_foreach (GUPPI_GROUP_VIEW (view), pref_cb, &pd);

  if (a) *a = pd.min;
  if (b) *b = pd.max;

  return pd.ok;
}

static void
add_hook (GuppiGroupView * grp, GuppiElementView * view)
{
  /* Handle layout.  In this case, it is simple. */
  guppi_group_view_layout_fill (grp, view, 0, 0, 0, 0);

  /* Connect the coordinate systems. */
  guppi_element_view_connect_view_intervals (GUPPI_ELEMENT_VIEW (grp), GUPPI_X_AXIS, view, GUPPI_X_AXIS);
  guppi_element_view_connect_view_intervals (GUPPI_ELEMENT_VIEW (grp), GUPPI_Y_AXIS, view, GUPPI_Y_AXIS);

  /* Connect the axis markers. */
  guppi_element_view_connect_axis_markers (GUPPI_ELEMENT_VIEW (grp), GUPPI_X_AXIS, view, GUPPI_X_AXIS);
  guppi_element_view_connect_axis_markers (GUPPI_ELEMENT_VIEW (grp), GUPPI_Y_AXIS, view, GUPPI_Y_AXIS);
}

/***************************************************************************/

/*
 * Unlike most view objects, we don't need to define print or canvas item
 * types; since this is derived from GroupView, default implementations
 * are provided.  These implementations are perfectly sufficient for
 * our purposes.
 */

static void
guppi_xybox_view_class_init (GuppiXYBoxViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);
  GuppiGroupViewClass *group_class = GUPPI_GROUP_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_GROUP_VIEW);

  object_class->finalize = guppi_xybox_view_finalize;

  view_class->preferred_range = preferred_range;

  group_class->add_hook = add_hook;
}

static void
guppi_xybox_view_init (GuppiXYBoxView * obj)
{

}

GtkType guppi_xybox_view_get_type (void)
{
  static GtkType guppi_xybox_view_type = 0;
  if (!guppi_xybox_view_type) {
    static const GtkTypeInfo guppi_xybox_view_info = {
      "GuppiXYBoxView",
      sizeof (GuppiXYBoxView),
      sizeof (GuppiXYBoxViewClass),
      (GtkClassInitFunc) guppi_xybox_view_class_init,
      (GtkObjectInitFunc) guppi_xybox_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_xybox_view_type =
      gtk_type_unique (GUPPI_TYPE_GROUP_VIEW, &guppi_xybox_view_info);
  }
  return guppi_xybox_view_type;
}

