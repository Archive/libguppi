/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-scatter-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCATTER_VIEW_H
#define _INC_GUPPI_SCATTER_VIEW_H

/* #include <gnome.h> */
#include <guppi-element-view.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiScatterView GuppiScatterView;
typedef struct _GuppiScatterViewClass GuppiScatterViewClass;

struct _GuppiScatterView {
  GuppiElementView parent;

  guppi_uniq_t last_x, last_y;
};

struct _GuppiScatterViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_SCATTER_VIEW (guppi_scatter_view_get_type())
#define GUPPI_SCATTER_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SCATTER_VIEW,GuppiScatterView))
#define GUPPI_SCATTER_VIEW0(obj) ((obj) ? (GUPPI_SCATTER_VIEW(obj)) : NULL)
#define GUPPI_SCATTER_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SCATTER_VIEW,GuppiScatterViewClass))
#define GUPPI_IS_SCATTER_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SCATTER_VIEW))
#define GUPPI_IS_SCATTER_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_SCATTER_VIEW(obj)))
#define GUPPI_IS_SCATTER_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SCATTER_VIEW))

GtkType guppi_scatter_view_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SCATTER_VIEW_H */

/* $Id$ */
