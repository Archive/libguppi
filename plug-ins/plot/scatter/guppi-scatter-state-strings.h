/*
 * Translatable strings file generated by Glade.
 * Add this file to your project's POTFILES.in.
 * DO NOT compile it as part of your application.
 */

gchar *s = N_("window1");
gchar *s = N_("Scatter Plot Data");
gchar *s = N_("X-Data");
gchar *s = N_("Y-Data");
gchar *s = N_("Primary\n" "Size Data");
gchar *s = N_("Secondary \n" "Size Data");
gchar *s = N_("Color Data");
