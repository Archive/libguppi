/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-scatter-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-scatter-print.h"
#include "guppi-scatter-state.h"
#include "guppi-scatter-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_scatter_print_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
print (GuppiElementPrint *ep)
{
  GuppiScatterState *state;
  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  gint i, i0, i1;

  state = GUPPI_SCATTER_STATE (guppi_element_print_state (ep));

  x_data = guppi_scatter_state_get_x_data (state);
  y_data = guppi_scatter_state_get_y_data (state);

  if (x_data == NULL || y_data == NULL)
    return;

  guppi_seq_common_bounds (GUPPI_SEQ (x_data), GUPPI_SEQ (y_data), &i0, &i1);

  for (i = i0; i <= i1; ++i) {
    GuppiMarker marker;
    double x, y, sz1, sz2;
    guint32 color;
    gboolean visible;
    
    guppi_scatter_state_get_point_properties (state, i, &visible, &marker, &color, &sz1, &sz2);
    
    if (visible) {
      x = guppi_seq_scalar_get (x_data, i);
      y = guppi_seq_scalar_get (y_data, i);

      guppi_element_print_vp2pt_auto (ep, &x, &y);

      guppi_element_print_setrgbacolor_uint (ep, color);
      guppi_marker_print (marker, sz1, sz2, ep, x, y);
    }
  }
}

static void
guppi_scatter_print_class_init (GuppiScatterPrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_scatter_print_finalize;
  ep_class->print = print;

}

static void
guppi_scatter_print_init (GuppiScatterPrint * obj)
{

}

GtkType guppi_scatter_print_get_type (void)
{
  static GtkType guppi_scatter_print_type = 0;
  if (!guppi_scatter_print_type) {
    static const GtkTypeInfo guppi_scatter_print_info = {
      "GuppiScatterPrint",
      sizeof (GuppiScatterPrint),
      sizeof (GuppiScatterPrintClass),
      (GtkClassInitFunc) guppi_scatter_print_class_init,
      (GtkObjectInitFunc) guppi_scatter_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_scatter_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_scatter_print_info);
  }
  return guppi_scatter_print_type;
}

/* $Id$ */
