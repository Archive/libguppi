/* $Id$ */

/*
 * guppi-scatter-tools.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SCATTER_TOOLS_H
#define _INC_GUPPI_SCATTER_TOOLS_H

/* #include <gnome.h> */
#include <guppi-plot-tool.h>
#include <guppi-plot-toolkit.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

GuppiPlotTool *guppi_scatter_tool_radius_brush (gint brush, double radius);
GuppiPlotTool *guppi_scatter_tool_frame_brush (gint brush);
GuppiPlotTool *guppi_scatter_tool_drag_point (void);

GuppiPlotToolkit *guppi_scatter_toolkit_default (void);
GuppiPlotToolkit *guppi_scatter_toolkit_drag (void);
GuppiPlotToolkit *guppi_scatter_toolkit_brush (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SCATTER-TOOLS_H */

/* $Id$ */
