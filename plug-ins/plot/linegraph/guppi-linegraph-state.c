/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-linegraph-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-useful.h>
#include <guppi-data-socket.h>
#include <guppi-data-flavor.h>
#include <guppi-rgb.h>
#include <guppi-seq-scalar.h>
#include <guppi-date-series.h>
#include "guppi-linegraph-state.h"
#include "guppi-linegraph-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_linegraph_state_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
guppi_linegraph_state_class_init (GuppiLinegraphStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_linegraph_state_finalize;

  state_class->view_type = GUPPI_TYPE_LINEGRAPH_VIEW;
}

static void
guppi_linegraph_state_init (GuppiLinegraphState *obj)
{
  GuppiAttributeBag *bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));
  const double inch = guppi_in2pt (1.0);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "ts_data::socket::adopt", NULL,
					guppi_data_socket_new_by_type (GUPPI_TYPE_DATE_SERIES));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "x_data::socket::adopt", NULL,
					guppi_data_socket_new_by_type (GUPPI_TYPE_SEQ_SCALAR));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "y_data::socket::adopt", NULL,
					guppi_data_socket_new_by_type (GUPPI_TYPE_SEQ_SCALAR));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "color", NULL, RGBA_BLUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "width", NULL, inch / 32.0);
}

GtkType guppi_linegraph_state_get_type (void)
{
  static GtkType guppi_linegraph_state_type = 0;
  if (!guppi_linegraph_state_type) {
    static const GtkTypeInfo guppi_linegraph_state_info = {
      "GuppiLinegraphState",
      sizeof (GuppiLinegraphState),
      sizeof (GuppiLinegraphStateClass),
      (GtkClassInitFunc) guppi_linegraph_state_class_init,
      (GtkObjectInitFunc) guppi_linegraph_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_linegraph_state_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_linegraph_state_info);
  }
  return guppi_linegraph_state_type;
}

GuppiElementState *
guppi_linegraph_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_linegraph_state_get_type ()));
}

/* $Id$ */
