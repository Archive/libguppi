/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-linegraph-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <guppi-2d.h>
#include <guppi-memory.h>
#include <guppi-seq-scalar.h>
#include <guppi-date-series.h>
#include "guppi-linegraph-view.h"
#include "guppi-linegraph-state.h"
#include "guppi-linegraph-item.h"
#include "guppi-linegraph-print.h"

static GtkObjectClass *parent_class = NULL;

static void
view_init (GuppiElementView *view)
{
  guppi_element_view_set_axis_marker_type (view, GUPPI_X_AXIS, GUPPI_AXIS_SCALAR);
  guppi_element_view_set_axis_marker_type (view, GUPPI_Y_AXIS, GUPPI_AXIS_SCALAR);
}

static gboolean
preferred_range (GuppiElementView *view, guppi_axis_t ax, double *a, double *b)
{
  GuppiElementState *state = guppi_element_view_state (view);
  GuppiSeqScalar *data = NULL;
  double min, max, W;

  if (ax == GUPPI_X_AXIS) {
    guppi_element_state_get (state,
			     "x_data", &data,
			     NULL);
  } else if (ax == GUPPI_Y_AXIS) {
    guppi_element_state_get (state,
			     "y_data", &data,
			     NULL);
  } else
    return FALSE;

  if (data == NULL)
    return FALSE;

  min = guppi_seq_scalar_min (data);
  max = guppi_seq_scalar_max (data);
  W = max - min;
  max += 0.025 * W;
  min -= 0.025 * W;

  if (a)
    *a = min;
  if (b)
    *b = max;

  guppi_unref (data);
  return TRUE;
}

static void
guppi_linegraph_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
guppi_linegraph_view_class_init (GuppiLinegraphViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_linegraph_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_LINEGRAPH_ITEM;
  view_class->print_type = GUPPI_TYPE_LINEGRAPH_PRINT;
  view_class->view_init = view_init;
  view_class->preferred_range = preferred_range;
}

static void
guppi_linegraph_view_init (GuppiLinegraphView * obj)
{

}

GtkType guppi_linegraph_view_get_type (void)
{
  static GtkType guppi_linegraph_view_type = 0;
  if (!guppi_linegraph_view_type) {
    static const GtkTypeInfo guppi_linegraph_view_info = {
      "GuppiLinegraphView",
      sizeof (GuppiLinegraphView),
      sizeof (GuppiLinegraphViewClass),
      (GtkClassInitFunc) guppi_linegraph_view_class_init,
      (GtkObjectInitFunc) guppi_linegraph_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_linegraph_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_linegraph_view_info);
  }
  return guppi_linegraph_view_type;
}

/* ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** */

static ArtVpath *
build_2seqscalar (GuppiLinegraphView *view,
		  GuppiSeqScalar *x_data,
		  GuppiSeqScalar *y_data)
{
  ArtVpath *path;
  gint i, j, i0, i1, N;

  guppi_seq_common_bounds (GUPPI_SEQ (x_data), GUPPI_SEQ (y_data), &i0, &i1);
  N = i1 - i0 + 1;

  path = guppi_new (ArtVpath, N + 1);

  j = 0;
  for (i=i0; i <= i1; ++i) {
    path[j].code = i == i0 ? ART_MOVETO_OPEN : ART_LINETO;
    path[j].x = guppi_seq_scalar_get (GUPPI_SEQ_SCALAR (x_data), i);
    path[j].y = guppi_seq_scalar_get (GUPPI_SEQ_SCALAR (y_data), i);
    ++j;
  }

  path[j].code = ART_END;
  path[j].x = path[j].y = 0;
  
  return path;
}

static ArtVpath *
build_date_series (GuppiLinegraphView *view, GuppiDateSeries *ser)
{
  double x0, x1;
  GDate sd, ed;
  gint N, i=0, j;
  double *tbuf;
  double *buf;
  ArtVpath *path = NULL;

  if (guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser)))
    return NULL;

  guppi_element_view_get_bbox_vp (GUPPI_ELEMENT_VIEW (view),
				  &x0, NULL, &x1, NULL);
  
  g_date_set_julian (&sd, (gint)floor (x0));
  g_date_set_julian (&ed, (gint)ceil (x1));

  guppi_date_indexed_decr (GUPPI_DATE_INDEXED (ser), &sd);
  guppi_date_indexed_incr (GUPPI_DATE_INDEXED (ser), &ed);
  
  N = (gint)g_date_get_julian (&ed) - (gint)g_date_get_julian (&sd) + 1;
  
  tbuf = guppi_new (double, N);
  buf = guppi_new (double, N);

  N = guppi_date_series_get_range_timecoded (ser, &sd, &ed, tbuf, buf, N);

  if (N > 0) {
    
    path = guppi_new (ArtVpath, N + 1);

    for (i=0; i<N; ++i) {

      path[i].code = ART_LINETO;
      path[i].x = tbuf[i];
      path[i].y = buf[i];

      if (i == 0) {
	path[i].code = ART_MOVETO_OPEN;
      }

    }
    path[i].code = ART_END;
  }
  
  guppi_free (tbuf);
  guppi_free (buf);

  return path;
}

#if 0
static ArtVpath *
build_curve (GuppiLinegraphView *view, GuppiCurve *curve,
	     double x_error, double y_error,
	     double scale_x, double scale_y)
{
  double t0, t1, x0, y0, x1, y1;
  ArtVpath *path;

  guppi_curve_parameter_bounds (curve, &t0, &t1);

  guppi_element_view_get_bbox_vp (GUPPI_ELEMENT_VIEW (view),
				  &x0, &y0, &x1, &y1);

  guppi_curve_clamp_to_bbox (curve, &t0, &t1, x0, y0, x1, y1);

  path = guppi_curve_approximate_to_path (curve, t0, t1,
					  x_error, y_error,
					  x0, y0, x1, y1,
					  scale_x, scale_y);

  return path;
}
#endif


ArtVpath *
guppi_linegraph_view_build_path (GuppiLinegraphView *view,
				 double x_error, double y_error,
				 double scale_x, double scale_y)
{
  GuppiElementState *state;
  GuppiSeqScalar *x_data;
  GuppiSeqScalar *y_data;
  GuppiDateSeries *ts_data;
  ArtVpath *path = NULL;

  g_return_val_if_fail (view && GUPPI_IS_LINEGRAPH_VIEW (view), NULL);

  state = guppi_element_view_state (GUPPI_ELEMENT_VIEW (view));
  guppi_element_state_get (state,
			   "ts_data", &ts_data,
			   "x_data", &x_data,
			   "y_data", &y_data,
			   NULL);

  if (ts_data) {
    path = build_date_series (view, ts_data);
  } else if (x_data && y_data) {
    path = build_2seqscalar (view, x_data, y_data);
  }

  guppi_unref (ts_data);
  guppi_unref (x_data);
  guppi_unref (y_data);

  return path;
}

/* $Id$ */
