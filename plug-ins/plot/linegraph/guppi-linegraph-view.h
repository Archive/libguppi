/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-linegraph-view.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_LINEGRAPH_VIEW_H
#define _INC_GUPPI_LINEGRAPH_VIEW_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-element-view.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiLinegraphView GuppiLinegraphView;
typedef struct _GuppiLinegraphViewClass GuppiLinegraphViewClass;

struct _GuppiLinegraphView {
  GuppiElementView parent;
};

struct _GuppiLinegraphViewClass {
  GuppiElementViewClass parent_class;
};

#define GUPPI_TYPE_LINEGRAPH_VIEW (guppi_linegraph_view_get_type())
#define GUPPI_LINEGRAPH_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_LINEGRAPH_VIEW,GuppiLinegraphView))
#define GUPPI_LINEGRAPH_VIEW0(obj) ((obj) ? (GUPPI_LINEGRAPH_VIEW(obj)) : NULL)
#define GUPPI_LINEGRAPH_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_LINEGRAPH_VIEW,GuppiLinegraphViewClass))
#define GUPPI_IS_LINEGRAPH_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_LINEGRAPH_VIEW))
#define GUPPI_IS_LINEGRAPH_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_LINEGRAPH_VIEW(obj)))
#define GUPPI_IS_LINEGRAPH_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_LINEGRAPH_VIEW))

GtkType guppi_linegraph_view_get_type (void);

ArtVpath *guppi_linegraph_view_build_path (GuppiLinegraphView *, double x_error, double y_error, double scale_x, double scale_y);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_LINEGRAPH_VIEW_H */

/* $Id$ */
