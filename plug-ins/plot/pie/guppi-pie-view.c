/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-pie-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <guppi-metrics.h>
#include <guppi-memory.h>
#include "guppi-pie-view.h"
#include "guppi-pie-state.h"
#include "guppi-pie-item.h"
#include "guppi-pie-print.h"
#include "guppi-pie-tool.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pie_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiCanvasItem *
make_canvas_item (GuppiElementView * view, GnomeCanvas * canvas,
		  GnomeCanvasGroup * group)
{
  GnomeCanvasItem *gnoci;
  GuppiCanvasItem *gci;

  gnoci = gnome_canvas_item_new (group, GUPPI_TYPE_PIE_ITEM, NULL);

  gci = GUPPI_CANVAS_ITEM (gnoci);

  return gci;
}

/***************************************************************************/

static void
guppi_pie_view_class_init (GuppiPieViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_pie_view_finalize;

  view_class->make_canvas_item = make_canvas_item;
  view_class->print_type = GUPPI_TYPE_PIE_PRINT;
}

static void
guppi_pie_view_init (GuppiPieView * obj)
{

}

GtkType guppi_pie_view_get_type (void)
{
  static GtkType guppi_pie_view_type = 0;
  if (!guppi_pie_view_type) {
    static const GtkTypeInfo guppi_pie_view_info = {
      "GuppiPieView",
      sizeof (GuppiPieView),
      sizeof (GuppiPieViewClass),
      (GtkClassInitFunc) guppi_pie_view_class_init,
      (GtkObjectInitFunc) guppi_pie_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_pie_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_pie_view_info);
  }
  return guppi_pie_view_type;
}

double
guppi_pie_view_max_radius (GuppiPieView *view)
{
  GuppiPieState *state;
  double w, h;
  GuppiGeometry *geom;
  double max_offset, max_r, edge_width, base_offset;
  GnomeFont *font;
  double inch = guppi_in2pt (1.0);
  gboolean show_perc;

  state = GUPPI_PIE_STATE (guppi_element_view_state (GUPPI_ELEMENT_VIEW (view)));

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "label_font", &font,
			   "edge_width", &edge_width,
			   "show_percentage", &show_perc,
			   "base_offset", &base_offset,
			   NULL);

  geom = guppi_element_view_geometry (GUPPI_ELEMENT_VIEW (view));
  w = guppi_geometry_width (geom);
  h = guppi_geometry_height (geom);

  /* We allow for an extra edge_width worth of space as a margin of error. */
  max_r = MIN (w, h) / 2 - 2 * edge_width;

  /* This is "reaching under the skirt" of GuppiPieState, which we
     really shouldn't do. */
  max_offset = 0;
  if (state->slice_offsets)
    max_offset = MAX (guppi_seq_scalar_max (state->slice_offsets), 0);
  max_r -= MAX (base_offset + max_offset, 0);

  if (show_perc && font != NULL) {
    double font_w, font_h, font_l;

    font_w = gnome_font_get_width_string (font, "100%");
    font_h = gnome_font_get_ascender (font);
    font_l = sqrt (font_w * font_w + font_h * font_h);

    max_r -= inch/32 + 0.667 * font_l + MAX (font_w / 2, font_h / 2);
  }

  if (max_r < inch/32)
    max_r = inch/32;

  guppi_unref0 (font);

  return max_r;
}

double
guppi_pie_view_effective_radius (GuppiPieView * view)
{
  GuppiPieState *state;
  double r, max_r;
  gboolean r_maximize, r_lock;

  state = GUPPI_PIE_STATE (guppi_element_view_state (GUPPI_ELEMENT_VIEW (view)));

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "radius", &r,
			   "radius_maximize", &r_maximize,
			   "radius_lock", &r_lock,
			   NULL);

  if (r_maximize || r_lock) {
    max_r = guppi_pie_view_max_radius (view);
    r = r_maximize ? max_r : MIN (r, max_r);
  }

  return r;
}

/* $Id$ */
