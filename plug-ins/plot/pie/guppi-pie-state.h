/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-pie-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PIE_STATE_H
#define _INC_GUPPI_PIE_STATE_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <libgnomeprint/gnome-font.h>
#include <guppi-element-state.h>
#include <guppi-seq-scalar.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiPieState GuppiPieState;
typedef struct _GuppiPieStateClass GuppiPieStateClass;

struct _GuppiPieState {
  GuppiElementState parent;

  GuppiSeqScalar *data_copy;
  double data_abs_sum;
  GuppiSeqScalar *slice_offsets;
};

struct _GuppiPieStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_PIE_STATE (guppi_pie_state_get_type())
#define GUPPI_PIE_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PIE_STATE,GuppiPieState))
#define GUPPI_PIE_STATE0(obj) ((obj) ? (GUPPI_PIE_STATE(obj)) : NULL)
#define GUPPI_PIE_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PIE_STATE,GuppiPieStateClass))
#define GUPPI_IS_PIE_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PIE_STATE))
#define GUPPI_IS_PIE_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_PIE_STATE(obj)))
#define GUPPI_IS_PIE_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PIE_STATE))

GtkType guppi_pie_state_get_type (void);

GuppiElementState *guppi_pie_state_new (void);


/* Front-end for accessing data */

void guppi_pie_state_slice_bounds (GuppiPieState *pie, gint *i0, gint *i1);

double guppi_pie_state_slice_percentage (GuppiPieState *pie, gint i);

gboolean guppi_pie_state_need_separate_label_data (GuppiPieState * pie);
const gchar *guppi_pie_state_slice_label (GuppiPieState *, gint i);

guint32 guppi_pie_state_slice_color (GuppiPieState *, gint i);

double guppi_pie_state_slice_offset (GuppiPieState *, gint i);
void guppi_pie_state_set_slice_offset (GuppiPieState *, gint i, double x);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PIE_STATE_H */

/* $Id$ */
