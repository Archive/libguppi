/* $Id$ */

/*
 * guppi-pie-common.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PIE_COMMON_H
#define _INC_GUPPI_PIE_COMMON_H

/* #include <gnome.h> */
#include <libart_lgpl/art_vpath.h>

#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

ArtVpath *guppi_pie_slice_vpath (double center_x, double center_y,
				 double offset, double radius,
				 double theta1, double theta2,
				 double smoothness);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PIE_COMMON_H */

/* $Id$ */
