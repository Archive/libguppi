/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-pie-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PIE_ITEM_H
#define _INC_GUPPI_PIE_ITEM_H

/* #include <gnome.h> */
#include <guppi-defs.h>
#include <guppi-canvas-item.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiPieItem GuppiPieItem;
typedef struct _GuppiPieItemClass GuppiPieItemClass;

struct _GuppiPieItem {
  GuppiCanvasItem parent;

  GList *slice_fill_svp;
  GList *slice_edge_svp;

  gint edge_percent_template_count;
  GList *edge_percent_templates;
};

struct _GuppiPieItemClass {
  GuppiCanvasItemClass parent_class;

  void (*clicked_slice) (GuppiPieItem *, gint slice, guint button,
			 gint state);
};

#define GUPPI_TYPE_PIE_ITEM (guppi_pie_item_get_type())
#define GUPPI_PIE_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_PIE_ITEM,GuppiPieItem))
#define GUPPI_PIE_ITEM0(obj) ((obj) ? (GUPPI_PIE_ITEM(obj)) : NULL)
#define GUPPI_PIE_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_PIE_ITEM,GuppiPieItemClass))
#define GUPPI_IS_PIE_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_PIE_ITEM))
#define GUPPI_IS_PIE_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_PIE_ITEM(obj)))
#define GUPPI_IS_PIE_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_PIE_ITEM))

GtkType guppi_pie_item_get_type (void);

GtkObject *guppi_pie_item_new (void);

gboolean guppi_pie_item_in_slice (GuppiPieItem *, gint x, gint y,
				  gint * slice);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_PIE_ITEM_H */

/* $Id$ */
