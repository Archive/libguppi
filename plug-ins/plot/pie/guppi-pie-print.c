/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-pie-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <guppi-useful.h>
#include "guppi-pie-print.h"
#include "guppi-pie-view.h"
#include "guppi-pie-state.h"
#include "guppi-pie-common.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pie_print_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/**************************************************************************/

static void
print (GuppiElementPrint * ep)
{
  GuppiPieState *state = GUPPI_PIE_STATE (guppi_element_print_state (ep));
  GuppiPieView *view = GUPPI_PIE_VIEW (guppi_element_print_view (ep));
  double r, run_ang;
  gint i, i0, i1, j;
  double x0, y0, x1, y1, base_angle;
  guint32 edge_color, label_color;
  gboolean show_perc;
  GnomeFont *label_font;

  guppi_pie_state_slice_bounds (state, &i0, &i1);
  if (i0 > i1)
    return;

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "base_angle", &base_angle,
			   "edge_color", &edge_color,
			   "show_percentage", &show_perc,
			   "label_font", &label_font,
			   "label_color", &label_color,
			   NULL);

  guppi_element_print_get_bbox (ep, &x0, &y0, &x1, &y1);

  r = guppi_pie_view_effective_radius (view);
  run_ang = base_angle;

  guppi_element_print_setlinejoin (ep, ART_PATH_STROKE_JOIN_ROUND);

  for (i = i0; i <= i1; ++i) {
    double perc = guppi_pie_state_slice_percentage (state, i);
    double th = 2 * M_PI * perc;
    ArtVpath *path;
    double offset = guppi_pie_state_slice_offset (state, i);

    path = guppi_pie_slice_vpath ((x0 + x1) / 2.0, (y0 + y1) / 2.0,
				  offset, r, run_ang, run_ang + th, 0);

    /* flip the image */
    for (j = 0; path[j].code != ART_END; ++j)
      path[j].y = y0 + y1 - path[j].y;

    guppi_element_print_newpath (ep);
    guppi_element_print_vpath (ep, path, FALSE);
    guppi_element_print_setrgbacolor_uint (ep, guppi_pie_state_slice_color (state, i));
    guppi_element_print_fill (ep);

    guppi_element_print_newpath (ep);
    guppi_element_print_vpath (ep, path, FALSE);
    guppi_element_print_setlinewidth (ep, 72.0 / 32);
    guppi_element_print_setrgbacolor_uint (ep, edge_color);

    guppi_element_print_stroke (ep);

    guppi_free (path);

    /* Print the percentage labels on the edges of the slices,
       if needed. */
    if (show_perc) {
      gchar label[32];
      double w, h, label_r, x, y;

      guppi_element_print_setrgbacolor_uint (ep, label_color);
      guppi_element_print_setfont (ep, label_font);

      g_snprintf (label, 32, "%d%%", (gint) rint (100 * perc));
      w = gnome_font_get_width_string (label_font, label);
      h = gnome_font_get_ascender (label_font);

      label_r = offset + r + 72 / 32.0 + 0.667 * sqrt (w * w + h * h);
      x = (x0 + x1) / 2 + label_r * cos (run_ang + th / 2) - w / 2;
      y = (y0 + y1) / 2 - label_r * sin (run_ang + th / 2) - h / 2;

      guppi_element_print_moveto (ep, x, y);
      guppi_element_print_show (ep, label);
    }

    run_ang += th;
  }

  guppi_unref (label_font);
}

/**************************************************************************/

static void
guppi_pie_print_class_init (GuppiPiePrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_pie_print_finalize;

  ep_class->print = print;
}

static void
guppi_pie_print_init (GuppiPiePrint * obj)
{

}

GtkType guppi_pie_print_get_type (void)
{
  static GtkType guppi_pie_print_type = 0;
  if (!guppi_pie_print_type) {
    static const GtkTypeInfo guppi_pie_print_info = {
      "GuppiPiePrint",
      sizeof (GuppiPiePrint),
      sizeof (GuppiPiePrintClass),
      (GtkClassInitFunc) guppi_pie_print_class_init,
      (GtkObjectInitFunc) guppi_pie_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_pie_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_pie_print_info);
  }
  return guppi_pie_print_type;
}

/* $Id$ */
