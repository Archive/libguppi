/* $Id$ */

/*
 * guppi-pie-common.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libart_lgpl/art_vpath.h>

#include <math.h>
#include <guppi-memory.h>
#include "guppi-pie-common.h"

ArtVpath *
guppi_pie_slice_vpath (double center_x, double center_y,
		       double offset, double radius,
		       double theta1, double theta2, double smoothness)
{
  gint N;
  ArtVpath *path;
  gint i;
  double px, py, offx, offy;

  if (radius <= 0)
    return NULL;

  if (offset < 0)
    offset = 0;

  /* ignore smoothness variable for now */
  N = 3 + (gint) fabs (90 * (theta2 - theta1) / (2 * M_PI));

  path = guppi_new (ArtVpath, N + 3);

  offx = offset * cos ((theta1 + theta2) / 2);
  offy = offset * sin ((theta1 + theta2) / 2);

  /* The point of the pie slice. */
  path[0].code = ART_MOVETO;
  path[0].x = px = center_x + offx;
  path[0].y = py = center_y + offy;

  /* Approximate the curve by line segments. */
  for (i = 0; i < N; ++i) {
    double q = i / ((double) N - 1);
    double th = q * theta1 + (1 - q) * theta2;
    path[i + 1].code = ART_LINETO;
    path[i + 1].x = px + radius * cos (th);
    path[i + 1].y = py + radius * sin (th);
  }

  /* Go back to where we started. */
  path[N + 1].code = ART_LINETO;
  path[N + 1].x = px;
  path[N + 1].y = py;

  path[N + 2].code = ART_END;

  return path;
}






/* $Id$ */
