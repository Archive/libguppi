/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-pricebars-tools.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_PRICEBARS_TOOLS_H
#define _INC_GUPPI_PRICEBARS_TOOLS_H

#include <guppi-defs.h>
#include <guppi-plot-toolkit.h>

BEGIN_GUPPI_DECLS;

GuppiPlotToolkit *guppi_pricebars_toolkit_default (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_PRICEBARS-TOOLS_H */

/* $Id$ */
