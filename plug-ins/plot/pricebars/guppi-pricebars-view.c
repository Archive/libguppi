/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-pricebars-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-pricebars-view.h"

#include <math.h>
#include <guppi-convenient.h>
#include "guppi-pricebars-state.h"
#include "guppi-pricebars-item.h"
#include "guppi-pricebars-print.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_pricebars_view_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static gboolean
preferred_range (GuppiElementView *view, guppi_axis_t ax, double *a, double *b)
{
  GuppiPriceSeries *ser;
  GuppiViewInterval *vi;

  vi = guppi_element_view_axis_view_interval (view, ax);

  guppi_element_state_get (guppi_element_view_state (view),
			   "data", &ser,
			   NULL);

  if (ser == NULL || guppi_date_indexed_empty (GUPPI_DATE_INDEXED (ser))) {
    guppi_unref (ser);
    return FALSE;
  }

  if (ax == GUPPI_X_AXIS) {
    gint prefdays, sd, ed;
    guppi_element_state_get (guppi_element_view_state (view),
			     "preferred_days", &prefdays,
			     NULL);

    sd = g_date_get_julian ((GDate *) guppi_date_indexed_start (GUPPI_DATE_INDEXED (ser)));
    ed = g_date_get_julian ((GDate *) guppi_date_indexed_end (GUPPI_DATE_INDEXED (ser)));
      
    if (a)
      *a = MAX (sd - 2, ed - prefdays);
    if (b)
      *b = ed + 2;
    
    guppi_unref (ser);
    return TRUE;
  }

  if (ax == GUPPI_Y_AXIS) {
    double m, M, slack;
    GDate sd, ed;
    double x0, x1;

    guppi_element_view_get_bbox_vp (view, &x0, NULL, &x1, NULL);
    x0 = floor (x0);
    x1 = ceil (x1);

    if (g_date_valid_julian ((gint)x0) && g_date_valid_julian ((gint)x1)) {

      g_date_set_julian (&sd, (gint)x0);
      g_date_set_julian (&ed, (gint)x1);

      if (guppi_price_series_get_bounds (ser, PRICE_LOW, &sd, &ed, &m, NULL) 
	  && guppi_price_series_get_bounds (ser, PRICE_HIGH, &sd, &ed, NULL, &M)) {
	
	if (guppi_view_interval_is_logarithmic (vi)) {
	  m *= 1 - GUPPI_PRICEBARS_VIEW (view)->y_view_slack;
	  M *= 1 + GUPPI_PRICEBARS_VIEW (view)->y_view_slack;
	} else {
	  slack = (M - m) * GUPPI_PRICEBARS_VIEW (view)->y_view_slack;
	  m -= slack;
	  M += slack;
	}
	if (a) *a = m;
	if (b) *b = M;
	
	guppi_unref (ser);
	return TRUE;
      }
    }
  }

  guppi_unref (ser);
  return FALSE;
}

static void
guppi_pricebars_view_class_init (GuppiPricebarsViewClass *klass)
{
  GtkObjectClass* object_class = (GtkObjectClass *)klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_pricebars_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_PRICEBARS_ITEM;
  view_class->print_type = GUPPI_TYPE_PRICEBARS_PRINT;
  
  view_class->preferred_range = preferred_range;
}

static void
guppi_pricebars_view_init (GuppiPricebarsView *obj)
{
  obj->y_view_slack = 0.025;
}

GtkType
guppi_pricebars_view_get_type (void)
{
  static GtkType guppi_pricebars_view_type = 0;
  if (!guppi_pricebars_view_type) {
    static const GtkTypeInfo guppi_pricebars_view_info = {
      "GuppiPricebarsView",
      sizeof (GuppiPricebarsView),
      sizeof (GuppiPricebarsViewClass),
      (GtkClassInitFunc)guppi_pricebars_view_class_init,
      (GtkObjectInitFunc)guppi_pricebars_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_pricebars_view_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_pricebars_view_info);
  }
  return guppi_pricebars_view_type;
}

gboolean
guppi_pricebars_view_date_range (GuppiPricebarsView *view,
				 GDate *start, GDate *end)
{
  GuppiPricebarsState *state;
  GuppiDateIndexed *ser;
  double x0, x1;
  guint32 min_jul_d, max_jul_d;

  g_return_val_if_fail (view && GUPPI_IS_PRICEBARS_VIEW (view), FALSE);

  state = GUPPI_PRICEBARS_STATE (guppi_element_view_state (GUPPI_ELEMENT_VIEW (view)));

  guppi_element_state_get (guppi_element_view_state (GUPPI_ELEMENT_VIEW (view)),
			   "data", &ser,
			   NULL);

  if (ser == NULL || guppi_date_indexed_empty (ser)) {
    guppi_unref (ser);
    return FALSE;
  }

  guppi_element_view_get_bbox_vp (GUPPI_ELEMENT_VIEW (view),
				  &x0, NULL, &x1, NULL);

  min_jul_d = (guint32)floor (x0);
  max_jul_d = (guint32)ceil (x1);

  if (!g_date_valid_julian (min_jul_d) || !g_date_valid_julian (max_jul_d)) {
    guppi_unref (ser);
    return FALSE;
  }

  if (start) {
    g_date_set_julian (start, min_jul_d);
    guppi_date_indexed_clamp (ser, start);
  }

  if (end) {
    g_date_set_julian (end, max_jul_d);
    guppi_date_indexed_clamp (ser, end);
  }
  
  guppi_unref (ser);
  return TRUE;
}
