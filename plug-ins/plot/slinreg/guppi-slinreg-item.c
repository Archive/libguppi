/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <math.h>
#include <libart_lgpl/libart.h>
#include <guppi-convenient.h>
#include <guppi-rgb.h>
#include <guppi-useful.h>
#include "guppi-slinreg-common.h"
#include "guppi-slinreg-item.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_slinreg_item_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  }
}

static void
guppi_slinreg_item_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  }
}

static void
guppi_slinreg_item_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_slinreg_item_finalize (GtkObject * obj)
{
  GuppiSlinregItem *si = GUPPI_SLINREG_ITEM (obj);

  guppi_unref0 (si->line_label);

  if (si->line_svp) {
    art_svp_free (si->line_svp);
    si->line_svp = NULL;
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
update (GuppiCanvasItem * gci, double aff[6], ArtSVP * svp, gint flags)
{
  GuppiSlinregItem *si;
  GuppiSlinregState *state;
  double sc;
  GuppiAlphaTemplate *label_template;

  si = GUPPI_SLINREG_ITEM (gci);
  state = GUPPI_SLINREG_STATE (guppi_canvas_item_state (gci));
  sc = guppi_canvas_item_scale (gci);

  if (si->line_svp) {
    art_svp_free (si->line_svp);
    si->line_svp = NULL;
  }

  if (state->slr->valid) {
    double m, b, lw;
    double vx0, vy0, vx1, vy1;
    double x0, y0, x1, y1, xspan;
    double cx0, cy0, cx1, cy1;
    ArtVpath linepath[3];

    m = guppi_simple_linreg_slope (state->slr);
    b = guppi_simple_linreg_intercept (state->slr);

    lw = guppi_slinreg_state_line_width (state) * sc;

    guppi_canvas_item_get_bbox_vp (gci, &vx0, &vy0, &vx1, &vy1);

    /* Always pick endpoints for our regression line that are just
       outside of the viewport, so we create the illusion that we are
       looking at a segment of an infinite line. */

    xspan = fabs (vx1 - vx0);
    x0 = vx0 - xspan / 10;
    x1 = vx1 + xspan / 10;
    y0 = m * x0 + b;
    y1 = m * x1 + b;

    guppi_canvas_item_vp2c_d (gci, x0, y0, &cx0, &cy0);
    guppi_canvas_item_vp2c_d (gci, x1, y1, &cx1, &cy1);

    linepath[0].code = ART_MOVETO;
    linepath[0].x = cx0;
    linepath[0].y = cy0;

    linepath[1].code = ART_LINETO;
    linepath[1].x = cx1;
    linepath[1].y = cy1;

    linepath[2].code = ART_END;

    si->line_svp = art_svp_vpath_stroke (linepath,
					 ART_PATH_STROKE_JOIN_ROUND,
					 ART_PATH_STROKE_CAP_ROUND,
					 lw, 4, 0.25);

    si->label_is_visible = FALSE;

    if (guppi_slinreg_state_show_label (state)) {
      double theta, dx, dy, lsz;
      gchar *label_text;
      GnomeFont *font;

      label_text = guppi_slinreg_state_label (state);

      /* Figure out if we need to re-rasterize the text for the label. */

      font = guppi_slinreg_state_label_font (state);
      lsz = gnome_font_get_ascender (font) * sc;
      theta = atan2 (cy1 - cy0, cx1 - cx0);

      guppi_raster_text_set_text (si->line_label, label_text);
      guppi_raster_text_set_font (si->line_label, font);
      guppi_raster_text_set_scale (si->line_label, sc);
      guppi_raster_text_set_angle (si->line_label, 180 * theta / M_PI);

      guppi_free (label_text);
      label_text = NULL;

      /* Calculate the location of the center of the visible part of
         the regression line. */

      guppi_slinreg_segment_endpoints (m, b, vx0, vy0, vx1, vy1,
				       &x0, &y0, &x1, &y1);
      x0 = (x0 + x1) / 2;
      y0 = (y0 + y1) / 2;

      if (vx0 <= x0 && x0 <= vx1 && vy0 <= y0 && y0 <= vy1) {

	guppi_canvas_item_vp2c_d (gci, x0, y0, &cx0, &cy0);

	/* Adjust for the size of the font and the width of the line. */

	dx = guppi_x_pt2px (-(lw / 2 + lsz / 2) * sin (theta));
	dy = guppi_y_pt2px ((lw / 2 + lsz / 2) * cos (theta));

	cx0 += dx;
	cy0 += dy;

	label_template = guppi_raster_text_template (si->line_label);

	/* Set our label to be centered around (cx0, cy0) */
	si->label_pos_x = cx0 - label_template->width / 2;
	si->label_pos_y = cy0 - label_template->height / 2;

	si->label_is_visible = TRUE;
      }
    }
  }
}

static void
render (GuppiCanvasItem * gci, GnomeCanvasBuf * buf)
{
  GuppiSlinregItem *si;
  GuppiSlinregState *state;

  si = GUPPI_SLINREG_ITEM (gci);
  state = GUPPI_SLINREG_STATE (guppi_canvas_item_state (gci));

  /* This is nice and easy. */
  if (si->line_svp != NULL) {

    gnome_canvas_render_svp (buf, si->line_svp,
			     guppi_slinreg_state_line_color (state));

    if (guppi_slinreg_state_show_label (state) && si->line_label) {

      guint r, g, b, a;

      UINT_TO_RGBA (guppi_slinreg_state_label_color (state), &r, &g, &b, &a);

      if (si->label_is_visible)
	guppi_alpha_template_print (guppi_raster_text_template
				    (si->line_label), si->label_pos_x,
				    si->label_pos_y, r, g, b, a, buf);
    }
  }
}

static void
view_changed (GuppiCanvasItem * gci)
{
  guppi_canvas_item_request_update (gci);
}

static void
state_changed (GuppiCanvasItem * gci)
{
  guppi_canvas_item_request_update (gci);
}

/***************************************************************************/

static void
guppi_slinreg_item_class_init (GuppiSlinregItemClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->get_arg = guppi_slinreg_item_get_arg;
  object_class->set_arg = guppi_slinreg_item_set_arg;
  object_class->destroy = guppi_slinreg_item_destroy;
  object_class->finalize = guppi_slinreg_item_finalize;

  gci_class->state_changed = state_changed;
  gci_class->view_changed = view_changed;
  gci_class->guppi_update = update;
  gci_class->guppi_render = render;
}

static void
guppi_slinreg_item_init (GuppiSlinregItem * obj)
{
  obj->line_label = guppi_raster_text_new (NULL);
}

GtkType guppi_slinreg_item_get_type (void)
{
  static GtkType guppi_slinreg_item_type = 0;
  if (!guppi_slinreg_item_type) {
    static const GtkTypeInfo guppi_slinreg_item_info = {
      "GuppiSlinregItem",
      sizeof (GuppiSlinregItem),
      sizeof (GuppiSlinregItemClass),
      (GtkClassInitFunc) guppi_slinreg_item_class_init,
      (GtkObjectInitFunc) guppi_slinreg_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_slinreg_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM,
					       &guppi_slinreg_item_info);
  }
  return guppi_slinreg_item_type;
}

/* $Id$ */
