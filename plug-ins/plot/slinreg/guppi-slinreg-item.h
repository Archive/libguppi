/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-item.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SLINREG_ITEM_H
#define _INC_GUPPI_SLINREG_ITEM_H

/* #include <gnome.h> */
#include <libart_lgpl/art_svp.h>

#include <guppi-canvas-item.h>
#include <guppi-raster-text.h>
#include "guppi-slinreg-state.h"
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiSlinregItem GuppiSlinregItem;
typedef struct _GuppiSlinregItemClass GuppiSlinregItemClass;

struct _GuppiSlinregItem {
  GuppiCanvasItem parent;

  ArtSVP *line_svp;

  gboolean label_is_visible;
  GuppiRasterText *line_label;
  gint label_pos_x, label_pos_y;
};

struct _GuppiSlinregItemClass {
  GuppiCanvasItemClass parent_class;
};

#define GUPPI_TYPE_SLINREG_ITEM (guppi_slinreg_item_get_type())
#define GUPPI_SLINREG_ITEM(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SLINREG_ITEM,GuppiSlinregItem))
#define GUPPI_SLINREG_ITEM0(obj) ((obj) ? (GUPPI_SLINREG_ITEM(obj)) : NULL)
#define GUPPI_SLINREG_ITEM_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SLINREG_ITEM,GuppiSlinregItemClass))
#define GUPPI_IS_SLINREG_ITEM(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SLINREG_ITEM))
#define GUPPI_IS_SLINREG_ITEM0(obj) (((obj) == NULL) || (GUPPI_IS_SLINREG_ITEM(obj)))
#define GUPPI_IS_SLINREG_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SLINREG_ITEM))

GtkType guppi_slinreg_item_get_type (void);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SLINREG_ITEM_H */

/* $Id$ */
