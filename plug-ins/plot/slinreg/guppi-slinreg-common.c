/* $Id$ */

/*
 * guppi-slinreg-common.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
/* #include <gnome.h> */
#include "guppi-slinreg-common.h"

#define SWAP(a,b) { double t=a; a=b; b=t; }
void
guppi_slinreg_segment_endpoints (double m, double b,
				 double vx0, double vy0,
				 double vx1, double vy1,
				 double *x0, double *y0,
				 double *x1, double *y1)
{
  double x[4];

  x[0] = vx0;
  x[1] = vx1;
  x[2] = (vy0 - b) / m;
  x[3] = (vy1 - b) / m;

  /* Now we sort! */
  /* Yes, this is the minimal number of compares for sorting N=4 items. */

  if (x[0] > x[1])
    SWAP (x[0], x[1]);

  if (x[2] > x[3])
    SWAP (x[2], x[3]);

  if (x[0] > x[2])
    SWAP (x[0], x[2]);

  if (x[1] > x[3])
    SWAP (x[1], x[3]);

  if (x[1] > x[2])
    SWAP (x[1], x[2]);

  if (x0)
    *x0 = x[1];
  if (x1)
    *x1 = x[2];
  if (y0)
    *y0 = m * x[1] + b;
  if (y1)
    *y1 = m * x[2] + b;
}



/* $Id$ */
