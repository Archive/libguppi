/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-state.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_SLINREG_STATE_H
#define _INC_GUPPI_SLINREG_STATE_H

/* #include <gnome.h> */
#include <libgnomeprint/gnome-font.h>

#include <guppi-seq-scalar.h>
#include <guppi-element-state.h>
#include <guppi-simple-linreg.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS

typedef struct _GuppiSlinregState GuppiSlinregState;
typedef struct _GuppiSlinregStateClass GuppiSlinregStateClass;

struct _GuppiSlinregState {
  GuppiElementState parent;

  double line_width;
  guint32 line_color;

  gboolean show_label;
  GnomeFont *label_font;
  double label_size;
  guint32 label_color;

  GuppiSimpleLinreg *slr;
};

struct _GuppiSlinregStateClass {
  GuppiElementStateClass parent_class;
};

#define GUPPI_TYPE_SLINREG_STATE (guppi_slinreg_state_get_type())
#define GUPPI_SLINREG_STATE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_SLINREG_STATE,GuppiSlinregState))
#define GUPPI_SLINREG_STATE0(obj) ((obj) ? (GUPPI_SLINREG_STATE(obj)) : NULL)
#define GUPPI_SLINREG_STATE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_SLINREG_STATE,GuppiSlinregStateClass))
#define GUPPI_IS_SLINREG_STATE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_SLINREG_STATE))
#define GUPPI_IS_SLINREG_STATE0(obj) (((obj) == NULL) || (GUPPI_IS_SLINREG_STATE(obj)))
#define GUPPI_IS_SLINREG_STATE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_SLINREG_STATE))

GtkType guppi_slinreg_state_get_type (void);

GuppiElementState *guppi_slinreg_state_new (void);

GuppiSeqScalar *guppi_slinreg_state_x_data (GuppiSlinregState *);
GuppiSeqScalar *guppi_slinreg_state_y_data (GuppiSlinregState *);
#define guppi_slinreg_state_line_width(x) ((x)->line_width)
#define guppi_slinreg_state_line_color(x) ((x)->line_color)
#define guppi_slinreg_state_show_label(x) ((x)->show_label)
#define guppi_slinreg_state_label_font(x) ((x)->label_font)
#define guppi_slinreg_state_label_size(x) ((x)->label_size)
#define guppi_slinreg_state_label_color(x) ((x)->label_color)

void guppi_slinreg_state_set_x_data (GuppiSlinregState *, GuppiSeqScalar *);
void guppi_slinreg_state_set_y_data (GuppiSlinregState *, GuppiSeqScalar *);
void guppi_slinreg_state_set_line_width (GuppiSlinregState *, double);
void guppi_slinreg_state_set_line_color (GuppiSlinregState *, guint32);
void guppi_slinreg_state_set_show_label (GuppiSlinregState *, gboolean);
void guppi_slinreg_state_set_label_font (GuppiSlinregState *, GnomeFont *);
void guppi_slinreg_state_set_label_size (GuppiSlinregState *, double);
void guppi_slinreg_state_set_label_color (GuppiSlinregState *, guint32);

gchar *guppi_slinreg_state_label (GuppiSlinregState *);

END_GUPPI_DECLS

#endif /* _INC_GUPPI_SLINREG_STATE_H */

/* $Id$ */
