/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-slinreg-state.h"
#include "guppi-slinreg-view.h"
#include "guppi-slinreg-item.h"
#include "guppi-slinreg-print.h"

static GtkObjectClass *parent_class = NULL;

enum {
  ARG_0
};

static void
guppi_slinreg_view_get_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_slinreg_view_set_arg (GtkObject * obj, GtkArg * arg, guint arg_id)
{
  switch (arg_id) {

  default:
    break;
  };
}

static void
guppi_slinreg_view_destroy (GtkObject * obj)
{
  if (parent_class->destroy)
    parent_class->destroy (obj);
}

static void
guppi_slinreg_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiCanvasItem *
make_canvas_item (GuppiElementView * view, GnomeCanvas * canvas,
		  GnomeCanvasGroup * group)
{
  GnomeCanvasItem *gci;

  gci = gnome_canvas_item_new (group, GUPPI_TYPE_SLINREG_ITEM, NULL);

  return GUPPI_CANVAS_ITEM (gci);
}

static void
state_changed (GuppiElementView * view)
{

}

/***************************************************************************/

static void
guppi_slinreg_view_class_init (GuppiSlinregViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->get_arg = guppi_slinreg_view_get_arg;
  object_class->set_arg = guppi_slinreg_view_set_arg;
  object_class->destroy = guppi_slinreg_view_destroy;
  object_class->finalize = guppi_slinreg_view_finalize;

  view_class->make_canvas_item = make_canvas_item;
  view_class->print_type = GUPPI_TYPE_SLINREG_PRINT;
  view_class->state_changed = state_changed;
}

static void
guppi_slinreg_view_init (GuppiSlinregView * obj)
{

}

GtkType guppi_slinreg_view_get_type (void)
{
  static GtkType guppi_slinreg_view_type = 0;
  if (!guppi_slinreg_view_type) {
    static const GtkTypeInfo guppi_slinreg_view_info = {
      "GuppiSlinregView",
      sizeof (GuppiSlinregView),
      sizeof (GuppiSlinregViewClass),
      (GtkClassInitFunc) guppi_slinreg_view_class_init,
      (GtkObjectInitFunc) guppi_slinreg_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_slinreg_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_slinreg_view_info);
  }
  return guppi_slinreg_view_type;
}


/* $Id$ */
