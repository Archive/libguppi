/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-slinreg-print.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <guppi-memory.h>
#include "guppi-slinreg-common.h"
#include "guppi-slinreg-print.h"
#include "guppi-slinreg-state.h"
#include "guppi-slinreg-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_slinreg_print_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
print (GuppiElementPrint * ep)
{
  GuppiSlinregState *state;
  GuppiElementView *view;
  GuppiSimpleLinreg *slr;
  GnomeFont *font;
  double w, m, b, vx0, vy0, vx1, vy1, x0, y0, x1, y1, x, y;

  state = GUPPI_SLINREG_STATE (guppi_element_print_state (ep));
  view = guppi_element_print_view (ep);

  slr = state->slr;
  if (slr == NULL || !slr->valid)
    return;

  m = guppi_simple_linreg_slope (slr);
  b = guppi_simple_linreg_intercept (slr);

  guppi_element_print_get_bbox_vp (ep, &vx0, &vy0, &vx1, &vy1);

  w = guppi_slinreg_state_line_width (state);
  guppi_element_print_setlinewidth (ep, w);

  guppi_element_print_setrgbacolor_uint (ep,
					 guppi_slinreg_state_line_color
					 (state));

  guppi_element_print_newpath (ep);

  x0 = vx0 - (vx1 - vx0) / 3;
  y0 = m * x0 + b;
  guppi_element_print_moveto_vp (ep, x0, y0);

  x1 = vx1 + (vx1 - vx0) / 3;
  y1 = m * x1 + b;
  guppi_element_print_lineto_vp (ep, x1, y1);

  guppi_element_print_stroke (ep);

  if (guppi_slinreg_state_show_label (state)) {
    gchar *label = guppi_slinreg_state_label (state);
    double theta, lw, lh;
    double matrix[6];

    guppi_element_print_setrgbacolor_uint (ep,
					   guppi_slinreg_state_label_color
					   (state));

    font = guppi_slinreg_state_label_font (state);
    guppi_element_print_setfont (ep, font);

    lw = gnome_font_get_width_string (font, label);
    lh = gnome_font_get_ascender (font);

    /* Find center point */
    guppi_slinreg_segment_endpoints (m, b, vx0, vy0, vx1, vy1,
				     &x0, &y0, &x1, &y1);

    x = (x0 + x1) / 2;
    y = (y0 + y1) / 2;

    if (vx0 <= x && x <= vx1 && vy0 <= y && y <= vy1) {

      guppi_element_print_vp2pt_auto (ep, &x, &y);
      guppi_element_print_vp2pt_auto (ep, &x0, &y0);
      guppi_element_print_vp2pt_auto (ep, &x1, &y1);

      art_affine_translate (matrix, x, y);
      guppi_element_print_concat (ep, matrix);

      theta = 180 * atan2 (y1 - y0, x1 - x0) / M_PI;
      art_affine_rotate (matrix, theta);
      guppi_element_print_concat (ep, matrix);

      /* We add a 32nd of an inch for good luck... */
      guppi_element_print_moveto (ep, -lw / 2, -(lh + w / 2 + 1.0 / 32));

      guppi_element_print_show (ep, label);
    }

    guppi_free (label);
  }
}

static void
guppi_slinreg_print_class_init (GuppiSlinregPrintClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementPrintClass *ep_class = GUPPI_ELEMENT_PRINT_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_PRINT);

  object_class->finalize = guppi_slinreg_print_finalize;
  ep_class->print = print;
}

static void
guppi_slinreg_print_init (GuppiSlinregPrint * obj)
{

}

GtkType guppi_slinreg_print_get_type (void)
{
  static GtkType guppi_slinreg_print_type = 0;
  if (!guppi_slinreg_print_type) {
    static const GtkTypeInfo guppi_slinreg_print_info = {
      "GuppiSlinregPrint",
      sizeof (GuppiSlinregPrint),
      sizeof (GuppiSlinregPrintClass),
      (GtkClassInitFunc) guppi_slinreg_print_class_init,
      (GtkObjectInitFunc) guppi_slinreg_print_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_slinreg_print_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_PRINT, &guppi_slinreg_print_info);
  }
  return guppi_slinreg_print_type;
}

/* $Id$ */
