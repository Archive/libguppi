/* $Id$ */

/*
 * guppi-slinreg-statviewer.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>

#include <gtk/gtklabel.h>
#include <gtk/gtksignal.h>

#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-config.h>
#include <libgnome/gnome-i18n.h>

#include <glade/glade.h>
#include <gnan.h>
#include <guppi-convenient.h>
#include <guppi-paths.h>
#include "guppi-slinreg-statviewer.h"

static void
sv_set (GtkWidget * sv, gboolean valid, const gchar * entry,
	const gchar * format, double x)
{
  gchar buffer[32];
  GladeXML *glade_xml;
  GtkWidget *w;

  g_return_if_fail (sv != NULL);
  g_return_if_fail (entry != NULL);

  glade_xml = (GladeXML *) gtk_object_get_data (GTK_OBJECT (sv), "glade_xml");
  g_return_if_fail (glade_xml != NULL);

  w = glade_xml_get_widget (glade_xml, entry);
  g_return_if_fail (w != NULL);
  g_return_if_fail (GTK_IS_LABEL (w));

  if (g_isnan (x) || !valid)
    gtk_label_set_text (GTK_LABEL (w), _("n/a"));
  else {
    g_snprintf (buffer, 32, format ? format : "%g", x);
    gtk_label_set_text (GTK_LABEL (w), buffer);
  }
}

static void
guppi_slinreg_statviewer_update (GtkWidget * w)
{
  GuppiSimpleLinreg *slr;
  gboolean valid;

  g_return_if_fail (w != NULL);

  slr = GUPPI_SIMPLE_LINREG (gtk_object_get_data (GTK_OBJECT (w), "slr"));
  g_return_if_fail (slr != NULL);

  valid = slr->valid;

  sv_set (w, valid, "model_ss", NULL, slr->ess);
  sv_set (w, valid, "model_df", NULL, 1);
  sv_set (w, valid, "model_ms", NULL, slr->ess);
  sv_set (w, valid, "residual_ss", NULL, slr->rss);
  sv_set (w, valid, "residual_df", NULL, slr->count - 2);
  sv_set (w, valid, "residual_ms", NULL, slr->rss / (slr->count - 2));
  sv_set (w, valid, "total_ss", NULL, slr->tss_y);
  sv_set (w, valid, "total_df", NULL, slr->count - 1);
  sv_set (w, valid, "total_ms", NULL, slr->tss_y / (slr->count - 1));

  sv_set (w, valid, "number_of_obs", NULL, slr->count);
  sv_set (w, valid, "F", NULL, slr->F);
  sv_set (w, valid, "prob_F", NULL, slr->p);
  sv_set (w, valid, "R_square", NULL, slr->Rsq);
  sv_set (w, valid, "adj_R_square", NULL, slr->adj_Rsq);
  sv_set (w, valid, "root_mse", NULL, slr->residual_sdev);

  sv_set (w, valid, "y_mean", NULL, slr->mean_y);

  sv_set (w, valid, "x_coefficient", NULL, slr->slope);
  sv_set (w, valid, "x_std_error", NULL, slr->slope_serr);
  sv_set (w, valid, "x_t", NULL, slr->slope_t);
  sv_set (w, valid, "x_prob_t", NULL, slr->slope_p);
  sv_set (w, valid, "x_mean", NULL, slr->mean_x);

  sv_set (w, valid, "constant_coefficient", NULL, slr->intercept);
  sv_set (w, valid, "constant_std_error", NULL, slr->intercept_serr);
  sv_set (w, valid, "constant_t", NULL, slr->intercept_t);
  sv_set (w, valid, "constant_prob_t", NULL, slr->intercept_p);
  sv_set (w, valid, "constant_mean", NULL, 1);
}

/* Clean up attached data */
static void
destroy_cb (GtkWidget * w)
{
  gpointer p;

  p = gtk_object_get_data (GTK_OBJECT (w), "slr");
  if (p) {
    gtk_signal_disconnect_by_data (GTK_OBJECT (p), w);
    guppi_unref (p);
  }
}

GtkWidget *
guppi_slinreg_statviewer_new (GuppiSimpleLinreg * slr)
{
  const gchar *path;
  GladeXML *glade_xml;
  GtkWidget *w;

  path = guppi_glade_path ("slinreg-statviewer.glade");
  glade_xml = path ? glade_xml_new (path, "main_vbox") : NULL;

  if (glade_xml == NULL)
    g_error ("Couldn't find \"slinreg-statviewer.glade\"");

  w = glade_xml_get_widget (glade_xml, "main_vbox");
  gtk_object_set_data (GTK_OBJECT (w), "glade_xml", glade_xml);
  gtk_object_set_data (GTK_OBJECT (w), "slr", slr);

  gtk_signal_connect_object (GTK_OBJECT (slr),
			     "changed",
			     GTK_SIGNAL_FUNC
			     (guppi_slinreg_statviewer_update),
			     GTK_OBJECT (w));
  gtk_signal_connect (GTK_OBJECT (w), "destroy", GTK_SIGNAL_FUNC (destroy_cb),
		      NULL);

  guppi_ref (slr);

  guppi_slinreg_statviewer_update (w);

  return w;
}

/* $Id$ */
