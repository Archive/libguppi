/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-item.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-linechart-item.h"

#include <math.h>
#include <guppi-memory.h>
#include <guppi-rgb.h>
#include "guppi-linechart-state.h"
#include "guppi-linechart-view.h"


static GtkObjectClass *parent_class = NULL;

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *clip_path, gint flags)
{

}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiElementState *state = guppi_canvas_item_state (gci);
  GuppiLinechartState *lc_state = GUPPI_LINECHART_STATE (state);

  GuppiElementView *view = guppi_canvas_item_view (gci);
  GuppiLinechartView *lc_view = GUPPI_LINECHART_VIEW (view);

  double x0, y0, x1, y1;
  gint i, ix0, ix1, i0, i1;

  guppi_element_view_get_bbox_vp (view, &x0, &y0, &x1, &y1);
  ix0 = (gint) floor (x0);
  ix1 = (gint) ceil (x1);

  guppi_linechart_state_get_node_bounds (lc_state, &i0, &i1);

  i0 = MAX (i0, ix0);
  i1 = MIN (i1, ix1);
  
  if (i0 >= i1)
    return;

  for (i = i0; i < i1; ++i) {
    double px0, py0, px1, py1;
    double cx0, cy0, cx1, cy1;

    guppi_linechart_view_node_position (lc_view, i, &px0, &py0, NULL, NULL);
    guppi_linechart_view_node_position (lc_view, i+1, &px1, &py1, NULL, NULL);
    
    guppi_canvas_item_pt2c_d (gci, px0, py0, &cx0, &cy0);
    guppi_canvas_item_pt2c_d (gci, px1, py1, &cx1, &cy1);

    guppi_paint_wide_line (buf, cx0, cy0, cx1, cy1, 1.0, RGBA_RED);
  }
  
}

static void
guppi_linechart_item_finalize (GtkObject *obj)
{
  GuppiLinechartItem *x = GUPPI_LINECHART_ITEM(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_linechart_item_class_init (GuppiLinechartItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_linechart_item_finalize;

  gci_class->guppi_update = update;
  gci_class->guppi_render = render;

}

static void
guppi_linechart_item_init (GuppiLinechartItem *obj)
{

}

GtkType
guppi_linechart_item_get_type (void)
{
  static GtkType guppi_linechart_item_type = 0;
  if (!guppi_linechart_item_type) {
    static const GtkTypeInfo guppi_linechart_item_info = {
      "GuppiLinechartItem",
      sizeof (GuppiLinechartItem),
      sizeof (GuppiLinechartItemClass),
      (GtkClassInitFunc)guppi_linechart_item_class_init,
      (GtkObjectInitFunc)guppi_linechart_item_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_linechart_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM, &guppi_linechart_item_info);
  }
  return guppi_linechart_item_type;
}
