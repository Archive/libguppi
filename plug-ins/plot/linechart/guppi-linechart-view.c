/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-view.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-linechart-view.h"

#include <guppi-memory.h>
#include <guppi-seq-scalar.h>
#include "guppi-linechart-state.h"
#include "guppi-linechart-item.h"
#include "guppi-linechart-print.h"

static GtkObjectClass *parent_class = NULL;

static gboolean
preferred_range (GuppiElementView *view, guppi_axis_t ax, double *a, double *b)
{
  if (ax == GUPPI_X_AXIS) {
    gint i0, i1;
    guppi_linechart_state_get_node_bounds ((GuppiLinechartState *) guppi_element_view_state (view), &i0, &i1);
    if (a)
      *a = i0;
    if (b)
      *b = i1+1;
    return TRUE;
  }

  if (ax == GUPPI_Y_AXIS) {
    GuppiSeqScalar *seq;
    double min, max;
    guppi_element_state_get (guppi_element_view_state (view), "data", &seq, NULL);
    if (! seq)
      return FALSE;

    min = guppi_seq_scalar_min (seq);
    max = guppi_seq_scalar_max (seq);

    if (a)
      *a = MIN (min, 0.0);
    if (b)
      *b = max;

    return TRUE;
  }

  return FALSE;
}

static void
guppi_linechart_view_finalize (GtkObject *obj)
{
  GuppiLinechartView *x = GUPPI_LINECHART_VIEW(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_linechart_view_class_init (GuppiLinechartViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);
  
  object_class->finalize = guppi_linechart_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_LINECHART_ITEM;
  view_class->print_type = GUPPI_TYPE_LINECHART_PRINT;

  view_class->preferred_range = preferred_range;
}

static void
guppi_linechart_view_init (GuppiLinechartView *obj)
{

}

GtkType
guppi_linechart_view_get_type (void)
{
  static GtkType guppi_linechart_view_type = 0;
  if (!guppi_linechart_view_type) {
    static const GtkTypeInfo guppi_linechart_view_info = {
      "GuppiLinechartView",
      sizeof (GuppiLinechartView),
      sizeof (GuppiLinechartViewClass),
      (GtkClassInitFunc)guppi_linechart_view_class_init,
      (GtkObjectInitFunc)guppi_linechart_view_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_linechart_view_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_linechart_view_info);
  }
  return guppi_linechart_view_type;
}

gboolean
guppi_linechart_view_node_position (GuppiLinechartView *lc_view,
				    gint i,
				    double *x, double *y,
				    GuppiMarker *marker, guint32 *color)
{
  GuppiLinechartState *lc_state;
  double val;

  g_return_val_if_fail (GUPPI_IS_LINECHART_VIEW (lc_view), FALSE);

  lc_state = GUPPI_LINECHART_STATE (guppi_element_view_state (GUPPI_ELEMENT_VIEW (lc_view)));

  if (! guppi_linechart_state_get_node_properties (lc_state, i, &val, marker, color))
    return FALSE;

  if (x || y)
    guppi_element_view_vp2pt (GUPPI_ELEMENT_VIEW (lc_view), i+0.5, val, x, y);

  return TRUE;
}
