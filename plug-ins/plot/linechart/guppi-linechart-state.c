/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-linechart-state.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-linechart-state.h"

#include <guppi-memory.h>
#include <guppi-data-socket.h>
#include <guppi-data-flavor.h>
#include <guppi-seq-scalar.h>
#include <guppi-rgb.h>
#include <guppi-marker.h>
#include <guppi-linechart-view.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_linechart_state_finalize (GtkObject *obj)
{
  GuppiLinechartState *x = GUPPI_LINECHART_STATE(obj);

  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_linechart_state_class_init (GuppiLinechartStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_linechart_state_finalize;

  state_class->name = _("Line Chart");
  state_class->view_type = GUPPI_TYPE_LINECHART_VIEW;
}

static void
guppi_linechart_state_init (GuppiLinechartState *obj)
{
  GuppiAttributeBag *bag;

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "data::socket::adopt", NULL, guppi_data_socket_new ());
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "labels::socket::adopt", NULL, guppi_data_socket_new ());

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,    "fallback_color", NULL, RGBA_BLUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN, "drop_fill",      NULL, FALSE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT,     "marker",         NULL, GUPPI_MARKER_NONE);
}

GtkType
guppi_linechart_state_get_type (void)
{
  static GtkType guppi_linechart_state_type = 0;
  if (!guppi_linechart_state_type) {
    static const GtkTypeInfo guppi_linechart_state_info = {
      "GuppiLinechartState",
      sizeof (GuppiLinechartState),
      sizeof (GuppiLinechartStateClass),
      (GtkClassInitFunc)guppi_linechart_state_class_init,
      (GtkObjectInitFunc)guppi_linechart_state_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_linechart_state_type = gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_linechart_state_info);
  }
  return guppi_linechart_state_type;
}

GuppiElementState *
guppi_linechart_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_linechart_state_get_type ()));
}

gboolean
guppi_linechart_state_get_node_bounds (GuppiLinechartState *state,
				       gint *i0, gint *i1)
{
  GuppiSeq *seq;

  g_return_val_if_fail (GUPPI_IS_LINECHART_STATE (state), FALSE);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "data", &seq,
			   NULL);

  guppi_seq_bounds (seq, i0, i1);
  guppi_unref (seq);
  return TRUE;
}

gboolean
guppi_linechart_state_get_node_properties (GuppiLinechartState *state,
					   gint i,
					   double *value,
					   GuppiMarker *marker,
					   guint32 *color)
{
  GuppiSeqScalar *seq;
  GuppiMarker our_marker;
  guint32 our_color;

  g_return_val_if_fail (GUPPI_IS_LINECHART_STATE (state), FALSE);

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "data", &seq,
			   "marker", &our_marker,
			   "fallback_color", &our_color,
			   NULL);

  if (value)
    *value = guppi_seq_scalar_get (seq, i);

  if (marker)
    *marker = our_marker;

  if (color)
    *color = our_marker;

  guppi_unref (seq);

  return TRUE;
}
