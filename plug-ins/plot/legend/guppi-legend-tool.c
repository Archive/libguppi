/* $Id$ */

/*
 * guppi-legend-tool.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include <guppi-memory.h>
#include <guppi-metrics.h>
#include "guppi-legend-tool.h"
#include "guppi-legend-state.h"
#include "guppi-legend-view.h"
#include "guppi-legend-item.h"


static void
box_callback_cb (GuppiPlotTool * tool, GuppiCanvasItem * gci)
{
  gint box;

  /* If the pointer is inside of a slice, execute the callback
     with the slice # as an argument. */
  if (guppi_legend_item_in_box (GUPPI_LEGEND_ITEM (gci),
				tool->raw_x, tool->raw_y, &box)) {
    GuppiLegendBoxFunc func = tool->ptr_arg1;
    func (box, tool->ptr_arg2 /* == user data */ );
  }
}

GuppiPlotTool *
guppi_legend_tool_new_box_callback (GuppiLegendBoxFunc func,
				    const gchar * name, gpointer user_data)
{
  GuppiPlotTool *tool;

  g_return_val_if_fail (func != NULL, NULL);

  tool = guppi_plot_tool_new ();
  tool->name = guppi_strdup (name);
  tool->tracks_motion = FALSE;
  tool->repeating = FALSE;
  tool->first = box_callback_cb;
  tool->ptr_arg1 = (gpointer) func;
  tool->ptr_arg2 = user_data;

  return tool;
}




/* $Id$ */
