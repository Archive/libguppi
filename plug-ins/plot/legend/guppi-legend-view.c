/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-legend-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-legend-state.h"
#include "guppi-legend-view.h"
#include "guppi-legend-item.h"
#include "guppi-legend-print.h"
#include "guppi-legend-tool.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_legend_view_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static GuppiCanvasItem *
make_canvas_item (GuppiElementView * view, GnomeCanvas * canvas,
		  GnomeCanvasGroup * group)
{
  GnomeCanvasItem *gnoci;
  GuppiCanvasItem *gci;

  gnoci = gnome_canvas_item_new (group, GUPPI_TYPE_LEGEND_ITEM, NULL);

  gci = GUPPI_CANVAS_ITEM (gnoci);

  return gci;
}

static void
view_init (GuppiElementView *view)
{
  GuppiLegendState *state = GUPPI_LEGEND_STATE (guppi_element_view_state (view));
  guppi_element_view_changed_size (view,
				   guppi_legend_state_natural_width (state),
				   guppi_legend_state_natural_height (state));

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->view_init)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->view_init (view);
}

static void
changed_state (GuppiElementView *view)
{
  GuppiLegendState *state = GUPPI_LEGEND_STATE (guppi_element_view_state (view));
  guppi_element_view_changed_size (view,
				   guppi_legend_state_natural_width (state),
				   guppi_legend_state_natural_height (state));

  if (GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state)
    GUPPI_ELEMENT_VIEW_CLASS (parent_class)->changed_state (view);
}

/***************************************************************************/

static void
guppi_legend_view_class_init (GuppiLegendViewClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_legend_view_finalize;

  view_class->make_canvas_item = make_canvas_item;
  view_class->print_type       = GUPPI_TYPE_LEGEND_PRINT;
  view_class->view_init        = view_init;
  view_class->changed_state    = changed_state;
}

static void
guppi_legend_view_init (GuppiLegendView *obj)
{
  /* GuppiElementView *ev = GUPPI_ELEMENT_VIEW (obj); */
}

GtkType guppi_legend_view_get_type (void)
{
  static GtkType guppi_legend_view_type = 0;
  if (!guppi_legend_view_type) {
    static const GtkTypeInfo guppi_legend_view_info = {
      "GuppiLegendView",
      sizeof (GuppiLegendView),
      sizeof (GuppiLegendViewClass),
      (GtkClassInitFunc) guppi_legend_view_class_init,
      (GtkObjectInitFunc) guppi_legend_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_legend_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_legend_view_info);
  }
  return guppi_legend_view_type;
}

/* $Id$ */
