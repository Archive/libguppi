/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-state.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-boxplot-state.h"

#include <guppi-rgb.h>
#include <guppi-memory.h>
#include <guppi-metrics.h>
#include <guppi-data-flavor.h>
#include <guppi-data-socket.h>
#include "guppi-boxplot-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_boxplot_state_finalize (GtkObject * obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

GuppiElementState *
guppi_boxplot_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_boxplot_state_get_type ()));
}

void
guppi_boxplot_state_prepare_stats (GuppiBoxplotState *state)
{
  GuppiSeqScalar *sd = NULL;
  g_return_if_fail (GUPPI_IS_BOXPLOT_STATE (state));

  if (! state->stats_ready) {

    guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			     "data", &sd,
			     NULL);

    if (sd != NULL && guppi_seq_size (GUPPI_SEQ (sd)) > 0) {
      state->md = guppi_seq_scalar_median (sd);
      state->q1 = guppi_seq_scalar_q1 (sd);
      state->q3 = guppi_seq_scalar_q3 (sd);
      state->whisker_lower = guppi_seq_scalar_percentile (sd, 0.05);
      state->whisker_upper = guppi_seq_scalar_percentile (sd, 0.95);
      state->stats_ready = TRUE;
    }
    
  }
}

void
guppi_boxplot_state_get_size (GuppiBoxplotState *state, double *w, double *h)
{
  double thickness, box_size, tick_size, sz;
  gboolean horizontal;

  g_return_if_fail (GUPPI_IS_BOXPLOT_STATE (state));

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "horizontal", &horizontal,
			   "line_thickness", &thickness,
			   "box_size", &box_size,
			   "tick_size", &tick_size,
			   NULL);

  sz = MAX (box_size + thickness, tick_size + thickness);

  if (horizontal) {
    if (w) *w = -1;
    if (h) *h = sz;
  } else {
    if (w) *w = sz;
    if (h) *h = -1;
  }
}

static void
guppi_boxplot_state_class_init (GuppiBoxplotStateClass * klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_STATE);

  object_class->finalize = guppi_boxplot_state_finalize;

  state_class->view_type = GUPPI_TYPE_BOXPLOT_VIEW;
}

static void
bag_changed (GuppiAttributeBag *bag, const gchar *key, gpointer closure)
{
  GuppiBoxplotState *bps = GUPPI_BOXPLOT_STATE (closure);

  if (!strcmp (key, "data")) {
    bps->stats_ready = FALSE;
  } else if (!strcmp (key, "line_thickness")
	     || !strcmp (key, "box_size")
	     || !strcmp (key, "tick_size")
	     || !strcmp (key, "horizontal")) {
    double w, h;

    guppi_boxplot_state_get_size (bps, &w, &h);
    guppi_element_state_changed_size (GUPPI_ELEMENT_STATE (bps), w, h);
  }
}



static void
guppi_boxplot_state_init (GuppiBoxplotState *obj)
{
  GuppiAttributeBag *bag;
  double inch = guppi_in2pt (1.0);

  bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DATA_SOCKET, "data::socket::adopt", NULL,
					guppi_data_socket_new_by_type (GUPPI_TYPE_SEQ_SCALAR));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,  "horizontal",      NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DOUBLE,   "iqr_factor",      NULL, 0.2);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,     "frame_color",     NULL, RGBA_BLACK);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION,"line_thickness",  NULL, inch / 64);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_BOOLEAN,   "fill_box",       NULL, TRUE);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_RGBA,      "box_color",      NULL, RGBA_YELLOW);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "box_size",       NULL, inch / 8);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "tick_size",      NULL, inch / 12);

  gtk_signal_connect (GTK_OBJECT (bag),
		      "changed",
		      GTK_SIGNAL_FUNC (bag_changed),
		      obj);

  obj->stats_ready = FALSE;
}

GtkType
guppi_boxplot_state_get_type (void)
{
  static GtkType guppi_boxplot_state_type = 0;
  if (!guppi_boxplot_state_type) {
    static const GtkTypeInfo guppi_boxplot_state_info = {
      "GuppiBoxplotState",
      sizeof (GuppiBoxplotState),
      sizeof (GuppiBoxplotStateClass),
      (GtkClassInitFunc) guppi_boxplot_state_class_init,
      (GtkObjectInitFunc) guppi_boxplot_state_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_boxplot_state_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_STATE, &guppi_boxplot_state_info);
  }

  return guppi_boxplot_state_type;
}

/* $Id$ */
