/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-item.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <unistd.h>
#include <fcntl.h>
#include <guppi-convenient.h>
#include <guppi-metrics.h>
#include <guppi-debug.h>
#include "guppi-boxplot-item.h"
#include "guppi-boxplot-state.h"

#include <libart_lgpl/libart.h>

static GtkObjectClass *parent_class = NULL;

static void
guppi_boxplot_item_finalize (GtkObject *obj)
{
  GuppiBoxplotItem *item = GUPPI_BOXPLOT_ITEM (obj);

  if (item->box_svp) {
    art_svp_free (item->box_svp);
    item->box_svp = NULL;
  }

  if (item->frame_svp) {
    art_svp_free (item->frame_svp);
    item->frame_svp = NULL;
  }

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

/***************************************************************************/

static void
update (GuppiCanvasItem *gci, double aff[6], ArtSVP *clip_path, gint flags)
{
  GuppiBoxplotItem *bpi;
  GuppiBoxplotState *state;
  ArtVpath path[16];
  ArtVpath boxpath[6];
  double t0, ta, tm, tb, t1;
  double md, q1, q3, iqr, wu, wl;
  double tsize, bsize, lthick;
  gint i, j;
  double scale;
  gint cx0, cy0, cx1, cy1;
  gboolean horizontal, fill_box;

  bpi = GUPPI_BOXPLOT_ITEM (gci);
  state = GUPPI_BOXPLOT_STATE (guppi_canvas_item_state (gci));
  scale = guppi_canvas_item_scale (gci);

  if (! state->stats_ready) {
    guppi_boxplot_state_prepare_stats (state);
  }

  if (! state->stats_ready) {
    if (bpi->box_svp) {
      art_svp_free (bpi->box_svp);
      bpi->box_svp = NULL;
    }
    if (bpi->frame_svp) {
      art_svp_free (bpi->frame_svp);
      bpi->frame_svp = NULL;
    }
    return;
  }

  guppi_element_state_get (GUPPI_ELEMENT_STATE (state),
			   "horizontal", &horizontal,
			   "tick_size", &tsize,
			   "box_size", &bsize,
			   "line_thickness", &lthick,
			   "fill_box", &fill_box,
			   NULL);
  tsize *= scale;
  bsize *= scale;
  lthick *= scale;

  guppi_canvas_item_get_bbox_c (gci, &cx0, &cy0, &cx1, &cy1);

  if (horizontal) {
    guppi_canvas_item_vp2c_d (gci, state->md, 0, &md, NULL);
    guppi_canvas_item_vp2c_d (gci, state->q1, 0, &q1, NULL);
    guppi_canvas_item_vp2c_d (gci, state->q3, 0, &q3, NULL);
    guppi_canvas_item_vp2c_d (gci, state->whisker_lower, 0, &wl, NULL);
    guppi_canvas_item_vp2c_d (gci, state->whisker_upper, 0, &wu, NULL);
    tsize = guppi_y_pt2px (tsize);
    bsize = guppi_y_pt2px (bsize);
    t0 = (cy0 + cy1) / 2.0 - bsize / 2;
    t1 = (cy0 + cy1) / 2.0 + bsize / 2;
  } else {
    guppi_canvas_item_vp2c_d (gci, 0, state->md, NULL, &md);
    guppi_canvas_item_vp2c_d (gci, 0, state->q1, NULL, &q1);
    guppi_canvas_item_vp2c_d (gci, 0, state->q3, NULL, &q3);
    guppi_canvas_item_vp2c_d (gci, 0, state->whisker_lower, NULL, &wl);
    guppi_canvas_item_vp2c_d (gci, 0, state->whisker_upper, NULL, &wu);

    tsize = guppi_x_pt2px (tsize);
    bsize = guppi_x_pt2px (bsize);
    t0 = (cx0 + cx1) / 2.0 - bsize / 2;
    t1 = (cx0 + cx1) / 2.0 + bsize / 2;
  }

  /* This is rather abusive */
  if (! horizontal) {
    double tmp = cx0;
    cx0 = cy0;
    cy0 = cx0;
    tmp = cx1;
    cx1 = cy1;
    cy1 = tmp;
  }

  lthick = guppi_pt2px (lthick);

  iqr = q3 - q1;

  tm = (t0 + t1) / 2;
  ta = tm - tsize / 2;
  tb = tm + tsize / 2;

  i = 0;
  j = 0;

  /* Draw low-end tick */
  if (guppi_between (cx0, wl, cx1)) {
    path[i].code = ART_MOVETO;
    path[i].x = wl;
    path[i].y = ta;
    ++i;

    path[i].code = ART_LINETO;
    path[i].x = wl;
    path[i].y = tb;
    ++i;
  }

  /* Draw low-end whisker */
  if (MAX (cx0, MIN (wl, q1)) < MIN (cx1, MAX (wl, q1))) {
    path[i].code = ART_MOVETO;
    path[i].x = wl;
    path[i].y = tm;
    ++i;

    path[i].code = ART_LINETO;
    path[i].x = q1;
    path[i].y = tm;
    ++i;
  }

  /* Draw IQR frame */
  if (MAX (cx0, MIN (q1, q3)) < MIN (cx1, MAX (q1, q3))) {

    path[i].code = ART_MOVETO;
    boxpath[j].code = ART_MOVETO;
    path[i].x = boxpath[j].x = q1;
    path[i].y = boxpath[j].y = t1;
    ++i;
    ++j;

    path[i].code = boxpath[j].code = ART_LINETO;
    path[i].x = boxpath[j].x = q1;
    path[i].y = boxpath[j].y = t0;
    ++i;
    ++j;

    path[i].code = boxpath[j].code = ART_LINETO;
    path[i].x = boxpath[j].x = q3;
    path[i].y = boxpath[j].y = t0;
    ++i;
    ++j;

    path[i].code = boxpath[j].code = ART_LINETO;
    path[i].x = boxpath[j].x = q3;
    path[i].y = boxpath[j].y = t1;
    ++i;
    ++j;

    path[i].code = boxpath[j].code = ART_LINETO;
    path[i].x = boxpath[j].x = q1;
    path[i].y = boxpath[j].y = t1;
    ++i;
    ++j;
  }

  boxpath[j].code = ART_END;

  /* Draw median line */
  if (guppi_between (cx0, md, cx1)) {
    path[i].code = ART_MOVETO;
    path[i].x = md;
    path[i].y = t0;
    ++i;

    path[i].code = ART_LINETO;
    path[i].x = md;
    path[i].y = t1;
    ++i;
  }

  /* Draw high-end tick */

  if (guppi_between (cx0, wu, cx1)) {
    path[i].code = ART_MOVETO;
    path[i].x = wu;
    path[i].y = ta;
    ++i;

    path[i].code = ART_LINETO;
    path[i].x = wu;
    path[i].y = tb;
    ++i;
  }

  /* Draw high-end whisker */

  if (MAX (cx0, MIN (wu, q3)) < MIN (cx1, MAX (wu, q3))) {
    path[i].code = ART_MOVETO;
    path[i].x = wu;
    path[i].y = tm;
    ++i;

    path[i].code = ART_LINETO;
    path[i].x = q3;
    path[i].y = tm;
    ++i;
  }

  path[i].code = ART_END;
  path[i].x = path[i].y = 0;

  /* Clamp coordinates to be not too far off of the visible area.
     (This can cause guppi to crash at extreme zoom levels) */
  for (i = 0; path[i].code != ART_END; ++i) {
    path[i].x = CLAMP (path[i].x, cx0 - 10, cx1 + 10);
    path[i].y = CLAMP (path[i].y, cy0 - 10, cy1 + 10);
  }
  for (i = 0; boxpath[i].code != ART_END; ++i) {
    boxpath[i].x = CLAMP (boxpath[i].x, cx0 - 10, cx1 + 10);
    boxpath[i].y = CLAMP (boxpath[i].y, cy0 - 10, cy1 + 10);
  }

  /* Transpose path coordinates if not horizontal */
  if (! horizontal) {
    for (i = 0; path[i].code != ART_END; ++i) {
      tm = path[i].x;
      path[i].x = path[i].y;
      path[i].y = tm;
    }

    for (i = 0; boxpath[i].code != ART_END; ++i) {
      tm = boxpath[i].x;
      boxpath[i].x = boxpath[i].y;
      boxpath[i].y = tm;
    }
  }

  if (bpi->box_svp) {
    art_svp_free (bpi->box_svp);
    bpi->box_svp = NULL;
  }

  if (bpi->frame_svp) {
    art_svp_free (bpi->frame_svp);
    bpi->frame_svp = NULL;
  }

  {
    /* This is *so* abusive; but libart_lgpl spews those damn
       colinear warning messages everywhere... */
    static int devnull = -1;
    static int stdout_dup = -1;

    if (!guppi_is_very_verbose ()) {
      if (devnull < 0)
	devnull = open ("/dev/null", O_WRONLY);
      if (stdout_dup < 0)
	stdout_dup = dup (STDERR_FILENO);

      close (STDERR_FILENO);
      dup2 (devnull, STDERR_FILENO);
    }

    if (path[0].code != ART_END) {
      bpi->frame_svp = art_svp_vpath_stroke (path,
					     ART_PATH_STROKE_JOIN_ROUND,
					     ART_PATH_STROKE_CAP_ROUND,
					     lthick, 4, 0.25);
    }

    if (fill_box && boxpath[0].code != ART_END)
      bpi->box_svp = art_svp_from_vpath (boxpath);

    if (!guppi_is_very_verbose ()) {
      fflush (stdout);		/* This keeps FILE* buffering from screwing us up */
      dup2 (stdout_dup, STDERR_FILENO);
    }
  }
}

static void
render (GuppiCanvasItem *gci, GnomeCanvasBuf *buf)
{
  GuppiBoxplotItem *bpi;
  GuppiBoxplotState *state;
  guint32 box_color, frame_color;

  bpi = GUPPI_BOXPLOT_ITEM (gci);
  state = GUPPI_BOXPLOT_STATE (guppi_canvas_item_state (gci));

  guppi_element_state_get (guppi_canvas_item_state (gci),
			   "box_color", &box_color,
			   "frame_color", &frame_color,
			   NULL);

  if (state->stats_ready) {

    if (bpi->box_svp)
      gnome_canvas_render_svp (buf, bpi->box_svp, box_color);

    if (bpi->frame_svp) 
      gnome_canvas_render_svp (buf, bpi->frame_svp, frame_color);

  }
}

static gboolean
data_drop (GuppiCanvasItem *gci, GuppiData *data)
{
  if (data == NULL || GUPPI_IS_SEQ_SCALAR (data)) {
    guppi_element_state_set (guppi_canvas_item_state (gci),
			     "data", data);
    return TRUE;
  }

  return FALSE;
}


/***************************************************************************/

static void
guppi_boxplot_item_class_init (GuppiBoxplotItemClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiCanvasItemClass *gci_class = GUPPI_CANVAS_ITEM_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_CANVAS_ITEM);

  object_class->finalize = guppi_boxplot_item_finalize;

  gci_class->guppi_update = update;
  gci_class->guppi_render = render;
  gci_class->data_drop = data_drop;

  gci_class->uses_vp_coordinates = TRUE;
}

static void
guppi_boxplot_item_init (GuppiBoxplotItem *obj)
{

}

GtkType guppi_boxplot_item_get_type (void)
{
  static GtkType guppi_boxplot_item_type = 0;
  if (!guppi_boxplot_item_type) {
    static const GtkTypeInfo guppi_boxplot_item_info = {
      "GuppiBoxplotItem",
      sizeof (GuppiBoxplotItem),
      sizeof (GuppiBoxplotItemClass),
      (GtkClassInitFunc) guppi_boxplot_item_class_init,
      (GtkObjectInitFunc) guppi_boxplot_item_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_boxplot_item_type = gtk_type_unique (GUPPI_TYPE_CANVAS_ITEM,
					       &guppi_boxplot_item_info);
  }
  return guppi_boxplot_item_type;
}

/* $Id$ */
