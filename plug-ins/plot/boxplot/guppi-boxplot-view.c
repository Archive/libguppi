/* This is -*- C -*- */
/* $Id$ */

/*
 * guppi-boxplot-view.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org> and
 * Havoc Pennington <hp@pobox.com>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-boxplot-view.h"
#include "guppi-boxplot-item.h"
#include "guppi-boxplot-print.h"
#include "guppi-boxplot-state.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_boxplot_view_finalize (GtkObject *obj)
{
  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
view_init (GuppiElementView *view)
{
  GuppiBoxplotState *state = GUPPI_BOXPLOT_STATE (guppi_element_view_state (GUPPI_ELEMENT_VIEW (view)));
  double w, h;

  guppi_boxplot_state_get_size (state, &w, &h);
  guppi_element_view_changed_size (GUPPI_ELEMENT_VIEW (view), w, h);
}

static void
guppi_boxplot_view_class_init (GuppiBoxplotViewClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *) klass;
  GuppiElementViewClass *view_class = GUPPI_ELEMENT_VIEW_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_ELEMENT_VIEW);

  object_class->finalize = guppi_boxplot_view_finalize;

  view_class->canvas_item_type = GUPPI_TYPE_BOXPLOT_ITEM;
  view_class->print_type = GUPPI_TYPE_BOXPLOT_PRINT;
  view_class->view_init = view_init;
}

static void
guppi_boxplot_view_init (GuppiBoxplotView *obj)
{

}

GtkType guppi_boxplot_view_get_type (void)
{
  static GtkType guppi_boxplot_view_type = 0;
  if (!guppi_boxplot_view_type) {
    static const GtkTypeInfo guppi_boxplot_view_info = {
      "GuppiBoxplotView",
      sizeof (GuppiBoxplotView),
      sizeof (GuppiBoxplotViewClass),
      (GtkClassInitFunc) guppi_boxplot_view_class_init,
      (GtkObjectInitFunc) guppi_boxplot_view_init,
      NULL, NULL, (GtkClassInitFunc) NULL
    };
    guppi_boxplot_view_type =
      gtk_type_unique (GUPPI_TYPE_ELEMENT_VIEW, &guppi_boxplot_view_info);
  }
  return guppi_boxplot_view_type;
}

/* $Id$ */
