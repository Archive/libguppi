/* $Id$ */

/*
 * init.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-plot-plug-in.h>
#include "guppi-compass-box-state.h"

GuppiPlugIn *guppi_plug_in (void);

/*************************************************************************/

static GuppiElementState *
make_element (void)
{
  return guppi_compass_box_state_new ();
}

GuppiPlugIn *
guppi_plug_in (void)
{
  GuppiPlugIn *pi = guppi_plot_plug_in_new ();

  pi->magic_number = GUPPI_PLUG_IN_MAGIC_NUMBER;
  GUPPI_PLOT_PLUG_IN (pi)->element_constructor = make_element;

  /* Do any other necessary initialization here. */

  return pi;
}

/* $Id$ */
