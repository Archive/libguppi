/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-compass-box-state.c
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "guppi-compass-box-state.h"

#include <guppi-memory.h>
#include <guppi-enums.h>
#include "guppi-compass-box-view.h"

static GtkObjectClass *parent_class = NULL;

static void
guppi_compass_box_state_finalize (GtkObject *obj)
{
  guppi_finalized (obj);

  if (parent_class->finalize)
    parent_class->finalize (obj);
}

static void
guppi_compass_box_state_class_init (GuppiCompassBoxStateClass *klass)
{
  GtkObjectClass *object_class = (GtkObjectClass *)klass;
  GuppiElementStateClass *state_class = GUPPI_ELEMENT_STATE_CLASS (klass);

  parent_class = gtk_type_class (GUPPI_TYPE_GROUP_STATE);

  object_class->finalize = guppi_compass_box_state_finalize;

  state_class->view_type = GUPPI_TYPE_COMPASS_BOX_VIEW;
}

static void
guppi_compass_box_state_init (GuppiCompassBoxState *obj)
{
  GuppiAttributeBag *bag = guppi_element_state_attribute_bag (GUPPI_ELEMENT_STATE (obj));

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_INT,       "position",      NULL, GUPPI_NORTH);

  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "margin_left",   NULL, 0.0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "margin_right",  NULL, 0.0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "margin_top",    NULL, 0.0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "margin_bottom", NULL, 0.0);
  guppi_attribute_bag_add_with_default (bag, GUPPI_ATTR_DIMENSION, "margin_inner",  NULL, 0.0);
}

GtkType
guppi_compass_box_state_get_type (void)
{
  static GtkType guppi_compass_box_state_type = 0;
  if (!guppi_compass_box_state_type) {
    static const GtkTypeInfo guppi_compass_box_state_info = {
      "GuppiCompassBoxState",
      sizeof (GuppiCompassBoxState),
      sizeof (GuppiCompassBoxStateClass),
      (GtkClassInitFunc)guppi_compass_box_state_class_init,
      (GtkObjectInitFunc)guppi_compass_box_state_init,
      NULL, NULL, (GtkClassInitFunc)NULL
    };
    guppi_compass_box_state_type = gtk_type_unique (GUPPI_TYPE_GROUP_STATE, &guppi_compass_box_state_info);
  }
  return guppi_compass_box_state_type;
}

GuppiElementState *
guppi_compass_box_state_new (void)
{
  return GUPPI_ELEMENT_STATE (guppi_type_new (guppi_compass_box_state_get_type ()));
}
