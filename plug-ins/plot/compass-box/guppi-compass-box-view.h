/* This is -*- C -*- */
/* vim: set sw=2: */
/* $Id$ */

/*
 * guppi-compass-box-view.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_COMPASS_BOX_VIEW_H
#define _INC_GUPPI_COMPASS_BOX_VIEW_H

#include <guppi-group-view.h>
#include <guppi-defs.h>

BEGIN_GUPPI_DECLS;

typedef struct _GuppiCompassBoxView GuppiCompassBoxView;
typedef struct _GuppiCompassBoxViewClass GuppiCompassBoxViewClass;

struct _GuppiCompassBoxView {
  GuppiGroupView parent;

  guppi_compass_t current_pos;
  GuppiElementView *floating;
  GuppiElementView *fixed;
};

struct _GuppiCompassBoxViewClass {
  GuppiGroupViewClass parent_class;
};

#define GUPPI_TYPE_COMPASS_BOX_VIEW (guppi_compass_box_view_get_type ())
#define GUPPI_COMPASS_BOX_VIEW(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_COMPASS_BOX_VIEW,GuppiCompassBoxView))
#define GUPPI_COMPASS_BOX_VIEW0(obj) ((obj) ? (GUPPI_COMPASS_BOX_VIEW(obj)) : NULL)
#define GUPPI_COMPASS_BOX_VIEW_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_COMPASS_BOX_VIEW,GuppiCompassBoxViewClass))
#define GUPPI_IS_COMPASS_BOX_VIEW(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_COMPASS_BOX_VIEW))
#define GUPPI_IS_COMPASS_BOX_VIEW0(obj) (((obj) == NULL) || (GUPPI_IS_COMPASS_BOX_VIEW(obj)))
#define GUPPI_IS_COMPASS_BOX_VIEW_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_COMPASS_BOX_VIEW))

GtkType guppi_compass_box_view_get_type (void);

END_GUPPI_DECLS;

#endif /* _INC_GUPPI_COMPASS_BOX_VIEW_H */

/* $Id$ */
