/* $Id$ */

/*
 * guppi-tank-init.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <guppi-useful.h>
#include <guppi-stat-init.h>
#include <guppi-data-init.h>
#include <guppi-plot-init.h>

#include "guppi-tank-init.h"

static gboolean tank_is_initialized = FALSE;

gboolean guppi_tank_is_initialized (void)
{
  return tank_is_initialized;
}

void
guppi_tank_init (void)
{
  if (tank_is_initialized)
    return;

  tank_is_initialized = TRUE;

  /* Init our core Guppi libraries. */
  guppi_useful_init_without_guile ();
  guppi_set_verbosity (GUPPI_VERBOSE);

  guppi_stat_init ();
  guppi_data_init ();
  guppi_plot_init ();

  /* We'll need our plug-ins. */
  guppi_plug_in_load_all ();
}

void
guppi_tank_shutdown (void)
{

}

void
guppi_tank_exit (void)
{

}

/* $Id$ */
