/* $Id$ */

/*
 * guppi-embed-test.c
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* #include <gnome.h> */
#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-init.h>

#include <guppi-memory.h>
#include "guppi-tank.h"

static const gint data_size = 7;
static const double data[] = { 1500, 500, 150, 600, 350, 450, 120 };
static const gchar *labels[] = { "Rent", "Food", "Utilities", "Car",
  "Pinball", "Beer", "Pork Rinds"
};
static const gchar *colors[] = { "red", "blue", "orange", "green",
  "purple", "goldenrod", "pink"
};

static void
slice_click_cb (gint i, gpointer user_data)
{
  printf ("Spent $%g on %s (slice)\n", data[i], labels[i]);
}

static void
box_click_cb (gint i, gpointer user_data)
{
  printf ("Spent $%g on %s (legend)\n", data[i], labels[i]);
}

static void
delete_event_cb (GtkWidget *w, GdkEvent *ev, gpointer user_data)
{
  gtk_widget_destroy (w);
  guppi_tank_exit ();
}

int
main (gint argc, gchar * argv[])
{

  GuppiObject *pie_obj;
  GuppiObject *title_obj;

  GtkWidget *win;
  GtkWidget *w;

  gnome_init ("guppi_embed_test", "0.0", argc, argv);
  guppi_tank_init ();

  win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_usize (win, 400, 300);



  pie_obj = guppi_object_new ("pie",
			      4 * 72, 3 * 72,
			      "data_size", data_size,
			      "data", data,
			      "labels", labels,
			      "colors", colors,
			      "slice_callback1", slice_click_cb,
			      "legend_callback1", box_click_cb, NULL);

  title_obj = guppi_object_new ("title",
				4 * 72, 3 * 72,
				"title", "Monthly Spending",
				"subtitle",
				"This is a \"compound\" GuppiObject",
				/* We embed the pie object inside of the
				   title object.  Cool, huh? */
				"subobject", pie_obj, "on_top", TRUE, NULL);

  w = guppi_object_build_widget (title_obj);

  guppi_unref (title_obj);

  gtk_container_add (GTK_CONTAINER (win), w);
  gtk_widget_show_all (win);

  gtk_signal_connect (GTK_OBJECT (win),
		      "delete_event",
		      GTK_SIGNAL_FUNC (delete_event_cb), 
		      NULL);

  gtk_main ();

  return 0;
}




/* $Id$ */
