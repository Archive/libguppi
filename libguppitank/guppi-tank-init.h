/* $Id$ */

/*
 * guppi-tank-init.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 *
 * Developed by Jon Trowbridge <trow@gnu.org>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_TANK_INIT_H
#define _INC_GUPPI_TANK_INIT_H

#include <glib.h>

gboolean guppi_tank_is_initialized (void);
void guppi_tank_init (void);

void guppi_tank_shutdown (void);
void guppi_tank_exit (void);

#endif /* _INC_GUPPI_TANK-INIT_H */

/* $Id$ */
