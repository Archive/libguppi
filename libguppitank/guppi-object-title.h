/* This is -*- C -*- */
/* vim: set sw=2: */

/*
 * guppi-object-title.h
 *
 * Copyright (C) 2000 EMC Capital Management, Inc.
 * Copyright (C) 2001 The Free Software Foundation
 *
 * Developed by Jon Trowbridge <trow@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _INC_GUPPI_OBJECT_TITLE_H
#define _INC_GUPPI_OBJECT_TITLE_H

/* #include <gnome.h> */
#include "guppi-object.h"
#include <guppi-element-view.h>

typedef struct _GuppiObjectTitle GuppiObjectTitle;
typedef struct _GuppiObjectTitleClass GuppiObjectTitleClass;

struct _GuppiObjectTitle {
  GuppiObject parent;

  gboolean on_top;

  gchar *title;
  gchar *subtitle;

  GnomeFont *title_font;
  GnomeFont *subtitle_font;

  GuppiObject *subobject;
  
  /* Constructed views */
  GuppiElementView *title_view;
  GuppiElementView *subtitle_view;
};

struct _GuppiObjectTitleClass {
  GuppiObjectClass parent_class;
};

#define GUPPI_TYPE_OBJECT_TITLE (guppi_object_title_get_type ())
#define GUPPI_OBJECT_TITLE(obj) (GTK_CHECK_CAST((obj),GUPPI_TYPE_OBJECT_TITLE,GuppiObjectTitle))
#define GUPPI_OBJECT_TITLE0(obj) ((obj) ? (GUPPI_OBJECT_TITLE(obj)) : NULL)
#define GUPPI_OBJECT_TITLE_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass),GUPPI_TYPE_OBJECT_TITLE,GuppiObjectTitleClass))
#define GUPPI_IS_OBJECT_TITLE(obj) (GTK_CHECK_TYPE((obj), GUPPI_TYPE_OBJECT_TITLE))
#define GUPPI_IS_OBJECT_TITLE0(obj) (((obj) == NULL) || (GUPPI_IS_OBJECT_TITLE(obj)))
#define GUPPI_IS_OBJECT_TITLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GUPPI_TYPE_OBJECT_TITLE))

GtkType guppi_object_title_get_type (void);

GtkObject *guppi_object_title_new (void);

#endif /* _INC_GUPPI_OBJECT_TITLE_H */

/* $Id$ */

